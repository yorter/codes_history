<?php

use MongoDB\Driver\Manager;


require "MongoSql.php";
$mongo = new MongoSql("han", "shuhai", new Manager("mongodb://192.168.8.8:27017"));

$mongo->update(
    [
        'num' => 6
    ],
    [
        '$set' => ['name' => '萨摩耶']
    ],
    [
        'multi' => false,//是否修改所有匹配行，默认是false
        'upsert' => false,//如果数据不存在，是否新增，默认false
    ]
);

//$mongo->update(
//    [
//        'num' => 6
//    ],
//    [
//        ['$unset' => ['name']]
//    ],
//    [
//        'multi' => false,//是否修改所有匹配行，默认是false
//        'upsert' => false,//如果数据不存在，是否新增，默认false
//    ]
//);


