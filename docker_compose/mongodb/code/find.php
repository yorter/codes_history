<?php

use MongoDB\Driver\Manager;


require "MongoSql.php";
$mongo = new MongoSql("han", "shuhai", new Manager("mongodb://192.168.8.8:27017"));

$count=$mongo->count([
    'num' => 6,
]);
var_dump($count);


//$关键字：
//	$gt 大于
//	$gte 大于等于
//	$lt 小于
//	$lte 小于等于
//	$eq 等于
//  $ne 不等于


$rel = $mongo->find([
    'age' => ['$gte' => 19]
], [
    'sort' => ['age' => -1],
    'skip' => 1,
    'limit' => 2,
]);
//多个等于
$rel = $mongo->find([
    'num' => 8,
    'age' => 21,
], [

]);

//or操作
$rel = $mongo->find([
    '$or' => [
        ['num' => 10],
        ['age' => 23],
    ]
]);

//in
$rel = $mongo->find([
    'age' => ['$in' => [23, 31]],
]);
//使用正则
$rel = $mongo->find([
    'name' => ['$regex' => '^N.*']
]);
