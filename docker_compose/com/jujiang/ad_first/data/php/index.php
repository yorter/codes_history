<?php
//$dsn = 'mysql:dbname=test;host=127.0.0.1';
$dsn = 'mysql:dbname=test;host=192.168.11.30';
$user = 'root';
$password = '123456';
//$password = '437935430';

try {
    //设置字符集
    $opts_values = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4');
    $pdo = new PDO($dsn, $user, $password, $opts_values);
    //设置错误报告（sql语句错误时会抛出错误）
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //返回一个索引为结果集列名的数组
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
    echo 'code: ' . $e->getCode();
}
