from config.orm.base import ModelBase
from config.orm.db import Db
from config.orm.log_system import LogSystem
from config.orm.process import Process


class ConfigInfo(ModelBase):
    def __init__(self):
        self.db = Db()
        self.log_system = LogSystem()
        self.process = Process()

    # debug模式
    debug = False
    # 每次启动自动生成的uuid
    uuid = ""
    # 是否打印sql
    print_sql = False
    # 是否为重载模式(忽略已经飘逸的文件指针)
    is_reload = False
    root_path = ""  # 根目录地址，config类中进行计算
    config_path = ""  # 配置文件目录地址
    action_type = "log"  # 类型
    mark_line = True  # 记录行号
    log_action = ""  # 仅处理某一种日志信息
    ignore_pid = False  # 不进行pid判断和生成pid文件
    # 单独指定分析某个百度请求
    file = ""
    # 数据库相关配置
    db: Db
    # 日志分析相关配置
    log_system: LogSystem
    # 进程相关配置
    process: Process
