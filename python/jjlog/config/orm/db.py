from config.orm.base import ModelBase


class MysqlConfig(ModelBase):
    host = "localhost"
    username = "root"
    password = ""
    database = "jujiang"
    port = 3306


class RedisConfig(ModelBase):
    def keys(self):
        return ['host', 'port', 'password', 'decode_responses']

    host = "127.0.0.1"
    port = 6379
    password = ""
    decode_responses = True  # 结果返回字符串而非Unicode编码


class Db(ModelBase):
    def __init__(self):
        self.mysql = MysqlConfig()
        self.redis = RedisConfig()

    mysql: MysqlConfig
    redis: RedisConfig
