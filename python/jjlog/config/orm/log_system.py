from config.orm.base import ModelBase


class LogsConfig(ModelBase):
    log_dir = "/log"  # 日志文件目录
    mark_dir = "data/mark"  # 记录文件指针的目录
    max_days = 1  # 最大天数间隔，0表示不限制
    max_hours = 2  # 最大小时间隔，对于用小时拆分文件而言
    mysql_save_time_interval = 60  # 保存到数据库的时间间隔
    disable_log_actions = ""  # 禁用的log_类型action，以英文逗号隔开
    log_suffix = ".log"  # 日志的后缀
    pid_file = "data/log_system.pid"  # pid文件位置


class DeviceConfig(ModelBase):
    mark_db = 13
    expire_time = 1
    save_db = 14
    md5_save_db = 15
    prefix = 'pick_devices_'


class LogSystem(ModelBase):
    def __init__(self):
        self.logs = LogsConfig()
        self.device = DeviceConfig()

    logs = LogsConfig
    device = DeviceConfig
