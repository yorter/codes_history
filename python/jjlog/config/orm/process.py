from config.orm.base import ModelBase


class Log(ModelBase):
    """
    程序日志文件
    """
    file_name_by_date_format: str = "%Y-%m-%d"
    log_dir: str = "log/"


class Ding(ModelBase):
    webhook = "https://oapi.dingtalk.com/robot/send?access_token=f23adc64d680da8f1af39a17d685f08d16cb170af6a148db4a171bef8a8cc0b6"
    secret = "SECf6f1522f72fc5b0dd3697bd6f62d9f6f99e6d76d046ec5b3c7c93c6e0a3bb426"
    at_mobiles = "18910020904"


class Process(ModelBase):
    def __init__(self):
        self.log = Log()
        self.ding_exception = Ding()
        self.ding_message = Ding()

    log: Log
    ding_exception: Ding
    ding_message: Ding
