class ModelBase:
    def keys(self):  # 获取字典的键
        return []

    def __setitem__(self, key, value):
        if hasattr(self, key):
            current_value = getattr(self, key)
            if isinstance(current_value, ModelBase):
                for sub_key in value.keys():
                    current_value[sub_key] = value[sub_key]
            else:
                t = type(current_value)
                if t == bool and not isinstance(value, bool):
                    value = str(value).strip().lower().strip("0").strip('false')
                setattr(self, key, type(current_value)(value))

    def __getitem__(self, item):  # 获取键对应的值
        return getattr(self, item)  # getattr获取对象下某个属性的值
