import argparse
import configparser
import os
import uuid

from config.orm.config_info import ConfigInfo
from tool.data_trans import get_absolutely_path


class CommandArgv:
    # 是否开启debug模式
    debug: bool
    # 使用模块
    type: str


class Config:
    """
    分析配置文件和获取配置的主要类
    """

    # 单例对象
    __instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = object.__new__(cls)  # 传入我们要产生对象的这个类
            cls.__instance.__init_config__()
        return cls.__instance

    # 配置对象
    __config: ConfigInfo

    @classmethod
    def get(cls):
        if cls.__instance is None:
            return Config().__config
        return cls.__instance.__config

    def __init_config__(self):
        """
        解析配置文件
        :return:
        """
        self.__config = ConfigInfo()
        self.__config.uuid = uuid.uuid1().__str__()
        # 解析命令行
        self.__from_command()
        # 根目录地址
        self.__config.root_path = os.path.realpath("%s/.." % os.path.dirname(__file__))
        self.reload_config()

    def __from_command(self):
        """
        解析命令行参数
        :return:
        """
        parser = argparse.ArgumentParser()
        parser.add_argument('-d', '--debug', dest='debug', action='store_true', help='开启debug模式')
        parser.add_argument('-s', '--sql', dest='print_sql', action='store_true', help='开启打印sql(需开启debug模式)')
        parser.add_argument('-m', '--mark_line', dest='mark_line', action='store_false', help='关闭记录文件行数')
        parser.add_argument('-t', '--type', dest='action_type', default="logv2", help='要执行的功能')
        parser.add_argument('--log_action', dest='log_action', default="", help='仅处理某一种日志信息')
        parser.add_argument('--ignore_pid', dest='ignore_pid', action='store_true', help='不判断pid，并且不生成pid')
        parser.add_argument('-c', '--config', dest='config_path', type=str, default="config",
                            help='配置文件夹地址，默认为config目录')
        parser.add_argument('-r', '--reload', dest='is_reload', action='store_true',
                            help='重读模式(会忽略已经存储的读取的文件指针位置)')
        parser.add_argument('-f', '--file', dest='file', type=str, default="", help='单独分析某一个日志文件，指定文件名')
        commands = parser.parse_args().__dict__
        for k in commands.keys():
            self.__config[k] = commands[k]

    @classmethod
    def get_instance(cls):
        """
        获取配置类单例对象
        :return:
        """
        return Config()

    def reload_config(self):
        """
        从新读取配置文件
        :return:
        """
        # 配置文件目录地址
        self.__config.config_path = os.path.realpath(
            get_absolutely_path(self.__config.config_path, self.__config.root_path)
        )
        config_files = os.listdir(self.__config.config_path)
        for file_name in config_files:
            if not file_name.endswith(".ini"):
                continue
            path = "%s/%s" % (self.__config.config_path, file_name)
            file_key = file_name.removesuffix('.ini')
            cnf = configparser.ConfigParser()  # 类实例化
            # 第一种读取ini文件方式,通过read方法
            cnf.read(path, encoding="utf-8")
            config_dict: dict = cnf.__dict__['_sections']
            self.__config[file_key] = config_dict
