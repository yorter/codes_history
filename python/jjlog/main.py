import importlib
from typing import Dict

from config import Config
from core.ActionBase import ActionBase
from core.exception.exception_base import ProcessNormalOver
from tool.dingding import DingDing
from tool.log import file_log
from tool.statistics import statistics

types: Dict[str, tuple] = {
    'logv2': ('logv2', 'log_v2'),
    'device_detail': ('dingding_message', 'device_detail'),
}


def start():
    cnf = Config.get()
    # 反射导入模块
    action_type = types[cnf.action_type]
    module = importlib.import_module("core.%s.%s" % (action_type[0], action_type[1]))
    c = getattr(module, 'Action')
    if cnf.debug:  # debug模式，只截取程序终止异常
        try:
            obj: ActionBase = c()
            obj.run()
        except ProcessNormalOver:
            pass
    else:  # 非debug模式，截取所有异常
        try:
            obj: ActionBase = c()
            obj.run()
        except ProcessNormalOver:
            pass
        except Exception as e:
            file_log.error("程序异常终止:%s" % e)
            ding = DingDing()
            ding.send_error(e, 'over')
    statistics.end()


if __name__ == '__main__':
    start()
