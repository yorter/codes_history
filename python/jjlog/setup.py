from distutils.core import setup

setup(
    name='jjlog',
    version="1.0",
    url='',
    license='',
    author='韩石强',
    description='巨匠-日志分析',
    install_requires=[
        'pymysql==1.0.2',
        'sqlalchemy==1.4.31',
        'psutil==5.9.0',
        'redis==4.1.4',
        'DingtalkChatbot==1.5.3'
    ]
)
