import time

from tool.log import file_log


class Statistics:
    """
    统计类
    """
    __instance = None
    # 是否已经初始化
    __is_init = False

    # 单例
    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = object.__new__(cls)  # 传入我们要产生对象的这个类
        return cls.__instance

    def __init__(self):
        if not Statistics.__is_init:
            Statistics.__is_init = True
            self.__start_time = time.time()
            file_log.info("脚本开始执行")

    __start_time: float
    __end_time: float = 0

    def end(self):
        self.__end_time = time.time()
        file_log.info("脚本执行结束,共耗时%s秒" % round(self.__end_time - self.__start_time, 4))


statistics = Statistics()
