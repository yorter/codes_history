import os

import psutil

import config
from core.exception.exception_base import ProcessNormalOver
from tool import data_trans

cnf = config.Config.get()


def pid_save(path):
    """
    验证pid存在与否与，存在且
    :param path:pid文件位置
    :return:
    """
    pid_path = data_trans.get_absolutely_path(path, cnf.root_path)
    if not cnf.ignore_pid and os.path.exists(pid_path):
        with open(pid_path, mode='r') as f:
            pid = int(f.read())
            f.close()
            if pid in psutil.pids():
                if cnf.debug:
                    print("其他程序正在运行中，清等待结束后再次执行")
                raise ProcessNormalOver()
            else:
                os.remove(pid_path)
    if not cnf.ignore_pid:  # 非忽略pid则写入pid文件
        with open(pid_path, 'w') as f:
            f.write(str(os.getpid()))
            f.close()


def pid_delete(path):
    """
    删除pid文件
    :param path:
    :return:
    """
    pid_path = data_trans.get_absolutely_path(path, cnf.root_path)
    if not cnf.ignore_pid and os.path.exists(pid_path):
        os.remove(pid_path)
