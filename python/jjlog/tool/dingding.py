import socket

from dingtalkchatbot.chatbot import DingtalkChatbot

from config import Config
from config.orm.process import Ding


class DingDing:
    _config: Ding
    _hostname = socket.gethostname()

    def __init__(self, config=Config.get().process.ding_exception):
        self._config = config
        self._robot = DingtalkChatbot(config.webhook, secret=config.secret, pc_slide=False, fail_notice=True)
        self._at_mobiles = config.at_mobiles.split(',') if config.at_mobiles else []

    # _webhook = Config.get().process.ding.webhook
    # _secret = Config.get().process.ding.secret
    # 新版安全设置为‘加签’时，需要传入请求秘钥
    # 同时支持设置消息链接跳转方式，默认pc_slide_False为跳转到浏览器，pc_slide为在pc段侧边栏打开
    # 同时支持设置消息发送失败时提醒，默认fail_notice为False不提醒
    _robot: DingtalkChatbot

    # _at_mobiles = Config.get().process.ding.at_mobiles.split(',') if Config.get().process.ding.at_mobiles else []

    def send_error(self, e: Exception, msg=""):
        try:
            self._robot.send_text("服务器:%s\n错误信息:%s\n文件位置:%s\n行号:%s\n信息:%s" % (
                self._hostname,
                e.__repr__(),
                e.__traceback__.tb_frame.f_code.co_filename,
                e.__traceback__.tb_lineno,
                msg
            ), at_mobiles=self._at_mobiles)
        except:
            pass

    def send_message(self, msg: str):
        msg = "服务器:%s\n%s" % (self._hostname, msg)
        try:
            self._robot.send_text(str(msg))
        except:
            pass
