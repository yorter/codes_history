import logging
import os
import time

from config import Config

__all__ = ('log', 'file_log', 'flush_print')

from core.exception.process_error import ConfigNotExistent


class Log:
    """
    日志
    """

    def __init__(self, dir_path: str):
        cfg = Config.get()
        try:
            log_name_format = cfg.process.log.file_name_by_date_format
        except ConfigNotExistent:
            log_name_format = '%Y-%m-%d'
        log_file_name = time.strftime(log_name_format, time.localtime())
        if cfg.debug:  # debug模式区分文件名
            log_file_name = "DEBUG_%s" % log_file_name
        if not dir_path.startswith("/"):
            dir_path = '%s/%s' % (cfg.root_path, dir_path)
        os.makedirs(dir_path, exist_ok=True)
        log_path = '%s/%s.log' % (os.path.realpath(dir_path), log_file_name)

        formatter = "%(asctime)s\t%(levelname)s\t" + cfg.uuid + "\t%(message)s"
        log_formatter = logging.Formatter(formatter)
        log_formatter.datefmt = "%Y-%m-%d %H:%M:%S"

        # 打印
        self.print = logging.getLogger("print")
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(log_formatter)
        self.print.addHandler(stream_handler)
        self.print.setLevel(logging.INFO)
        # 文件
        self.file = logging.getLogger("file")
        file_handler = logging.FileHandler(log_path, encoding='utf-8')  # 文件句柄
        file_handler.setFormatter(log_formatter)
        self.file.addHandler(file_handler)
        self.file.setLevel(logging.INFO)

    print = None
    file = None


log_obj = Log(Config.get().process.log.log_dir)
log = log_obj.print
file_log = log_obj.file


def flush_print(s: str):
    print("\r%s" % s, end="", flush=True)
