import os
import time
from datetime import datetime
from typing import TypeVar

from tool.chunzhen import CzIp

T = TypeVar('T')


def dict2obj(data: dict, t: T) -> T:
    """
    将字典类型数据映射到对象
    :param data:
    :param t:
    :return:
    """
    td = t()
    for k in data.keys():
        if hasattr(td, k):
            setattr(td, k, data[k])
    return td


def datetime2today_int(d: datetime):
    """
    datetime格式的时间转为当天零点时间戳
    :return:
    """
    date_str = d.strftime("%Y-%m-%d")
    return int(time.mktime(time.strptime(date_str, '%Y-%m-%d')))


def datetime_str2int(d: str):
    """
    时间字符串转换为整型时间戳
    :param d:
    :return:
    """
    return int(time.mktime(time.strptime(d, "%Y-%m-%d %H:%M:%S")))


def datetime2datestr(d: datetime):
    """
    将datetime转为日期字符串
    :param d:
    :return:
    """
    return d.strftime("%Y-%m-%d")


def get_absolutely_path(path, relative):
    """
    将相对地址转为绝对地址，相对地址会生成不存在的目录
    :param path:
    :param relative:
    :return:
    """
    if not path.startswith("/"):
        # relative = os.path.dirname(relative)
        path = "%s/%s" % (relative, path)
        dir_path = os.path.dirname(path)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
    return os.path.realpath(path)


def ip2location(ip: str):
    """
    ip地址转实际地址
    :param ip:
    :return:
    """
    cz = CzIp()
    return cz.get_addr_by_ip(ip)
