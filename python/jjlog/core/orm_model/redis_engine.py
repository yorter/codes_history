import hashlib
from typing import Dict

from redis import Redis

from config import Config
from config.orm.db import RedisConfig


class RedisEngine:
    # 缓存redis连接信息
    __conns: Dict[str, Redis] = {

    }

    @classmethod
    def get_conn(cls, db=15, is_single=True, cnf=Config.get().db.redis) -> Redis:
        cache_key = "%s_%s" % (id(cnf), db)
        if is_single and cls.__conns.get(cache_key):
            return cls.__conns.get(cache_key)
        elif is_single and not cls.__conns.get(cache_key):
            conn = cls.__create_redis_conn(cnf, db)
            cls.__conns[cache_key] = conn
            return conn
        else:
            return cls.__create_redis_conn(cnf, db)

    @classmethod
    def __create_redis_conn(cls, cnf: RedisConfig, db: int) -> Redis:
        cnf = dict(cnf)
        cnf['db'] = db
        conn = Redis(**cnf)
        conn.ping()
        # try:
        #     cls.redis_is_ok = conn.ping()
        # except ConnectionError:
        #     if Config.get().debug:
        #         print("连接redis失败")
        #     cls.redis_is_ok = False
        return conn


class RedisLock:
    """
    redis分布式锁
    """
    _uuid: str
    _script = """if redis.call("get",KEYS[1]) == ARGV[1] then
    return redis.call("del",KEYS[1])
else
    return 0
end"""
    _is_lock = False
    _is_release = False

    def __init__(self, key: str, db: int = 0):
        self.key = key
        self.db = db
        self._is_lock = False
        self._is_release = False
        self._uuid = hashlib.md5(Config.get().uuid.encode('utf8')).hexdigest()

    def lock(self, expire=60):
        redis = RedisEngine.get_conn(self.db)
        self._is_lock = redis.setnx(self.key, self._uuid)
        if self._is_lock:
            redis.expire(self.key, expire)
        return self._is_lock

    def release(self):
        if self._is_lock:
            redis = RedisEngine.get_conn(self.db)
            rel = redis.eval(self._script, 1, self.key, self._uuid)
            self._is_release = True
            return rel
        else:
            return False

    def __del__(self):
        if not self._is_release:
            self.release()
