class OrmDataBase:
    """
    ORM基类之一，为了统一的一些方法
    """

    def to_dict(self):
        """
        将结果转为字典
        :return:
        """
        result = {}
        for key in self.__mapper__.c.keys():
            if getattr(self, key) is not None:
                result[key] = str(getattr(self, key))
            else:
                result[key] = getattr(self, key)
        return result

    def __getitem__(self, item):
        return getattr(self, item)
