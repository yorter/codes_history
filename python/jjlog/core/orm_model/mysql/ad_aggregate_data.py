from sqlalchemy import Column, CHAR, VARCHAR, INT, DECIMAL, DATE

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class AdAggregateData(orm_model.Base, OrmDataBase):
    """
    广告统计聚合数据
    """
    __tablename__ = 'j_ad_aggregate_data'
    # 主键
    id = Column(INT(), primary_key=True, autoincrement=True)
    # 用户id
    # account_id = Column(INT())
    """
    维度类型：
      产品维度:1001.产品维度,1002日期维度
      推广计划维度:2001.推广计划维度 2002日期维度
      推广单位维度:3001.推广单元维度 3002日期维度 3003日期维度
      媒体维度:4001
      创意维度:5001.创意维度 5002尺寸维度
      地域维度:6001.省份维度 6002城市维度
    """
    type = Column(CHAR(2))
    # 推广单元id
    unit_id = Column(INT())
    # 推广计划id
    plan_id = Column(INT())
    # 关联推广单元id或推广计划id或小时、媒体id等关联id等
    p_data = Column(VARCHAR(20))
    # 出价
    # bid = Column(DECIMAL(10, 2))
    # 出价
    bid = Column(DECIMAL(10, 2))
    # 默认出价
    default_price = Column(DECIMAL(10, 2))
    # 点击数
    click = Column(INT())
    # 查看数
    view = Column(INT())
    # deep link 唤醒次数
    dp_count = Column(INT())
    # 胜出数
    win_count = Column(INT())
    # 总请求次数
    bidding_count = Column(INT())
    # 总费用
    total_cost = Column(DECIMAL(10, 2))
    # 数据日期
    date = Column(DATE())
    # 填充数
    fill_count = Column(INT())
    # 填充成功数
    fill_ok_count = Column(INT())
    # 竞价失败
    bid_fail = Column(VARCHAR(50))
