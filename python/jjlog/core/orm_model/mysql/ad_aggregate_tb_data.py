from sqlalchemy import Column, INT, DATE

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class AdAggregateTBData(orm_model.Base, OrmDataBase):
    """
    大航海数据统计信息
    """
    __tablename__ = 'j_ad_aggregate_tb_data'
    id = Column(INT(), primary_key=True, autoincrement=True)
    type = Column(INT())
    p_data = Column(INT())
    transform_type = Column(INT())
    count = Column(INT())
    date = Column(DATE())
