from sqlalchemy import Column, INT, DECIMAL

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class AdPromotionUnit(orm_model.Base, OrmDataBase):
    """
    推广单元表
    """

    __tablename__ = 'j_ad_promotion_unit'
    # 推广单元id
    id = Column(INT(), primary_key=True)
    # 推广计划id
    plan_id = Column(INT())
    # 用户id
    account_id = Column(INT())
    # 默认出价
    default_price = Column(DECIMAL(10, 2))
