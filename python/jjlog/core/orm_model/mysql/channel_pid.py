from sqlalchemy import Column, VARCHAR, INT

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class ChannelPid(orm_model.Base, OrmDataBase):
    """
    pid信息
    """
    __tablename__ = 'j_channel_pid'
    id = Column(INT(), primary_key=True, autoincrement=True)
    # 平台id
    ch_id = Column(INT())
    # appid
    appid = Column(INT())
    pid = Column(VARCHAR(255))
    # width = Column(INT())
    # height = Column(INT())
    # dahanghai_space_id = Column(VARCHAR(50))
    # dahanghai_channel_id = Column(VARCHAR(50))
