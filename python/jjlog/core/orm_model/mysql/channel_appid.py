from sqlalchemy import Column, INT

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class ChannelAppid(orm_model.Base, OrmDataBase):
    """
    appid信息
    """
    __tablename__ = 'j_channel_appid'
    id = Column(INT(), primary_key=True, autoincrement=True)
    platform_id = Column(INT())
    appid = Column(INT())
