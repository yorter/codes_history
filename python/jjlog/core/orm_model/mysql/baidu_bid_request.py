from sqlalchemy import Column, VARCHAR, INT, TIMESTAMP, ForeignKey
from sqlalchemy.orm import relationship

from core import orm_model
from core.orm_model.mysql.baidu_bid_monitor import BaiduBidMonitor
from core.orm_model.public.orm_data_base import OrmDataBase


class BaiduBidRequest(orm_model.Base, OrmDataBase):
    """
    请求表
    """
    __tablename__ = 'j_baidu_bid_request'

    id = Column(INT(), primary_key=True)
    request_id = Column(VARCHAR(255), ForeignKey('j_baidu_bid_monitor.request_id'))
    user_info = Column(VARCHAR(255), default="{}")
    geo_info = Column(VARCHAR(255), default="{}")
    web_info = Column(VARCHAR(255), default="{}")
    mobile_info = Column(VARCHAR(255), default="{}")
    mobile_app_info = Column(VARCHAR(255), default="{}")
    video = Column(VARCHAR(255), default="{}")
    ad_slot = Column(VARCHAR(255), default="{}")
    response_unit_id = Column(INT())
    response_plan_id = Column(INT())
    response_size = Column(VARCHAR(50))
    create_time = Column(TIMESTAMP())

    monitors = relationship(BaiduBidMonitor, uselist=False, back_populates="requests")
