from sqlalchemy import Column, VARCHAR, INT, TIMESTAMP
from sqlalchemy.orm import relationship

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class BaiduBidMonitor(orm_model.Base, OrmDataBase):
    """
    百度竞价成功结果
    """
    __tablename__ = 'j_baidu_bid_monitor'

    id = Column(INT(), primary_key=True)
    # 百度请求Id
    request_id = Column(VARCHAR(255))
    # 创意Id
    creative_id = Column(INT())
    # 广告交易价格,单位：分
    price = Column(INT())
    # 创建时间
    create_time = Column(TIMESTAMP())

    requests = relationship("BaiduBidRequest", uselist=False, back_populates="monitors")
