from sqlalchemy import Column, CHAR, VARCHAR, INT, TIMESTAMP, SMALLINT

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class BaiduBidCallback(orm_model.Base, OrmDataBase):
    """
    广告点击曝光日志
    """
    # 表的名字:
    __tablename__ = 'j_baidu_bid_callback'

    id = Column(INT(), primary_key=True)
    # 请求回调类型：1：曝光，2：点击
    type = Column(CHAR(1))
    # 广告创意Id
    creative_id = Column(INT())
    # 推广单元id
    unit_id = Column(INT())
    # 素材尺寸
    size = Column(VARCHAR(50))
    # 广告请求Id
    bid_id = Column(VARCHAR(255))
    # 用户Id
    baidu_user_id = Column(VARCHAR(255))
    # status状态 1存储未分析 2分析中 3日志分析完成 4分析失败
    status = Column(SMALLINT())
    # 创建时间
    create_time = Column(TIMESTAMP())
    # 修改时间
    modify_time = Column(TIMESTAMP())
