from sqlalchemy import Column, INT, VARCHAR

from core import orm_model
from core.orm_model.public.orm_data_base import OrmDataBase


class AdPlatform(orm_model.Base, OrmDataBase):
    __tablename__ = 'j_ad_platform'
    id = Column(INT(), primary_key=True, autoincrement=True)
    name = Column(VARCHAR(255))
