from urllib import parse

from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine as SqlalchemyEngine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session

from config import Config

# 创建对象的基类:
Base = declarative_base()


class Engine:
    _engine: SqlalchemyEngine = None

    @classmethod
    def get_session(cls) -> Session:
        if cls._engine is None:
            mysql_config = Config.get().db.mysql
            cls._engine = create_engine(
                'mysql+pymysql://%s:%s@%s:%s/%s' % (
                    mysql_config.username,
                    parse.quote_plus(mysql_config.password),
                    mysql_config.host,
                    mysql_config.port,
                    mysql_config.database,
                ),
                echo=Config.get().debug and Config.get().print_sql
            )
        db_session = sessionmaker(bind=cls._engine)
        return db_session()

    @classmethod
    def get_conn(cls):
        return cls._engine.connect()
