import json
import time
from typing import Dict

from config import Config
from config.orm.config_info import ConfigInfo
from core.logv2.cache import CacheBase
from core.logv2.struct.aggregate_data import AggregateData
from core.orm_model.mysql.ad_aggregate_data import AdAggregateData
from core.orm_model.mysql_engine import Engine
from core.orm_model.redis_engine import RedisLock


class MemoryCache(CacheBase):
    def __init__(self):
        super().__init__()
        self._cache = {}
        self._config = Config.get()

    _config: ConfigInfo
    _cache: Dict[str, AggregateData]

    def cache_data(self, data: AggregateData):
        """
        缓存数据
        :param data:
        :return:
        """
        key = "%s_%s_%s_%s_%s" % (data.unit_id, data.plan_id, data.type, data.p_data, data.date)
        if key in self._cache:
            self._cache[key] = self._add_aggregate_data(self._cache[key], data)
        else:
            self._cache[key] = data

    @staticmethod
    def _add_aggregate_data(obj: AggregateData, add: AggregateData):
        """
        将两个统计数据进行合并
        :param obj:
        :param add:
        :return:
        """
        obj.bid = add.bid
        obj.default_price = add.default_price
        obj.view += add.view
        obj.click += add.click
        obj.dp_count += add.dp_count
        obj.win_count += add.win_count
        obj.bidding_count += add.bidding_count
        obj.total_cost += add.total_cost
        obj.fill_count += add.fill_count
        obj.fill_ok_count += add.fill_ok_count
        if add.bid_fail:
            for code in add.bid_fail:
                if code in obj.bid_fail:
                    obj.bid_fail[code] += add.bid_fail[code]
                else:
                    obj.bid_fail[code] = add.bid_fail[code]
        return obj

    @staticmethod
    def _add_aggregate_data_with_db_data(cache: AggregateData, db_data: AdAggregateData):
        db_data.view = int(db_data.view) + cache.view
        db_data.click = int(db_data.click) + cache.click
        db_data.dp_count = int(db_data.dp_count) + cache.dp_count

        db_data.default_price = db_data.default_price
        db_data.bid = db_data.bid

        db_data.win_count = int(db_data.win_count) + cache.win_count
        db_data.bidding_count = int(db_data.bidding_count) + cache.bidding_count
        db_data.total_cost = float(db_data.total_cost) + cache.total_cost
        db_data.fill_count = int(db_data.fill_count) + cache.fill_count
        db_data.fill_ok_count = int(db_data.fill_ok_count) + cache.fill_ok_count
        # 计算各种错误code的次数
        if db_data.bid_fail:
            try:
                bid_fail = json.loads(db_data.bid_fail)
            except json.decoder.JSONDecodeError:
                bid_fail = {}
        else:
            bid_fail = {}
        for code in cache.bid_fail:
            if code in bid_fail:
                bid_fail[code] += cache.bid_fail[code]
            else:
                bid_fail[code] = cache.bid_fail[code]
        db_data.bid_fail = json.dumps(bid_fail)
        return db_data

    def update_db(self, must=False):
        """
        更新到数据库
        :return:
        """
        if len(self._cache) == 0:
            return
        lock = RedisLock("python_log_mysql_save_lock", 0)
        while True:
            is_lock = lock.lock(5 * 60)
            if is_lock:  # 上锁成功，跳出循环
                break
            elif not must:  # 上锁失败，非必须的情况下跳出函数，等待下次同步
                return
            elif must:  # 上锁失败，必须要等上锁成功，休眠后从来
                time.sleep(0.1)
        if self._config.debug:
            print("开始保存到数据库%s" % len(self._cache))
        mysql = Engine.get_session()
        for key in self._cache:
            cache_data: AggregateData = self._cache[key]
            # 检查数据库中有没有存在相同的数据
            data = mysql.query(AdAggregateData).filter(
                AdAggregateData.type == cache_data.type
            ).filter(
                AdAggregateData.unit_id == cache_data.unit_id
            ).filter(
                AdAggregateData.plan_id == cache_data.plan_id
            ).filter(
                AdAggregateData.p_data == cache_data.p_data
            ).filter(
                AdAggregateData.date == cache_data.date
            ).first()
            if isinstance(data, AdAggregateData):
                self._add_aggregate_data_with_db_data(cache_data, data)
            else:
                info = cache_data.to_db_dict()
                mysql.add(AdAggregateData(**info))
            mysql.commit()
        self._cache = {}
        lock.release()
