import time
from typing import Dict

from core.logv2.cache import CacheBase
from core.logv2.struct.aggregate_tb_data import AggregateTBData
from core.orm_model.mysql.ad_aggregate_tb_data import AdAggregateTBData
from core.orm_model.mysql_engine import Engine
from core.orm_model.redis_engine import RedisLock


class TBCache(CacheBase):
    def __init__(self):
        super().__init__()
        self._cache = {}

    _cache: Dict[str, AggregateTBData]

    def cache_data(self, data: AggregateTBData):
        key = "%s_%s_%s_%s" % (data.type, data.p_data, data.transform_type, data.date)
        if key in self._cache:
            self._cache[key] = self._cache[key] + data
        else:
            self._cache[key] = data

    def update_db(self, must=False):
        if len(self._cache) == 0:
            return
        lock = RedisLock("python_log_mysql_save_lock_tb_callback", 0)
        while True:
            is_lock = lock.lock(5 * 60)
            if is_lock:  # 上锁成功，跳出循环
                break
            elif not must:  # 上锁失败，非必须的情况下跳出函数，等待下次同步
                return
            elif must:  # 上锁失败，必须要等上锁成功，休眠后从来
                time.sleep(0.1)
        if self._config.debug:
            print("开始保存到数据库%s" % len(self._cache))
        mysql = Engine.get_session()
        for key in self._cache:
            cache_data: AggregateTBData = self._cache[key]
            # 检查数据库中有没有存在相同的数据
            data = mysql.query(AdAggregateTBData).filter(
                AdAggregateTBData.type == cache_data.type
            ).filter(
                AdAggregateTBData.p_data == cache_data.p_data
            ).filter(
                AdAggregateTBData.date == cache_data.date
            ).filter(
                AdAggregateTBData.transform_type == cache_data.transform_type
            ).first()
            if isinstance(data, AdAggregateTBData):
                # self._add_aggregate_data_with_db_data(cache_data, data)
                data.count = int(data.count) + cache_data.count
            else:
                info = cache_data.__dict__
                # info['count'] = json.dumps(info['count'])
                mysql.add(AdAggregateTBData(**info))
            mysql.commit()
        self._cache = {}
        lock.release()

    # @classmethod
    # def _add_aggregate_data_with_db_data(cls, cache_data: AggregateTBData, data: AdAggregateTBData):
    #     count = cache_data.count
    #     db_count = json.loads(data.count)
    #     for trans in count:
    #         if trans in db_count:
    #             db_count[trans] += count[trans]
    #         else:
    #             db_count[trans] = count[trans]
    #     data.count = json.dumps(db_count)
