from config import Config
from config.orm.config_info import ConfigInfo


class CacheBase:
    def __init__(self):
        self._config = Config.get()

    _config: ConfigInfo

    def cache_data(self, data):
        pass

    def update_db(self, must=False):
        pass
