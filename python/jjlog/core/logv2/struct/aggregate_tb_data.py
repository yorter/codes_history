class AggregateTBData:
    def __init__(self, t: int, p_data: int, date: str, transform_type: int, count: int = 1):
        self.type = t
        self.p_data = p_data
        self.date = date
        self.transform_type = transform_type
        self.count = count

    type: int
    p_data: int
    transform_type: int
    count: int
    date: str

    def __add__(self, other):
        if isinstance(other, AggregateTBData):
            #     for trans in other.count:
            #         if trans in self.count:
            #             self.count[trans] += other.count[trans]
            #         else:
            #             self.count[trans] = other.count[trans]
            self.count += other.count
        return self
