import json
from typing import Dict


class AggregateData:

    def __init__(self,
                 t,
                 date,
                 unit_id=0,
                 plan_id=0,
                 p_data="0",
                 bid=0,
                 default_price=0,
                 click=0,
                 view=0,
                 dp_count=0,
                 win_count=0,
                 bidding_count=0,
                 total_cost=0,
                 fill_count=0,
                 fill_ok_count=0,
                 bid_fail={}
                 ):
        self.type = t
        self.unit_id = unit_id
        self.plan_id = plan_id
        self.p_data = p_data
        self.bid = bid
        self.default_price = default_price
        self.click = click
        self.view = view
        self.dp_count = dp_count
        self.win_count = win_count
        self.bidding_count = bidding_count
        self.total_cost = total_cost
        self.fill_count = fill_count
        self.fill_ok_count = fill_ok_count
        self.bid_fail = bid_fail
        self.date = date

    type: int
    unit_id: int
    plan_id: int
    p_data: str
    bid: float
    default_price: float
    click: int
    view: int
    dp_count: int
    win_count: int
    bidding_count: int
    total_cost: float
    fill_count: int
    fill_ok_count: int
    bid_fail: Dict[str, int]
    date: str

    def to_db_dict(self):
        d = self.__dict__
        d['bid_fail'] = json.dumps(self.bid_fail)
        return d
