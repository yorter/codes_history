from config.orm.base import ModelBase


# 保存文件的offset信息的映射对象类
class FIleOffsetMark(ModelBase):
    def __init__(self):
        self.offset = 0
        self.last_update = 0

    offset = 0  # 指针位置
    last_update = 0  # 上次更新时间


# awk方式统计日志记录对象
class AwkFIleOffsetMark(ModelBase):
    def __init__(self):
        self.line = 0
        self.last_update = 0

    line = 0
    last_update = 0
