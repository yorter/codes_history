import datetime
import json
import os
import time

from config import Config
from config.orm.config_info import ConfigInfo
from core.logv2.cache.memory_cache import MemoryCache
from core.logv2.struct.file_offset_mark import AwkFIleOffsetMark
from tool.data_trans import get_absolutely_path, dict2obj
from tool.dingding import DingDing


class AwkAnalysisBase:
    _date: datetime.datetime
    _offset = 0  # 文件指针偏移位置
    _config: ConfigInfo  # 配置信息
    _file_mark: AwkFIleOffsetMark  # 记录文件处理行数的对象
    _file_mark_path: str  # 记录文件处理行数的对象的文件位置
    _max_line = 0  # 文件行数
    cache: MemoryCache  # 缓存管理对象
    file_name: str  # 操作的文件名
    _file = None
    exception_ding: DingDing

    def __init__(self, file_name, log_datetime: datetime.datetime):
        self._date = log_datetime
        self.file_name = file_name
        self.cache = MemoryCache()
        self._file_mark = AwkFIleOffsetMark()
        self._config = Config.get()
        self.exception_ding = DingDing()
        root_path = self._config.root_path
        # 文件处理行数的存档文件位置
        base_dir = self._config.log_system.logs.mark_dir
        mark_dir = get_absolutely_path(
            "%s/%s-%s/%s/" % (base_dir, log_datetime.year, log_datetime.month, log_datetime.day),
            root_path
        )
        self._file_mark_path = "%s/%s" % (mark_dir, file_name)
        if os.path.exists(self._file_mark_path) and not self._config.is_reload:  # 文件存在且不是重新读写模式
            with open(self._file_mark_path, mode='r', encoding="utf8") as mark_f:
                mark_json = mark_f.read()
                if mark_json:
                    mark_dict = json.loads(mark_json)
                    self._file_mark = dict2obj(mark_dict, AwkFIleOffsetMark)
                else:
                    self._save()
        else:
            self._save()

    def analysis(self):
        """
        开始分析文件
        :return:
        """

    @staticmethod
    def _command(command: str):
        return os.popen(command)

    def _save(self, line=0, db_must=True):
        self.cache.update_db(db_must)
        if not self._config.mark_line:
            return
        with open(self._file_mark_path, mode='w', encoding="utf8") as mark_f:
            self._file_mark.last_update = time.time()
            self._file_mark.line = line
            mark_f.write(json.dumps(self._file_mark.__dict__))
