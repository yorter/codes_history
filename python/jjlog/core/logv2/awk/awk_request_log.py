from core.logv2.awk.awk_analysis_base import AwkAnalysisBase
from core.logv2.data.dimension import Dimension
from core.logv2.data.shell_command import ShellCommand
from core.logv2.struct.aggregate_data import AggregateData
from tool.log import file_log


class AwkRequestLog(AwkAnalysisBase):
    """
    使用awk获取请求数
    """

    def analysis(self):
        date = self._date.strftime("%Y-%m-%d")
        path = "%s/%s" % (self._config.log_system.logs.log_dir, self.file_name)
        command = ShellCommand.wc_l % path
        if self._config.debug:
            print(command)
        rel = self._command(command)
        line = rel.readline().strip()
        rel.close()
        max_line = int(line.split(' ')[0])
        if self._file_mark.line >= max_line:
            self._save(max_line)
            return
        command = ShellCommand.request % (self._file_mark.line, max_line, path)
        if self._config.debug:
            print(command)
        rel = self._command(command)
        while True:
            line = rel.readline().strip()
            if self._config.debug:  # debug模式
                if not self._read_line(line, date):
                    break
            else:  # 非debug模式
                try:
                    if not self._read_line(line, date):
                        break
                except Exception as e:
                    self.exception_ding.send_error(e, line)
                    file_log.error(e)
        rel.close()
        self._save(max_line)

    def _read_line(self, line: str, date: str):
        line = line.strip()
        if not line:
            return False
        line = line.split(' ')
        line_len = len(line)
        count = int(line[0])
        platform = int(line[1])
        appid = line[2] if line_len >= 3 else None
        pid = line[3] if line_len >= 4 else None
        self.cache.cache_data(
            AggregateData(Dimension.platform, date, p_data=platform, fill_count=count))
        if appid:  # appid维度增加错误码出现次数
            self.cache.cache_data(
                AggregateData(Dimension.appid, date, unit_id=platform, p_data=appid, fill_count=count)
            )
        if pid:  # pid维度增加错误码出现次数
            self.cache.cache_data(
                AggregateData(Dimension.pid, date, unit_id=platform, p_data=pid, fill_count=count)
            )
        return True
