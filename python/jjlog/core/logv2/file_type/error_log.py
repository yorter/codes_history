from core.logv2.data.dimension import Dimension
from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.struct.aggregate_data import AggregateData


class ErrorLog(FileTypeBase):

    def _analysis_line(self):
        code = self.get_current_line_item(3)
        log_date = self._get_datetime()
        date = log_date.strftime("%Y-%m-%d")
        # 平台维度增加错误码出现次数
        platform = int(self.get_current_line_item(4))
        self.cache.cache_data(AggregateData(Dimension.platform, date, p_data=platform, bid_fail={code: 1}))
        appid = self.get_current_line_item(6)
        if appid:  # appid维度增加错误码出现次数
            self.cache.cache_data(
                AggregateData(Dimension.appid, date, unit_id=platform, p_data=appid, bid_fail={code: 1})
            )
        pid = self.get_current_line_item(7)
        if pid:  # pid维度增加错误码出现次数
            self.cache.cache_data(
                AggregateData(Dimension.pid, date, unit_id=platform, p_data=pid, bid_fail={code: 1})
            )
