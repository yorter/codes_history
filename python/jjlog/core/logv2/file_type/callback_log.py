from core.logv2.data.action_type import CallbackActionType
from core.logv2.data.dimension import Dimension
from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.struct.aggregate_data import AggregateData
from core.orm_model.redis_engine import RedisEngine


class CallbackLog(FileTypeBase):
    """
    点击数、展示数、dp唤醒数统计，需要按照request_id去重
    """

    def _analysis_line(self):
        log_date = self._get_datetime()
        date = log_date.strftime("%Y-%m-%d")
        platform = int(self.get_current_line_item(3))
        request_id = self.get_current_line_item(4)
        action_type = int(self.get_current_line_item(7))
        if not request_id or not action_type:
            return

        # 过滤重复请求
        redis_key = "unique_request:%s:%s:%s" % (platform, request_id, action_type)
        redis_conn = RedisEngine.get_conn(12)
        if not redis_conn.setnx(redis_key, 1):
            return
        else:
            redis_conn.expire(redis_key, 10 * 60)

        unit_id = int(self.get_current_line_item(8))
        plan_id = int(self.get_current_line_item(9))
        size = self.get_current_line_item(10)
        creative_id = int(self.get_current_line_item(11))
        appid = self.get_current_line_item(5)
        pid: str = self.get_current_line_item(6)
        self.get_aggregate_data_and_cache(  # 推广单元维度
            AggregateData(Dimension.unit, date, unit_id=unit_id, plan_id=plan_id), action_type
        )
        self.get_aggregate_data_and_cache(  # 推广单元日期维度
            AggregateData(Dimension.unit_date, date, unit_id=unit_id, plan_id=plan_id), action_type
        )
        self.get_aggregate_data_and_cache(  # 推广单元小时维度
            AggregateData(Dimension.unit_hour, date, unit_id=unit_id, plan_id=plan_id, p_data=log_date.hour),
            action_type
        )
        self.get_aggregate_data_and_cache(  # 推广计划维度
            AggregateData(Dimension.plan, date, unit_id=unit_id, plan_id=plan_id), action_type
        )
        self.get_aggregate_data_and_cache(  # 推广计划日期维度
            AggregateData(Dimension.plan_date, date, unit_id=unit_id, plan_id=plan_id), action_type
        )
        self.get_aggregate_data_and_cache(  # 创意维度
            AggregateData(Dimension.creative, date, unit_id=unit_id, plan_id=plan_id, p_data=creative_id),
            action_type
        )
        self.get_aggregate_data_and_cache(  # 尺寸维度
            AggregateData(Dimension.size, date, unit_id=unit_id, plan_id=plan_id, p_data=size),
            action_type
        )
        self.get_aggregate_data_and_cache(  # 平台维度
            AggregateData(Dimension.platform, date, p_data=platform),
            action_type
        )
        self.get_aggregate_data_and_cache(  # appid维度
            AggregateData(Dimension.appid, date, unit_id=platform, p_data=appid),
            action_type
        )
        self.get_aggregate_data_and_cache(  # pid维度
            AggregateData(Dimension.pid, date, unit_id=platform, p_data=pid),
            action_type
        )

    def get_aggregate_data_and_cache(self, data: AggregateData, action_type: int):
        """
        根据action_type类型添加不同的数据，并保存到变量中
        :param data:
        :param action_type:
        :return:
        """
        if action_type == CallbackActionType.click:
            data.click = 1
        elif action_type == CallbackActionType.view:
            data.view = 1
        else:
            data.dp_count = 1
        self.cache.cache_data(data)
