from core.logv2.data.dimension import Dimension
from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.struct.aggregate_data import AggregateData
from core.orm_model.redis_engine import RedisEngine


class BidLog(FileTypeBase):
    """
    竞价日志，包含参与竞价与竞价成功的日志信息
    竞价成功属于回调，需要去重
    """

    def _analysis_line(self):
        log_date = self._get_datetime()
        date = log_date.strftime("%Y-%m-%d")
        platform = self.get_current_line_item(3)
        request_id = self.get_current_line_item(4)
        appid = self.get_current_line_item(5)
        pid = self.get_current_line_item(6)
        unit_id = int(self.get_current_line_item(7))
        plan_id = int(self.get_current_line_item(8))
        size = self.get_current_line_item(9)
        creative_id = int(self.get_current_line_item(10))
        bid = self.get_current_line_item(13)
        if bid:
            bid = round(float(bid) / 100)
        else:
            bid = None
        price = round(float(self.get_current_line_item(11)) / 100, 2)
        cost = round(price / 1000, 5)
        bid_type = int(self.get_current_line_item(12))
        if bid_type == 1:  # 竞价成功去重
            # 过滤重复请求
            redis_key = "unique_bid:%s:%s" % (platform, request_id)
            redis_conn = RedisEngine.get_conn(12)
            if not redis_conn.setnx(redis_key, 1):
                return
            else:
                redis_conn.expire(redis_key, 10 * 60)
        self.save_bid(AggregateData(  # 推广单元维度
            Dimension.unit, date, unit_id=unit_id, plan_id=plan_id, default_price=price
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 推广单元日期维度
            Dimension.unit_date, date, unit_id=unit_id, plan_id=plan_id, default_price=price
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 推广单元小时维度
            Dimension.unit_hour, date, unit_id=unit_id, plan_id=plan_id, default_price=price, p_data=log_date.hour
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 推广计划维度
            Dimension.plan, date, unit_id=unit_id, plan_id=plan_id, default_price=price
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 推广日期计划维度
            Dimension.plan_date, date, unit_id=unit_id, plan_id=plan_id, default_price=price
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 创意维度
            Dimension.creative, date, unit_id=unit_id, plan_id=plan_id, default_price=price, p_data=creative_id
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 尺寸维度
            Dimension.size, date, unit_id=unit_id, plan_id=plan_id, default_price=price, p_data=size
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # 平台维度
            Dimension.platform, date, p_data=platform
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # appid维度
            Dimension.appid, date, unit_id=platform, p_data=appid
        ), bid_type, bid, cost)
        self.save_bid(AggregateData(  # pid维度
            Dimension.pid, date, unit_id=platform, p_data=pid
        ), bid_type, bid, cost)

    def save_bid(self, data: AggregateData, bid_type: int, bid: float, cost: float):
        if bid_type == 0:  # 竞价填充
            data.fill_ok_count = 1
        else:  # 竞价成功
            data.win_count = 1
            data.total_cost = cost
        if bid:
            data.bid = float(bid)
        self.cache.cache_data(data)
