from typing import Dict

from core.logv2.file_type.file_type_base import FileTypeBase


class ExceptionLogCount:
    def __init__(self, msg, file, line):
        self.msg = msg
        self.file = file
        self.line = line

    msg: str
    file: str
    line: int
    times: int = 1


class ExceptionLog(FileTypeBase):
    exception_cache: Dict[str, ExceptionLogCount] = {}

    def _analysis_line(self):
        msg = self.get_current_line_item(4)
        file = self.get_current_line_item(5)
        line = self.get_current_line_item(6)
        key = "%s_%s_%s" % (msg, file, line)
        if key in self.exception_cache:
            self.exception_cache[key].times += 1
        else:
            self.exception_cache[key] = ExceptionLogCount(msg, file, line)

    def __del__(self):
        if not len(self.exception_cache):
            return
        for key in self.exception_cache:
            err = self.exception_cache[key]
            msg = "错误信息:%s\n文件:%s\n行:%s\n出现次数:%s" % (err.msg, err.file, err.line, err.times)
            if self._config.debug:
                print(msg)
            else:
                self.exception_ding.send_message(msg)
