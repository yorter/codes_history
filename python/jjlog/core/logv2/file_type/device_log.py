import json
import zlib

from config import Config
from core.exception.data_error import LogAnalysisError
from core.logv2.file_type.file_type_base import FileTypeBase
from core.orm_model.redis_engine import RedisEngine
from tool.data_trans import ip2location, datetime_str2int


class DeviceInfo:
    os: int
    ip: str
    location: str


class Device:
    def __init__(self, message: dict, create_time: str):
        self.create_time = create_time
        self.message = message

    new_device_num = 0  # 新设备数量(保存到redis中的设备数量)
    message: dict
    mark_db = Config.get().log_system.device.mark_db
    save_db = Config.get().log_system.device.save_db
    md5_save_db = Config.get().log_system.device.md5_save_db
    expire_time = Config.get().log_system.device.expire_time * 3600
    prefix = Config.get().log_system.device.prefix

    keys = {
        'ip': (None, ""),
        'mac': (None, ""),
        'ua': (None, ""),
        'type': (int, 0),
        'os': (int, 0),
        'osv': (None, ""),
        'brand': (None, ""),
        'model': (None, ""),
        'sw': (int, ""),
        'sh': (int, ""),
        'lat': (float, ""),
        'lon': (float, ""),
        'orientation': (int, 0),
        'mcc': (None, ""),
        'mnc': (None, ""),
        'connection': (int, 0),
        'density': (None, ""),
        'lang': (None, ""),
        'appstorever': (None, ""),
        'hmscore': (None, ""),
        'updatemark': (None, ""),
        'bootmark': (None, ""),
        'osvcode': (int, 0),
        'appid': (None, ""),
        'platform': (None, ""),
    }

    android = {
        'anid': (None, ""),
        'adid': (None, ""),
        'imei': (None, ""),
        'oaid': (None, ""),
        'imsi': (None, ""),
    }
    ios = {
        'idfa': (None, ""),
        'idfv': (None, ""),
    }

    def save(self):
        device = DeviceInfo()
        self.__fill_device_info(device, self.keys)
        device.os = self.message.get('os')
        if device.os is None or device.os == "":  # 没有注明os，则自己判断os
            unique_id = self.message.get('idfa')
            if unique_id:  # ios
                device.os = 0
                unique_key: str = "ios___%s" % unique_id
                self.__fill_device_info(device, self.ios)
            else:  # 安卓
                device.os = 1
                unique_id = self.__get_android_unique_id()
                unique_key: str = unique_id
                self.__fill_device_info(device, self.android)
        else:  # 已经注明os
            device.os = int(device.os)
            if device.os == 0:  # ios
                unique_id = self.message.get('idfa')
                unique_key: str = "ios___%s" % unique_id
                self.__fill_device_info(device, self.ios)
            elif device.os == 1:  # 安卓
                unique_id = self.__get_android_unique_id()
                unique_key: str = unique_id
                self.__fill_device_info(device, self.android)
            else:
                return
        if not unique_id:
            return
        mark_conn = RedisEngine.get_conn(self.mark_db)
        if mark_conn.exists(unique_key):
            return
        else:
            mark_conn.set(unique_key, 1, ex=self.expire_time)
        # 获取ip实际地址
        if device.ip == "127.0.0.1":
            device.location = ""
        else:
            try:
                location = ip2location(device.ip)
                if isinstance(location, str):
                    locations = location.split(' ')
                    device.location = " ".join(locations[:-1])
                else:
                    return
            except:
                return

        queue_no = zlib.crc32(unique_key.encode('utf8')) % 100
        id_len = len(unique_id)
        save_conn = RedisEngine.get_conn(self.md5_save_db if (id_len == 32 or id_len == 64) else self.save_db)
        device.uniq_key = unique_key
        device.use_time = datetime_str2int(self.create_time)
        save_conn.lpush("%s%s" % (self.prefix, queue_no), json.dumps(device.__dict__))
        Device.new_device_num += 1

    def __fill_device_info(self, device: DeviceInfo, keys: dict):
        for field in keys:
            [t, d] = keys[field]
            value = self.message.get(field)
            if t is None:
                setattr(device, field, value if value is not None else d)
            else:
                if value is None or value == "":
                    setattr(device, field, d)
                else:
                    setattr(device, field, t(value))

    def __get_android_unique_id(self):
        """
        获取安卓唯一id
        :return:
        """
        imei = self.message.get('imei')
        oaid = self.message.get('oaid')
        if imei and isinstance(imei, str) and len(imei) in [15, 16, 32, 64]:
            return imei
        elif oaid and isinstance(oaid, str):
            return oaid
        else:
            raise LogAnalysisError("invalid Android unique_id")


class DeviceLog(FileTypeBase):
    """
    设备日志，目前设备信息直接入redis
    """

    def _analysis_line(self):
        try:
            device_json = json.loads(self.get_current_line_item(7))
            device = Device(device_json, self.get_current_line_item(0)[:19])
            device.save()
        except json.decoder.JSONDecodeError as e:
            return
        except ValueError:
            return

    def __del__(self):  # 保存新设备数量
        if Device.new_device_num > 0:
            date = self._log_date.strftime("%Y-%m-%d")
            redis_key = "new_device:%s" % date
            redis = RedisEngine.get_conn(0)
            redis.incr(redis_key, Device.new_device_num)
            redis.expire(redis_key, 48 * 3600)

        # mark_dir = get_absolutely_path("%s/count/" % self._config.log_system.logs.mark_dir, self._config.root_path)
        # mark_file = "%s/%s" % (mark_dir, date)
        # count = 0
        # if os.path.exists(mark_file):
        #     with open(mark_file, mode='r', encoding="utf8") as f:
        #         count = int(f.read().strip())
        # total_count = count + Device.new_device_num
        # print(Device.new_device_num)
        # with open(mark_file, mode='w', encoding="utf8") as f:
        #     f.write(str(total_count))
