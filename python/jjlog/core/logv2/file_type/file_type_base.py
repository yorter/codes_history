import datetime
import json
import os
import time

from config import Config
from config.orm.config_info import ConfigInfo
from core.exception.exception_base import LogExceptionBase
from core.exception.success.success_exception import SuccessException
from core.logv2.cache import CacheBase
from core.logv2.cache.memory_cache import MemoryCache
from core.logv2.struct.file_offset_mark import FIleOffsetMark
from tool.data_trans import get_absolutely_path, dict2obj
from tool.dingding import DingDing
from tool.log import flush_print, file_log


class FileTypeBase:
    _offset = 0  # 文件指针偏移位置
    _config: ConfigInfo  # 配置信息
    _file_mark: FIleOffsetMark  # 记录文件处理行数的对象
    _file_mark_path: str  # 记录文件处理行数的对象的文件位置
    _size = 0  # 文件大小
    cache: CacheBase  # 缓存管理对象
    file_name: str  # 操作的文件名
    _file = None
    _log_date: datetime.datetime
    exception_ding: DingDing

    def __init__(self, file_name: str, log_datetime: datetime.datetime):
        """
        通用初始化
        :param file_name: 文件名称
        """
        seq_date = datetime.datetime.strptime('20220512', '%Y%m%d')  # 从指定日期开始，使用新的mark标识地址
        self._log_date = log_datetime
        self.file_name = file_name
        self.cache = MemoryCache()
        self.current_line = []  # 当前行内容
        self.current_line_len = 0  # 当前行内容数组长度
        self._file_mark = FIleOffsetMark()
        self._config = Config.get()
        self.exception_ding = DingDing()
        root_path = self._config.root_path
        # 获取文件的的指针位置,从指定日期开始，使用新的mark标识地址
        if log_datetime >= seq_date:
            base_dir = self._config.log_system.logs.mark_dir
            mark_dir = get_absolutely_path(
                "%s/%s-%s/%s/" % (base_dir, log_datetime.year, log_datetime.month, log_datetime.day),
                root_path
            )
        else:
            mark_dir = get_absolutely_path("%s/" % self._config.log_system.logs.mark_dir, root_path)
        self._file_mark_path = "%s/%s" % (mark_dir, file_name)
        if os.path.exists(self._file_mark_path) and not self._config.is_reload:  # 文件存在且不是重新读写模式
            with open(self._file_mark_path, mode='r', encoding="utf8") as mark_f:
                mark_json = mark_f.read()
                if mark_json:
                    mark_dict = json.loads(mark_json)
                    self._file_mark = dict2obj(mark_dict, FIleOffsetMark)
                else:
                    self._save()
        else:
            self._save()
        path = "%s/%s" % (self._config.log_system.logs.log_dir, file_name)
        # 打开要读的日志文件并修改指针位置
        self._size = os.path.getsize(path)
        self._file = open(path, mode='r', encoding="utf8")
        self._file.seek(self._file_mark.offset)

    def analysis(self):
        """
        开始分析文件
        :return:
        """
        while True:
            self.readline()

    def _analysis_line(self):
        """
        分析一行的行为，由子类完成
        :return:
        """

    def readline(self):
        """
        读取文件的一行，然后按照制表格拆分字符串
        :return:
        """
        if self._file.tell() >= self._size:  # 读完整个文件
            self._save(True)
            raise SuccessException()
        else:
            delta = time.time() - self._file_mark.last_update
            if delta >= self._config.log_system.logs.mysql_save_time_interval:  # 到达存储到mysql的时间
                self._save()
        line = self._file.readline()
        if self._config.debug:
            flush_print("处理文件%s,进度%s%%,offset:%s,size%s" % (
                self.file_name, round((self._file.tell() / self._size) * 100, 2), self._file.tell(), self._size))
        self.current_line = line.split("\t")
        self.current_line_len = len(self.current_line)
        if self._config.debug:
            try:
                self._analysis_line()
            except LogExceptionBase as e:
                pass
        else:
            try:
                self._analysis_line()
            except LogExceptionBase as e:
                pass
            except Exception as e:
                file_log.error(e)
                msg = "解析文件:%s\n行内容%s" % (self.file_name, ' '.join(self.current_line))
                self.exception_ding.send_error(e, msg)
                pass

    current_line: list  # 当前行内容
    current_line_len: int  # 当前行数组长度

    def get_current_line_item(self, index: int):
        """
        获取当前行数组对象的下标字符串，返回字符或者None
        :param index:
        :return:
        """
        if self.current_line_len < index:
            return None
        else:
            s: str = self.current_line[index]
            return s.strip()

    def _save(self, db_must=False):
        """
        保存文件处理的指针位置
        :return:
        """
        self.cache.update_db(db_must)
        if not self._config.mark_line:
            return
        with open(self._file_mark_path, mode='w', encoding="utf8") as mark_f:
            self._file_mark.last_update = time.time()
            self._file_mark.offset = self._file.tell() if self._file else 0
            mark_f.write(json.dumps(self._file_mark.__dict__))

    def _get_datetime(self):
        """
        获取时间戳
        :return:
        """
        return datetime.datetime.strptime(self.get_current_line_item(0)[:19], "%Y-%m-%d %H:%M:%S")

    def _get_client_ip(self):
        """
        获取客户端ip
        :return:
        """
        return self.current_line[1]

    def _get_version(self):
        """
        获取日志版本号
        :return:
        """
        return int(self.current_line[2])

    def __del__(self):
        self.cache.update_db(True)
