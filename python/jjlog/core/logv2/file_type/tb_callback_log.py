import datetime
import json
from typing import List, Dict

from sqlalchemy.orm import Session, Query

from core.logv2.cache.tb_callback_cache import TBCache
from core.logv2.data.dimension import Dimension
from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.struct.aggregate_tb_data import AggregateTBData
from core.orm_model.mysql.channel_appid import ChannelAppid
from core.orm_model.mysql.channel_pid import ChannelPid
from core.orm_model.mysql_engine import Engine
from core.orm_model.redis_engine import RedisEngine


class AppidCache:
    def __init__(self, platform, id):
        self.platform = platform
        self.id = id

    platform: int
    id: int


class PidCache:
    def __init__(self, platform, id):
        self.platform = platform
        self.id = id

    platform: int
    id: int


class TBCallback(FileTypeBase):
    session: Session
    _pid_cache: Dict[str, PidCache] = {}
    _pid_fail_cache = set()
    _appid_cache: Dict[str, AppidCache] = {}
    _appid_fail_cache = set()

    def __init__(self, file_name: str, log_datetime: datetime.datetime):
        super().__init__(file_name, log_datetime)
        self.cache = TBCache()
        self.session = Engine.get_session()

    def _analysis_line(self):
        log_date = self._get_datetime()
        date = log_date.strftime("%Y-%m-%d")
        info: str = self.get_current_line_item(3)
        try:
            tb_data: dict = json.loads(info)
        except json.decoder.JSONDecodeError:
            return
        if "transformType" not in tb_data:
            return
        trans = int(tb_data['transformType'])

        # ocpx回调去重
        request_id = tb_data.get('request_id')
        platform = tb_data.get('platform')
        unique_key = "tb_callback_unique:%s_%s:%s" % (request_id, platform, trans)
        redis_conn = RedisEngine.get_conn(12)
        if not redis_conn.setnx(unique_key, 1):
            return
        else:
            redis_conn.expire(unique_key, 10 * 60)

        # if "adid" in tb_data:  # 推广单元
        #     unit_id = int(tb_data['adid'])
        #     self.cache.cache_data(AggregateTBData(Dimension.unit, unit_id, date, trans))
        # if "creative_id" in tb_data:  # 创意
        #     creative_id = int(tb_data['creative_id'])
        #     self.cache.cache_data(AggregateTBData(Dimension.creative, creative_id, date, trans))
        # if "plan_id" in tb_data:  # 推广计划
        #     plan_id = int(tb_data['plan_id'])
        #     self.cache.cache_data(AggregateTBData(Dimension.plan, plan_id, date, trans))
        # if "size" in tb_data:  # 尺寸
        #     size = tb_data['size']
        #     pass
        appid = None
        appid_id = None
        platform = None
        if "platform" in tb_data:
            platform = int(tb_data['platform'])
        if "appid" in tb_data:
            appid_value = tb_data['appid']
            if appid_value not in self._appid_fail_cache:
                if appid_value in self._appid_cache:  # 从缓存读取
                    cache = self._appid_cache[appid_value]
                    self.cache.cache_data(AggregateTBData(Dimension.appid, cache.id, date, trans))
                    platform = cache.platform
                    appid_id = cache.id
                else:  # 从数据库读取
                    # 根据appid查询appid主键id
                    query: Query = self.session.query(ChannelAppid).filter(ChannelAppid.appid == appid_value)
                    if platform:
                        query = query.filter(ChannelAppid.platform_id == platform)
                    appids: List[ChannelAppid] = query.all()
                    if isinstance(appids, list) and len(appids) == 1:  # 如果查找出多条记录，则摄取，因为无法判断具体归属
                        for appid_info in appids:
                            appid = appid_info.id
                            platform = appid_info.platform_id
                            self._appid_cache[appid_value] = AppidCache(appid_info.platform_id, appid_info.id)
                            self.cache.cache_data(AggregateTBData(Dimension.appid, appid, date, trans))
                    else:
                        self._appid_fail_cache.add(appid_value)

        if "pid" in tb_data:  # pid维度
            pid_value = str(tb_data['pid'])
            if pid_value not in self._pid_fail_cache:
                if pid_value in self._pid_cache:  # 已经缓存则使用缓存
                    cache = self._pid_cache[pid_value]
                    self.cache.cache_data(AggregateTBData(Dimension.pid, cache.id, date, trans))
                    platform = cache.platform
                else:  # 没有缓存，则使用数据库查询
                    query: Query = self.session.query(ChannelPid).filter(ChannelPid.pid == pid_value)
                    if appid_id:
                        query = query.filter(ChannelPid.appid == appid_id)
                    if platform:
                        query = query.filter(ChannelPid.ch_id == platform)
                    pids: List[ChannelPid] = query.all()
                    if isinstance(pids, list) and len(pids) == 1:  # 如果查找出多条记录，则摄取，因为无法判断具体归属
                        for pid_info in pids:
                            platform = pid_info.ch_id
                            self._pid_cache[pid_value] = PidCache(pid_info.ch_id, pid_info.id)  # 缓存已经查询过的pid信息
                            self.cache.cache_data(AggregateTBData(Dimension.pid, pid_info.id, date, trans))
                    else:
                        self._pid_fail_cache.add(pid_value)
        if platform:
            self.cache.cache_data(AggregateTBData(Dimension.platform, platform, date, trans))
