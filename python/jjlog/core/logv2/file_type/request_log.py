from core.logv2.data.dimension import Dimension

from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.struct.aggregate_data import AggregateData


class RequestLog(FileTypeBase):

    def _analysis_line(self):
        log_type = self.get_current_line_item(4)
        if log_type is None or log_type != "req":
            return
        log_date = self._get_datetime()
        date = log_date.strftime("%Y-%m-%d")
        platform = int(self.get_current_line_item(3))
        self.cache.cache_data(AggregateData(Dimension.platform, date, fill_count=1, p_data=platform))
        appid = self.get_current_line_item(6)
        if appid:
            self.cache.cache_data(AggregateData(Dimension.appid, date, fill_count=1, unit_id=platform, p_data=appid))
        pid: str = self.get_current_line_item(7)
        if pid:
            pids = pid.split(',')
            for pid in pids:
                if pid:
                    self.cache.cache_data(
                        AggregateData(Dimension.pid, date, fill_count=1, unit_id=platform, p_data=pid)
                    )
