import datetime
import os
import re

from config import Config
from config.orm.config_info import ConfigInfo
from core.ActionBase import ActionBase
from core.exception.success.success_exception import SuccessException
from core.logv2.awk.awk_error_code import AwkErrorCode
from core.logv2.awk.awk_request_log import AwkRequestLog
from core.logv2.file_type.bid_log import BidLog
from core.logv2.file_type.callback_log import CallbackLog
from core.logv2.file_type.error_log import ErrorLog
from core.logv2.file_type.exception_log import ExceptionLog
from core.logv2.file_type.file_type_base import FileTypeBase
from core.logv2.file_type.request_log import RequestLog
from core.logv2.file_type.tb_callback_log import TBCallback
from tool import pid
from tool.dingding import DingDing
from tool.log import file_log


class Action(ActionBase):
    """
    新版日志分析
    """
    # 配置信息
    _config: ConfigInfo
    exception_ding: DingDing

    start_str_to_class = {
        'request_': RequestLog,
        'error_code_': ErrorLog,
        'callback_': CallbackLog,
        'bid_': BidLog,
        'tb_callback_': TBCallback,
        'exception_': ExceptionLog,
    }

    awk_start_str_to_class = {
        'error_code_': AwkErrorCode,
        'request_': AwkRequestLog,
    }

    # 以日期区分
    file_re_date = re.compile('^\d{8}$')
    # 以日期小时区分
    file_re_date_hour = re.compile('^\d{10}$')
    delete_pid = False  # 是否删除pid文件

    def __init__(self):
        self._config = Config.get()
        pid.pid_save(self._config.log_system.logs.pid_file)
        self.delete_pid = True
        self.exception_ding = DingDing()

    def run(self):  # 入口函数
        now = datetime.datetime.now()
        log_dir = self._config.log_system.logs.log_dir
        files = os.listdir(log_dir)
        disable_log_actions = self._config.log_system.logs.disable_log_actions.split(',')
        for f in files:
            for start_str in self.start_str_to_class:
                if f.startswith(start_str):  # 如果文件是以某个字符串开头，则创建对应的对象
                    # 判断log_action，当指定log_action之后仅执行log_action的日志
                    if self._config.log_action and start_str != self._config.log_action:
                        break
                    elif self._config.log_system.logs.disable_log_actions:
                        # 有禁用的log_action，判断是否是禁用的log_action
                        if start_str in disable_log_actions:
                            break
                    # 判断文件的时间
                    log_date = f.lstrip(start_str).rstrip(self._config.log_system.logs.log_suffix)
                    if self.file_re_date.search(log_date):  # 以天为单位
                        time_format = "%Y%m%d"
                        log_datetime = datetime.datetime.strptime(log_date, time_format)
                        max_days = self._config.log_system.logs.max_days
                        # 要求日志天数不等于0，则进行对日志的时间做判断
                        # 如果指定了文件名，则不做时间判断
                        if max_days != 0 and not self._config.file:
                            delta = now - log_datetime
                            if delta.days == 1 and now.hour == 0:  # 以天为单位，为了防止0点前一段时间数据丢失
                                pass
                            elif delta.days >= self._config.log_system.logs.max_days:
                                break
                    elif self.file_re_date_hour.search(log_date):  # 以小时为单位
                        time_format = "%Y%m%d%H"
                        log_datetime = datetime.datetime.strptime(log_date, time_format)
                        max_hours = self._config.log_system.logs.max_hours
                        # 要求日志小时数不等于0，则进行对日志的时间做判断
                        # 如果指定了文件名，则不做时间判断
                        if max_hours != 0 and not self._config.file:
                            delta = now - log_datetime
                            if delta.days != 0 or delta.seconds >= (max_hours * 3600):
                                break
                    else:
                        break
                    # 根据情况使用不同的类
                    if start_str in self.awk_start_str_to_class:
                        _class = self.awk_start_str_to_class[start_str]
                    else:
                        _class = self.start_str_to_class[start_str]
                    if self._config.file and self._config.file != f:  # 如果指定了文件名，则其他文件名直接跳过
                        break
                    if self._config.debug:
                        try:
                            obj: FileTypeBase = _class(f, log_datetime)
                            obj.analysis()
                        except SuccessException:
                            break
                    else:
                        try:
                            obj: FileTypeBase = _class(f, log_datetime)
                            obj.analysis()
                        except SuccessException:
                            break
                        except Exception as e:
                            file_log.error(e)
                            msg = "解析文件:%s" % f
                            self.exception_ding.send_error(e, msg)
                            break

    def __del__(self):
        if self.delete_pid:
            pid.pid_delete(self._config.log_system.logs.pid_file)
