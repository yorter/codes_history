class ShellCommand:
    wc_l = "/usr/bin/wc -l %s"  # 统计行数
    awk = "/usr/bin/awk "  # awk路径
    error_code = awk + "-F'\\t' 'NR>%d && NR<=%d{print $4,$5,$7,$8}' %s |sort|uniq -c"  # 错误码统计
    request = awk + "-F'\\t' 'NR>%d && NR<=%d && $5==\"req\"{print $4,$7,$8}' %s |sort|uniq -c"  # 请求数统计
