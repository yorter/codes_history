class Dimension:
    """
    维度id
    """
    plan = 2001
    plan_date = 2002

    unit = 3001
    unit_date = 3002
    unit_hour = 3003

    creative = 5001
    size = 5002

    platform = 7001
    appid = 7002
    pid = 7003
