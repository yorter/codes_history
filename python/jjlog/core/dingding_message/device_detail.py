import time
from datetime import datetime, timedelta

from config import Config
from core.ActionBase import ActionBase
from core.orm_model.mysql.ad_platform import AdPlatform
from core.orm_model.mysql_engine import Engine
from core.orm_model.redis_engine import RedisEngine
from tool.dingding import DingDing


class Action(ActionBase):
    def run(self):
        seq_date = datetime.strptime('20220528', '%Y%m%d')  # 从指定日期开始，重复设备和新设备使用新格式
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        if yesterday >= seq_date:
            self.new_type_info(yesterday)
            return
        cnf = Config.get()

        date = yesterday.strftime("%Y-%m-%d")
        count = "未知"
        redis_key = "new_device:%s" % date
        redis = RedisEngine.get_conn(0)
        if redis.exists(redis_key):
            redis.expire(redis_key, 36 * 3600)
            count = int(redis.get(redis_key))
        repeat = "未知"
        redis_key = "repeat_device_:%s" % date
        if redis.exists(redis_key):
            redis.expire(redis_key, 36 * 3600)
            repeat = int(redis.get(redis_key))
        ding = DingDing(cnf.process.ding_message)
        ding.send_message(
            "日期:%s\n新录入设备数量:%s\n重复设备数量:%s" % (date, count, repeat)
        )

    @staticmethod
    def new_type_info(yesterday: datetime):
        cnf = Config.get()
        date = yesterday.strftime("%Y-%m-%d")
        redis = RedisEngine.get_conn(0)
        new_key = "new_device:%s" % date
        redis.expire(new_key, 36 * 3600)
        repeat_key = "repeat_device_:%s" % date
        redis.expire(repeat_key, 36 * 3600)

        db = Engine.get_session()
        platform_id = 0
        ding = DingDing(cnf.process.ding_message)
        while True:
            platform = db.query(AdPlatform).filter(AdPlatform.id > platform_id).first()
            if isinstance(platform, AdPlatform):
                platform_id = platform.id
                print(platform_id)
                # 新设备数量
                if redis.hexists(new_key, str(platform_id)):
                    new_count = int(redis.hget(new_key, str(platform_id)))
                else:
                    new_count = 0
                # 重复设备数量
                if redis.hexists(repeat_key, str(platform_id)):
                    repeat_count = int(redis.hget(repeat_key, str(platform_id)))
                else:
                    repeat_count = 0
                if new_count == 0 and repeat_count == 0:
                    continue
                ding.send_message(
                    "日期:%s\n平台:%s\n新录入设备数量:%s\n重复设备数量:%s" % (date, platform.name, new_count, repeat_count)
                )
                time.sleep(3)
            else:
                break
