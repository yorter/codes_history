class LogExceptionBase(Exception):
    """
    自定义错误的基类
    """
    _error_info = None

    def __init__(self, error_info=None):
        self._error_info = error_info

    def __str__(self):
        if self._error_info is None:
            return super().__str__()
        return self._error_info


class ProcessNormalOver(Exception):
    """
    程序正常停止
    """
