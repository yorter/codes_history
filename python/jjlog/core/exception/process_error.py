from core.exception.exception_base import LogExceptionBase


class ConfigNotExistent(LogExceptionBase):
    """
    配置不存在
    """

    def __str__(self):
        return "配置项不存在"


class GetSqliteEngineError(LogExceptionBase):
    """
    获取sqlite orm引擎错误
    """

    def __str__(self):
        return "获取sqlite orm引擎错误"
