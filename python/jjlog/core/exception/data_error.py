from core.exception.exception_base import LogExceptionBase


class UnitNotFound(LogExceptionBase):
    """
    没有找到推广单元
    """

    def __init__(self, unit_id):
        super().__init__()


class LogAnalysisError(LogExceptionBase):
    """
    日志解析异常
    """
