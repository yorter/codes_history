import os
import sys
import re
import datetime
import winreg


def delete_sub_key(key0, current_key):
    open_key = winreg.OpenKey(key0, current_key, 0, winreg.KEY_ALL_ACCESS)
    info_key = winreg.QueryInfoKey(open_key)
    for x in range(0, info_key[0]):
        sub_key = winreg.EnumKey(open_key, x)
        try:
            winreg.DeleteKey(open_key, sub_key)
            print("Removed %s\\%s " % (current_key, sub_key))
        except OSError:
            delete_sub_key(key0, "%s/%s" % (current_key, 1), sub_key)

    winreg.DeleteKey(open_key, "")
    open_key.Close()
    print("Removed %s" % current_key)
    return


class Reset:
    def __init__(self):
        self.path = "%s/JetBrains" % os.environ.get("APPDATA")

    # 存放JetBrains相关文件的位置
    path: str
    dir_zz = re.compile("\.?(IntelliJIdea|GoLand|CLion|PyCharm|DataGrip|RubyMine|AppCode|PhpStorm|WebStorm|Rider).*")

    def run(self):
        paths = os.listdir(self.path)
        for path in paths:
            dir_path = "%s/%s" % (self.path, path)
            if not os.path.isdir(dir_path):
                continue
            s = self.dir_zz.search(path)
            eval_path = "%s/eval" % dir_path
            if not s or not os.path.isdir(eval_path):
                continue
            # 删除目录下所有文件
            files = os.listdir(eval_path)
            for f in files:
                file_path = "%s/%s" % (eval_path, f)
                if not os.path.isfile(file_path):
                    continue
                print("Remove File %s" % file_path)
                os.remove(file_path)
            # 删除options\other.xml文件
            other_file_path = "%s/options/other.xml" % dir_path
            if os.path.isfile(other_file_path):
                os.remove(other_file_path)
            self.del_reg(s.group(1))

    def del_reg(self, soft: str):
        key_str = "Software\JavaSoft\Prefs\jetbrains\%s" % soft.lower()
        try:
            self.delete_key(key_str)
        except OSError:
            return

    def delete_key(self, current_key):
        open_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, current_key, 0, winreg.KEY_ALL_ACCESS)
        info_key = winreg.QueryInfoKey(open_key)
        for x in range(0, info_key[0]):
            try:
                sub_key = winreg.EnumKey(open_key, x)
            except OSError:
                continue
            try:
                winreg.DeleteKey(open_key, sub_key)
                print("Removed %s\\%s " % (current_key, sub_key))
            except OSError:
                try:
                    self.delete_key("%s\%s" % (current_key, sub_key))
                except Exception:
                    continue
        winreg.DeleteKey(open_key, "")
        open_key.Close()
        print("Removed %s" % current_key)


if __name__ == '__main__':
    # day = datetime.datetime.now().day
    # if day not in [1, 15]:
    #     exit(0)
    r = Reset()
    r.run()
    input("按回车键结束!!!")
