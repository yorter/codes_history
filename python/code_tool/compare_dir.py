import hashlib
import os
import sys


class Compare:
    dirs = []
    dir_1: str
    dir_2: str

    dir_md5 = [

    ]

    def __init__(self, d_1: str, d_2: str):
        if not os.path.isdir(d_1) or not os.path.isdir(d_2):
            raise Exception("请输入正确的目录地址")
        self.dirs.append(d_1)
        self.dirs.append(d_2)
        self.dir_md5.append([])
        self.dir_md5.append([])

    @staticmethod
    def get_file_md5(filename):
        """
        计算文件md5值
        :param filename: 文件地址
        :return:文件md5值
        """
        myhash = hashlib.md5()
        f = open(filename, 'rb')
        while True:
            b = f.read(8096)
            if not b:
                break
            myhash.update(b)
        f.close()
        return myhash.hexdigest()

    def __check_dir(self, dirname: str, i: int):
        files = os.listdir(dirname)
        for f in files:
            file_path = "%s/%s" % (dirname, f)
            if os.path.isfile(file_path):
                self.dir_md5[i].append(self.get_file_md5(file_path))
            else:
                self.__check_dir(file_path, i)

    @staticmethod
    def __count(big: set, small: set):
        same_count = 0
        for md5 in small:
            if md5 in big:
                same_count = same_count + 1
        print("两个文件夹文件数为 %s  %s" % (len(big), len(small)))
        print("两个文件夹相同文件数为 %s" % same_count)

    def compare(self):
        for i in [0, 1]:
            self.__check_dir(self.dirs[i], i)
        d_1 = set(self.dir_md5[0])
        d_2 = set(self.dir_md5[1])
        if len(d_1) > len(d_2):
            self.__count(d_1, d_2)
        else:
            self.__count(d_2, d_1)


if __name__ == '__main__':
    dir_1 = sys.argv[1]
    dir_2 = sys.argv[2]
    compare = Compare(dir_1, dir_2)
    compare.compare()
