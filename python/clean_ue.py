import shutil
import os
import sys
import re

delete_dirs = ['Binaries', 'Intermediate']


def remove_files(dir_path):
    for dir_name in delete_dirs:
        path = "%s/%s" % (dir_path, dir_name)
        if os.path.exists(path):
            shutil.rmtree(path)


def clean_dir(dir_path: str):
    files = os.listdir(dir_path)
    is_plugin_dir = False
    for file in files:
        if isinstance(file, str) and file.endswith('.uplugin'):
            is_plugin_dir = True
            break
    if is_plugin_dir:
        remove_files(dir_path)
    else:
        for file in files:
            file_path = "%s/%s" % (dir_path, file)
            if os.path.isdir(file_path):
                clean_dir(file_path)


input_path = input('请输入目录---->')
if len(input_path):
    root_path = input_path
else:
    root_path = os.path.dirname(os.path.realpath(os.path.dirname(sys.argv[0])))

print(root_path)
remove_files(root_path)
plugins_path = "%s/Plugins" % root_path
if os.path.exists(plugins_path):
    clean_dir(plugins_path)
