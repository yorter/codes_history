from setuptools import setup, find_packages

setup(
    name="小说公众号内小说获取获取",
    description="通过phantomjs模仿用户登录获取账号内小说信息",
    long_description="通过phantomjs模仿用户登录获取账号内小说信息（已取消）",
    author="饿魔",
    author_email="yorter@126.com",

    packages=find_packages(),
    include_package_data=True,
    platforms="any",
    install_requires=[
        'redis',
        'pymysql',
        'BeautifulSoup4',
        'requests',
        'pyquery',
        'selenium'
    ],

    scripts=[],
    entry_points={
        'console_scripts': [
            'test = test.help:main'
        ]
    }
)
