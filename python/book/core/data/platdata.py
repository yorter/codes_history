import re
import math


class PlatData:
    # 平台id
    platform = 0
    # 时间
    date = ""
    # 充值金额(分)
    value = 0

    # 充值比数
    recharge_count = 0

    # 普通充值
    normal_charge = 0

    # 普通充值支付订单数
    normal_charge_count = 0

    # 年费VIP会员
    vip_charge = 0

    # 年费VIP会员支付订单数
    vip_charge_count = 0

    # 充值人均
    @property
    def average(self):
        try:
            return math.floor(self.value / (self.vip_charge_count + self.normal_charge_count))
        except:
            return 0

    def get_insert_data(self) -> dict:
        return {
            'platform': self.platform,
            'data': self.date,  # 日期
            'value': self.value,  # 充值金额(分)
            'normal_charge': self.normal_charge,  # 普通充值
            'normal_charge_count': self.normal_charge_count,  # 普通充值支付订单数
            'vip_charge': self.vip_charge,  # 年费VIP会员
            'vip_charge_count': self.vip_charge_count,  # 年费VIP会员支付订单数
            'recharge_count': self.recharge_count,  # 充值比数
            'average': self.average,  # 充值人均
        }

    def get_update_data(self) -> dict:
        return {
            'value': self.value,
            'recharge_count': self.recharge_count,
            'normal_charge': self.normal_charge,
            'normal_charge_count': self.normal_charge_count,
            'vip_charge': self.vip_charge,
            'vip_charge_count': self.vip_charge_count,
            'average': self.average
        }

    """
    以下是清理方法
    """
    zz = re.compile('(\.){1}')
    before_dp = re.compile('(.+)\.')

    # 去除逗号，返回字符串
    @staticmethod
    def __remove_str_to_float(s) -> float:
        str_num = str(s).replace(",", "")
        return float(str_num)

    def __remove_str_to_int(self, s) -> int:
        str_num = str(s).replace(",", "")
        if self.zz.search(str_num):
            return int(self.before_dp.search(str_num).group(1))
        return int(str_num)

    def __remove_floatstr_to_int(self, v):
        return int(self.__remove_str_to_float(v) * 100)
