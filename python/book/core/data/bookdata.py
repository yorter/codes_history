import re
import json
import sys

import redis

from config import redis_key
from config.dbconfig import redis_config
from core.mylibs.mysql import Mysql
from core.mylibs.table import Table


class BookData:
    def __init__(self, user):
        self._db = Mysql()
        # 获取采集时间
        self.timestamp = int(self.redis.get(redis_key.nowtime))
        # 平台
        self.pid = user['pid']
        self.pname = user['aname']
        self.check_public(user)

    _db = None

    # 内外推类型列表(0:内推 1:外推)
    _tui_types = [0, 1]

    # 平台id
    pid = 1
    # 公众号名称
    pname = ''

    """
    公众号管理
    """

    def check_public(self, account):
        """
        检查公众号是否存在
        :param account:账户信息
        :return:None
        """
        where = {
            'username': account['username'],
            'pid': account['pid'],
        }
        public_info = self._db.table('public').where(where).find()
        if not public_info:
            user_data = {
                'username': account['username'],
                'name': account['aname'],
                'pid': account['pid'],
                'belongto': 0,
            }
            self._db.table('public').insert(user_data)
            self.public_id = self._db.lastrowid()
        else:
            self.public_id = public_info['id']
        return

    """
    bookinfov2
    """
    # 连接id
    linkdesc_id = ''
    # 小说id
    book_id = ''
    # 连接名
    linkdesc = ''
    # 小说名
    book_name = ''
    # 公众号id(即将废弃)
    public_id = 0

    # 内容外推类型 0:内推 1:外推
    _tui = 0

    @property
    def tui(self):
        """
        获取推
        :return:
        """
        return self._tui

    @tui.setter
    def tui(self, v):
        """
        设置tui
        :param v:
        :return:
        """
        v = int(v)
        if v not in self._tui_types:
            raise Exception("不存在的推类型")
        else:
            self._tui = v

    # 成本，默认为0，不修改
    cost_amount = 0

    def save_bookinfov2(self):
        """
        检查书是否已经检查过并且检查数据库中是否存在,(如果连接名或者数名发生发生更新则会修改数据库)
        :return:
        """
        where = {
            'linkdesc_id': self.linkdesc_id,
            'pid': self.pid
        }
        find = self._db.table('bookinfov2').where(where).find()
        if find:
            self.cost_amount = int(find['cost_amount'])
            self.book_info_id = find['id']
            # 如果小说名或者链接名发生变化，修改小说信息
            if (find['name'] != self.book_name) or (find['linkdesc'] != self.linkdesc) or (
                    int(find['tui']) != self.tui) or (find['public_id'] != self.public_id):
                update_data = {
                    'name': self.book_name,
                    'linkdesc': self.linkdesc,
                    'tui': self.tui,
                    'public_id': self.public_id
                }
                self._db.table('bookinfov2').where(where).update(update_data)
        else:
            # 如果小说不在数据库中，写入数据库
            self.cost_amount = 0
            save_data = {
                'linkdesc_id': self.linkdesc_id,
                'book_id': self.book_id,
                'pid': self.pid,
                'linkdesc': self.linkdesc,
                'name': self.book_name,
                'tui': self.tui,
                'public_id': self.public_id,
                'cost_amount': self.cost_amount,
            }
            self._db.table('bookinfov2').insert(save_data)
            self.book_info_id = self._db.lastrowid()

    """
    bookv2
    """
    # bookinfov2中的id
    book_info_id = 0

    # 新增阅读用户
    _member_count = 0

    @property
    def member_count(self):
        return self._member_count

    @member_count.setter
    def member_count(self, v):
        # if float(v) == 0:
        #     self._member_count = 0
        #     return
        self._member_count = self.__remove_str_to_int(v)

    # 关注
    _subscribe_count = 0

    @property
    def subscribe_count(self):
        return self._subscribe_count

    @subscribe_count.setter
    def subscribe_count(self, v):
        self._subscribe_count = self.__remove_str_to_int(v)

    # 充值金额
    _paid_amount = 0

    @property
    def paid_amount(self):
        return self._paid_amount

    @paid_amount.setter
    def paid_amount(self, v):
        self._paid_amount = self.__remove_str_to_float(v)

    # 关注率
    subscribe_rate = 0

    # 充值笔数
    _paid_count = 0

    @property
    def paid_count(self):
        return self._paid_count

    @paid_count.setter
    def paid_count(self, v):
        if float(v) == 0:
            self._paid_count = 0
            return
        self._paid_count = self.__remove_str_to_int(v)

    # 点击数
    _clicks = 0

    @property
    def clicks(self):
        return self._clicks

    @clicks.setter
    def clicks(self, v):
        if float(v) == 0:
            self._clicks = 0
            return
        self._clicks = self.__remove_str_to_int(v)

    # 充值比例
    paid_rate = 0

    # 利润
    # _profit_amount = 0
    #
    # @property
    # def profit_amount(self):
    #     return self._profit_amount
    #
    # @profit_amount.setter
    # def profit_amount(self, v):
    #     self._profit_amount = float(self.__remove_comma(v))

    # 采集时间戳
    timestamp = 0
    # 单粉回报
    SingleReturn = 0
    # 小时
    hour = 0
    # 分钟
    minute = 0
    # 源站成本
    _cost = 0

    @property
    def cost(self):
        return self._cost

    @cost.setter
    def cost(self, v):
        try:
            if float(v) == 0:
                self._cost = 0
                return
            self._cost = self.__remove_str_to_int(v)
        except:
            self._cost = 0

    def save_bookv2(self):
        save_data = {
            # 'linkdesc_id': self.linkdesc_id,
            # 'pid': self.pid,
            # bookinfov2中对应的id
            'book_info_id': self.book_info_id,
            # 新增阅读用户
            'member_count': self.member_count,
            # 新增关注
            'subscribe_count': self.subscribe_count,
            # 充值金额
            'paid_amount': self.paid_amount,
            # 关注率
            'subscribe_rate': self.subscribe_rate,
            # 充值比数
            'paid_count': self.paid_count,
            # 点击数
            'clicks': self.clicks,
            # 充值比例
            'paid_rate': self.paid_rate,
            # 利润
            'profit_amount': round(self.paid_amount - self.cost_amount, 2),
            # 回本率
            'profit_rate': self.profit_rate,
            # 时间戳
            'timestamp': self.timestamp,
            # 单粉回报
            # 'SingleReturn': self.single_return(),
            'hour': self.hour,
            'minute': self.minute,
            # 源站成本
            # 'cost': self.cost,
        }
        table_name = Table.get_table_name(self.timestamp)
        self._db.table(table_name).insert(save_data)
        self.save_in_redis(save_data)

    def save_in_redis(self, save_data: dict):
        """
        在redis中保存数据
        :param save_data:
        :return:
        """
        update_dict = {
            'linkdesc': self.linkdesc,
            'bookname': self.book_name,
            'pname': self.pname,
            'pid': self.pid,
            'linkdesc_id': self.linkdesc_id,
            'cost': self.cost_amount,
        }
        save_data.update(update_dict)
        # 保存链接id到小说id下
        # redis_book_id = redis_key.book_id_has_linkid % self.book_id
        # self.redis.rpush(redis_book_id, self.linkdesc_id)
        # 保存链接信息
        link_value = json.dumps(save_data, ensure_ascii=False)
        book_info_key = redis_key.book_info_key % (self.book_id, self.tui)
        self.redis.hset(book_info_key, self.linkdesc_id, link_value)
        # 保存到凌晨信息
        if (self.hour == 0) and (self.minute == 0):
            mid_night = redis_key.link_info_every_mid_night % self.linkdesc_id
            self.redis.lpush(mid_night, link_value)
            # 检查午夜的长度
            mid_night_num = self.redis.llen(mid_night)
            if mid_night_num > redis_key.link_mid_night_len:
                self.redis.ltrim(mid_night_num, 0, redis_key.link_mid_night_len)

    def single_return(self):
        """
        单粉回报
        :return:
        """
        if self.subscribe_count == 0:
            return 0
        return round(self.paid_amount / self.subscribe_count, 2)

    @property
    def profit_rate(self):
        """
        回本率
        :return:
        """
        if self.cost_amount == 0:
            return 0
        return round(self.paid_amount / self.cost_amount * 100, 2)

    """
    以下是清理方法
    """
    zz = re.compile('(\.){1}')
    before_dp = re.compile('(.+)\.')

    # 去除逗号，返回字符串
    @staticmethod
    def __remove_str_to_float(s) -> float:
        str_num = str(s).replace(",", "")
        return float(str_num)

    def __remove_str_to_int(self, s) -> int:
        str_num = str(s).replace(",", "")
        if self.zz.search(str_num):
            return int(self.before_dp.search(str_num).group(1))
        return int(str_num)

    def reset(self):
        """
        每次采集完一本书，需要修正采集数据
        :return:
        """
        self.cost_amount = 0

    """
    redis相关
    """
    # redis
    redis = redis.Redis(**redis_config)
