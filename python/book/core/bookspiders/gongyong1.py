# encoding=utf8


import copy
import math
import os
import sys
import time
from urllib.parse import urlencode

import requests

from core.bookspiders.BookInfoBase import BookInfoBase
from core.mylibs.public import timeprint

"""阳光书城和七悦书城共用"""


class BookInfo(BookInfoBase):
    user_agent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400X-Requested-With: XMLHttpRequest"
    # session对象
    session = requests.session()
    # 登录头信息
    login_headers = {
        1: {
            'X-Requested-With': 'XMLHttpRequest',
        },
        2: {
            'User-Agent': user_agent,
        }
    }

    # 请求数据头部
    data_headers = {
        1: {
            'Host': 'admin.yifengaf.cn',
            'Connection': 'keep-alive',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
        },
        2: {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Content-Type': 'application/json',
            'Referer': 'https://www.zhangwenwenhua.com/admin/referral/referral?addtabs=1',
            'User-Agent': user_agent,
            'X-Requested-With': 'XMLHttpRequest',
        },
    }
    # 请求数据参数
    dataparam = {
        'push': '0',
        'sort': 'state desc,id desc',
        'order': '',
        'offset': '0',
        'limit': '10',
        'filter': {},
        'op': {},
        '_': '1564365326257',
    }

    # 0外推 1内推(数据库中存储的类型)
    tuitypes = [0, 1]
    # 内推外推名称
    tuinames = [
        "内推",
        "外推",
    ]
    # 采集的书城的名称
    names = {
        1: '阳光书城',
        2: '七悦书城',
    }
    # 登录地址
    loginurls = {
        1: "https://admin.yifengaf.cn/admin/index/login",  # 阳光登录地址
        2: "https://www.zhangwenwenhua.com/admin/index/login?url=/admin",  # 七悦登录地址
    }
    # 获取数据地址
    dataurls = {
        1: "https://admin.yifengaf.cn/admin/referral.referral/index",  # 阳光
        2: "https://www.zhangwenwenhua.com/admin/referral.referral/index",  # 七悦
    }

    def __init__(self, account):
        self.account = account
        self.init_info(account)
        timeprint('\033[31m开始使用%s账号 %s\033[0m' % (self.names[self.book_data.pid], account['username']))

    def start(self):
        """
        区分内外推
        :return:
        """
        self.login()
        # exit()
        for tuitype in self.tuitypes:
            # 切换参数，因为请求时参数内外推值和数据库中的的值表达方式不一样
            self.book_data.tui = 1 if tuitype == 0 else 0
            # 如果是内推，只是每日凌晨采集
            if (not self.is_mid_night) and (self.book_data.tui == 0):
                continue
            timeprint("%s账号%s" % (self.names[self.book_data.pid], self.account['username']))
            timeprint("%s" % self.tuinames[self.book_data.tui])
            self.dataparam['push'] = str(tuitype)
            self.startpage()

    # 登录
    def login(self):
        self.session = requests.session()
        data = {
            'username': self.account['username'],
            'password': self.account['password'],
            'keeplogin': '1',
        }
        # 登录地址
        url = self.loginurls[self.book_data.pid]
        req = self.session.post(url, data, headers=self.login_headers[self.book_data.pid])
        a = dict(self.session.cookies.items())
        if a['keeplogin']:
            timeprint("\033[0;34;0m成功获取用户%s的user_token:\033[0m" % self.account['username'])
        else:
            raise Exception("获取%s用户的user_token失败" % self.account['username'])

    def startpage(self):
        """
        开始按页采集
        :return:
        """
        lastpage = 1
        pageindex = 0
        while (pageindex < lastpage) and self.continue_get_info:
            total = self.getonepage(pageindex)
            if total:
                if pageindex == 0:
                    lastpage = total
                    timeprint("%s账号%s共%s页" % (self.names[self.book_data.pid], self.account['username'], total))
            pageindex += 1
            time.sleep(3)

    def getonepage(self, pageindex):
        """
        采集一页信息
        :param pageindex:页码
        :return:总页数
        """
        timeprint("正在获取%s账号%s获取第%s页" % (self.names[self.book_data.pid], self.account['username'], pageindex + 1))
        # 准备请求参数
        dataurlbase = self.dataurls[self.book_data.pid]
        param = copy.copy(self.dataparam)
        param['offset'] = pageindex * int(param['limit'])
        param['_'] = str(int(time.time() * 1000))
        dataurl = '%s?%s' % (dataurlbase, urlencode(param))
        datareq = self.session.get(dataurl, headers=self.data_headers[self.book_data.pid])
        try:
            datas = datareq.json()
            if 'rows' not in datas:
                raise Exception("获取第%s失败" % pageindex)
        except Exception as i:
            timeprint("%s使用账号%s获取第%s页失败:%s" % (self.names[self.book_data.pid], self.account['username'], pageindex, i),
                      os.path.basename(__file__), sys._getframe().f_lineno)
            return False
        for data in datas['rows']:
            if 'book' in data.keys():
                self.book_data.reset()
                if self.continue_get_info:
                    timeprint("开始获取阳光书城链接id%s" % data['id'])
                    try:
                        # 判断内推的时间，超过配置中的时间则中断循环
                        if self.book_data.tui == 0:
                            seq_time = self.book_data.timestamp - data['createtime']
                            if seq_time > self.catch_limit_time:
                                self.continue_get_info = False
                                break
                        self.savebook(data)
                        self.savedata(data)
                    except Exception as e:
                        # print(data)
                        # print(e)
                        # exit()
                        continue
                    timeprint("链接id%s结束" % data['id'])
            else:
                return math.ceil(datas['total'] / int(param['limit']))
        return math.ceil(datas['total'] / int(param['limit']))

    # 保存书信息
    def savebook(self, book):
        # 判断小说链接创建时间是否超过180天
        if not self.check_continue(int(book['createtime'])):
            raise Exception("超过最大时间限制")
        # 链接id
        self.book_data.linkdesc_id = book['id']
        # 小说id
        self.book_data.book_id = book['book']['id']
        # 链接名
        self.book_data.linkdesc = book['name']
        # 小说名
        self.book_data.book_name = book['book']['name']
        self.book_data.save_bookinfov2()

    # 保存具体书信息
    def savedata(self, info_dict):
        # 新增阅读用户
        self.book_data.member_count = info_dict['uv']
        # 新增关注
        self.book_data.subscribe_count = info_dict['follow']
        # 充值金额
        self.book_data.paid_amount = info_dict['money']
        # 关注率
        self.book_data.subscribe_rate = 0
        # 充值比数
        self.book_data.paid_count = info_dict['orders_num']
        # 点击数
        self.book_data.clicks = info_dict['uv']
        # 充值比例
        try:
            self.book_data.paid_rate = round(self.book_data.paid_count / self.book_data.subscribe_count * 100, 2)
        except:
            self.book_data.paid_rate = 0
        # 源站成本
        self.book_data.cost = info_dict['cost']
        self.book_data.save_bookv2()
