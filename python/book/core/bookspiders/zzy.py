import os
import sys
import time

import requests
from bs4 import BeautifulSoup as soup
from pyquery import PyQuery as pq

from core.bookspiders.BookInfoBase import BookInfoBase
from core.mylibs.public import timeprint


class BookInfo(BookInfoBase):
    """
    掌中云采集
    """

    def __init__(self, account: dict):
        self.account = account
        # 初始化时间、公众号检查，采集条件变量设置
        self.init_info(account)

    def start(self):
        self.login()
        self.get_pages()

    # 登录地址
    login_url = "https://inovel.818tu.com/login/dologin"
    # 页面地址
    page_url = "https://inovel.818tu.com/backend/referral_links/index?page=%s"
    # 链接信息接口地址
    api_url = "https://inovel.818tu.com/backend/referral_links/api_get_counter/%s"

    # session对象
    session = requests.session()
    # 请求头信息
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Host': 'inovel.818tu.com',
        # 'Cookie': '%s' % ','.join(Cookie),
        'Cookie': 'user_token=%s',
        'Referer': 'https://inovel.818tu.com/backend/notices',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
        # 'Host':'inovel',
    }

    # 登录
    def login(self):
        """
        账号登录
        :return:
        """
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        postdata = {
            'name': self.account['username'],
            'password': self.account['password'],
            'csessionid': '',
            'sig': '',
            'token': '',
            'scene': '',
        }
        req = self.session.post(self.login_url, data=postdata, headers=headers)
        a = dict(self.session.cookies.items())
        if a['user_token']:
            timeprint("\033[0;34;0m成功获取用户%s的user_token:\033[0m" % self.account['username'])
        else:
            raise Exception("获取%s用户的user_token失败" % self.account['username'])

    # 当前采集的页数
    current_page = 1
    # 最后一页(第一页时判断获取）
    last_page = 1

    # 获取页面内容
    def get_pages(self):
        while self.current_page <= self.last_page:
            timeprint('正在获取%s的第%s页' % (self.account['username'], self.current_page))
            tuiguangurl = self.page_url % self.current_page
            try:
                tuiguangweb = self.session.get(url=tuiguangurl, timeout=60)
            except:
                timeprint('开始请求掌中云页码页%s失败' % tuiguangurl, os.path.basename(__file__), sys._getframe().f_lineno)
                return
            Bsoup = soup(tuiguangweb.text, 'html.parser')
            # 如果是第一页获取总页数
            pageNumEm = Bsoup.select('.pager .pager-summary em')
            if len(pageNumEm) == 2:
                try:
                    self.last_page = int(pageNumEm[1].text)
                except:
                    pass
            self.current_page += 1
            # 解析书本内容
            doc = pq(tuiguangweb.text)
            bookIds = doc('.referral-link-item')
            for bookinfo in bookIds.items():
                if self.continue_get_info:
                    self.book_data.reset()
                    self.book_data.linkdesc_id = bookinfo.attr('data-link-id')
                    timeprint("开始分析掌中云链接id%s" % self.book_data.linkdesc_id)
                    html = bookinfo.html()
                    # self.get_book_data(html)
                    try:
                        self.get_book_data(html)
                    except:
                        continue
                else:
                    return

    def get_book_data(self, bk_html):
        doc = pq(bk_html)
        # 获取小说创建时间,判断是否继续采集
        # create_time = doc('.text-muted').eq(1).text().strip("创建时间:").replace('推广时间:', '').strip()
        create_time_div = doc('.text-muted').eq(1)
        create_time_div.remove('div')
        create_time = create_time_div.text().strip("创建时间:").strip()
        create_local_time = time.strptime(create_time, '%Y-%m-%d %H:%M:%S')
        create_time_stamp = time.mktime(create_local_time)
        if not self.check_continue(create_time_stamp):
            print(create_time)
            # exit()
            return

        # 获取小说id
        self.book_data.book_id = doc('td div a').attr('href').split("/")[-1]
        # 判断内推还是外推
        nw = doc('.text-muted').eq(0).text()
        # self.book_info['tui'] = 0 if nw == "(内推)" else 1
        self.book_data.tui = 0 if nw == "(内推)" else 1
        # 如果是内推
        if self.book_data.tui == 0:
            seq_time = self.book_data.timestamp - create_time_stamp
            if seq_time > self.catch_limit_time:
                return
            elif self.is_mid_night:
                pass
            else:
                return
        # 小说链接名
        self.book_data.linkdesc = doc('.link-desc').text().strip()
        info_url = self.api_url % self.book_data.linkdesc_id
        try:
            timeprint('开始请求掌中云书本详情页%s' % info_url)
            req = self.session.get(info_url, timeout=60)
            info_dict = req.json()
        except:
            datastr = "\033[0;31;0m请求掌中云书本详情页%s失败\033[0m"
            timeprint(datastr % info_url, os.path.basename(__file__), sys._getframe().f_lineno)
            return
        # 获取小说名
        self.book_data.book_name = doc('td div a').eq(0).text().strip()
        timeprint('掌中云《%s》(%s)%s开始' % (self.book_data.book_name, self.book_data.linkdesc_id, nw))
        # 检查书是否已经检查过并且检查数据库中是否存在
        self.book_data.save_bookinfov2()
        # 保存数据
        self.save_book_data(info_dict)
        timeprint('掌中云《%s》结束' % self.book_data.book_name)

    def save_book_data(self, data):
        """
        保存本次采集的信息
        :param data:
        :return:
        """
        # 新增阅读用户
        self.book_data.member_count = data['member_count']
        # 新增关注
        self.book_data.subscribe_count = data['subscribe_count']
        # 充值金额
        self.book_data.paid_amount = data['paid_amount']
        # 关注率
        self.book_data.subscribe_rate = 0
        # 充值比数
        self.book_data.paid_count = data['paid_count']
        # 充值比例
        self.book_data.paid_rate = float(data['paid_rate']) * 100
        # 点击数
        self.book_data.clicks = data['clicks']
        # 源站成本
        self.book_data.cost = data['cost_amount']
        # print(self.book_data.clicks)
        # print(data['paid_amount'])
        # exit()
        self.book_data.save_bookv2()
