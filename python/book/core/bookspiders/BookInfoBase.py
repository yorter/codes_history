import time

from config import timelimit
from core.data.bookdata import BookData


class BookInfoBase:
    """
    采集基类
    """
    # 账号信息
    account = {}
    # 公众号id

    # 采集创建的时间限制
    catch_limit_time = timelimit.all_time_limit_day * 24 * 3600
    # 最晚采集的时间
    limit_time = 0

    # 是否是每月一号凌晨(此时可以采集所有的小说)
    is_catch_all = False

    # 是否是午夜
    is_mid_night = False

    # 采集的小说的信息
    _book_data = None

    @property
    def book_data(self) -> BookData:
        return self._book_data

    def init_info(self, user):
        """
        初始化(在子类中需要在__init__中执行)
        :param user:采集账户
        :return: None
        """
        self._book_data = BookData(user)
        # 初始化时间相关
        # self.book_data.timestamp = int(self.redis.get(redis_key_config['nowtime']))
        local_time = time.localtime(self.book_data.timestamp)
        # 小时
        self.book_data.hour = local_time.tm_hour
        # 分钟
        self.book_data.minute = local_time.tm_min
        # 判断是否是月首(采集所有的条件)
        if (local_time.tm_mday == 1) and (local_time.tm_hour == 0) and (local_time.tm_min == 0):
            self.is_catch_all = True
        # 判断是不是午夜(内推采集的条件之一)
        if (local_time.tm_hour == 0) and (local_time.tm_min == 0):
            self.is_mid_night = True

    def start(self):
        """
        入口函数
        :return:
        """
        pass

    # 是否继续向下获取书本信息
    continue_get_info = True

    # 检查是否达到上限
    def check_continue(self, t: float):
        # 是否是每月一号
        if self.is_catch_all:
            return True
        else:
            if self.limit_time > t:
                self.continue_get_info = False
                return False
            else:
                return True
