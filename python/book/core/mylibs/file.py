# 读文件
def read(path):
    with open(path, mode='r', encoding='utf8') as f:
        data = f.read()
    return data


# 写入文件
def write(path, data):
    with open(path, mode='w', encoding='utf8') as f:
        f.write(data)


# 追加文件
def append(path, data):
    with open(path, mode='a', encoding='utf8') as f:
        f.write(data)
