import requests
from config.account import vip_accounts as accounts
import re
import time
from core.mylibs.mysql import Mysql
from urllib.parse import urlencode
import math

"""
阳光书城vip账号抓取
"""


class Zzy:
    login_url = 'https://vip.818tu.com/login/dologin'
    # 首页地址
    vipindex_url = 'https://vip.818tu.com/backend/stats/orders'
    #
    api_part_url = 'https://vip.818tu.com/backend/order_stats/api_get_multi_users_stats_today?uids=%s'
    # 昨日信息接口地址
    last_day_api_url = 'https://vip.818tu.com/backend/order_stats/api_get_multi_users_daily_stats'
    # 昨日信息接口地址请求参数
    last_day_api_url_param = {
        'uids': '',
        'limit': 30,
        'last_date': '',
    }
    session = ''
    # 登录信息
    login_data = {
        'name': '',
        'password': '',
    }
    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 匹配uid值
    uidzz = re.compile('var uid = \[(.*)\];')
    # 保存信息
    save_data = {
        'platform': '1',
        'data': '',
        'value': 0,
        'recharge_count': 0,
        'newuser': '',
        # 'olduser': '',
        'normal_charge': 0,
        'normal_charge_count': 0,
        'vip_charge': 0,
        'vip_charge_count': 0,
        'average': 0,
        'save_as_yesterday': 0,
    }
    # 区分平台
    platform = 1
    # 更新的字段
    updata_key = [
        'value',
        'recharge_count',
        'normal_charge',
        'normal_charge_count',
        'vip_charge',
        'vip_charge_count',
        'average'
    ]
    # 数据库对象
    db = ''
    # uid
    uid = ''
    # 是否有下一页
    have_next_page = True

    def __init__(self):
        self.session = requests.session()
        self.login_data['name'] = accounts['zzy']['username']
        self.login_data['password'] = accounts['zzy']['password']
        # now = int(time.time())
        # self.save_data['data'] = time.strftime('%Y-%m-%d', time.localtime(now))
        # self.last_save_data['data'] = time.strftime('%Y-%m-%d', time.localtime(now - 24 * 3600))
        self.db = Mysql()
        # self.save_data['platform'] = self.last_save_data['platform'] = self.platform
        self.save_data['platform'] = self.platform

    def start(self):
        self.session.post(self.login_url, self.login_data)
        req = self.session.get(self.vipindex_url)
        # 获取uid
        self.uid = self.uidzz.search(req.text).group(1).replace('"', '')
        while self.have_next_page:
            self.getlastday()
        # exit()
        self.savevalue()

    def getvalue(self, text):
        """
        在字符串中匹配金额并返回整数
        :param text:匹配的字符串
        :return:金额整数（单位分
        """
        valuestr = self.intzz.search(text).group().replace('.', '')
        return int(valuestr)

    def savevalue(self):
        """
        保存信息
        :return:
        """
        # 保存今日信息
        where = {
            'platform': self.save_data['platform'],
            'data': self.save_data['data'],
        }
        find = self.db.table('recharge').where(where).find()
        if find:
            data = {}
            for k in self.updata_key:
                data[k] = self.save_data[k]
            self.db.table('recharge').where(where).update(data)
        else:
            self.db.table('recharge').insert(self.save_data)
        # 保存昨日信息
        # if self.last_save_data:
        #     where_yes = {
        #         'platform': self.last_save_data['platform'],
        #         'data': self.last_save_data['data'],
        #     }
        #     find_yesterday = self.db.table('recharge').where(where_yes).find()
        #     if find_yesterday:
        #         if int(find_yesterday['save_as_yesterday']) == 1:
        #             data = {
        #                 'save_as_yesterday': 0,
        #             }
        #             for k in self.updata_key:
        #                 data[k] = self.last_save_data[k]
        #             self.db.table('recharge').where(where_yes).update(data)
        #     else:
        #         self.db.table('recharge').insert(self.last_save_data)

    def getlastday(self):
        """
        获取昨日信息
        :return:
        """
        self.last_day_api_url_param['uids'] = self.uid
        param = urlencode(self.last_day_api_url_param)
        url = self.last_day_api_url + "?" + param
        req = self.session.get(url)
        try:
            datas = req.json()
        except:
            self.have_next_page = False
            return
        if len(datas) < self.last_day_api_url_param['limit']:
            self.have_next_page = False
        last_date = 0
        for data in datas:
            now_get_date = int(data['date'])
            last_date = now_get_date if now_get_date > last_date else last_date
            # if now_get_date > last_date:
            #     last_date = now_get_date
            self.save_data['data'] = time.strftime('%Y-%m-%d', time.localtime(last_date))
            self.save_data['value'] = int(data['paid_amount'])
            self.save_data['newuser'] = data['new_member_paid_amount']
            self.save_data['olduser'] = int(data['paid_amount']) - int(data['new_member_paid_amount'])
            self.save_data['normal_charge'] = data['welth_order_paid_amount']
            self.save_data['normal_charge_count'] = data['welth_order_paid_count']
            self.save_data['vip_charge'] = data['vip_order_paid_amount']
            self.save_data['vip_charge_count'] = data['vip_order_paid_count']
            self.save_data['recharge_count'] = int(data['welth_order_paid_count']) + int(data['vip_order_paid_count'])
            try:
                self.save_data['average'] = math.floor(
                    int(self.save_data['value']) / self.save_data['recharge_count'])
            except:
                self.save_data['average'] = 0
            print(self.save_data)
            # exit()
            self.savevalue()
        self.last_day_api_url_param['last_date'] = last_date


if __name__ == '__main__':
    a = Zzy()
    a.start()
