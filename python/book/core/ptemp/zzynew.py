from core.data.platdata import PlatData
from core.mylibs.mysql import Mysql


class Zzy:
    def __init__(self):
        self.data = PlatData()
        self.data.platform = self.platform
        self.db = Mysql("book")

    data = ''
    db = ''
    platform = 3

    def start(self):
        with open(__file__+"/../../../temp/818.csv") as f:
            for line in f:
                arr = line.split(',')
                for i in range(len(arr)):
                    arr[i] = arr[i].strip()
                self.save_data(arr)

    def save_data(self, data: list):
        self.data.date = data[0]
        self.data.value = int(float(data[1]) * 100)
        self.data.normal_charge = int(float(data[2]) * 100)
        self.data.normal_charge_count = int(data[3])
        self.data.vip_charge = int(float(data[4]) * 100)
        self.data.vip_charge_count = int(data[5])
        self.data.recharge_count = self.data.normal_charge_count + self.data.vip_charge_count
        self.save_info()

    def save_info(self):
        where = {
            'platform': self.platform,
            'data': self.data.date,
        }
        find = self.db.table('recharge').where(where).find()
        if find:
            self.db.table('recharge').where(where).update(self.data.get_update_data())
        else:
            self.db.table('recharge').insert(self.data.get_insert_data())


if __name__ == '__main__':
    z = Zzy()
    z.start()
