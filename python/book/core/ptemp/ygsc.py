import requests
from config.account import vip_accounts as accounts
import re
import math
import time
from core.mylibs.mysql import Mysql
from urllib.parse import urlencode
from core.data.platdata import PlatData

"""
阳光书城vip账号抓取
"""


class Ygsc:
    login_url = 'https://vip.yifengaf.cn/admin/index/login'
    # 首页地址
    vipindex_url = 'https://vip.yifengaf.cn/admin/vipindex'
    session = ''
    # 登录信息
    login_data = {
        'username': '',
        'password': '',
        'keeplogin': '1',
    }
    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 区分平台
    platform = 1
    data = PlatData()

    # 数据库对象
    db = Mysql()
    db_spider = Mysql('book_spider')
    # 请求接口地址
    dates_api_url = "https://vip.yifengaf.cn/admin/vip/admin/ordercollect/index?%s"
    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Host': 'vip.yifengaf.cn',
        'Pragma': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3722.400 QQBrowser/10.5.3751.400',
        'X-Requested-With': 'XMLHttpRequest',
        # 'aaa':'aaa',
        'Content-Type': 'application/json',
    }
    last_date_param = {
        'tab': 'day_sum',
        'sort': 'createday',
        'order': 'desc',
        'offset': '0',
        'limit': 10,
        'filter': '{"createdate":"%s - %s"}',
        'op': '{"createdate":"="}',
        '_': 0
    }
    filter = '{"createdate":"%s - %s"}'
    # 是否有下一页
    have_next_page = True
    # 开始的时间戳
    start_time = int(time.time()) - (24 * 3600)

    def __init__(self):
        self.session = requests.session()
        self.data.platform = self.platform

    def start(self):
        self.get_account()
        self.session.post(self.login_url, self.login_data)
        while self.have_next_page:
            self.getlastday()

    def get_account(self):
        """
        获取VIP账户信息
        :return:
        """
        where = {
            'isvip': '1',
            'pid': self.platform,
        }
        user = self.db_spider.table('account').where(where).find()
        # 获取VIP账号
        self.login_data['username'] = user['username']
        self.login_data['password'] = user['password']
        self.db_spider.close()

    def getvalue(self, text):
        """
        在字符串中匹配金额并返回整数
        :param text:匹配的字符串
        :return:金额整数（单位分
        """
        valuestr = self.intzz.search(text).group().replace('.', '')
        return int(valuestr)

    def savevalue(self):
        """
        保存信息
        :return:
        """
        # 保存今日信息
        where = {
            'platform': self.data.platform,
            'data': self.data.date,
        }
        find = self.db.table('recharge').where(where).find()
        if find:
            self.db.table('recharge').where(where).update(self.data.get_update_data())
        else:
            self.db.table('recharge').insert(self.data.get_insert_data())

    def getlastday(self):
        """
        获取往日信息
        :return:
        """
        self.last_date_param['_'] = int(time.time() * 1000)
        start_date = time.strftime('%Y-%m-%d', time.localtime(self.start_time))
        end_timestamp = self.start_time - self.last_date_param['limit'] * 24 * 3600
        end_time = time.strftime('%Y-%m-%d', time.localtime(end_timestamp))
        self.last_date_param['filter'] = self.filter % (end_time, start_date)
        url = self.dates_api_url % urlencode(self.last_date_param)
        req = self.session.get(url, headers=self.headers)
        try:
            datas = req.json()['rows']
        except:
            self.have_next_page = False
            return
        if len(datas) < self.last_date_param['limit']:
            self.have_next_page = False
        for data in datas:
            cd = data['createdate']
            self.data.date = '%s-%s-%s' % (cd[0:4], cd[4:6], cd[6:])
            self.data.value = self.replacepoint(data['recharge_money'])
            self.data.normal_charge = self.replacepoint(data['normal_recharge_money'])
            self.data.normal_charge_count = int(data['normal_recharge_orders'])
            self.data.vip_charge = self.replacepoint(data['vip_recharge_money'])
            self.data.vip_charge_count = int(data['vip_recharge_orders'])
            self.data.recharge_count = self.data.normal_charge_count + self.data.vip_charge_count
            self.savevalue()
            self.start_time = end_timestamp
            # exit()

    def replacepoint(self, withpoint):
        """
        去掉小数点返回整数（即单位为分
        :param withpoint:
        :return:
        """
        return int(withpoint.replace('.', ''))


if __name__ == '__main__':
    t = Ygsc()
    t.start()
