import requests
from config.account import vip_accounts as accounts
import re
import math
import time
from core.mylibs.mysql import Mysql
from urllib import parse

"""
阳光书城vip账号抓取
"""


class Qysc:
    login_url = 'https://www.zhangwenwenhua.cn/admin/index/login'
    # 首页地址
    vipindex_url = 'https://www.zhangwenwenhua.cn/admin/collect/index?addtabs=1'
    #
    api_url = 'https://www.zhangwenwenhua.cn/admin/collect/ajaxtoday?channel_id=0'
    yes_api_url = 'https://www.zhangwenwenhua.cn/admin/collect/index?%s'
    yes_api_param = {
        'channel_id': 0,
        'sort': 'createdate',
        'order': 'desc',
        'offset': 0,
        'limit': 50,
        '_': 0,
    }
    # 采集的页数
    page = 0
    session = ''
    # 登录信息
    login_data = {
        'username': '',
        'password': '',
        'keeplogin': 1
    }
    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 匹配groupId值
    groupIdzz = re.compile('groupId":(\d+),')
    # 匹配authId值
    authIdzz = re.compile('authId":(\d+)\}')
    # 保存信息
    save_data = {
        'platform': '1',
        'data': '',
        'value': 0,
        'recharge_count': 0,
        # 'newuser': '',
        # 'olduser': '',
        'normal_charge': 0,
        'normal_charge_count': 0,
        'vip_charge': 0,
        'vip_charge_count': 0,
        'average': 0,
        'save_as_yesterday': 0,
    }
    # 区分平台
    platform = 3
    # 更新的字段
    updata_key = [
        'value',
        'recharge_count',
        'normal_charge',
        'normal_charge_count',
        'vip_charge',
        'vip_charge_count',
        'average'
    ]
    # 数据库对象
    db = ''
    # 请求头
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3722.400 QQBrowser/10.5.3751.400',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Pragma': 'no-cache',
        'Host': 'www.zhangwenwenhua.cn',
        'X': 'XMLHttpRequest',
    }
    have_next_page = True
    # 是否进行加法运算
    is_plus = False

    def __init__(self):
        self.session = requests.session()
        # now = int(time.time())
        # self.save_data['data'] = time.strftime('%Y-%m-%d', time.localtime(now))
        # self.last_save_data['data'] = time.strftime('%Y-%m-%d', time.localtime(now - 24 * 3600))
        self.db = Mysql()
        # self.save_data['platform'] = self.platform
        self.save_data['platform'] = self.platform

    def start(self):
        for i in range(len(accounts['qysc'])):
            account = accounts['qysc'][i]
            print(account)
            self.is_plus = True if i > 0 else False
            self.have_next_page = True
            print(self.is_plus)
            # for account in accounts['qysc']:
            self.session = requests.session()
            self.login_data['username'] = account['username']
            self.login_data['password'] = account['password']
            self.getuserdata()
        # self.save_data['recharge_count'] = self.save_data['normal_charge_count'] + self.save_data['vip_charge_count']
        # try:
        #     self.save_data['average'] = round((self.save_data['value'] / 100) / self.save_data['recharge_count'], 2)
        # except:
        #     self.save_data['average'] = 0
        #
        # # 计算昨日
        # self.last_save_data['recharge_count'] = self.last_save_data['normal_charge_count'] + self.last_save_data[
        #     'vip_charge_count']
        # try:
        #     self.last_save_data['average'] = round(
        #         (self.last_save_data['value'] / 100) / self.last_save_data['recharge_count'], 2)
        # except:
        #     self.save_data['average'] = 0
        # self.savevalue()

    def getuserdata(self):
        self.session.post(self.login_url, self.login_data)
        while self.have_next_page:
            self.getlastday()
        # print(self.last_save_data)
        # exit()

    def floattoint(self, text):
        return int(str(text).replace('.', ''))

    def getvalue(self, text):
        """
        在字符串中匹配金额并返回整数
        :param text:匹配的字符串
        :return:金额整数（单位分
        """
        valuestr = self.intzz.search(text).group().replace('.', '')
        return int(valuestr)

    def savevalue(self):
        """
        保存信息
        :return:
        """
        where = {
            'platform': self.save_data['platform'],
            'data': self.save_data['data'],
        }
        find = self.db.table('recharge').where(where).find()
        if find:
            data = {}
            if self.is_plus:
                save_data = {
                    'value': int(find['value']) + self.save_data['value'],
                    'recharge_count': int(find['recharge_count']) + self.save_data['recharge_count'],
                    'normal_charge': int(find['normal_charge']) + self.save_data['normal_charge'],
                    'normal_charge_count': int(find['normal_charge_count']) + self.save_data['normal_charge_count'],
                    'vip_charge': int(find['vip_charge']) + self.save_data['vip_charge'],
                    'vip_charge_count': int(find['vip_charge_count']) + self.save_data['vip_charge_count'],
                }
                try:
                    save_data['average'] = math.floor(save_data['value'] / save_data['recharge_count'])
                except:
                    save_data['average'] = 0
            else:
                save_data = self.save_data
            for k in self.updata_key:
                data[k] = save_data[k]
            self.db.table('recharge').where(where).update(data)
        else:
            self.db.table('recharge').insert(self.save_data)
        # # 保存昨日信息
        # where_yes = {
        #     'platform': self.last_save_data['platform'],
        #     'data': self.last_save_data['data'],
        # }
        # find_yesterday = self.db.table('recharge').where(where_yes).find()
        # if find_yesterday:
        #     if int(find_yesterday['save_as_yesterday']) == 1:
        #         data = {
        #             'save_as_yesterday': 0,
        #         }
        #         for k in self.updata_key:
        #             data[k] = self.last_save_data[k]
        #         self.db.table('recharge').where(where_yes).update(data)
        # else:
        #     self.last_save_data['save_as_yesterday'] = 0
        #     self.db.table('recharge').insert(self.last_save_data)

    def getlastday(self):
        # 获取昨日信息
        self.yes_api_param['_'] = int(time.time())
        url = self.yes_api_url % parse.urlencode(self.yes_api_param)
        req = self.session.get(url, headers=self.headers)
        try:
            datas = req.json()['rows']
        except:
            self.have_next_page = False
            return
        if len(datas) < self.yes_api_param['limit']:
            self.have_next_page = False
        for reldata in datas:
            cd = reldata['createdate']
            self.save_data['data'] = '%s-%s-%s' % (cd[0:4], cd[4:6], cd[6:])
            self.save_data['value'] = self.floattoint(reldata['recharge_money'])
            self.save_data['normal_charge'] = self.floattoint(reldata['normal_recharge_money'])
            self.save_data['normal_charge_count'] = int(reldata['normal_recharge_orders'])
            self.save_data['vip_charge'] = self.floattoint(reldata['vip_recharge_money'])
            self.save_data['vip_charge_count'] = int(reldata['vip_recharge_orders'])
            self.save_data['recharge_count'] = self.save_data['normal_charge_count'] + self.save_data[
                'vip_charge_count']
            try:
                self.save_data['average'] = math.floor(self.save_data['value'] / self.save_data['recharge_count'])
            except:
                self.save_data['average'] = 0
            self.savevalue()
            print(self.save_data)
            # exit()
        self.yes_api_param['offset'] += self.yes_api_param['limit']


if __name__ == '__main__':
    t = Qysc()
    t.start()
