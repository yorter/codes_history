import re
import time
from core.mylibs.mysql import Mysql
from core.data.platdata import PlatData
from config import plat


class PlatInfoBase:
    # 平台id
    platform = 0
    # 账号列表
    accounts = []

    # 数据库
    db = Mysql("book_spider")
    book_db = Mysql("book")

    # 数据对象
    data = PlatData()
    # 昨日数据
    yesterday_data = PlatData()

    # localtime
    localtime = time.localtime()

    def init_info(self):
        self.data = PlatData()
        self.yesterday_data = PlatData()
        self.data.platform = self.yesterday_data.platform = self.platform
        # 初始化时间
        now = int(time.time())
        self.data.date = time.strftime('%Y-%m-%d', time.localtime(now))
        self.yesterday_data.date = time.strftime('%Y-%m-%d', time.localtime(now - 24 * 3600))
        # 获取所有vip账号
        where = {
            'isvip': 1,
            'pid': self.platform,
        }
        self.accounts = self.db.table('account').where(where).select()

    def save_info(self):
        where = {
            'platform': self.platform,
            'data': self.data.date,
        }
        find = self.book_db.table('recharge').where(where).find()
        if find:
            self.book_db.table('recharge').where(where).update(self.data.get_update_data())
        else:
            self.book_db.table('recharge').insert(self.data.get_insert_data())
        # 保存昨日信息
        if self.localtime.tm_hour == plat.get_yesterday_hour:
            where_yes = {
                'platform': self.platform,
                'data': self.yesterday_data.date,
            }
            find_yesterday = self.book_db.table('recharge').where(where_yes).find()
            if find_yesterday:
                self.book_db.table('recharge').where(where_yes).update(self.yesterday_data.get_update_data())
            else:
                self.book_db.table('recharge').insert(self.yesterday_data.get_insert_data())
