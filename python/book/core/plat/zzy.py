import re
from urllib.parse import urlencode

import requests

from config import plat
from core.plat.platbase import PlatInfoBase

"""
阳光书城vip账号抓取
"""


class Zzy(PlatInfoBase):
    login_url = 'https://vip.818tu.com/login/dologin'
    # 首页地址
    vipindex_url = 'https://vip.818tu.com/backend/stats/orders'
    #
    api_part_url = 'https://vip.818tu.com/backend/order_stats/api_get_multi_users_stats_today?uids=%s'
    # 昨日信息接口地址
    last_day_api_url = 'https://vip.818tu.com/backend/order_stats/api_get_multi_users_daily_stats'
    # 昨日信息接口地址请求参数
    last_day_api_url_param = {
        'uids': '',
        'limit': 1,
        'last_date': '',
    }
    session = requests.session()

    # 登录信息
    login_data = {
        'name': '',
        'password': '',
    }

    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 匹配uid值
    uidzz = re.compile('var uid = \[(.*)\];')

    # 区分平台
    platform = 3

    # uid
    uid = ''

    def __init__(self):
        self.session = requests.session()
        self.init_info()

    def start(self):
        for account in self.accounts:
            self.session = requests.session()
            self.login_data['name'] = account['username']
            self.login_data['password'] = account['password']
            self.session.post(self.login_url, self.login_data)
            req = self.session.get(self.vipindex_url)
            self.uid = self.uidzz.search(req.text).group(1).replace('"', '')
            self.get_data()
        self.save_info()

    def get_data(self):
        """
        获取今日信息
        :return:
        """
        url = self.api_part_url % self.uid
        req = self.session.get(url)
        reldata = req.json()
        self.data.value += int(reldata['paid_amount'])
        self.data.normal_charge += int(reldata['welth_order_paid_amount'])
        self.data.normal_charge_count += int(reldata['welth_order_paid_count'])
        self.data.vip_charge += int(reldata['vip_order_paid_amount'])
        self.data.vip_charge_count += int(reldata['vip_order_paid_count'])
        self.data.recharge_count = self.data.normal_charge_count + self.data.vip_charge_count
        if self.localtime.tm_hour == plat.get_yesterday_hour:
            self.get_yesterday_data()

    def get_yesterday_data(self):
        """
        获取昨日信息
        :return:
        """
        self.last_day_api_url_param['uids'] = self.uid
        param = urlencode(self.last_day_api_url_param)
        url = self.last_day_api_url + "?" + param
        req = self.session.get(url)
        data = req.json()[0]
        self.yesterday_data.value += int(data['paid_amount'])
        self.yesterday_data.normal_charge += int(data['welth_order_paid_amount'])
        self.yesterday_data.normal_charge_count += int(data['welth_order_paid_count'])
        self.yesterday_data.vip_charge += int(data['vip_order_paid_amount'])
        self.yesterday_data.vip_charge_count += int(data['vip_order_paid_count'])
        self.yesterday_data.recharge_count = self.yesterday_data.normal_charge_count + self.yesterday_data.vip_charge_count
