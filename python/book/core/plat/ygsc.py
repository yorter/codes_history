import re

import requests
from pyquery import PyQuery as pq

from core.plat.platbase import PlatInfoBase

"""
阳光书城vip账号抓取
"""


class Ygsc(PlatInfoBase):
    login_url = 'https://vip.yifengaf.cn/admin/index/login'
    # 首页地址
    vipindex_url = 'https://vip.yifengaf.cn/admin/vipindex'
    session = requests.session()
    # 登录信息
    login_data = {
        'username': '',
        'password': '',
        'keeplogin': '1',
    }
    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')

    # 区分平台
    platform = 1

    def __init__(self):
        self.init_info()

    def start(self):
        for account in self.accounts:
            self.session = requests.session()
            self.login_data['username'] = account['username']
            self.login_data['password'] = account['password']
            self.get_data()
        self.save_info()

    def get_data(self):
        self.session.post(self.login_url, self.login_data)
        req = self.session.get(self.vipindex_url)
        doc = pq(req.text)
        today_box = doc('.panel-div.pannel_tody_box')
        # 匹配今日充值金额
        span = today_box.find('.first span').eq(0).text()
        self.data.value += self.getvalue(span)
        # 匹配普通充值
        normal_first = today_box.find('#normal_first')
        # #匹配普通充值金额
        self.data.normal_charge += self.getvalue(normal_first.find('#normal_recharge_money').text())
        # #匹配普通充值比数
        self.data.normal_charge_count += int(self.done_countzz.search(normal_first.text()).group(1))
        # 匹配年费VIP会员
        vip_first = today_box.find('#vip_first')
        # # 匹配年费VIP会员充值金额
        self.data.vip_charge += self.getvalue(vip_first.find('span').text())
        # 匹配年费VIP会员充值比数
        self.data.vip_charge_count += int(self.done_countzz.search(vip_first.text()).group(1))
        self.data.recharge_count = self.data.normal_charge_count + self.data.vip_charge_count

        # 获取昨日信息
        yestoday_box = doc('.panel-div').eq(1)
        # 匹配昨日充值金额
        span = yestoday_box.find('.first span').eq(0).text()
        self.yesterday_data.value += self.getvalue(span)
        # 匹配普通充值
        normal_first = yestoday_box.find('.pannel-bottom .first')
        # #匹配普通充值金额
        self.yesterday_data.normal_charge += self.getvalue(normal_first.find('span').text())
        # #匹配普通充值比数
        self.yesterday_data.normal_charge_count += int(self.done_countzz.search(normal_first.text()).group(1))
        # 匹配年费VIP会员
        vip_first = yestoday_box.find('.pannel-bottom .second')
        # # 匹配年费VIP会员充值金额
        self.yesterday_data.vip_charge += self.getvalue(vip_first.find('span').text())
        # # 匹配年费VIP会员充值比数
        self.yesterday_data.vip_charge_count += int(self.done_countzz.search(vip_first.text()).group(1))
        recharge_count = self.yesterday_data.normal_charge_count + self.yesterday_data.vip_charge_count
        self.yesterday_data.recharge_count = recharge_count

    def getvalue(self, text):
        """
        在字符串中匹配金额并返回整数
        :param text:匹配的字符串
        :return:金额整数（单位分
        """
        valuestr = self.intzz.search(text).group().replace('.', '')
        return int(valuestr)
