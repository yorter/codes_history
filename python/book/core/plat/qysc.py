import math
import re
import time
from urllib import parse

import requests

from config.account import vip_accounts as accounts
from core.mylibs.mysql import Mysql
from core.plat.platbase import PlatInfoBase
from config import plat

"""
阳光书城vip账号抓取
"""


class Qysc(PlatInfoBase):
    login_url = 'https://www.zhangwenwenhua.cn/admin/index/login'
    # 首页地址
    vipindex_url = 'https://www.zhangwenwenhua.cn/admin/collect/index?addtabs=1'
    #
    api_url = 'https://www.zhangwenwenhua.cn/admin/collect/ajaxtoday?channel_id=0'
    yes_api_url = 'https://www.zhangwenwenhua.cn/admin/collect/index?%s'
    yes_api_param = {
        'channel_id': 0,
        'sort': 'createdate',
        'order': 'desc',
        'offset': 0,
        'limit': 10,
        '_': 0,
    }
    session = requests.session()
    # 登录信息
    login_data = {
        'username': '',
        'password': '',
        'keeplogin': 1
    }
    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 匹配groupId值
    groupIdzz = re.compile('groupId":(\d+),')
    # 匹配authId值
    authIdzz = re.compile('authId":(\d+)\}')

    # 区分平台
    platform = 2

    # 请求头
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3722.400 QQBrowser/10.5.3751.400',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Pragma': 'no-cache',
        'Host': 'www.zhangwenwenhua.cn',
        'X': 'XMLHttpRequest',
    }

    def __init__(self):
        self.init_info()

    def start(self):
        for account in self.accounts:
            self.session = requests.session()
            self.login_data['username'] = account['username']
            self.login_data['password'] = account['password']
            self.get_today()
        self.save_info()

    def get_today(self):
        req = self.session.get(self.vipindex_url)
        # 获取uid
        group_id = self.groupIdzz.search(req.text).group(1).replace('"', '')
        auth_id = self.authIdzz.search(req.text).group(1).replace('"', '')
        data = {
            'groupId': group_id,
            'authId': auth_id,
        }
        req = self.session.post(self.api_url, data=data)
        reldata = req.json()
        self.data.value += self.floattoint(reldata['recharge_money'])
        self.data.normal_charge += self.floattoint(reldata['normal_recharge_money'])
        self.data.normal_charge_count += int(reldata['normal_recharge_orders'])
        self.data.vip_charge += self.floattoint(reldata['vip_recharge_money'])
        self.data.vip_charge_count += int(reldata['vip_recharge_orders'])
        self.data.recharge_count = self.data.normal_charge_count + self.data.vip_charge_count
        if self.localtime.tm_hour == plat.get_yesterday_hour:
            self.get_yesterday()

    def get_yesterday(self):
        self.session.post(self.login_url, self.login_data)
        # 获取昨日信息
        self.yes_api_param['_'] = int(time.time())
        url = self.yes_api_url % parse.urlencode(self.yes_api_param)
        req = self.session.get(url, headers=self.headers)
        reldata = req.json()['rows'][0]
        self.yesterday_data.value += self.floattoint(reldata['recharge_money'])
        self.yesterday_data.normal_charge += self.floattoint(reldata['normal_recharge_money'])
        self.yesterday_data.normal_charge_count += int(reldata['normal_recharge_orders'])
        self.yesterday_data.vip_charge += self.floattoint(reldata['vip_recharge_money'])
        self.yesterday_data.vip_charge_count += int(reldata['vip_recharge_orders'])
        recharge_count = self.yesterday_data.normal_charge_count + self.yesterday_data.vip_charge_count
        self.yesterday_data.recharge_count = recharge_count

    @staticmethod
    def floattoint(text) -> int:
        return int(str(text).replace('.', ''))

    def getvalue(self, text) -> int:
        """
        在字符串中匹配金额并返回整数
        :param text:匹配的字符串
        :return:金额整数（单位分
        """
        valuestr = self.intzz.search(text).group().replace('.', '')
        return int(valuestr)
