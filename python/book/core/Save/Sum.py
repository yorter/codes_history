import redis
from config.redis_key import config as redis_key_config
from config.dbconfig import redis_config
from core.mylibs.public import timeprint
import json
import math

"""
计算每本书总值
"""


class Sum:
    redis = redis.Redis(**redis_config)

    @classmethod
    def start(cls):
        timeprint("开始计算总和")
        cls.clean_last_list()
        # 获取所有的小说id
        for tui in [0, 1]:
            keys = cls.redis.sunion(redis_config['bookid_key_keys'] % tui)
            for book_id in keys:
                redis_key = redis_key_config['bookid_key'] % (tui, book_id)
                links = cls.redis.hgetall(redis_key)
                # 将json转换为字典
                for link_id in links:
                    links[link_id] = json.loads(links[link_id])
                book_data = {
                    'clicks': 0,
                    'subscribe_count': 0,
                    'paid_amount': 0,
                    'cost_amount': 0,
                }
                for link_id in links:
                    book_data['clicks'] += int(links[link_id]['clicks'])
                    book_data['subscribe_count'] += int(links[link_id]['subscribe_count'])
                    book_data['paid_amount'] += float(links[link_id]['paid_amount'])
                    book_data['cost_amount'] += int(links[link_id]['cost_amount'])
                # 利润
                book_data['profit_amount'] = book_data['paid_amount'] - book_data['cost_amount']
                # 回本率
                try:
                    book_data['profit_rate'] = book_data['paid_amount'] / book_data['cost_amount'] * 100
                except:
                    book_data['profit_rate'] = 0
                # 单粉回报
                try:
                    book_data['SingleReturn'] = book_data['paid_amount'] / book_data['subscribe_count']
                except:
                    book_data['SingleReturn'] = 0
                for k in book_data:
                    book_data[k] = round(book_data[k], 2)
                book_data['book_id'] = book_id
                book_data['name'] = cls.redis.hget(redis_key_config['bookidtoname'], book_id)
                cls.redis.hmset(redis_key_config['bookidsum'] % (tui, book_id), book_data)
                zdata = {book_id: book_data['cost_amount']}
                cls.redis.zadd(redis_key_config['bookidsort'] % tui, zdata)
                cls.redis.delete(redis_key)
        cls.make_json()

    @classmethod
    def make_json(cls):
        for tui in [0, 1]:
            idssort = list(reversed(cls.redis.zrange(redis_key_config['bookidsort'] % tui, 0, -1)))
            book_sort_infos = []
            for book_id in idssort:
                bookinfo = cls.redis.hgetall(redis_key_config['bookidsum'] % (tui, book_id))
                cls.redis.delete(redis_key_config['bookidsum'] % (tui, book_id))
                book_sort_infos.append(bookinfo)
            json_data = json.dumps(book_sort_infos, ensure_ascii=False)
            cls.redis.set(redis_key_config['bookidssums'] % tui, json_data)

    @classmethod
    def clean_last_list(cls):
        redis_id = redis_key_config['lastinfolist']
        redis_s_id = redis_key_config['lastinfokeys']
        cls.redis.delete(redis_id)
        slen = cls.redis.zcard(redis_s_id)
        timeprint("共查询到%s个链接" % slen)
        limit = 50
        count = math.ceil(slen / limit)
        for page in range(count):
            start = page * limit
            end = start + limit - 1
            items = cls.redis.zrange(redis_s_id, start, end)
            for book_id in items:
                cls.redis.lpush(redis_id, book_id)
        # exit()
