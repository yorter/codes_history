import json
import redis
from config.dbconfig import redis_config
from config.redis_key import config as redis_key_config


class SaveByBookId:
    redis = redis.Redis(**redis_config)

    def save(self, book_id, tui, link_id, book_info):
        """
        :param book_id:小说id
        :param link_id:链接id
        :param book_info:小说信息
        :return:
        """
        self.redis.hset(
            redis_key_config['Bookinfo'] % (book_id, tui), link_id, json.dumps(book_info, ensure_ascii=False)
        )
