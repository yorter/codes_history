from core.mylibs.mysql import Mysql
from config.redis_key import config as redis_key_config
from config.dbconfig import redis_config
import redis
import json
from copy import copy
from core.mylibs import log


class Total:
    db = None
    redis = None

    find_all_link = "SELECT MAX(id) as id FROM `{table}` GROUP BY book_info_id"

    order_by = "cost_amount"

    books = {
        0: {},  # 内瑞
        1: {},  # 外推
    }

    table_name = None

    def __init__(self, table_name: str):
        self.table_name = table_name
        self.find_all_link = self.find_all_link.format(**{"table": table_name})
        self.db = Mysql()
        self.redis = redis.Redis(**redis_config)

    def sum(self):
        log.timeprint("开始查询所有链接最新的信息")
        self.db.execute(self.find_all_link)
        alllinks = self.db.db.fetchall()
        log.timeprint("查询完毕，开始统计最新信息")
        for link in alllinks:
            linkdata = self.db.table(self.table_name).where({"id": link["id"]}).find()
            # 查询链接信息
            linkinfo = self.db.table("bookinfov2").where({"id": linkdata["book_info_id"]}).find()
            if linkinfo:
                if linkinfo['book_id'] in self.books[linkinfo['tui']].keys():
                    d = self.books[linkinfo['tui']][linkinfo['book_id']]
                    self.books[linkinfo['tui']][linkinfo['book_id']] = {
                        "clicks": linkdata['clicks'] + d['clicks'],  # 点击数
                        "subscribe_count": linkdata['subscribe_count'] + d['subscribe_count'],  # 新增关注
                        "paid_amount": linkdata['paid_amount'] + d['paid_amount'],  # 充值金额
                        "cost_amount": linkinfo['cost_amount'] + d['cost_amount'],  # 成本
                        "name": linkinfo['name']
                    }
                else:
                    self.books[linkinfo['tui']][linkinfo['book_id']] = {
                        "clicks": linkdata['clicks'],  # 点击数
                        "subscribe_count": linkdata['subscribe_count'],  # 新增关注
                        "paid_amount": linkdata['paid_amount'],  # 充值金额
                        "cost_amount": linkinfo['cost_amount'],  # 成本
                        # "profit_amount": linkinfo['tui'],
                        # "profit_rate": linkinfo['tui'],
                        # "SingleReturn": linkinfo['tui'],
                        # "book_id": linkinfo['tui'],
                        "name": linkinfo['name']
                    }
            else:
                log.timeprint(linkdata)
        self.sorttui()

    def sorttui(self):
        # pprint(self.books)
        # exit()
        for tui in self.books.keys():
            self.sort(tui)

    def sort(self, tui):
        log.timeprint("开始为%s排序" % tui)
        all_book_ids = set()
        all_books = self.books[tui]
        for book in all_books.values():
            all_book_ids.add(book[self.order_by])
        save_data = []
        for i in range(len(all_book_ids)):
            maxv = max(all_book_ids)
            for book_id in all_books.keys():
                if all_books[book_id][self.order_by] == maxv:
                    bookdata = copy(all_books[book_id])
                    bookdata['book_id'] = book_id
                    # 利润
                    try:
                        bookdata['profit_amount'] = round(bookdata['paid_amount'] - bookdata['cost_amount'], 2)
                    except:
                        bookdata['profit_amount'] = 0
                    # 单粉回报
                    try:
                        bookdata['SingleReturn'] = round(bookdata['paid_amount'] / bookdata['subscribe_count'], 2)
                    except:
                        bookdata['SingleReturn'] = 0
                    # 回本率
                    try:
                        bookdata['profit_rate'] = round(bookdata['paid_amount'] / bookdata['cost_amount'] * 100, 2)
                    except:
                        bookdata['profit_rate'] = 0
                    save_data.append(bookdata)
            all_book_ids.remove(maxv)
        log.timeprint("开始保存%s信息" % tui)
        redis_id = redis_key_config['bookidssums'] % tui
        json_data = json.dumps(save_data, ensure_ascii=False)
        self.redis.set(redis_id, json_data)
