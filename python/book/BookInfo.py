import os
import time
from multiprocessing import Pool

import redis

from config.dbconfig import redis_config
from config import redis_key
from core.Save.Totalv3 import Total
from core.mylibs.mysql import Mysql
from core.mylibs.public import timeprint
from core.mylibs.table import Table

"""
小说采集程序起始程序
"""


# 限制的平台使用此方法
def platform_limit(platform_data, accounts_data):
    for account_data in accounts_data:
        if (account_data['pid'] == platform_data['pid']) and account_data['enable']:
            try:
                model(platform_data, account_data)
                # break
            except:
                continue
            # model(platform_data, account_data)
            # break


# 反射获取爬虫对象
def model(platform_data: dict, account_data: dict):
    m = __import__("core.bookspiders.%s" % platform_data['object'], fromlist=True)
    book_info = getattr(m, "BookInfo")
    b = book_info(account_data)
    b.start()


if __name__ == '__main__':
    timeprint("开始采集脚本")
    redisobj = redis.Redis(**redis_config)
    # 将当前时间存入redis
    nowtime = int(time.time())
    timeprint(nowtime)
    redisobj.set(redis_key.nowtime, nowtime)

    # 创建对应的表
    t = Table(nowtime)
    t.check_table()
    t.db.close()
    # exit()

    # 进程池
    processnum = (os.cpu_count() or 1) + 1
    pnum = processnum if processnum < 5 else 5
    timeprint("共开启%s个采集进程" % pnum)
    ps = Pool(pnum)

    # 查询平台和账号
    db = Mysql("book_spider")
    platforms = db.table("platform").select()
    where = {
        'isvip': '0',
        'enable': '1',
    }
    accounts = db.table("account").where(where).select()
    db.close()
    for platform in platforms:
        if platform['enable']:
            if platform['sync']:
                for account in accounts:
                    if (account['pid'] == platform['pid']) and account['enable']:
                        # model(platform, account)
                        # break
                        ps.apply_async(func=model, args=(platform, account))
            else:
                ps.apply_async(func=platform_limit, args=(platform, accounts))
                # platform_limit(platform, accounts)
    ps.close()
    ps.join()
    # 清除开始时间
    # redisobj.delete(redis_config['nowtime'])
    # 更新书列表
    t = Total(Table.get_table_name(nowtime))
    t.sum()
