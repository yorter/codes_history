from core.plat.zzy import Zzy
from core.plat.ygsc import Ygsc
from core.plat.qysc import Qysc

try:
    t = Zzy()
    t.start()
except:
    pass

try:
    t = Ygsc()
    t.start()
except:
    pass

# try:
#     t = Qysc()
#     t.start()
# except:
#     pass
