class Single(object):
    __instence = None

    def __new__(cls, *args, **kwargs):
        if not cls.__instence:
            cls.__instence = object.__new__(cls)
        return cls.__instence
