import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import time
import math

url = 'https://d1.music.126.net/dmusic/cloudmusicsetup_2.5.1.196734.exe'
r = requests.get(url, stream=True, verify=False)
total_size = int(r.headers['Content-Length'])
temp_size = 0
d_time = math.floor(time.time())
with open('pycharm.exe', 'wb') as f:
    for chunk in r.iter_content(chunk_size=1024):
        now_time = math.floor(time.time())
        if now_time==d_time:
            temp_size += len(chunk)
        else:
            print('下载速度为%s' % temp_size)
            temp_size = 0
            d_time = now_time

        if chunk:
            temp_size += len(chunk)
            f.write(chunk)
            f.flush()

