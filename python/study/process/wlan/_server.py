import math
import json
import random
import socket
import time

sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sender.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

PORT = 9999
machineName = socket.getfqdn(socket.gethostname())
machineAddress = socket.gethostbyname(machineName)

message = {
    "ip": machineAddress,
    "random": math.floor(random.random() * 10000)
}

network = '<broadcast>'
while 1:
    sender.sendto(json.dumps(message).encode('utf-8'), (network, PORT))
    time.sleep(5)