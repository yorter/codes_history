from multiprocessing import Process,Value,Lock,Semaphore
import random
import time


def get(index, l, num):
    order = index + 1
    l.acquire()
    a = random.randint(5, 15)
    if a % 2:
        print('第%s个人决定买买买' % order)
        num.value -= 1
    else:
        print('第%s个人决定放弃' % order)
    time.sleep(a)
    l.release()


if __name__ == '__main__':
    lock = Semaphore(5)

    num = Value('i', 100)
    for i in range(100):
        p = Process(target=get, args=(i, lock, num))
        p.start()
        # p.join()
        print(num.value)
