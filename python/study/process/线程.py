from threading import Thread
from threading import Timer
import time
import random
import os


def func(i=100):
    time.sleep(random.randint(1, 3))
    print(i, os.getpid())


for i in range(10):
    t = Thread(target=func, args=(i,))
    t.start()
print(os.getpid())

# 定时器(定时开启一个任务)

t = Timer(3,func)
t.start()