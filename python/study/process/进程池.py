from multiprocessing import Pool
import sys
import os
import time
import random


def func(num):
    # time.sleep(random.randint(5, 10) / 10)
    time.sleep(0.5)
    new_num = num + 1
    # print(new_num)
    return new_num


def call_back(res):
    print(res)


if __name__ == '__main__':
    pass
    # p = Pool(os.cpu_count() + 1)
    # # 同步的方法
    # for i in range(10):
    #     res = p.apply(func, args=(i,))
    #     print(res)
    # p.close()
    #
    # # 异步的方法
    # p = Pool(os.cpu_count() + 1)
    # res_list = []
    # for i in range(10):
    #     res2 = p.apply_async(func, args=(i,))
    #     res_list.append(res2)
    # p.close()
    # p.join()
    # [print(res2.get()) for res2 in res_list]

    # map方法 参数必须是可迭代的
    # p = Pool(os.cpu_count() + 1)
    # res = p.map(func, range(10))
    # print(res)

    # 回调函数(map不支持)
    # p = Pool(os.cpu_count() + 1)
    # res_list = []
    # for i in range(10):
    #     p.apply_async(func, args=(i,), callback=call_back)
    #
    # p.close()
    # p.join()
    # [print(res2.get()) for res2 in res_list]
