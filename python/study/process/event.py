from multiprocessing import Process,Event
# 信号量 事件 锁 递归锁
import time
import random

def light(e):
    while 1:
        a = random.randint(1, 3)
        if not e.is_set():
            print('现在是红灯，请停止前行')
            time.sleep(a)
            e.set()
            print('红灯灭，绿灯亮')
        else:
            print('现在是绿灯，可以前行')
            time.sleep(a)
            e.clear()
            print('绿灯灭，红灯亮')


def cars(e, car_e, index):
    e.wait()
    while 1:
        car_e.wait()
        car_e.clear()
        print(car_e.is_set())
        print('\t第%s辆车正在通过红绿灯' % index)
        time.sleep(random.randint(1, 3))
        car_e.set()
        break
    pass


if __name__ == '__main__':
    e = Event()
    car_e = Event()
    car_e.set()
    traff = Process(target=light, args=(e,))
    traff.start()
    for i in range(5000):
        time.sleep(1)
        car = Process(target=cars, args=(e, car_e, i+1))
        car.start()
