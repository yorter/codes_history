from threading import Thread as thread, RLock
import time, random, os
import threading

# class Test():
#     """测试类"""
#     def thread(self):
#         start = time.time()
#         l = RLock()
#         for num in range(1, 100):
#             t = thread(target=self.func, args=(num, l))
#             t.start()
#             # self.func(num,l)
#         # t.join()
#         print(time.time()-start)
#         pass
#
#     def func(self, num, l):
#         # l.acquire()
#         print(num)
#         # time.sleep(0.02)
#         # l.release()
#         pass
#
# tt = Test()
# tt.thread()


class Myt(thread):
    def run(self):
        time.sleep(1)
        print(os.getpid())

for i in range(10):
    t = Myt()
    t.run()

# 返回正在进行的线程列表（包含主进程）
print(threading.enumerate())
# 返回正在进行的线程数量（包含主进程）
print(threading.activeCount())

