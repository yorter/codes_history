import threading
# 条件
con = threading.Condition()
con.acquire()
con.notify(5)   # 放行5个线程
con.notify_all() # 放行所有线程
con.release()
