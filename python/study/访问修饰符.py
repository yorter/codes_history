class C(object):
    def __init__(self):
        self._x = 5

    x = 500
    _x = 100
    __x = 200


    def getx(self):
        return self._x


    def setx(self, value):
        self._x = value+200

    def delx(self):
        self._x += 10

    x = property(getx, setx, delx, "I'm the 'x' property.")

c = C()
print(c.x)
c.x = 5
print(c.x)
del c.x
print(c.x)