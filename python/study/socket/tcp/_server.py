import socket
import struct

pack_type = '<i'

# sk = socket.socket(type=socket.SOCK_DGRAM)
sk = socket.socket()
sk.bind(('127.0.0.1', 9000))
sk.listen()  # udp协议不需要
while True:
    conn, addr = sk.accept()
    while True:
        buffer_len = struct.unpack(pack_type, conn.recv(4))[0]
        msg = conn.recv(buffer_len).decode('utf-8')
        print(msg)
        sent_msg = input('>>>').encode('utf-8')
        conn.send(struct.pack(pack_type, len(sent_msg)))
        conn.send(sent_msg)
conn.close()
sk.close()
