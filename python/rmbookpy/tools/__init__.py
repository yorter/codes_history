import time
import re
import xlrd


def xlrd2int(t):
    try:
        xt = xlrd.xldate.xldate_as_datetime(float(t), 0)
        tmp_t = time.mktime(
            time.strptime('%s %s' % (xt.date(), xt.time()), "%Y-%m-%d %H:%M:%S"))
        return int(tmp_t)
    except:
        return 0


def xlrdt2date(t):
    try:
        xt = xlrd.xldate.xldate_as_datetime(float(t), 0)
        return str(xt.date())
    except:
        return "1970-01-01"


date_time_zz = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}')


def strtime2int(t: str):
    try:
        s = date_time_zz.search(t)
        return int(time.mktime(time.strptime(s.group(0), "%Y-%m-%d %H:%M:%S")))
    except:
        return 0
