"""
MySQL配置
"""
mysql_config = {
    'host': "127.0.0.1",  # 地址
    'user': 'root',  # 用户名
    'password': '43793543',  # 密码
    'port': 3306,  # 端口号
    'database': 'book2',  # 默认库
    'charset': 'utf8',  # 字符集
}
"""
redis配置
"""
redis_config = {
    'host': 'localhost',  # 连接地址
    'port': 6379,  # 端口号
    'decode_responses': True,  # 结果返回字符串而非Unicode编码
    'db': 8,  # 链接库(python默认无法修改库)
    # 'password': "FightForFreedom",
}
