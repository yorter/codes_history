import time

from core.analysis import Analysis
from config.db import mysql_config
from core.errs import AnalysisFail
from core.file_field import FileField
from core.mysql import Mysql


def AnalysisLog(log: dict):
    a = Analysis(path, log_id)
    a.start()
    db.table('uploadlog').where({'id': log_id}).update({
        'time_analysis': int(time.time()),
    })


if __name__ == '__main__':
    # 查询上传文件
    db = Mysql(**mysql_config)
    logs = db.query('SELECT * FROM uploadlog WHERE `status` IN ("1,6") LIMIT 1')
    if logs:
        print("有程序进行中，此次任务中止")
        exit()
    logs = db.table('uploadlog').where({'status': 0}).select()
    # db.close()
    if not logs:
        exit()
    ids = []
    for log in logs:
        ids.append(str(log['id']))
    sql = 'UPDATE uploadlog SET `status` = 6 WHERE id IN (%s)' % ','.join(ids)
    db.query(sql)
    for log in logs:
        log_id = log['id']
        path = log['path']
        print(path)
        # AnalysisLog(log)

        try:
            AnalysisLog(log)
            # break
        except AnalysisFail as e:
            db.table('uploadlog').where({'id': log_id}).update({
                'status': 3,
                'time_analysis': int(time.time()),
            })
        except FileNotFoundError:
            db.table('uploadlog').where({'id': log_id}).update({
                'status': 4,
                'time_analysis': int(time.time()),
            })
        except Exception as e:
            print(e)
            db.table('uploadlog').where({'id': log_id}).update({
                'status': 5,
                'time_analysis': int(time.time()),
                'err_info': str(e),
            })
