import time

import tools
from core.types.type_base import TypeBase


class Daily(TypeBase):
    """
    每日数据
    """

    def start(self):
        t = 'daily'
        self.save_status(status=1, type_id=4)
        for line in range(1, self.table.nrows):
            self.index = line
            print('\r解析 进度 %s / %s' % (line, self.row), end='', flush=True)
            try:
                rel = self.save()
                if not rel:
                    self.err += 1
                    continue
            except Exception as e:
                # self.errs_info.append(str(e))
                self.err += 1
                continue
            self.deal += 1
        self.save_status(2, True)

    def save(self):
        data = {
            'date': tools.xlrdt2date(self.get_table_value(0)),  # 商户单号
            'value': int(self.get_table_value(1) * 100),  # 消耗金额
            'fans': int(self.get_table_value(2)),  # 粉丝数
            'newer': int(self.get_table_value(3)),  # 新增用户
            'official_id': str(self.get_table_value(4)).strip(),  # 公众号名
            'book_id': str(self.get_table_value(5)).strip(),  # 小说名
        }
        # 获取公众号
        official = self.db.table('officials').where({
            'name': data['official_id']
        }).find()
        if not official:
            self.errs_info.append("公众号%s不存在" % data['official_id'])
            return False
        data['official_id'] = official['id']

        plt_id = official['plt_id']
        # 获取小说名
        book = self.db.table('books').where({
            'plt_id': plt_id,
            'name': data['book_id']
        }).find()
        if not book:
            # 创建小说
            self.db.table('books').insert({
                'plt_id': plt_id,
                'name': data['book_id'],
                'time_create': int(time.time()),
            })
            book_id = self.db.lastrowid()
        else:
            book_id = book['id']
        data['book_id'] = book_id
        find_log = self.db.table('daily').where({
            # 'book_id': data['book_id'],
            'official_id': data['official_id'],
            'date': data['date'],
        }).find()
        if not find_log:
            self.db.table('daily').insert(data)
        else:
            self.db.table('daily').where({'id': find_log['id']}).update(data)
        return True
