import json

import xlrd

from config.db import mysql_config
from core.file_field import FileField
from core.mysql import Mysql


class TypeBase:
    def __init__(self, table: xlrd.sheet.Sheet, file_index: list, upload_id: int):
        self.db = Mysql(**mysql_config)
        self.table = table
        self.file_fields_index = file_index
        self.upload_id = upload_id
        # self.save_status(1)
        self.row = table.nrows - 1
        self.errs_info = []

    db: Mysql
    table: xlrd.sheet.Sheet
    file_fields_index = []

    # 总行数
    row = 0
    # 错误行数
    err = 0
    # 处理行数
    deal = 0

    errs_info = []

    upload_id = 0

    index = 0

    def get_table_value(self, index):
        # print('<-------------------')
        # print(self.index)
        # print(self.file_fields_index)
        # print(index)
        # print('------------------->')
        return self.table.cell_value(self.index, self.file_fields_index[index])

    def save_status(self, status: int, other=False, type_id=0):
        update = {
            'status': status,
        }
        if other:
            update.update({
                'row': self.row,
                'err': self.err,
                'deal': self.deal,
                # 'err_info': json.dumps(self.errs_info),
            })
        if type_id:
            update.update({
                'type': type_id,
            })
        self.db.table('uploadlog').where({
            'id': self.upload_id
        }).update(update)

    def __del__(self):
        self.db.close()
