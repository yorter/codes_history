import time

import xlrd

import tools
from core.types.type_base import TypeBase


class Order(TypeBase):
    """
    订单
    """

    def start(self):
        self.save_status(status=1, type_id=3)
        t = 'order'
        for line in range(1, self.table.nrows):
            self.index = line
            print('\r解析 进度 %s / %s' % (line, self.row), end='', flush=True)
            try:
                rel = self.save_order()
                if not rel:
                    self.err += 1
                    continue
            except Exception as e:
                print(e)
                self.err += 1
                continue
            self.deal += 1
        self.save_status(2, True)

    index = 0

    def save_order(self):
        data = {
            'open_id': self.get_table_value(1),  # openid
            'time_create': tools.strtime2int(str(self.get_table_value(2)).strip()),  # 创建时间
            'value': int(self.get_table_value(3) * 100),  # 订单金额
            'official_id': str(self.get_table_value(4)).strip(),  # 公众号
            # 'book_id': str(self.get_table_value(5)).strip(),  # 小说名
            'pay_time': tools.strtime2int(str(self.get_table_value(5)).strip()),  # 商户单号
        }
        data['create_date'] = time.strftime('%Y-%m-%d', time.localtime(data['time_create']))
        data['pay_date'] = time.strftime('%Y-%m-%d', time.localtime(data['pay_time']))
        # 商户单号
        try:
            data['order_id'] = int(self.get_table_value(0))
        except ValueError:
            data['order_id'] = str.lstrip(self.get_table_value(0), '`').strip()
        # 获取公众号
        official = self.db.table('officials').where({
            'name': data['official_id']
        }).find()
        if not official:
            self.errs_info.append("公众号%s未找到" % data['official_id'])
            return False
        data['official_id'] = official['id']
        daily = self.db.table('daily').where({
            'official_id': data['official_id'],
            'date': data['create_date']
        }).find()
        if not daily:
            # print('--------------->')
            # print(data)
            # t = self.get_table_value(2)
            # print(time.localtime(data['pay_time']))
            # print('<---------------------')
            self.errs_info.append("每日消耗%s-%s未找到" % (data['official_id'], data['create_date']))
            return False
        # plt_id = official['plt_id']
        # 获取小说id
        # book = self.db.table('books').where({
        #     'plt_id': plt_id,
        #     'name': data['book_id']
        # }).find()
        # if not book:
        #     # 创建小说
        #     self.db.table('books').insert({
        #         'plt_id': plt_id,
        #         'name': data['book_id'],
        #         'time_create': int(time.time()),
        #     })
        #     book_id = self.db.lastrowid()
        # else:
        #     book_id = book['id']
        data['book_id'] = daily['book_id']
        find_log = self.db.table('orders').where({
            'order_id': data['order_id'],
        }).find()
        if not find_log:
            self.db.table('orders').insert(data)
        else:
            self.db.table('orders').where({'id': find_log['id']}).update(data)
        return True
