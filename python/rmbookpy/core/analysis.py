import os
import time

import xlrd

from config.db import mysql_config
from core import errs
from core.file_field import FileField
from core.mysql import Mysql
from core.types.order import Order
from core.types.daily import Daily


class Analysis:
    _path: str

    def __init__(self, path, upload_id):
        self._path = path
        self.upload_id = upload_id
        for rule_name in FileField.rules:
            for key, val in enumerate(FileField.rules[rule_name][0]):
                FileField.rules[rule_name][0][key] = val.lower()

    # 多少行
    _row: int
    # 多少列
    _col: int

    __type = ''

    upload_id = 0

    book: xlrd.book.Book
    table: xlrd.sheet.Sheet

    def start(self):
        self.file_fields_index = []
        global o
        if not os.path.exists(self._path):
            raise FileNotFoundError("文件不存在")
        self.book = xlrd.open_workbook(self._path)
        self.table = self.book.sheet_by_index(0)
        self.__type = self.check_type()
        if isinstance(self.__type, bool):
            raise errs.AnalysisFail("解析文件类型失败")
        self.sort()
        print("解析文件类型为%s" % self.__type)
        if self.__type == 'order':
            o = Order(self.table, self.file_fields_index, self.upload_id)
            o.start()
        elif self.__type == 'daily':
            o = Daily(self.table, self.file_fields_index, self.upload_id)
            o.start()
        print('总计%s 处理%s 错误%s' % (o.row, o.deal, o.err))
        print(o.errs_info)

    file_fields = []
    file_fields_index = []

    def check_type(self):
        self.file_fields = []
        """
        解析文件类型
        :return: 文件类型或者False
        """
        for i in range(self.table.ncols):
            s = str.strip(self.table.cell(0, i).value).lower()
            self.file_fields.append(s)
        for rule_name in FileField.rules:
            fields = set(FileField.rules[rule_name][0])
            # debug
            print('<---------------')
            print(fields)
            print(self.file_fields)
            print(fields.difference(self.file_fields))
            print('--------------->')
            # debug end

            if not fields.difference(self.file_fields):
                return rule_name
        return False

    def sort(self):
        for t in FileField.rules[self.__type][0]:
            self.file_fields_index.append(self.file_fields.index(t))
