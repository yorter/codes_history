class FileField:
    rules = {
        'order': [
            ['商户单号', 'OpenId', '用户Id创建时间', '订单金额', '渠道', '支付时间'],
            ['order_id', 'open_id', 'time_create', 'value', 'official_id', 'paytime'],
        ],
        'daily': [
            ['日期', '消耗金额', '粉丝数量', '新付费用户数', '公众号', '消耗小说'],
            ['date', 'value', 'fans', 'newer', 'official_id', 'book_id'],
        ],
    }
