import time
import sys

from core.analysis import Analysis
from config.db import mysql_config
from core.errs import AnalysisFail
from core.file_field import FileField
from core.mysql import Mysql


def AnalysisLog(file_path, file_log_id):
    a = Analysis(file_path, file_log_id)
    a.start()
    db.table('uploadlog').where({'id': log_id}).update({
        'time_analysis': int(time.time()),
    })


log_id = sys.argv[1]

db = Mysql(**mysql_config)
log = db.table('uploadlog').where({'id': log_id}).find()
print(log)
if not log:
    sys.exit()
path = log['path']
log_id = log['id']
# AnalysisLog(path, log_id)
# exit()
try:
    AnalysisLog(path, log_id)
    # break
except AnalysisFail as e:
    db.table('uploadlog').where({'id': log_id}).update({
        'status': 3,
        'time_analysis': int(time.time()),
    })
except FileNotFoundError:
    db.table('uploadlog').where({'id': log_id}).update({
        'status': 4,
        'time_analysis': int(time.time()),
    })
except Exception as e:
    print(e)
    db.table('uploadlog').where({'id': log_id}).update({
        'status': 5,
        'time_analysis': int(time.time()),
        'err_info': str(e),
    })
