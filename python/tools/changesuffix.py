import sys
import os
import re

orgin_suffix = input("请输入要修改的原后缀(默认为html):")
orgin_suffix = orgin_suffix if orgin_suffix else 'html'
after_suffix = input("请输入要修改后的后缀(默认为php):")
after_suffix = after_suffix if after_suffix else 'php'

current_dir = os.path.dirname(sys.argv[0])

# 正则匹配需要的后缀
contrast = re.compile('^(.*\.)%s$' % orgin_suffix)


def change_suffix(path):
    """
    修改指定文件夹下的指定文件名的后缀
    :param path:
    :return:
    """
    dir_paths = os.listdir(path)
    # print(l)
    for dir_path in dir_paths:
        new_path = '%s/%s' % (path, dir_path)
        if os.path.isdir(new_path):
            change_suffix(new_path)
        else:
            s = contrast.search(dir_path)
            if s:
                new_name = '%s/%s%s' % (path, s.group(1), after_suffix)
                os.rename(new_path, new_name)
                print('将【%s】修改为【%s】' % (new_path, new_name))


change_suffix(current_dir)
