import sys
import os
import re

suffix = input("请输入要删除的后缀名文件:")
if len(suffix) <= 0:
    print("没有指定后缀名，结束")
    exit()

current_dir = os.path.dirname(sys.argv[0])

# 正则匹配需要的后缀
contrast = re.compile('^(.*\.)%s$' % suffix)


def change_suffix(path):
    """
    修改指定文件夹下的指定文件名的后缀
    :param path:
    :return:
    """
    dir_paths = os.listdir(path)
    # print(l)
    for dir_path in dir_paths:
        new_path = '%s/%s' % (path, dir_path)
        if os.path.isdir(new_path):
            change_suffix(new_path)
        else:
            s = contrast.search(new_path)
            if s:
                print(new_path)
                os.unlink(new_path)


change_suffix(current_dir)
