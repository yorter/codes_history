from model_data.model_data_base import ModelDataBase


class Database(ModelDataBase):
    user: str
    password: str
    database: str
    host: str
    port: int

    def __init__(self, **kwargs):
        self.user = "root"
        self.password = ""
        self.database = ""
        self.host = "localhost"
        self.port = 3306

    def keys(self):  # 获取字典的键
        return ['user', 'password', 'database', 'host', 'port']
