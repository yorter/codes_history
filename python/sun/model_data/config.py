from model_data.database import Database
from model_data.model_data_base import ModelDataBase


class Config(ModelDataBase):
    def __init__(self):
        self.wy = Database()
        self.zy = Database()

    # 物业数据库配置
    wy: Database
    # 置业数据库配置
    zy: Database
