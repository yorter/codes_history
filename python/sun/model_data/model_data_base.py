class ModelDataBase:
    def keys(self):  # 获取字典的键
        return []

    def __setitem__(self, key, value):
        if hasattr(self, key):
            current_value = getattr(self, key)
            if isinstance(current_value, ModelDataBase):
                for sub_key in value.keys():
                    current_value[sub_key] = value[sub_key]
            else:
                setattr(self, key, type(current_value)(value))

    def __getitem__(self, item):  # 获取键对应的值
        return getattr(self, item)  # getattr获取对象下某个属性的值
