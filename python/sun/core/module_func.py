import importlib
import sqlite3

from core.container import Container
from core.data import containerg_key
from core.data.table_name import program
from core.model_data.sqlite.program.module_data import ModuleData
from dbquery import func
from dbquery.db import Database


def run_module(module_id: int):
    conn = Container.get(containerg_key.program, sqlite3.Connection)
    m = Database(conn)
    m.table(program.modules).where.eq('id', module_id)
    module_data = m.find()
    md = func.orm(module_data, ModuleData)
    a = importlib.import_module("modules.%s" % md.module_name)
    c = getattr(a, md.class_name)
    obj = c()
    getattr(obj, md.func_name)()
