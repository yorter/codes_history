from typing import TypeVar

from model_data.config import Config

T = TypeVar('T')


class Container:
    """
    容器
    """
    __container = {}

    @classmethod
    def add(cls, k: str, v):
        cls.__container[k] = v

    @classmethod
    def get(cls, k: str, t: T = object) -> T:
        return cls.__container.get(k)

    @classmethod
    def get_config(cls) -> Config:
        return cls.__container.get('config')
