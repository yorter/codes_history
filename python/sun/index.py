import configparser
import os
import signal
import sqlite3

import prettytable
import pymysql
import openpyxl

from core import module_func
from core.container import Container
from core.data import containerg_key
from core.data.table_name import program as p_table
from model_data.config import Config
from dbquery.db import Database
from tool import log


def sigint_handler(signum, frame):
    print("程序打断")


class Main:
    # 无意义的代码--start
    # 单纯为了打包exe的时候能够导入包且优化导入的时候不会删除导入
    import_value1 = pymysql.NULL
    import_value2 = openpyxl.LXML
    import_value3 = prettytable.ALL

    # 无意义的代码--end

    def __init__(self, ini_path: str):
        if os.path.isfile(ini_path):
            log.flush_print("正在加载配置")
            config = configparser.ConfigParser()  # 类实例化
            # 第一种读取ini文件方式,通过read方法
            config.read(ini_path, encoding="utf-8")
            self.config['wy'] = config['wy']
            self.config['zy'] = config['zy']
            log.flush_print("加载配置完毕")
        Container.add('config', self.config)
        # 生成数据库连接对象
        self.program = sqlite3.Connection("databases/program.db")
        Container.add(containerg_key.program, self.program)
        self.record = sqlite3.Connection("databases/record.db")
        Container.add(containerg_key.record, self.record)

    config = Config()

    t_head = {
        'sort': "选项",
        'name': "名称",
    }

    pid = 0

    program: sqlite3.Connection
    record: sqlite3.Connection

    def start(self):
        input_title = "请选择选项               "
        m = Database(self.program)
        is_continue = True
        while is_continue:
            m.table(p_table.options).where.eq('pid', self.pid)
            m.order('sort')
            options = m.select()
            log.print_table(self.t_head, options, "请选择")
            opts = {}
            for opt in options:
                opts[str(opt['sort'])] = opt['id']
            while is_continue:
                select = input(input_title)
                if select in opts:
                    m.table(p_table.option_module).where.eq('option_id', opts[select])
                    om = m.find()
                    if om:
                        is_continue = False
                        module_func.run_module(om['module_id'])
                        break
                    self.pid = select
                    input_title = "请选择选项               "
                    break
                else:
                    input_title = "选项不存在，请从新选择     "


if __name__ == '__main__':
    # 信号捕捉程序必须在循环之前设置
    try:
        signal.signal(signal.SIGINT, sigint_handler)  # 由Interrupt Key产生，通常是CTRL+C或者DELETE产生的中断
        signal.signal(signal.SIGHUP, sigint_handler)  # 发送给具有Terminal的Controlling Process，当terminal 被disconnect时候发送
        signal.signal(signal.SIGTERM, sigint_handler)  # 请求中止进程，kill命令缺省发送
    except:
        pass
    path = r"config.ini"
    m = Main(path)
    m.start()
    # try:
    #     m = Main(path)
    #     m.start()
    # except Exception as e:
    #     print('\n%s' % e)
    input("按回车键结束")
