from typing import List

from tool import log


class Base:
    # 生成选项
    @staticmethod
    def options(t_head: dict, t_body: List[dict], option_key: str, return_key=None, title=""):
        """
        生成选项
        :param t_head:输出表哥表头
        :param t_body:输出表哥表体
        :param option_key:选项key
        :param return_key:返回的key
        :param title:输出表提示
        :return:
        """
        if return_key is None:
            return_key = option_key
        option_name = t_head.get(option_key)
        log.print_table(t_head, t_body, title)
        selects = {}
        for item in t_body:
            selects[str(item.get(option_key))] = item.get(return_key)
        while True:
            s = input("请选择%s" % option_name).strip()
            if s in selects:
                return selects[s]
            else:
                print("选项不存在，请从新输入")
