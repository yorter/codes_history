import math
import os

import openpyxl
from openpyxl import Workbook
from openpyxl.utils.exceptions import InvalidFileException

from core.container import Container
from dbquery.db import Database
from modules.base import Base
from tool import log
from core.data.table_name.zy import area


class ImportRooms(Base):
    # 搜索项目每页显示数量
    __find_pid_limit = 20
    __excel = ['xlsx', 'xlsm', 'xltx', 'xltm']

    work_book: Workbook = None

    __db: Database = None
    # 项目id
    __pid: int = 0

    def imp(self):
        self.__get_work()
        config = Container.get_config()
        self.__db = Database(config=dict(config.zy))
        self.__get_project_id()

    def __get_work(self):
        while True:
            path = self.__get_file_path()
            try:
                wb = openpyxl.load_workbook(path)
            except InvalidFileException:
                log.error("文件格式错误")
                continue
            self.work_book = wb
            return

    def __get_file_path(self):
        while True:
            path = input("请输入文件绝对路径地址或者所在文件夹地址    ")
            if not os.path.exists(path):
                log.error("文件或者目录不存在")
                continue
            if os.path.isfile(path):
                return path
            else:
                return self.__find_in_dir(path)

    # 选择文件夹中的文件
    def __find_in_dir(self, d: str):
        """
        选择文件夹中的文件
        :param d: 文件夹地址
        :return:
        """
        files = os.listdir(d)
        i = 1
        t_body = []
        for file in files:
            path = "%s/%s" % (d, file)
            if os.path.isfile(path):
                t = path.split('.')[-1]
                if t not in self.__excel:
                    continue
                # 判断后缀
                t_body.append({
                    'i': i,
                    'name': file,
                })
                i = i + 1
        file_name = self.options(
            {
                'i': "序号",
                'name': "文件名",
            },
            t_body, 'i', 'name', "请选择文件"
        )
        return "%s/%s" % (d, file_name)

    __next_page = "+"
    __pre_page = "-"

    def __get_project_id(self):
        word = input("请输入项目关键字")
        count = self.__db.table(area.project).like('p_name', word, True, True).count()
        if count <= 0:
            log.error("没有找到项目")
            self.__get_project_id()
        page = 1
        last_page = math.ceil(count / self.__find_pid_limit)
        while True:
            data = self.__db.table(area.project, 'p') \
                .like('p_name', word, True, True) \
                .field(['p_id', 'p_name', 'c_name']) \
                .join("%s c" % area.company, 'p.p_cid=c.c_id') \
                .page(page, self.__find_pid_limit).select()
            if count > self.__find_pid_limit:
                if page > 1:
                    data.append({
                        'p_id': "-",
                        'p_name': "上一页",
                    })
                if page < last_page:
                    data.append({
                        'p_id': "+",
                        'p_name': "下一页",
                    })
            pid = self.options(
                {
                    'p_id': "项目id",
                    'p_name': "项目名称",
                    'c_name': "所属公司",
                }, data, 'p_id', title="选择项目第%s页" % page
            )
            if pid == '+':
                page = page + 1
            elif pid == '-':
                page = page - 1
            else:
                return int(pid)

    def __check_data(self):
        pass
