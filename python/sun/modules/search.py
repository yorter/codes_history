import sqlite3

from core.container import Container
from core.data.containerg_key import program
from core.data.table_name.program import modules
from dbquery.db import Database
from modules.base import Base


class Search(Base):
    def __init__(self):
        self.__conn = Container.get(program)
        self.__db = Database(self.__conn)

    __conn: sqlite3.Connection
    __db: Database

    __search_word = ""
    __page = 1
    __limit = 10

    def run(self):
        self.__search_word = input("请输入要搜索的功能")
        self.__db.like('name', self.__search_word, True, True)
        self.__db.table(modules).page(self.__page, self.__limit)
        data = self.__db.select()
        print(data)
