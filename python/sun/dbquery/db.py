import sqlite3
from typing import List

import pymysql

from dbquery import func
from dbquery.const import DbType, Action
from dbquery.query import Query


class Database(Query):
    __conn = None
    __cur = None
    __db_type: DbType
    __default_limit: int

    def __init__(self,
                 conn=None,
                 default_limit=15,
                 table_pre=None,
                 config: dict = None,
                 file_path: str = None
                 ):
        """
        对象初始化
        :param conn:Mysql或者sqlite连接对象
        :param default_limit:默认每页返回数
        :param table_pre:数据库表前缀
        :param config:mysql数据库配置信息
        :param file_path:sqlite文件地址
        """
        self.__default_limit = default_limit
        if conn is not None:  # 传入连接的方式
            if isinstance(conn, pymysql.Connection):  # pymysql连接
                self.__db_type = DbType.MYSQL
                self.__conn = conn
                self.__cur = conn.cursor(cursor=pymysql.cursors.DictCursor)
            elif isinstance(conn, sqlite3.Connection):  # sqlite连接
                self.__db_type = DbType.SQLITE
                self.__conn = conn
                self.__cur = conn.cursor()
            else:
                raise Exception("不支持的类型")
        elif config is not None:  # 传入数据库配置的方式
            self.__db_type = DbType.MYSQL
            self.__conn = pymysql.Connection(**config)
            self.__cur = self.__conn.cursor(cursor=pymysql.cursors.DictCursor)
        elif file_path is not None:  # sqlite传入文件地址
            self.__db_type = DbType.SQLITE
            self.__conn = sqlite3.Connection(file_path)
            self.__cur = self.__conn.cursor()
        else:
            raise Exception("你TM在逗我？？？")
        super().__init__(self.__db_type, default_limit=default_limit, table_pre=table_pre)

    def __del__(self):
        if self.__cur:
            self.__cur.close()

    def get_conn(self):
        return self.__conn

    def get_cursor(self):
        return self.__cur

    def find(self) -> dict:
        sql, args = super(Database, self).find()
        self.__execute(sql, args)
        row = self.__cur.fetchone()
        if self.__db_type == DbType.MYSQL:
            return row if row else {}
        else:
            if row:
                return func.dict_factory(self.__cur, row)
            else:
                return {}

    def select(self) -> List[dict]:
        sql, args = super(Database, self).select()
        self.__execute(sql, args)
        rows = self.__cur.fetchall()
        if self.__db_type == DbType.MYSQL:
            return rows
        else:
            data = []
            for row in rows:
                data.append(func.dict_factory(self.__cur, row))
            return data

    def insert(self, data: dict):
        sql, args = super(Database, self).insert(data)
        return self.__execute(sql, args)

    def insert_all(self, data: List[dict]):
        sql, args = super(Database, self).insert_all(data)
        return self.__execute(sql, args)

    def update(self, data: dict):
        sql, args = super(Database, self).update(data)
        return self.__execute(sql, args)

    def delete(self):
        sql, args = super(Database, self).delete()
        return self.__execute(sql, args)

    def begin(self):
        self.__execute(Action.BEGIN)

    def commit(self):
        self.__execute(Action.COMMIT)

    def rollback(self):
        self.__execute(Action.ROLLBACK)

    def __execute(self, sql, args=None):
        if args is None:
            args = []
        return self.__cur.execute(sql, args)

    def count(self, field="*"):
        self.field("Count(%s) count" % field)
        count = self.find()
        return count.get('count') if count else 0
