from typing import List

from dbquery import func
from dbquery.const import *
from dbquery.sql_data import SqlData
from dbquery.where import Where


class Query:
    __db_type: DbType
    __default_limit: int

    def __init__(self, db_type=DbType.MYSQL, default_limit=15, table_pre=""):
        self.where = Where(DbTypePlaceholder[db_type])
        self.__db_type = db_type
        self.__default_limit = default_limit
        self._sql_data = SqlData()
        self.__table_pre = table_pre

    __table = None  # 数据库表名
    __table_pre = None  # 数据库表名
    where: Where  # 查询条件
    having: Where  # 查询条件
    _sql_data: SqlData

    # 设置表名
    def table(self, table, alias: str = ""):
        if isinstance(table, str):
            t = "%s%s" % (self.__table_pre, table) if self.__table_pre else table
            self.__table = "%s %s" % (t, alias) if alias else t
        elif isinstance(table, Query):
            sql, args = table.select()
            self.__table = "(%s) %s" % (sql, alias)
            self._sql_data.args = args + self._sql_data.args
        return self

    # 设置字段
    def field(self, field):
        if isinstance(field, str):
            self._sql_data.fields = field.split(',')
        elif isinstance(field, list):
            self._sql_data.fields = field
        return self

    def set_where(self, where):
        self.where.analysis(where)
        return self

    # where条件
    def eq(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.eq(k, v, logic)
        return self

    def neq(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.neq(k, v, logic)
        return self

    def like(self, k: str, v, left=False, right=False, logic=Condition.LOGIC_AND):
        self.where.like(k, v, left, right, logic)
        return self

    def between(self, k: str, start, end, logic=Condition.LOGIC_AND):
        self.where.between(k, start, end, logic)
        return self

    def egt(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.egt(k, v, logic)
        return self

    def gt(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.gt(k, v, logic)
        return self

    def elt(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.elt(k, v, logic)
        return self

    def lt(self, k: str, v, logic=Condition.LOGIC_AND):
        self.where.lt(k, v, logic)
        return self

    # where条件结束

    def join(self, table, condition, join_type=JoinType.INNER):
        self._sql_data.joins.append("%s %s %s %s" % (join_type, table, Condition.ON, condition))
        return self

    def group(self, field):
        if isinstance(field, str):
            self._sql_data.group_by = field.split(',')
        elif isinstance(field, list):
            self._sql_data.group_by = field
        return self

    def having(self, where):
        self.having.analysis(where)
        return self

    def order(self, field):
        if isinstance(field, str):
            self._sql_data.order = field.split(',')
        elif isinstance(field, list):
            self._sql_data.order = field
        return self

    def page(self, page: int, limit: int = False):
        if limit is False:
            if len(self._sql_data.limit) == 1:
                limit = self._sql_data.limit[0]
            else:
                limit = self.__default_limit
        self._sql_data.limit = [(page - 1) * limit, limit]
        return self

    def limit(self, limit: int, forget_page=False):
        if forget_page or len(self._sql_data.limit) != 2:
            self._sql_data.limit = [limit]
        else:
            self._sql_data.limit[1] = limit

    def find(self):
        self.limit(1, True)
        return self.__select()

    # 查找多个
    def select(self):
        return self.__select()

    def __select(self):
        fields = self._sql_data.fields if self._sql_data.fields else ["*"]
        sql = Action.SELECT % (",".join(fields), self.__table, self.__other(SqlType.SELECT))
        args = self._sql_data.args
        self.__reset()
        return sql, args

    # 插入
    def insert(self, data: dict):
        keys = []
        for key, value in data.items():
            keys.append('`%s`' % key)
            self._sql_data.args.append(value)
        value = [DbTypePlaceholder[self.__db_type] for i in range(len(keys))]
        return self.__insert(keys, [",".join(value)])

    def insert_all(self, data: List[dict]):
        # 获取所有的插入的字段
        keys = {*data[0].keys()}
        for i in range(len(keys)):
            keys.update(data[i].keys())
        keys = list(keys)
        for i in range(len(keys)):
            keys[i] = '`%s`' % keys[i]
        # 设置插入值，没有的值用null代替
        values = []
        for value in data:
            values.append([value.get(k) for k in keys])
        return self.__insert(keys, values)

    def __insert(self, keys: list, values: list):
        for i in range(len(values)):
            values[i] = "(%s)" % values[i]
        sql = Action.INSERT % (self.__table, ",".join(keys), ",".join(values))
        args = self._sql_data.args
        self.__reset()
        return sql, args

    # 修改
    def update(self, data: dict):
        sets = []
        for key, value in data.items():
            sets.append(Action.UPDATE_SET % (key, self.__db_type))
            self._sql_data.args.append(value)
        sql = Action.UPDATE % (self.__table, ",".join(sets), self.__other(SqlType.UPDATE))
        args = self._sql_data.args
        self.__reset()
        return sql, args

    def delete(self):
        sql = Action.DELETE % (self.__table, self.__other(SqlType.DELETE))
        args = self._sql_data.args
        self.__reset()
        return sql, args

    def __other(self, t: SqlType) -> str:
        if t == SqlType.SELECT:
            return self.__other_build(
                join=self._sql_data.joins,
                where=self.where,
                group=self._sql_data.group_by,
                having=self.having,
                order=self._sql_data.order,
                limit=self._sql_data.limit,
            )
        elif t in [SqlType.UPDATE, SqlType.DELETE]:
            # 调整limit
            if len(self._sql_data.limit) == 2:
                self._sql_data.limit = [self._sql_data.limit[1]]
            return self.__other_build(
                where=self.where,
                limit=self._sql_data.limit,
            )
        elif t == SqlType.INSERT:
            return ""

    def __other_build(
            self,
            join: list = [],
            where: Where = None,
            group: list = [],
            having: Where = None,
            order: list = [],
            limit: List[int] = []
    ):
        build = [*join]
        if where:
            build = build + [Condition.WHERE, where.build()]
            self._sql_data.args = self._sql_data.args + where.get_replaces()
        if group:
            build = build + [Condition.GROUP, ",".join(group)]
            if having:
                build = build + [Condition.HAVING, having.build()]
                self._sql_data.args = self._sql_data.args + having.get_replaces()
        if order:
            build = build + [Condition.ORDER, func.join(order)]
        if limit:
            build = build + [Condition.LIMIT, func.join(limit)]
        return " ".join(build)

    def __reset(self):
        self.where = Where(DbTypePlaceholder[self.__db_type])
        self.having = Where(DbTypePlaceholder[self.__db_type])
        self._sql_data = SqlData()
