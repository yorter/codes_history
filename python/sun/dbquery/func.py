import sqlite3
from typing import TypeVar, List

T = TypeVar('T')


def join(data: list, seq=","):
    lc = []
    for item in data:
        lc.append(str(item))
    return seq.join(lc)


def dict_factory(cursor: sqlite3.Cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def orm_list(data: List[dict], t: T) -> List[T]:
    result = []
    for d in data:
        result.append(orm(d, t))
    return result


def orm(data: dict, t: T) -> T:
    td = t()
    for k in data.keys():
        if hasattr(td, k):
            setattr(td, k, data[k])
    return td
