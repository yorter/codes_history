from enum import Enum, unique


class Condition:
    """
    一些条件常量
    """
    WHERE = "WHERE"
    WHERE_EQ = "="
    WHERE_NEQ = "<>"
    WHERE_EGT = ">="
    WHERE_ELT = "<="
    WHERE_GT = ">"
    WHERE_LT = "<"
    WHERE_LIKE = "LIKE"
    WHERE_BETWEEN = "BETWEEN"
    WHERE_NOT_BETWEEN = "NOT BETWEEN"
    WHERE_IN = "IN"
    WHERE_NOT_IN = "NOT IN"
    LOGIC_AND = "AND"
    LOGIC_OR = "OR"
    ON = "ON"
    LIMIT = "LIMIT"
    GROUP = "GROUP BY"
    HAVING = "HAVING"
    ORDER = "ORDER BY"


@unique
class DbType(Enum):
    """
    数据库类型,字段为占位符的标志
    """
    MYSQL = "%s"
    SQLITE = "?"


DbTypePlaceholder = {
    DbType.MYSQL: "%s",
    DbType.SQLITE: "?",
}


class JoinType:
    LEFT = "LEFT JOIN"
    RIGHT = "RIGHT JOIN"
    INNER = "INNER JOIN"


@unique
class SqlType(Enum):
    SELECT = 1,
    INSERT = 2,
    UPDATE = 3,
    DELETE = 4,


class Action:
    SELECT = "SELECT %s FROM %s %s"
    INSERT = "INSERT INTO `%s` (%s) VALUES %s"
    UPDATE = "UPDATE `%s` SET %s %s"
    DELETE = "DELETE FROM %s %s"
    UPDATE_SET = "`%S`= ?"
    BEGIN = "BEGIN"
    ROLLBACK = "ROLLBACK"
    COMMIT = "COMMIT"
