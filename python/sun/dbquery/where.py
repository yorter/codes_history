from dbquery.const import DbType, Condition, DbTypePlaceholder
from dbquery.query import func


class Where:
    # 占位符
    __placeholder: str
    # 条件存储
    _where = []

    def __init__(self, placeholder=DbTypePlaceholder[DbType.MYSQL]):
        self.__placeholder = placeholder
        self._where = []
        self.__analysis = None
        self.__replace_values = []

    __analysis = None

    def analysis(self, where):
        if where:
            if self.__analysis is None:
                self.__analysis = WhereAnalysis(self)
            self.__analysis.analysis(where)

    def eq(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_EQ, self.__placeholder, logic, [v]])
        return self

    def neq(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_NEQ, self.__placeholder, logic, [v]])
        return self

    def like(self, k: str, v, left=False, right=False, logic=Condition.LOGIC_AND):
        if left:
            v = "%" + v
        if right:
            v = v + "%"
        self._where.append([k, Condition.WHERE_LIKE, self.__placeholder, logic, [v]])
        return self

    def between(self, k: str, start, end, logic=Condition.LOGIC_AND):
        self._where.append([
            k, Condition.WHERE_BETWEEN, self.__placeholder, Condition.LOGIC_AND, self.__placeholder, logic,
            [start, end]
        ])
        return self

    def egt(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_EGT, self.__placeholder, logic, [v]])
        return self

    def gt(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_GT, self.__placeholder, logic, [v]])
        return self

    def elt(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_ELT, self.__placeholder, logic, [v]])
        return self

    def lt(self, k: str, v, logic=Condition.LOGIC_AND):
        self._where.append([k, Condition.WHERE_LT, self.__placeholder, logic, [v]])
        return self

    def condition_in(self, condition, k, v: list, logic=Condition.LOGIC_AND):
        vl = len(v)
        vs = [self.__placeholder for i in range(vl)]
        self._where.append([
            k, condition,
            "(%s)" % func.join(vs),
            logic, v
        ])
        return self

    def where_in(self, k, v: list, logic=Condition.LOGIC_AND):
        self.condition_in(Condition.WHERE_IN, k, v, logic)

    def where_not_in(self, k, v: list, logic=Condition.LOGIC_AND):
        self.condition_in(Condition.WHERE_NOT_IN, k, v, logic)

    def condition_between(self, condition, k, v: list, logic=Condition.LOGIC_AND):
        self._where.append([k, condition, v[0], Condition.LOGIC_AND, v[1], logic, v])
        return self

    def where_between(self, k, v: list, logic=Condition.LOGIC_AND):
        self.condition_between(Condition.WHERE_BETWEEN, k, v, logic)

    def where_not_between(self, k, v: list, logic=Condition.LOGIC_AND):
        self.condition_between(Condition.WHERE_NOT_BETWEEN, k, v, logic)

    def build(self):
        build_list = []
        for w in self._where:
            first = w[0]
            if isinstance(first, Where):
                build_list.append([w[-1], "(", w.build(), ")"])
                self.__replace_values = self.__replace_values + w.get_replaces()
            elif isinstance(w, list):
                build_list.append([w[-2], func.join(w[0:-2], " ")])
                self.__replace_values = self.__replace_values + w[-1]
        # 删除第一个逻辑判断
        if len(build_list) >= 1:
            del build_list[0][0]
        where_list = []
        for w in build_list:
            where_list.append(" ".join(w))
        return " ".join(where_list)

    # 占位符对应的值
    __replace_values = []

    def get_replaces(self):
        return self.__replace_values

    def __bool__(self):
        return bool(self._where)


class WhereAnalysis:
    __where_obj: Where

    def __init__(self, where_obj: Where):
        self.__where_obj = where_obj

    def analysis(self, where):
        if isinstance(where, list):
            self.is_list(where)
        elif isinstance(where, dict):
            self.is_dict(where)

    def is_list(self, where: list):
        for w in where:
            self.list_line(w)

    def is_dict(self, where: dict):
        for k in where.keys():
            v = where[k]
            type_v = type(v)
            if type_v in [str, float, int]:
                self.__where_obj.eq(k, v)
            elif isinstance(v, list):
                self.list_line([k, *v])
        return self

    __NORMAL = [
        Condition.WHERE_EQ, Condition.WHERE_NEQ,
        Condition.WHERE_LT, Condition.WHERE_ELT,
        Condition.WHERE_GT, Condition.WHERE_EGT,
        Condition.WHERE_LIKE,
    ]

    def list_line(self, args: list):
        ll = len(args)
        if ll < 3:
            raise Exception("where参数解析错误")
        k = args[0]
        condition = str.upper(args[1])
        value = args[2]
        value_type = type(args[2])
        if condition in self.__NORMAL and value_type in [str, float, int]:
            logic = Condition.LOGIC_AND if ll >= 4 else args[3]
            if condition == Condition.WHERE_EQ:
                self.__where_obj.eq(k, value, logic)
            elif condition == Condition.WHERE_NEQ:
                self.__where_obj.neq(k, value, logic)
            elif condition == Condition.WHERE_LT:
                self.__where_obj.lt(k, value, logic)
            elif condition == Condition.WHERE_ELT:
                self.__where_obj.elt(k, value, logic)
            elif condition == Condition.WHERE_GT:
                self.__where_obj.gt(k, value, logic)
            elif condition == Condition.WHERE_EGT:
                self.__where_obj.egt(k, value, logic)
            elif condition == Condition.WHERE_EGT:
                self.__where_obj.like(k, value, logic=logic)
        elif condition in [Condition.WHERE_BETWEEN, Condition.WHERE_NOT_BETWEEN]:
            logic = Condition.LOGIC_AND if ll >= 4 else args[3]
            self.__where_obj.condition_between(condition, k, value, logic)
        elif condition in [Condition.WHERE_IN, Condition.WHERE_NOT_IN]:
            logic = Condition.LOGIC_AND if ll >= 4 else args[3]
            if isinstance(value, str):
                v = value.split(',')
                if len(value) == 1:
                    self.__where_obj.eq(k, v, logic)
                    return
                else:
                    value = v
            self.__where_obj.condition_in(condition, k, value, logic)
        elif condition == Condition.WHERE_LIKE and value_type == str:
            logic = Condition.LOGIC_AND if ll >= 4 else args[3]
            self.__where_obj.like(k, value, logic=logic)
