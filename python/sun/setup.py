from setuptools import setup, find_packages

setup(
    name="阳光置业值班工具",
    description="阳光置业值班工具",
    long_description="阳光置业值班工具（已取消）",
    author="饿魔",
    author_email="yorter@126.com",

    packages=find_packages(),
    include_package_data=True,
    platforms="windows",
    install_requires=[
        'pymysql',
        'openpyxl',
        'prettytable',
        'DBUtils',
    ],

    scripts=[],
    entry_points={
        'console_scripts': [
            'test = test.help:main'
        ]
    }
)
