from prettytable import PrettyTable
from typing import List


def flush_print(s: str):
    """
    刷新打印
    :param s:要打印的内容
    :return:None
    """
    print("\r%s" % s, end="", flush=True)


def error(s: str):
    print("\033[31m%s\033[0m" % s)


def print_table(t_head: dict, t_body: List[dict], title=""):
    """
    打印表格
    :param title: 标题
    :param t_head:表头，字典格式 key:字段key value:字段描述
    :param t_body:
    :return:
    """
    print('')
    if title:
        print(title)
    field_keys = t_head.keys()
    field_names = []
    for k in field_keys:
        field_names.append(t_head.get(k))
    tb = PrettyTable()
    tb.field_names = field_names
    for td in t_body:
        row = []
        for f in field_keys:
            row.append(td.get(f))
        tb.add_row(row)
    print(tb)
