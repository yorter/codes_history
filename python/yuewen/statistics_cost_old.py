import json
import time

import redis

from config.dbconfig import redis_config
from core.mylibs.mysql import Mysql
from core.mylibs.table import Table

near_link_num = 30
db = Mysql()
redis_config['db'] = 7
redis_obj = redis.Redis(**redis_config)
# links = db.table('bookinfov2').where({
#     'cost_amount': ['>=', 5000],
# }).select()
links = db.query("""
        SELECT
            book_id,
            `name`,
            SUM( cost_amount ) cost_amount,
            MIN( create_time ) create_time,
            pid 
        FROM
            bookinfov2 
        WHERE
            tui = 1 
        GROUP BY
            book_id 
        HAVING
            cost_amount >= 5000
""")

link_infos_sql = """
    SELECT
        * 
    FROM
        (
        SELECT
            id,
            book_info_id,
            profit_rate ,
            `timestamp`,
            paid_user_count
        FROM
            %s 
        WHERE
            book_info_id IN ( SELECT id FROM bookinfov2 WHERE book_id = %s AND tui=1 ) 
        ORDER BY
            id DESC 
            LIMIT 100 
        ) bookdata
        LEFT JOIN bookinfov2 ON bookinfov2.id = bookdata.book_info_id
"""
near_link_sql = """
    SELECT
        b.profit_rate,
        b.paid_amount,
        b.member_count,
        bv.id,
        bv.linkdesc_id,
        bv.create_time,
        bv.pid,
        b.`timestamp`,
        b.paid_user_count,
        cost_amount,
        p.`name` pname,
        a.`name` aname 
    FROM
        %s b
        LEFT JOIN bookinfov2 bv ON bv.id = b.book_info_id
        LEFT JOIN public p ON p.id = bv.public_id
        LEFT JOIN admin a ON a.uid = p.belongto 
    WHERE
        book_info_id = %s 
        AND `hour` = 0 
        AND `minute` = 0 
    ORDER BY
        b.id DESC 
        LIMIT %s
"""
table_name = Table.get_table_name(time.time())

t = time.localtime()
t = time.localtime(time.time() - (t.tm_mday + 1) * 24 * 3600)
last_month_table = Table.get_table_name(time.mktime(t))
# print(last_month_table)
# exit()
for link in links:
    # print(type(link['cost_amount']))
    # exit()
    book_id = link['book_id']
    link['cost_amount'] = int(link['cost_amount'])
    sql = link_infos_sql % (table_name, book_id)
    infos = db.query(sql)
    data = []
    if len(infos) >= 100:
        data = infos[:100]
    else:
        data = infos
        sql = link_infos_sql % (last_month_table, book_id)
        last_infos = db.query(sql)
        left_count = 100 - len(infos)
        if last_infos:
            data.extend(last_infos[:left_count])
    redis_obj.zadd('Book_Statistics_Cost_Zset', {book_id: link['cost_amount']})
    redis_obj.set("Book_Statistics_Cost_Json_%s" % book_id, json.dumps(data))
    redis_obj.hset('Book_Statistics_Cost_Hash', book_id, json.dumps(link))
    # 查询小说的所有链接
    book_links = db.table('bookinfov2').where({
        'book_id': book_id
    }).select()
    link_near_infos = []
    for book_link in book_links:
        book_info_id = book_link['id']
        the_link_near_infos = list(db.query(near_link_sql % (table_name, book_info_id, near_link_num)))
        if len(the_link_near_infos) < near_link_num:
            the_last_link_near_infos = db.query(near_link_sql % (last_month_table, book_info_id, near_link_num))
            if the_last_link_near_infos:
                the_link_near_infos.extend(the_last_link_near_infos[:(near_link_num - len(the_link_near_infos))])
        if len(the_link_near_infos) >= 1:
            link_near_infos.append(the_link_near_infos)
        # print(link_near_infos)
        # exit()
    redis_obj.set("Book_Statistics_NearProfitRate_Json_%s" % book_id, json.dumps(link_near_infos))
