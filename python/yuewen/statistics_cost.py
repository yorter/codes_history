import json
import math
import time

import redis

from config.dbconfig import redis_config
from core.mylibs.mysql import Mysql
from core.mylibs.table import Table

near_link_num = 30
db = Mysql()
redis_config['db'] = 7
redis_obj = redis.Redis(**redis_config)
# links = db.table('bookinfov2').where({
#     'cost_amount': ['>=', 5000],
#     'tui': 1,
# }).select()
sql = """
    SELECT
        bi.id,
        bi.pid,
        bi.paid_user_count,
        bi.paid_amount,
        linkdesc_id,
        create_time,
        book_id,
        bi.`name`,
        linkdesc,
        cost_amount,
        p.`name` pname,
        a.`name` aname 
    FROM
        bookinfov2 bi
        LEFT JOIN public p ON p.id = bi.public_id
        LEFT JOIN admin a ON a.uid = p.belongto 
    WHERE
        tui = 1 
        AND cost_amount > 5000
"""
links = db.query(sql)


# print(links)
# exit()


def get_next_month(t):
    local = time.localtime(t)
    y = local.tm_year + 1 if local.tm_mon == 12 else local.tm_year
    m = local.tm_mon + 1 if local.tm_mon != 12 else 1
    m = m if m >= 10 else "0%s" % m
    d = "01"
    dt = "%s-%s-%s" % (y, m, d)
    return time.mktime(time.strptime(dt, '%Y-%m-%d'))


for link in links:
    link_data = []
    link_id = link['linkdesc_id']
    cost = link['cost_amount']
    # if int(link_id) != 13565824:
    #     continue
    print(link_id)
    # 判断创建时间
    start_time = create_time = link['create_time']
    if (create_time == 0) or (create_time < 1590940800):
        start_time = 1590940800
    while start_time < time.time():
        table_name = Table.get_table_name(start_time)
        data = db.table(table_name).where({
            'book_info_id': link['id'],
            'hour': 0,
            'minute': 0,
        }).select()
        if len(data):
            for i in range(0, len(data)):
                paid_amount = data[i]['paid_amount']
                if paid_amount > 0:
                    try:
                        profit_rate = round(paid_amount / cost * 100, 2)
                    except:
                        profit_rate = 0
                else:
                    profit_rate = 0
                # catch_timestamp = data[i]['timestamp']
                # catch_date = time.strftime('%Y-%m-%d', time.localtime(catch_timestamp))
                # # print(catch_date)
                # # 查询成本
                # cost_data = db.table('link_cost').where({
                #     'linkdesc_id': link_id,
                #     'date': ['<=', catch_date],
                # }).order_by('date desc').limit(1).select()
                # # print(cost_data)
                # if cost_data:
                #     cost = cost_data[0]['cost']
                #     if paid_amount > 0:
                #         try:
                #             profit_rate = round(paid_amount / cost * 100, 2)
                #         except:
                #             profit_rate = 0
                #     else:
                #         profit_rate = 0
                # else:
                #     profit_rate = data[i]['profit_rate']
                link_data.append(profit_rate)
            if len(link_data) >= 30:
                link_data = link_data[:30]
                break
        start_time = get_next_month(start_time)
    if link_data:
        if db.table('statistics_cost').where({'book_info_id': link['id']}).find():
            db.table('statistics_cost').where({'book_info_id': link['id']}).update({
                'data': json.dumps(link_data),
            })
        else:
            db.table('statistics_cost').where({'book_info_id': link['id']}).insert({
                'data': json.dumps(link_data),
                'book_info_id': link['id'],
            })
    # exit(link_data)
    # redis_obj.zadd('Book_Statistics_link_Cost_Zset', {link_id: link['create_time']})
    # redis_obj.set("Book_Statistics_link_Cost_Json_%s" % link_id, json.dumps(link_data))
    # redis_obj.hset('Book_Statistics_link_Cost_Hash', link_id, json.dumps(link))
