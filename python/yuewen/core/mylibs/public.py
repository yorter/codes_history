import time
import hashlib
from config.app import config as app_config


# 打印当前时间以及操作内容
def timeprint(info, filename="", line=""):
    # if not hasattr(timeprint, 'debugmod'):
    #     redisobj = redis.Redis(host=redis_config['hostname'], port=redis_config['port'], decode_responses=True,
    #                            db=redis_config['db'])
    #     if redisobj.exists(redis_config['debug']) and int(redisobj.get(redis_config['debug'])):
    #         timeprint.debugmod = True
    #     else:
    #         timeprint.debugmod = False
    if app_config['debug']:
        localtime = time.localtime(time.time())
        nowtime = time.strftime('%Y-%m-%d %H:%M:%S', localtime)
        if filename:
            if line:
                print('[ %s ] %s (%s:%s)' % (nowtime, info, filename, line))
            else:
                print('[ %s ] %s (%s)' % (nowtime, info, filename))
        else:
            print('[ %s ] %s' % (nowtime, info))


# md5加密
def md5(s):
    md5obj = hashlib.md5()
    md5obj.update(s.encode('utf8'))
    return md5obj.hexdigest()
