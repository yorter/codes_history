from copy import copy
import pymysql
from config import dbconfig


# MySQL
class Mysql:
    def __init__(self, database: str = dbconfig.database):
        # 连接mysql
        config = copy(dbconfig.mysql_config)
        config['database'] = database
        coupons_db = pymysql.connect(**config)
        self.connect = coupons_db
        # 使用cursor()方法获取操作游标
        self.db = coupons_db.cursor(cursor=pymysql.cursors.DictCursor)  # 返回字典格式而非列表
        self.database = database

    connect = None  # 数据库连接对象
    db = None  # 数据库游标对象
    database = None  # 数据库库名
    tablename = None  # 数据库表名
    wherestr = None  # 查询条件
    fields = '*'  # 查询字段
    args = {}  # 查询字段
    last_sql = None
    order = None
    limitnum = None

    # 设置表名
    def table(self, table: str):
        """
        设置表名
        :param table:查询表名
        :return:
        """
        self.tablename = table
        return self

    # 设置字段
    def field(self, field):
        self.fields = field
        return self

    # where
    def where(self, where):
        # 设置查询条件
        where_condition = []
        for k in where.keys():
            if isinstance(where[k], list):
                condition = where[k][0]
                value = where[k][1]
            else:
                condition = '='
                value = where[k]
            if condition == 'between':
                value = ' %s and %s' % (value[0], value[1])
            else:
                if isinstance(value, str):
                    # value = '"%s"' % value
                    pass
                elif isinstance(value, list):
                    list_value = ''
                    for v in value:
                        if isinstance(v, int) or isinstance(v, float):
                            list_value = '%s,%s' % (list_value, v)
                        else:
                            list_value = '%s,"%s"' % (list_value, v)
                    value = '(%s)' % list_value.strip(',')
                else:
                    pass
            where_condition.append('`%s` %s ' % (k, condition) + '%(' + k + ')s')
            self.args[k] = value
        self.wherestr = 'WHERE %s' % ' AND '.join(where_condition)
        return self

    def order_by(self, order: str):
        self.order = order
        return self

    def limit(self, limit):
        self.limitnum = limit
        return self

    # 查找单个
    def find(self):
        sql = 'SELECT %s FROM `%s`.`%s`  %s LIMIT 1' % (
            self.fields, self.database, self.tablename, self.wherestr
        )
        rel = self.execute(sql)
        if rel:
            return self.db.fetchone()
        else:
            return {}

    # 查找多个
    def select(self):
        sql = 'SELECT %s FROM `%s`.`%s`  %s' % (
            self.fields, self.database, self.tablename, self.wherestr
        )
        if self.order:
            sql = ' %s order by %s' % (sql, self.order)

        if self.limitnum:
            sql = ' %s limit %s' % (sql, self.limitnum)

        # print(sql)
        # exit()
        rel = self.execute(sql)
        if rel:
            return self.db.fetchall()
        else:
            return []

    # 插入
    def insert(self, data={}):
        keys = []
        orgin_keys = []
        for key, value in data.items():
            keys.append('%s(%s)s' % ('%', key))
            orgin_keys.append('`%s`' % key)
            self.args[key] = value
        sql = 'INSERT INTO `%s`.`%s` (%s) VALUES (%s)' % (
            self.database, self.tablename, ','.join(orgin_keys), ','.join(keys)
        )
        self.execute(sql)

    # 获取最后自动增长的id
    def lastrowid(self):
        return self.db.lastrowid

    # 修改
    def update(self, data={}):
        keys = []
        set_strs = []
        for key, value in data.items():
            keys.append('%s(%s)s' % ('%', key))
            set_strs.append('`%s`=%s(%s)s' % (key, '%', key))
            self.args[key] = value
        sql = "UPDATE `%s`.`%s` SET %s %s" % (
            self.database, self.tablename, ','.join(set_strs), self.wherestr
        )
        self.execute(sql)

    def query(self, sql: str, args: dict = None):
        """
        自定义查询sql语句
        :param sql:sql语句
        :param args:替换参数
        :return:查询结果
        """
        self.args = args
        self.execute(sql)
        return self.db.fetchall()

    def table_exist(self, table):
        sql = "select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='%s' AND TABLE_NAME='%s'" % (
            self.database, table
        )
        rel = self.execute(sql)
        return True if rel else False

    # 执行
    def execute(self, query):
        self.last_sql = query
        # 重连
        self.connect.ping(reconnect=True)
        rel = self.db.execute(query, self.args)
        self.reset_param()
        return rel

    # 提交
    def commit(self):
        """
        提交
        :return:
        """
        self.connect.commit()

    # 回滚
    def rollback(self):
        """
        回滚
        :return:
        """
        try:
            self.connect.rollback()
        except:
            pass

    # 重置参数
    def reset_param(self):
        """
        重置参数
        :return:
        """
        self.wherestr = None
        self.fields = '*'
        self.args = {}
        self.order = False
        self.limitnum = False

    def close(self):
        self.connect.close()
