import os
import time
import sys
from core.mylibs import file
from core.mylibs.public import timeprint

"""生成日志的方法"""


class Log:
    # 日志目录路径
    logDir = '%s/log' % sys.path[0]

    # 初始化日志
    @classmethod
    def initlogs(cls):
        timeprint('正在初始化日志')
        # 检查日志目录知否存在
        if not os.path.isdir(cls.logDir):
            os.mkdir(cls.logDir)
        # 删除pid目录
        pid_dir = '%s/pid' % cls.logDir
        if os.path.exists(pid_dir) and os.path.isdir(pid_dir):
            # 删除pid目录下所有文件
            files = os.listdir(pid_dir)
            for f in files:
                file_path = '%s/%s' % (pid_dir, f)
                os.remove(file_path)
        else:
            os.mkdir(pid_dir)

    # 写入pid文件
    @classmethod
    def pid(cls, name, p):
        with open('%s/pid/%s' % (cls.logDir, name), mode='w', encoding='utf8') as f:
            f.write(str(p))

    # 写入日志
    @classmethod
    def write(cls, logtype='db', errorinfo='未知错误'):
        # 检查类型目录是否存在
        logtypedir = '%s/%s' % (cls.logDir, logtype)
        if not os.path.exists(logtypedir):
            os.mkdir(logtypedir)
        # 检查当日日志文件
        localtime = time.localtime(time.time())
        today = time.strftime('%Y-%m-%d', localtime)
        today_time = time.strftime('%H:%M:%S', localtime)
        file_path = '%s/%s' % (logtypedir, today)
        writestr = '[ %s ]  %s%s' % (today_time, errorinfo, os.linesep)
        if not os.path.exists(file_path):
            # 生成空内容日志文件
            file.write(file_path, writestr)
        else:
            # 写入日志文件
            file.append(file_path, writestr)
