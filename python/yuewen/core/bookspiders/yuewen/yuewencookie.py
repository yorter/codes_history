from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
import time


class YuewenCookie:
    def __init__(self, username: str, password: str):
        # self.browser = webdriver.Firefox()
        self.username = username
        self.password = password
        opt = Options()
        opt.add_argument('--headless')
        # opt.add_argument('blink-settings=imagesEnabled=false')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(options=opt)
        self.wait = WebDriverWait(self.browser, 10)

    username = None
    password = None
    browser = ''
    wait = ''
    index_url = "https://open.yuewen.com/pub/index/index.html"

    def get_cookie(self) -> [dict]:
        self.init_selenium()
        rel = self.move()
        if rel:
            cookie = self.browser.get_cookies()
        else:
            cookie = {}
        self.browser.quit()
        return cookie

    def init_selenium(self):
        """
        初始化自动化
        :return:
        """
        self.browser.get("https://open.yuewen.com/pub/public/login.html")
        self.browser.find_element_by_id('accountInput').send_keys(self.username)
        self.browser.find_element_by_id('passwordInput').send_keys(self.password)
        self.browser.find_element_by_id('login-btn').click()
        print('输入账号密码完毕，等待验证码')
        self.wait.until(ec.presence_of_element_located((By.ID, "tcaptcha_iframe")))
        # 等待背景图frame
        self.browser.switch_to.frame('tcaptcha_iframe')
        # 等待背景图
        self.wait.until(ec.presence_of_element_located((By.ID, "slideBlock")))
        time.sleep(3)

    retry_times = 0

    max_retry_times = 5

    tcaptcha_drag_thumb = None

    def move(self):
        self.tcaptcha_drag_thumb = self.browser.find_element_by_id("tcaptcha_drag_thumb")
        for i in range(self.max_retry_times):
            times = i + 1
            print('开始第%s次拖动验证码' % times)
            if self.move_offset():
                return True
        return False

    step = 20

    def move_offset(self):
        offset = 180
        for i in range(3):
            try:
                ActionChains(self.browser).click_and_hold(self.tcaptcha_drag_thumb).perform()
            except StaleElementReferenceException:
                return False
            for i in range(0, offset, self.step):
                ActionChains(self.browser).move_by_offset(xoffset=self.step, yoffset=0).perform()
                time.sleep(0.1)
            ActionChains(self.browser).release().perform()
            time.sleep(3)
            offset += 20
            if self.browser.current_url == self.index_url:
                return True
        return False
