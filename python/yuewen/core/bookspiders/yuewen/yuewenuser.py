import re
import time
from copy import copy

import redis
import requests
from pyquery import PyQuery

from config.dbconfig import redis_config
from core.bookspiders.yuewen.yuewencookie import YuewenCookie
from core.data.bookdata import BookData
from core.mylibs.mysql import Mysql


class YuewenUser:
    def __init__(self, user: dict, catch_type: int):
        self.catch_type = catch_type
        self.user = user
        self._data = BookData()
        self.last_catch_time_key = 'yuewen_last_catch_time'
        last_catch_time = self.redis.hget(self.last_catch_time_key, user['username'])
        self.last_catch_time = last_catch_time if last_catch_time else ''
        self.catch_full()

    redis = redis.Redis(**redis_config)
    catch_type = 0

    user = None
    index_url = "https://open.yuewen.com/pub/index/index.html"
    session = requests.session()
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Host': 'open.yuewen.com',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        # 'Referer': 'https://open.yuewen.com/pub/public/login.html',
        'User-Agent': None,
    }

    def catch_full(self):
        """
        判断是否需要采集全（最小时间戳）
        :return:
        """
        key_name = 'yuewen_last_summary_time'
        now_time = int(time.time())
        # 凌晨时间戳
        day_time = now_time - (now_time - time.timezone) % 86400
        # 七天前凌晨时间戳
        self.seven_days_ago = day_time - 7 * 24 * 3600
        # 上次总结的时间
        last_summary_time = self.redis.get(key_name)
        if last_summary_time is None:
            self.get_fans_limit = 0
            self.mark_get_fans = True
            # 没有记录过上次总结时间，则使用当天凌晨的七天前
            self.redis.set(key_name, self.seven_days_ago)
        else:
            last_summary_time = int(last_summary_time)
            print('last_summary_time:%s' % last_summary_time)
            local_time = time.localtime()
            print((local_time.tm_wday == 0) and (self.seven_days_ago >= last_summary_time))
            self.get_fans_limit = last_summary_time
            # 周一且超过上次采集时间七天(目的是为了仅限在周一执行)
            if (local_time.tm_wday == 0) and (self.last_catch_time != time.strftime('%Y-%m-%d')):
                self.mark_get_fans = True
                self.redis.set(key_name, self.seven_days_ago)
            else:
                self.mark_get_fans = False
        self.redis.hset(self.last_catch_time_key, self.user['username'], time.strftime('%Y-%m-%d'))

    get_fans_limit = 0
    # 七日内的新增粉丝数
    seven_days_ago = 0

    # 上次脚本采集时间
    last_catch_time = ''

    mark_get_fans = False

    cookie = {}

    public_users = []

    _data = None

    find_book_name = re.compile('《(.+)》')

    find_amount = re.compile('(\d+\.\d+)')
    find_count = re.compile('(\d+)')

    def start(self):
        print('开始登陆-%s' % self.user['username'])
        yuewen_cookie = YuewenCookie(self.user['username'], self.user['password'])
        agent = yuewen_cookie.browser.execute_script("return navigator.userAgent")
        self.headers['User-Agent'] = agent
        cookies = yuewen_cookie.get_cookie()
        if cookies:
            print("获取%s cookie成功" % self.user['username'])
            self.cookie = {}
            for cookie in cookies:
                self.cookie[cookie["name"]] = cookie['value']
                # add_cookie.append('%s=%s' % (cookie["name"], cookie['value']))
            if self.catch_type == 1:
                self.save_tongji_day()
            elif self.catch_type == 2:
                header = copy(self.headers)
                header['Referer'] = 'https://open.yuewen.com/pub/public/login.html'
                req = self.session.get(url=self.index_url, cookies=self.cookie, headers=header)
                pq = PyQuery(req.text)
                doc = pq("#appids option")
                for option in doc.items():
                    value = option.attr('value')
                    if int(value) == -1:
                        continue
                    name = option.text()
                    self.public_users.append((name, value))
                    # break
                self.get_users_data()

    def save_tongji_day(self):
        """
        保存日充值
        :return:
        """
        db = Mysql('repdmeecn')
        sql = 'update tongji_day set recharge_money=%s WHERE wx_id in (SELECT id FROM wx_config WHERE `name`="%s") AND days = %s'
        date_format = '%Y-%m-%d'
        yesterday_timestamp = int(
            time.mktime(
                time.strptime(time.strftime(date_format, time.localtime(time.time() - 24 * 3600)), date_format)
            )
        )
        # 获取总汇信息
        current_page = 1
        last_page = 1
        url = 'https://open.yuewen.com/service/_statistics/wxfx/p/%s.html'
        while current_page <= last_page:
            req = self.session.get(url % current_page, headers=self.headers, cookies=self.cookie)
            doc = PyQuery(req.text)
            # 获取最后一页
            if current_page == 1:
                max_page = doc('.pagination span').attr('maxpagenum')
                if max_page:
                    last_page = int(max_page)
            # 获取信息
            trs = doc('.table tbody tr').items()
            for tr in trs:
                name = tr.find('td').eq(1).text()
                value = self.find_amount.search(tr.find('td').eq(4).text()).group(1)
                true_sql = sql % (value, name, yesterday_timestamp)
                db.query(true_sql)
                db.commit()
            current_page += 1
        db.close()

    current_page = 1
    last_page = 1
    current_public_name = ''

    def get_users_data(self):
        """
        获取公众号信息
        :return:
        """
        for user in self.public_users:
            # 检查公众号是否存在
            self._data.check_public({
                'name': user[0],
                'pid': 5,
            })
            # 切换app
            self.current_public_name = user[0]
            self.change_app(user[1])
            self.current_page = 1
            self.last_page = 1
            while self.current_page <= self.last_page:
                self.get_page()
                self.current_page += 1
            # break

            # break

    def get_page(self):
        """
        获取当前页信息
        :return:
        """
        print('正在获取%s-%s第%s页' % (self.user['username'], self.current_public_name, self.current_page))
        url = 'https://open.yuewen.com/service/_wechat_statistics/promotionlist/type/2/p/%s.html' % self.current_page
        req = self.session.get(url, headers=self.headers, cookies=self.cookie)
        doc = PyQuery(req.text)
        if self.current_page == 1:
            maxpagenum = doc('.pagination span').attr('maxpagenum')
            if maxpagenum:
                self.last_page = int(maxpagenum)
        trs = doc('.table tbody tr').items()
        for tr in trs:
            try:
                t = tr.find('td').eq(3).text()
                # 小说名
                try:
                    self._data.book_name = self.find_book_name.search(t).group(1)
                except AttributeError:
                    self._data.book_name = t
                time_format = tr.find('td').eq(2).text()
                self._data.create_time = int(time.mktime(time.strptime(time_format, '%Y-%m-%d %H:%M:%S')))
                # 小说id
                self.get_book_id()
                # 链接id
                self._data.linkdesc_id = tr.find('td').eq(0).text()
                # 链接名
                self._data.linkdesc = tr.find('td').eq(1).text().strip().strip('&nbsp;')
                print('《%s》%s--%s' % (self._data.book_name, self._data.linkdesc, self._data.linkdesc_id))
                self._data.tui = 1
                self._data.save_bookinfov2()
                # 信息
                # 新增阅读
                self._data.member_count = tr.find('td').eq(8).text()
                # 关注
                # self._data.subscribe_count = tr.find('td').eq(9).text()
                # 充值金额
                self._data.paid_amount = self.find_amount.search(tr.find('td').eq(10).text()).group(1)
                # 关注率(pass)
                # 充值比数
                self._data.paid_count = self.find_count.search(tr.find('td').eq(11).text()).group(1)
                # 点击数
                data_url = tr.find('td').eq(14).find('a').eq(0).attr('href')
                # 获取需要增加的值增加
                self.get_plus_data(data_url)
                self._data.clicks = tr.find('td').eq(7).text()
                self._data.save_bookv2()
            except:
                self._data.reset_db()

    def get_plus_data(self, url):
        f = '%Y-%m-%d'
        # 获取数据库中记录的值
        old_data = self._data.get_link_date_info()
        # 数据库存储时间
        old_date_time = 0
        # 数据库存储的粉丝数（到前天的）
        old_date_member_count = 0
        # 数据库存储的充值用户数（到前天的）
        old_date_paid_user_count = 0
        if old_data:
            # 初始化数据库存储的数据
            old_date_time = int(time.mktime(time.strptime(str(old_data['date']), f)))
            old_date_member_count = old_data['member_count']
            old_date_paid_user_count = old_data['paid_user_count']
        # 当前页
        current_page = 1
        # 最后一页
        last_page = 1
        # 用户更新记录的值
        member_count = old_date_member_count  # 粉丝数(总的)
        paid_user_count = old_date_paid_user_count  # 充值人数(总的)
        # 当前时间
        current_time = time.time()
        # 今日日期
        current_date = time.strftime('%Y-%m-%d', time.localtime())
        # 保存时间
        save_info_time = current_time - 2 * 24 * 3600
        # 保存日的date yyyy-mm-dd
        save_info_date = time.strftime(f, time.localtime(save_info_time))
        # 保存日的时间戳
        save_info_time = int(time.mktime(time.strptime(save_info_date, f)))
        while current_page <= last_page:
            full_url = 'https://open.yuewen.com/%s/p/%s.html' % (url, current_page)
            req = self.session.get(full_url, headers=self.headers, cookies=self.cookie)
            doc = PyQuery(req.text)
            if current_page == 1:
                last_page = int(self.last_page_zz.search(req.text).group(1))
            trs = doc('.table.table-bordered.table-hover tbody tr')
            for tr in trs.items():
                date_str = tr.find('td').eq(0).text().strip()
                t = int(time.mktime(time.strptime(date_str, f)))
                tds = tr.find('td')
                # 判断今日时间
                if date_str == current_date:
                    # 今日充值
                    try:
                        self._data.today_paid_amount = float(tr.find('td').eq(4).text())
                    except ValueError:
                        self._data.today_paid_amount = 0
                else:
                    # 非当日
                    if t > old_date_time:
                        # 如果标签时间大于存储时间，则开始记录
                        try:
                            # 新用户数
                            new_add_member_count = int(tr.find('td').eq(9).text())
                        except:
                            new_add_member_count = 0
                        member_count += new_add_member_count
                        # print("新用户数 %s" % tr.find('td').eq(9).text())
                        try:
                            # 充值人数
                            new_add_paid_user_count = int(tr.find('td').eq(12).text())
                        except:
                            new_add_paid_user_count = 0
                        # print("充值人数 %s" % new_add_paid_user_count)
                        paid_user_count += new_add_paid_user_count
                        if t <= save_info_time:
                            # 如果标签时间小于存储时间，则增加存储数据
                            old_date_member_count += new_add_member_count
                            old_date_paid_user_count += new_add_paid_user_count
                            # print('%s-%s' % (old_date_member_count, old_date_paid_user_count))
                    else:
                        # 已经超过了存储的时间
                        break
            current_page += 1
        # 存储
        self._data.paid_user_count = paid_user_count
        self._data.subscribe_count = member_count
        self._data.save_link_date_info({
            'date': save_info_date,
            'member_count': old_date_member_count,
            'paid_user_count': old_date_paid_user_count,
        })

    # def get_charge(self, url):
    #     full_url = 'https://open.yuewen.com%s' % url
    #     req = self.session.get(full_url, headers=self.headers, cookies=self.cookie)
    #     doc = PyQuery(req.text)
    #     tr = doc('.table.table-bordered.table-hover tbody tr').eq(0)
    #     tds = tr.find('td')
    #     d = tds.eq(0).text()
    #     t = time.strftime("%Y-%m-%d", time.localtime(self._data.timestamp))
    #     if d == t:
    #         # 今日充值
    #         try:
    #             self._data.today_paid_amount = float(tds.eq(4).text())
    #         except ValueError:
    #             self._data.today_paid_amount = 0
    #
    #         # 充值人数
    #         try:
    #             self._data.paid_user_count = int(tds.eq(7).text())
    #         except ValueError:
    #             self._data.paid_user_count = 0
    #
    #         # 新用户充值笔数
    #         try:
    #             self._data.newer_paid_count = int(tds.eq(13).text())
    #         except ValueError:
    #             self._data.newer_paid_count = 0
    #     else:
    #         self._data.today_paid_amount = 0
    #         self._data.paid_user_count = 0
    #         self._data.newer_paid_count = 0
    #     # 获取总粉丝
    #     self.get_fans_count(full_url, req.text)
    #     # 整合历史中的
    #     key_name = 'yuewen_member_count_history'
    #     history_count = self.redis.hget(key_name, self._data.linkdesc_id)
    #     if history_count is None:
    #         history_count = 0
    #     else:
    #         history_count = int(history_count)
    #     if self.mark_get_fans:
    #         self.redis.hset(key_name, self._data.linkdesc_id, history_count + self._data.member_count)
    #     self._data.member_count += (self._data.seven_member_count + history_count)

    # 匹配获取总页数的正则
    last_page_zz = re.compile('条记录 1/(\d+)页')

    # def get_fans_count(self, url, page1text: str):
    #     """
    #     计算总粉丝数
    #     :return:
    #     """
    #     page = 1
    #     last_page = 1
    #     self._data.member_count = 0
    #     self._data.seven_member_count = 0
    #     while page <= last_page:
    #         if page == 1:
    #             doc = PyQuery(page1text)
    #             last_page = int(self.last_page_zz.search(page1text).group(1))
    #         else:
    #             full_url = '%s/p/%s.html' % (url, page)
    #             print("url:%s" % full_url)
    #             req = self.session.get(full_url, headers=self.headers, cookies=self.cookie)
    #             doc = PyQuery(req.text)
    #         trs = doc('.table.table-bordered.table-hover tbody tr')
    #         for tr in trs.items():
    #             date_str = tr.find('td').eq(0).text()
    #             t = int(time.mktime(time.strptime(date_str, '%Y-%m-%d')))
    #             print('t:%s  sfl:%s' % (t, self.get_fans_limit))
    #             if t <= self.get_fans_limit:
    #                 return
    #                 # 获取当日新增粉丝数
    #             try:
    #                 member_count = int(tr.find('td').eq(9).text())
    #             except ValueError:
    #                 member_count = 0
    #
    #             if t > self.seven_days_ago:
    #                 # 7天内新增的粉丝
    #                 self._data.seven_member_count += member_count
    #             else:
    #                 # 7天到14天内新增(七天前到get_fans_limit设置的时间)
    #                 self._data.member_count += member_count
    #             print('t:%s m:%s sm:%s ss:%s' % (
    #                 date_str, member_count, self._data.member_count, self._data.seven_member_count)
    #                   )
    #         page += 1

    def get_book_id(self):
        """
        获取小说id
        :return:
        """
        url = 'https://open.yuewen.com/service/wechat_spread/bookspread.html?title=%s'
        req = self.session.get(url % self._data.book_name, headers=self.headers, cookies=self.cookie)
        doc = PyQuery(req.text)
        trs = doc('table tbody tr').items()
        for tr in trs:
            name = tr.find('td').eq(1).text()
            if name == self._data.book_name:
                self._data.book_id = tr.find('td').eq(0).text()
                return

    def change_app(self, app):
        """
        切换公众号
        :param app:公众号id(阅文)
        :return:None
        """
        url = 'https://open.yuewen.com/Pub/Index/changeApp'
        post_data = {
            'id': app,
            'type': 2
        }
        req = self.session.post(
            url=url,
            data=post_data,
            headers=self.headers,
            cookies=self.cookie
        )
        # print(req.text)
