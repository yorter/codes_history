import sys

from core.bookspiders.yuewen.yuewenuser import YuewenUser


class BookInfo:
    def __init__(self, account: dict):
        self.account = account
        # 确认采集类型
        try:
            self.catch_type = int(sys.argv[1])
            self.catch_types.index(self.catch_type)
        except (IndexError, ValueError):
            self.catch_type = 2
        print("采集内容为%s" % self.catch_types_names[self.catch_type])

    account = None
    # 采集类型 1:充值信息 2:链接信息
    catch_types = [1, 2]
    catch_types_names = {
        1: "充值信息",
        2: "链接信息",
    }
    catch_type = 0

    timestamp = 0

    def start(self):
        app_user = YuewenUser(self.account, self.catch_type)
        app_user.start()


if __name__ == '__main__':
    b = BookInfo({
        'username': 'cqruimeng@126.com',
        'password': 'JGySCW7F',
    })
    b.start()
