import re
import time
from urllib.parse import urlencode

import requests
from pyquery import PyQuery
import pyquery

from config import plat
from core.data.bookdata import BookData
from core.bookspiders.zhangzhongyun.zzybrowser import ZzyBrowser
from bs4 import BeautifulSoup as bs

"""
掌中云vip账号抓取
"""


class BookInfo:
    # login_url = 'https://vip.818tu.com/login/dologin'
    # 首页地址
    # data_url = 'https://vip.818tu.com/backend/referral_links/index?page=%s&publish_type=%s'

    # session = requests.session()

    data = BookData()

    # 登录信息
    login_data = {
        'name': '',
        'password': '',
    }

    # 浮点型数字匹配
    intzz = re.compile('\d+\.\d{0,2}')
    # 正整数正则匹配
    # countzz = re.compile('\d+')
    # 匹配已完成比数
    done_countzz = re.compile('已支付:(\d+)笔')
    # 匹配uid值
    uidzz = re.compile('var uid = \[(.*)\];')
    # 匹配最大页数
    page_max_zz = re.compile('共 <em>(\d+)</em> 页')
    # 搜索创建时间
    create_time_zz = re.compile('([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})')

    # 区分平台
    platform = 3

    # uid
    uid = ''
    browser = None

    def __init__(self, account: dict):
        # self.session = requests.session()
        self.account = account
        self.browser = ZzyBrowser(account['username'], account['password'])

    account = {}

    def start(self):
        # self.session = requests.session()
        # self.login_data['name'] = self.account['username']
        # self.login_data['password'] = self.account['password']
        # self.session.post(self.login_url, self.login_data)
        self.browser.login()
        for publish_type in [1, 2]:
            self.current_page = 1
            self.last_page = 1
            self.publish_type = publish_type
            self.data.tui = 0 if publish_type == 1 else 1
            print("开始采集掌中云%s" % self.tui_types[self.data.tui])
            self.get_pages()

        # req = self.session.get(self.vipindex_url)
        # print(req.text)

    tui_types = {
        0: '内推',
        1: '外推',
    }
    # 掌中云请求内外推得参数 1：内 2：外
    publish_type = 1
    current_page = 1
    last_page = 1
    data_uid_to_public = {}

    def get_pages(self):
        while self.current_page <= self.last_page:
            # url = self.data_url % (self.current_page, self.publish_type)
            # print(url)
            # req = self.session.get(url)
            # doc = PyQuery(req.text)
            page_doc = self.browser.get_page(self.publish_type, self.current_page)
            doc = PyQuery(page_doc)
            if self.current_page == 1:
                max_page_search = self.page_max_zz.search(page_doc)
                if max_page_search:
                    self.last_page = int(max_page_search.group(1))
            trs = doc('#referral-link .referral-link-item')
            for tr in trs.items():
                try:
                    self.one_link(tr)
                except:
                    continue
            self.current_page += 1
            print("换页%s" % self.current_page)

    book_id_zz = re.compile('/view/([0-9]*)">')

    def one_link(self, tr: pyquery.pyquery.PyQuery):
        # 链接id
        self.data.linkdesc_id = tr.attr('data-link-id')
        tr = PyQuery(tr.html())
        # 链接名
        self.data.linkdesc = tr.find('td').eq(1).find('.link-desc').text().strip()
        # link = tr.find('text-primary').text()
        # print(link_id)
        # exit()
        data_uid = tr.find('td a').eq(0).attr("data-uid")
        # 小说名
        self.data.book_name = tr.find('td').eq(2).find('div a').text()
        # 小说id
        # search_rel = self.book_id_zz.search(tr.html())
        # if not search_rel:
        #     return
        href = tr.find('td').eq(2).find('div a').attr('href')
        if href is None:
            return
        self.data.book_id = href.split('/')[-1]
        # self.data.book_id = search_rel.group(1)
        # print(search_rel.group(1))
        # print(self.data.book_id)
        # print(self.data.book_name)
        # exit()
        self.check_public(data_uid)
        print('%s-%s' % (self.data.book_name, self.data.book_id))
        # 创建时间
        create_time_zone = tr.find('td').eq(1).find('.text-muted').text()
        search = self.create_time_zz.search(create_time_zone)
        if search:
            self.data.create_time = int(time.mktime(time.strptime(search.group(1), '%Y-%m-%d %H:%M:%S')))
        else:
            self.data.create_time = 0
        self.data.save_bookinfov2()
        self.get_link_data()
        # print(self.current_page)
        # 获取连接信息

    public_url = 'https://vip.818tu.com/backend/users/api_get_user_infos?ids=%s'

    def check_public(self, data_uid):
        """
        检查公众号
        :param data_uid:
        :return:
        """
        # url = self.public_url % data_uid
        # req = self.session.get(url)
        # pname = req.json()[data_uid]['nickname']
        pname = self.browser.get_public_name(data_uid)
        public_id = self.data_uid_to_public.get(data_uid)
        if public_id:
            self.data.public_id = public_id
            self.data.pname = pname
        else:
            self.data.check_public({
                'pid': self.platform,
                'name': pname,
            })
            self.data_uid_to_public[data_uid] = self.data.public_id

    link_api = 'https://vip.818tu.com/backend/referral_links/api_get_counter/%s?forever='

    def get_link_data(self):
        data = self.browser.link_api_data(self.data.linkdesc_id)
        # print(data)
        # exit()
        # url = self.link_api % self.data.linkdesc_id
        # req = self.session.get(url)
        # data = req.json()
        self.data.member_count = data['member_count']
        self.data.subscribe_count = data['subscribe_count']
        self.data.paid_amount = data['paid_amount']
        self.data.subscribe_rate = data['subscribe_rate']
        self.data.paid_amount = data['paid_amount']
        self.data.clicks = data['clicks']
        self.data.today_paid_amount = 0
        self.data.paid_user_count = int(str(data['paid_user_count']).replace(',', ''))
        self.data.newer_paid_count = 0
        self.data.save_bookv2()

    def __del__(self):
        self.browser.close()


if __name__ == '__main__':
    z = BookInfo({
        'username': 'jxds2020',
        'password': 'r36Tj509',
    })
    z.start()
