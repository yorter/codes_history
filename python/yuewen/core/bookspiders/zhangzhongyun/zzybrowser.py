from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
import time
import json
from pyquery import PyQuery


class ZzyBrowser:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--no-sandbox')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(options=opt)
        self.wait = WebDriverWait(self.browser, 10)

    browser = None
    wait = None

    def login(self):
        """
        登录
        :return:
        """
        self.browser.get('https://vip.818tu.com/login')
        self.browser.find_element_by_id('username').send_keys(self.username)
        self.browser.find_element_by_name('password').send_keys(self.password)
        self.browser.find_element_by_id('loginbtn').click()

    page_url = 'https://vip.818tu.com/backend/referral_links/index?publish_type=%s&page=%s'

    def get_page(self, tui=1, page=1):
        self.browser.get(self.page_url % (tui, page))
        return self.browser.page_source

    public_url = 'https://vip.818tu.com/backend/users/api_get_user_infos?ids=%s'

    def get_public_name(self, data_uid):
        self.browser.get(self.public_url % data_uid)
        s = self.browser.page_source
        # return json.loads(s)[str(data_uid)]['nickname']
        s = self.replace_html(s)
        return json.loads(s)[str(data_uid)]['nickname']

    link_api = 'https://vip.818tu.com/backend/referral_links/api_get_counter/%s?forever='

    def link_api_data(self, link_id):
        self.browser.get(self.link_api % link_id)
        s = self.replace_html(self.browser.page_source)
        return json.loads(s)

    def replace_html(self, s: str) -> str:
        # s = s.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '')
        # s = s.replace('</pre></body></html>', '')
        # return s
        doc = PyQuery(s)
        return doc.text()

    def close(self):
        self.browser.quit()
