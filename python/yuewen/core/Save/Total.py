import json

import redis

from config import redis_key
from config.dbconfig import redis_config
from core.mylibs.mysql import Mysql


class Total:
    db = None
    redis = None
    sql = "SELECT book_id,`name`,SUM(clicks) clicks,SUM(subscribe_count) subscribe_count,SUM(paid_amount) paid_amount,SUM(cost_amount) cost_amount FROM booksv2 bs LEFT JOIN bookinfov2 bi ON bs.linkdesc_id=bi.linkdesc_id WHERE bs.id in  (SELECT MAX(id) as id FROM `booksv2` GROUP BY linkdesc_id) AND bi.tui=%s GROUP BY book_id ORDER BY paid_amount DESC"

    def __init__(self):
        self.db = Mysql()
        self.redis = redis.Redis(**redis_config)

    def sum(self):
        for tui in [0, 1]:
            print("开始查询推类型为%s的" % tui)
            self.db.execute(self.sql % tui)
            fetchall = self.db.db.fetchall()
            flen = len(fetchall)
            print("查询完毕，共%s个" % flen)
            for i in range(flen):
                fo = fetchall[i]
                fetchall[i]['clicks'] = int(fo['clicks'])
                fetchall[i]['subscribe_count'] = int(fo['subscribe_count'])
                fetchall[i]['cost_amount'] = int(fo['cost_amount'])
                fetchall[i]['paid_amount'] = round(fo['paid_amount'], 2)

                fo = fetchall[i]
                # 利润
                try:
                    fetchall[i]['profit_amount'] = round(fo['paid_amount'] - fo['cost_amount'], 2)
                except:
                    fetchall[i]['profit_amount'] = 0
                # 单粉回报
                try:
                    fetchall[i]['SingleReturn'] = round(fo['paid_amount'] / fo['subscribe_count'], 2)
                except:
                    fetchall[i]['SingleReturn'] = 0
                # 回本率
                try:
                    fetchall[i]['profit_rate'] = round(fo['paid_amount'] / fo['cost_amount'] * 100, 2)
                except:
                    fetchall[i]['profit_rate'] = 0
            redis_id = redis_key.book_sort % tui
            json_data = json.dumps(fetchall, ensure_ascii=False)
            self.redis.set(redis_id, json_data)
