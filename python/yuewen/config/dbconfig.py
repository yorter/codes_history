host = 'cdb-qtgtqqir.gz.tencentcdb.com'  # mysql和redis连接地址
"""
MySQL配置
"""
mysql_config = {
    'host': 'localhost',  # 地址
    'user': 'root',  # 用户名
    'password': '43793543',  # 密码
    'port': 3306,  # 端口号
    'database': 'card',  # 默认库
    'charset': 'utf8',  # 字符集
    'autocommit': 'true',  # 自动提交
}
database = 'book'  # 库名

"""
Redis配置
"""
redis_config = {
    'host': 'localhost',  # 连接地址
    'port': 6379,  # 端口号
    'decode_responses': True,  # 结果返回字符串而非Unicode编码
    'db': 8,  # 链接库(python默认无法修改库)
    # 'password': "FightForFreedom",  # 链接库(python默认无法修改库)
}
