accounts = {
    # 阳光书城
    'yangguang': [
        {
            'publicname': '泽文学',
            'username': 'mimi05',
            'password': 'D40q91J8',
        },
        {
            'publicname': '盛文学',
            'username': 'shengwx',
            'password': 'D40q91J8',
        },
        {
            'publicname': '同文学',
            'username': 'twx09',
            'password': 'D40q91J8',
        },
        {
            'publicname': '郡文学',
            'username': 'gsy01',
            'password': '10y6b94E',
        },
        {
            'publicname': '彬文学',
            'username': 'gsy02',
            'password': '10y6b94E',
        },
        {
            'publicname': '琳文学',
            'username': 'lwx77',
            'password': '10y6b94E',
        },
        {
            'publicname': '织文学',
            'username': 'zwx99',
            'password': '10y6b94E',
        },
    ],
    # 七悦书城
    'qiyue': [
        {
            'publicname': '坚文学',
            'username': 'jwx',
            'password': 'D40q91J8',
        },
        {
            'publicname': '举文学',
            'username': 'juwx',
            'password': 'D40q91J8',
        },
    ],
    # 掌中云账号
    'zhangzhongyun': [
        {
            'publicname': '甫文学',
            'username': 'fuwenxue',
            'password': '8I154U0b',
        },
        {
            'publicname': '玖文学',
            'username': 'jiuwx',
            'password': '5381VP4Q',
        },
        {
            'publicname': '佳容文学',
            'username': 'jrwx',
            'password': 'N5c9264X',
        },
        {
            'publicname': '雨棉文学',
            'username': 'ymwx6',
            'password': '0J198Oz4',
        },
        {
            'publicname': '宇文学',
            'username': 'yuwenx',
            'password': '10y6b94E',
        },
        {
            'publicname': '宏文学',
            'username': 'hongwx',
            'password': '10y6b94E',
        },
        {
            'publicname': '沫文学',
            'username': 'mowenx',
            'password': '10y6b94E',
        },
        {
            'publicname': '颂文学',
            'username': 'songwx',
            'password': 'D40q91J8',
        },
        {
            'publicname': '卡文学',
            'username': 'kawx',
            'password': 'D40q91J8',
        },
        {
            'publicname': '子如文学',
            'username': 'ziru',
            'password': 'D40q91J8',
        },
        {
            'publicname': '水英文学',
            'username': 'sywx6',
            'password': 'D40q91J8',
        },
        {
            'publicname': '尧文学',
            'username': 'yaowx',
            'password': 'D40q91J8',
        },
        {
            'publicname': '盈文学',
            'username': '盈文学',
            'password': 'D40q91J8',
        },
    ],
    # 掌读
    'zhangdu': [
        {
            'publicname': '振文学',
            'username': 'zhenwx',
            'password': 'D40q91J8',
        },
    ],
}

vip_accounts = {
    'ygsc': {
        'username': 'cqrm',
        'password': '77gzqSkp'
    },
    'zzy': {
        'username': 'sunmeng',
        'password': '77gzqSkp'
    },
    'qysc': [
        {
            'username': 'jwx',
            'password': 'D40q91J8',
        },
        {
            'username': 'juwx',
            'password': 'D40q91J8',
        },
    ],
}
