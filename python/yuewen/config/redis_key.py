config = {
    'nowtime': 'Start_Catch_Time_Now',  # 开始采集时间rediskey
    # 每个链接存储信息个数的上限
    'link_limit': 2,
    # redis_key###
    'books_key': 'Books_List_%s',  # 小说链接信息
    'bookid_key': 'Books_Bookid_%s_%s',  # 小说链接信息(按照推类型)
    'bookid_key_keys': 'Books_Bookids_%s',  # 所有的小说id（按照推类型）
    'bookidsum': 'Books_Bookid_Sum_%s_%s',  # 单本书总计
    'bookidsort': 'Books_Bookid_Sort_%s',  # 书id排序
    'lastinfo': 'Book_LastUpdate',  # 小说最新更新
    'lastinfokeys': 'Book_LastUpdate_keys',  # 小说最新更新key
    'lastinfolist': 'Book_LastUpdate_keys_list',  # 小说最新更新key
    'Bookinfo': 'Book_Bookid_%s_%s',  # 小说id最新更新
    'bookidssums': "Books_Bookid_Sum_Sort_%s",  # 排序后的json
    'bookidtoname': "Books_bookidtoname",  # 小说名与id对应关系
    # 小说链接每日凌晨信息
    'dawn': {
        'key': "Books_Dawn_%s",  # rediskey名称
        'limit': 50,  # 留存最多个数
    },
    # 小说id包含所有的链接id
    'bookidcontain': 'Books_Bookid_Contain_%s',
}
# 开始采集时间
nowtime = 'Start_Catch_Time_Now'
# 链接存储个上限
link_save_limit = 200
# 小说id下的所有的链接id(列表)
book_id_has_linkid = "Books_has_link_ids_%s"
# 小说链接最新信息(哈希) 小说id_内外推
book_info_key = 'Books_id_info_%s_%s'
# 链接信息(列表)
link_info_key = 'Books_link_info_%s'
# 每日午夜链接信息(列表)
link_info_every_mid_night = "Link_at_mid_night_%s"
# 午夜保存链接最大长度
link_mid_night_len = 50
# 排序后的json(string)
book_sort = "Books_Bookid_Sum_Sort_%s",
