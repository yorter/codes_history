﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketData.Request.user
{
    class Login:RequestData
    {
        public Login()
        {
            r = "login";
        }

        /// <summary>
        /// 用户名
        /// </summary>
        public string u { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string p { get; set; }
    }
}
