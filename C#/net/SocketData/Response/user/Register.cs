﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketData.Response.user
{
    class Register:RequestData
    {
        public Register()
        {
            r = "register";
        }

        public string u { get; set; }
        public string p { get; set; }
        public string n { get; set; }
    }
}
