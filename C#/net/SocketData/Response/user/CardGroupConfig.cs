﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketData.Response.user
{
    class CardGroupConfig:ResponseData
    {
        /// <summary>
        /// 牌组中卡牌数量上限
        /// </summary>
        public int card_max { get; set; }

        /// <summary>
        /// 牌组中卡牌数量下限
        /// </summary>
        public int card_min { get; set; }

        /// <summary>
        /// 牌组数上限
        /// </summary>
        public int group_count_max { get; set; }
    }
}
