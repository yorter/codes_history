﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketData.Response.user
{
    /// <summary>
    /// 用户牌组
    /// </summary>
    class CardGroups :ResponseData
    {
        public int[][] group0 { get; set; }
        public int[][] group1 { get; set; }
        public int[][] group2 { get; set; }
        public int[][] group3 { get; set; }
        public int[][] group4 { get; set; }
        public int[][] group5 { get; set; }
    }
}
