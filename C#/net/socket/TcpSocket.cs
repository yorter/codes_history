﻿using System.Net;
using System.Net.Sockets;

class TcpSocket
{
    public TcpSocket(string ServerIp, int Port)
    {
        endPoint = new IPEndPoint(IPAddress.Parse(ServerIp), Port);
    }

    public TcpSocket(IPAddress ServerIp, int Port)
    {
        endPoint = new IPEndPoint(ServerIp, Port);
    }

    /// <summary>
    /// 终点
    /// </summary>
    public readonly IPEndPoint endPoint;

    /// <summary>
    /// Socket对象
    /// </summary>
    Socket sk = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

    /// <summary>
    /// 连接服务端
    /// </summary>
    public void Connect()
    {
        sk.Connect(endPoint);
    }

    /// <summary>
    /// 发送
    /// </summary>
    /// <param name="buffer">发送的数据</param>
    public int Send(byte[] buffer)
    {
        return sk.Send(buffer);
    }

    /// <summary>
    /// 接收数据
    /// </summary>
    /// <param name="len">接收数据的长度</param>
    /// <returns>接收的数据</returns>
    public byte[] Receive(int len)
    {
        byte[] recvBytes = new byte[len];
        sk.Receive(recvBytes);
        return recvBytes;
    }

    public void Close()
    {
        sk.Close();
    }
}
