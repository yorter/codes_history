﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

class TcpNetwork
{
    /// <summary>
    /// 封装的socket
    /// </summary>
    private readonly TcpSocket tcpSocket;

    public TcpNetwork(string ServerIp, int Port)
    {
        tcpSocket = new TcpSocket(ServerIp, Port);
    }

    public TcpNetwork(IPAddress ServerIp, int Port)
    {
        tcpSocket = new TcpSocket(ServerIp, Port);
    }

    public int Send(string s)
    {
        //发送的字符串转byte[]
        byte[] sBuffer = Encoding.UTF8.GetBytes(s);
        tcpSocket.Send(BitConverter.GetBytes(sBuffer.Length));
        return tcpSocket.Send(sBuffer);
    }


    /// <summary>
    /// 接收信息，解决黏包处
    /// </summary>
    /// <returns></returns>
    public string Receive()
    {
        byte[] buffer = tcpSocket.Receive(4);
        buffer = tcpSocket.Receive(BitConverter.ToInt32(buffer));
        return Encoding.UTF8.GetString(buffer);
    }

    /// <summary>
    /// <summary>
    /// 断开重连
    /// </summary>
    public void Restart()
    {
        tcpSocket.Close();
        tcpSocket.Connect();
    }

    /// <summary>
    /// 关闭连接
    /// </summary>
    public void Connect()
    {
        tcpSocket.Connect();
    }
}
