﻿namespace Fsm
{
    public class Action
    {
        #region 更新
        /// <summary>
        /// 技能开始释放
        /// </summary>
        public virtual void Start()
        {

        }

        /// <summary>
        /// 技能结束释放
        /// </summary>
        public virtual void End()
        {

        }

        /// <summary>
        /// 每帧更新
        /// </summary>
        public virtual void Update()
        {

        }

        /// <summary>
        /// FixedUpdate
        /// </summary>
        public virtual void FixedUpdate()
        {

        }
        #endregion

        #region 动画事件
        public virtual void OnAnimStart()
        {

        }

        public virtual void OnAnimEnd()
        {

        }
        #endregion
    }
}
