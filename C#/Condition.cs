﻿using System;
using System.Collections.Generic;

namespace Fsm.Condition
{
    /// <summary>
    /// 状态机条件的设置和检查
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Condition<T> where T : Enum
    {
        #region 默认行为、行为对应条件函数、检查范围及设置函数
        /// <summary>
        /// 默认行为(不需要检测的条件)
        /// </summary>
        public T defaultAction;

        /// <summary>
        /// 技能对应检查条件函数
        /// </summary>
        protected Dictionary<T, Func<bool>> checkConditionFunctions = new Dictionary<T, Func<bool>>();

        /// <summary>
        /// 检查排序(和范围)
        /// </summary>
        protected T[] checkActions = null;

        /// <summary>
        /// 初始化检查排序(和范围)
        /// </summary>
        /// <param name="actions"></param>
        public void SetCheckConditions(params T[] actions)
        {
            checkActions = actions;
        }
        #endregion

        #region 返回第一个满足的条件
        /// <summary>
        /// 检查所有的条件，返回第一个满足条件的行为，没有则返回默认行为
        /// </summary>
        /// <returns></returns>
        public T CheckAll()
        {
            foreach (T action in Enum.GetValues(typeof(T)))
            {
                if ((!defaultAction.Equals(action)) && checkConditionFunctions[action]())
                {
                    return action;
                }
            }
            return defaultAction;
        }


        /// <summary>
        /// 检查所有的(checkActions)行动条件
        /// </summary>
        /// <returns></returns>
        public T CheckSet()
        {
            if (checkActions != null)
            {
                return CheckArray(checkActions);
            }
            return defaultAction;
        }

        /// <summary>
        /// 按顺序指定检查技能释放按键条件
        /// </summary>
        /// <param name="actions">要检查的行动条件</param>
        /// <returns></returns>
        public T Check(params T[] actions)
        {
            return CheckArray(actions);
        }

        /// <summary>
        /// 在数组中检查条件
        /// </summary>
        /// <param name="actions"></param>
        /// <returns></returns>
        public T CheckArray(T[] actions)
        {
            foreach (T action in actions)
            {
                if ((!defaultAction.Equals(action)) && checkConditionFunctions[action]())
                {
                    return action;
                }
            }
            return defaultAction;
        }
        #endregion

        #region 获取所有满足的条件
        /// <summary>
        /// 检查所有的T类型
        /// </summary>
        /// <returns></returns>
        public T[] GetAll()
        {
            List<T> actions = new List<T>();
            foreach (T action in Enum.GetValues(typeof(T)))
            {
                if ((!defaultAction.Equals(action)) && checkConditionFunctions[action]())
                {
                    actions.Add(action);
                }
            }
            return actions.ToArray();
        }

        /// <summary>
        /// 获取所有符合的条件
        /// </summary>
        /// <returns>所有满足的条件</returns>
        public T[] GetAllSet()
        {
            if (checkActions == null)
            {
                return new T[] { };
            }
            else
            {
                return GetArrayAll(checkActions);
            }
        }

        /// <summary>
        /// 从参数中指定的行动中获取所有满足条件的行动
        /// </summary>
        /// <param name="actions"></param>
        /// <returns></returns>
        public T[] GetAll(params T[] actions)
        {
            return GetArrayAll(actions);
        }

        /// <summary>
        /// 从数组中检查满足的行动
        /// </summary>
        /// <param name="actions"></param>
        /// <returns></returns>
        public T[] GetArrayAll(T[] actions)
        {
            List<T> satisfiedActions = new List<T>();
            foreach (T action in actions)
            {
                if ((!defaultAction.Equals(action)) && checkConditionFunctions[action]())
                {
                    satisfiedActions.Add(action);
                }
            }
            return satisfiedActions.ToArray();
        }
        #endregion
    }
}
