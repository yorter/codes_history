﻿using System.Collections.Generic;

namespace DbQuery
{
    public enum JoinType
    {
        LEFT,
        RIGHT,
        INNER,
    }
    
    public static class EnumToObject
    {
        public static readonly Dictionary<JoinType, string> Join_Type = new Dictionary<JoinType, string>()
        {
            {JoinType.LEFT, ""},
            {JoinType.RIGHT, ""},
            {JoinType.INNER, ""},
        };
    }
}