﻿using System.Collections.Generic;

namespace DbQuery
{
    public class SqlData
    {
        public List<string> Field = new List<string>();
        public List<object> Args = new List<object>();
        public int[] Limit = new int[2];
        public List<string> Join = new List<string>();
        public List<string> Group = new List<string>();
        public List<string> Order = new List<string>();
    }
}