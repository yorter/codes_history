﻿namespace DbQuery
{
    public class Query
    {
        private int _defaul_limit;

        public Query(int defaul_limit=10)
        {
            this._defaul_limit = defaul_limit;
        }
        
        private string _table = "";
        private SqlData _data = new SqlData();
        
        /// <summary>
        /// 设置表名
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public Query Table(string t)
        {
            _table = t;
            return this;
        }

        public Query Join(string table, string alias,string condition,JoinType joinType)
        {
            
            return this;
        }

        #region ORDER
        public Query Order(string order)
        {
            _data.Order.Add(order);
            return this;
        }
        
        public Query Order(string[] order)
        {
            _data.Order.AddRange(order);
            return this;
        }
        #endregion
        
        
        #region LIMIT
        public Query Limit(int num)
        {
            _data.Limit[1] = num;
            return this;
        }
        public Query Limit(int page,int num)
        {
            if (_data.Limit[1]==0)
            {
                _data.Limit[1] = _defaul_limit;
            }
            _data.Limit[0] = (page - 1) * _data.Limit[1];
            return this;
        }
        #endregion
        
    }
}