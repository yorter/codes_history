﻿using System;
using System.Collections.Generic;

namespace Fsm
{
    public class FSM<T> where T : Enum
    {
        public FSM(T DefaultAction)
        {
            currentAction = defaultAction = DefaultAction;
        }

        public FSM(T DefaultAction, T FirstAction)
        {
            defaultAction = DefaultAction;
            currentAction = FirstAction;
        }

        #region 更新
        public void Update()
        {
            if (actions.ContainsKey(currentAction))
            {
                actions[currentAction].Update();
            }
        }

        public void FixedUpdate()
        {
            if (actions.ContainsKey(currentAction))
            {
                actions[currentAction].FixedUpdate();
            }
        }
        #endregion

        #region 行为属性
        /// <summary>
        /// 注册的技能列表
        /// </summary>
        private Dictionary<T, Action> actions = new Dictionary<T, Action>();

        /// <summary>
        /// 默认行为
        /// </summary>
        public readonly T defaultAction;

        /// <summary>
        /// 正在执行的行为类
        /// </summary>
        public T currentAction { get; private set; }
        #endregion

        #region 事件注册、解除、切换
        /// <summary>
        /// 注册事件
        /// </summary>
        /// <param name="action"></param>
        /// <param name="actionBase"></param>
        public void AddCallback(T action, Action actionBase)
        {
            actions.Add(action, actionBase);
        }

        /// <summary>
        /// 解除注册
        /// </summary>
        /// <param name="action"></param>
        public void DelCallback(T action)
        {
            actions.Remove(action);
        }

        /// <summary>
        /// 切换状态
        /// </summary>
        /// <param name="action"></param>
        /// <returns>是否可以切换</returns>
        public bool SwitchAction(T action)
        {
            if (actions.ContainsKey(action))
            {
                actions[currentAction].End();
                actions[action].Start();
                currentAction = action;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 切换到默认行为
        /// </summary>
        /// <returns></returns>
        public bool SwitchDefault()
        {
            if (actions.ContainsKey(defaultAction))
            {
                currentAction = defaultAction;
                return true;
            }
            return false;
        }
        #endregion
    }
}

