﻿using System;
using System.Collections.Generic;

namespace Fsm
{
    public abstract class ActionPoll<T> where T : Enum
    {
        /// <summary>
        /// 技能及技能控制对象的关系字典
        /// </summary>
        protected Dictionary<T, Action> actions = new Dictionary<T, Action>();

        /// <summary>
        /// 获得技能控制对象
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public virtual Action GetAction(T action)
        {
            if (actions.ContainsKey(action))
            {
                return actions[action];
            }
            return null;
        }

        /// <summary>
        /// 添加行动
        /// </summary>
        /// <param name="action_type">行动类型</param>
        /// <param name="action">行动</param>
        /// <param name="replace"></param>
        public virtual void SetAction(T action_type, Action action, bool replace = true)
        {
            if (!actions.ContainsKey(action_type) || replace)
            {
                actions[action_type] = action;
            }
        }
    }
}
