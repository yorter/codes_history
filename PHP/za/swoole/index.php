<?php
require 'MongoSql.php';


//创建Server对象，监听 127.0.0.1:9501端口
$serv = new Swoole\Server("0.0.0.0", 9501);

$serv->on('Start', function ($serv) {

});

//监听连接进入事件
$serv->on('Connect', function (Swoole\Server $server, int $fd, int $reactorId) {
    
});

//监听数据接收事件
$serv->on('Receive', function (Swoole\Server $server, int $fd, int $reactorId, string $data) {
    echo $data . "\n";
    $server->send($fd, "{$fd}在{$reactorId}");
});

//监听连接关闭事件
$serv->on('Close', function (Swoole\Server $server, int $fd, int $reactorId) {
    echo "{$fd}在{$reactorId}关闭了连接.\n";
});

//启动服务器
$serv->start();
