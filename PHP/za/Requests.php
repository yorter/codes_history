<?php

/**
 * Curl请求封装
 */
class Requests
{
    private function __construct()
    {
        $this->public_init();
    }

    private $curl_init = null;

    /**
     * @return Requests 请求单例对象
     */
    public static function instance()
    {
        return new self();
    }

    /**
     * GET  请求方式
     * @param string $url 请求地址
     * @return RequestsData 结果
     */
    public function get(string $url): RequestsData
    {
        return $this->exec($url, 'GET');
    }

    /**
     * POST请求方式
     * @param string $url 请求地址
     * @param array $data 请求参数
     * @return RequestsData 结果
     */
    public function post(string $url, array $data = []): RequestsData
    {
        if ($data) {
            //设置post方式提交
            curl_setopt($this->curl_init, CURLOPT_POST, true);
            //设置post数据
            curl_setopt($this->curl_init, CURLOPT_POSTFIELDS, $data);
        }
        return $this->exec($url, 'POST');
    }

    /**
     * PUT请求方式
     * @param string $url 请求地址
     * @param array $data 请求参数
     * @return RequestsData 结果
     */
    public function put(string $url, array $data = []): RequestsData
    {
        curl_setopt($this->curl_init, CURLOPT_POSTFIELDS, http_build_query($data));
        return $this->exec($url, 'PUT');
    }

    /**
     * DELETE请求方式
     * @param string $url 请求地址
     * @param array $data 请求参数
     * @return RequestsData 结果
     */
    public function delete(string $url, array $data = []): RequestsData
    {
        curl_setopt($this->curl_init, CURLOPT_POSTFIELDS, http_build_query($data));
        return $this->exec($url, 'DELETE');
    }

    /**
     * 执行(私有)
     * @param string $url 请求地址
     * @param string $method 请求方法
     * @return RequestsData 返回结果
     */
    private function exec(string $url, string $method)
    {
        curl_setopt($this->curl_init, CURLOPT_URL, $url);
        curl_setopt($this->curl_init, CURLOPT_CUSTOMREQUEST, $method);
        $text = curl_exec($this->curl_init);
        $http_info = curl_getinfo($this->curl_init);
        $error = curl_error($this->curl_init);
        curl_close($this->curl_init);
        return new RequestsData($text, $http_info, $error);
    }

    /**
     * 公共初始化部分
     */
    private function public_init()
    {
        $this->curl_init = curl_init();
        curl_setopt($this->curl_init, CURLOPT_FAILONERROR, false);
        //设为TRUE把curl_exec()结果转化为字串，而不是直接输出
        curl_setopt($this->curl_init, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl_init, CURLOPT_HEADER, 0);
        curl_setopt($this->curl_init, CURLOPT_SSL_VERIFYPEER, false);  //https
        curl_setopt($this->curl_init, CURLOPT_ENCODING, "gzip");
        curl_setopt($this->curl_init, CURLOPT_FOLLOWLOCATION, 1);      //重定向
    }

    /**
     * 设置头信息
     * @param array $headers 头信息
     * @return Requests 本对象
     */
    public function header(array $headers)
    {
        $headers_dict = [];
        foreach ($headers as $key => $val) {
            $headers_dict[] = $key . ":" . $val;
        }
        curl_setopt($this->curl_init, CURLOPT_HTTPHEADER, $headers_dict);
        return $this;
    }

    /**
     * 设置请求携带cookie
     * @param array $cookies
     * @return Requests 本对象
     */
    public function cookie(array $cookies): Requests
    {
        $cookie_dict = [];
        foreach ($cookies as $key => $val) {
            $cookie_dict[] = $key . "=" . $val;
        }
        curl_setopt($this->curl_init, CURLOPT_COOKIE, implode(";", $cookie_dict));
        return $this;
    }
}

class RequestsData
{
    /**
     * CURL请求返回对象
     * @param string $text 请求结果文本内容
     * @param array $http_info http信息
     * @param $error mixed 错误信息
     */
    public function __construct(string $text, array $http_info, $error)
    {
        $this->text = $text;
        $this->code = $http_info['http_code'];
        $this->url = $http_info['url'];
    }

    public $url = null;
    public $text = null;
    public $code = null;
}