<?php

/**
 * 数组操作类(一些非内置数组控制)
 */
class ArrayFunc
{
    /**
     * 递归生成家谱树
     * @param array $arr 要遍历的数组
     * @param $father_id mixed 要判定的父值
     * @param $father_key mixed 要判定的父的key
     * @param $son_key mixed 要与父做对比的子的key
     * @param string $children_key 子类存放的key
     * @param bool $save_empty_children 没有子的情况是否保留key
     * @return array 家谱树
     */
    public static function recursion(array $arr, $father_id, $father_key, $son_key, string $children_key, $save_empty_children = true)
    {
        $sons = [];
        foreach ($arr as $item) {
            if ($item[$father_key] == $father_id) {
                $children = self::recursion($arr, $item[$son_key], $father_key, $son_key, $children_key, $save_empty_children);
                if ($save_empty_children or !empty($children)) {
                    $item[$children_key] = $children;
                }
                $sons[] = $item;
            }
        }
        return $sons;
    }
}