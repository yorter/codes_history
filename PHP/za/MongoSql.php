<?php


use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\WriteResult;

/**
 * MongoDB简化查询方法(仅限PHP7.0+)
 */
class MongoSql
{
    /**
     * MongoSql初始化
     * @param string $database 要操控的库名
     * @param string $collection 要操控的表(集合)名
     * @param \MongoDB\Driver\Manager|null $manager MongoDB Manager(连接)对象
     */
    public function __construct(string $database, string $collection, \MongoDB\Driver\Manager $manager = null)
    {
        $this->namespace = $database . '.' . $collection;
        $this->manager = $manager ? $manager : new \MongoDB\Driver\Manager("mongodb://localhost:27017");
        $this->write_concern = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        echo "正在初始化MongoDB.\n";
    }

    /**
     * @var \MongoDB\Driver\Manager MongoDB Manager(连接)对象
     */
    private $manager = null;

    /**
     * @var string 库名。表(集合)名
     */
    private $namespace = null;

    /**
     * 生成BulkWrite
     * @return BulkWrite BulkWrite对象
     */
    private function get_bulk(): BulkWrite
    {
        return new BulkWrite;
    }

    private $write_concern = null;

    /**
     * 添加一行
     * @param array $insert_data 添加的数据的字典
     * @return WriteResult 结果
     */
    public function insert(array $insert_data)
    {
        $bulk = $this->get_bulk();
        $bulk->insert($insert_data);
        return $this->execute($bulk);
    }

    /**
     * 添加多行
     * @param array $data 添加的数据的数组
     * @return WriteResult 结果
     */
    public function insert_many(array $data)
    {
        $bulk = $this->get_bulk();
        foreach ($data as $insert_data) {
            $bulk->insert($insert_data);
        }
        return $this->execute($bulk);
    }

    /**
     * @param array $where 删除条件
     * @param array $option 选出选项,['limit' => 1] limit 为 1 时，删除第一条匹配数据,为 0 时，删除所有匹配数据
     * @return WriteResult 结果
     */
    public function delete(array $where, array $option = [])
    {
        $bulk = $this->get_bulk();
        $bulk->delete($where, $option);
        return $this->execute($bulk);
    }

    /**
     * @param array $where 修改条件
     * @param array $update 修改内容
     * @param array $option 选项
     * @return WriteResult 结果
     */
    public function update(array $where, array $update, array $option = [])
    {
        $bulk = $this->get_bulk();
//        $bulk->update(
//            ['x' => 2],
//            ['$set' => ['name' => '菜鸟工具', 'url' => 'tool.runoob.com']],
//            ['multi' => false, 'upsert' => false]
//        );
        $bulk->update($where, $update, $option);
        return $this->execute($bulk);
    }

    /**
     * @param array $filter 查找条件
     * @param array $options 选项
     * @return array 查找结果
     * @throws \MongoDB\Driver\Exception\Exception 异常
     */
    public function find(array $filter, array $options = []): array
    {
        $query = new \MongoDB\Driver\Query($filter, $options);
        $cursor = $this->manager->executeQuery($this->namespace, $query);
        return $cursor->toArray();
    }

    /**
     * 执行修改
     * @param BulkWrite $bulk 操作
     * @return WriteResult 执行结果
     */
    private function execute(BulkWrite $bulk)
    {
        return $this->manager->executeBulkWrite($this->namespace, $bulk, $this->write_concern);
    }

    /**
     * 清空表
     */
    public function clean()
    {
        $this->delete([]);
    }
}