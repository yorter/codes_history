<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use common\bid\meishu\GateWay;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\Utils\Parallel;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Coroutine;

/**
 * @AutoController()
 */
class IndexController extends AbstractController
{
    public function index()
    {
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        return [
            'method' => $method,
            'message' => "Hello {$user}.",
        ];
    }

    public function meishu()
    {
        $model = new GateWay($this->request);
        $ad = $model->getAd();
        if ($ad) {
            return get_object_vars($ad);
        }
    }

    private $a = [];

    public function test()
    {
        $parallel = new Parallel();
        $parallel->add(function () {
            $this->a[]=10;
            sleep(1);
            throw new \Exception("出错了");
            return Coroutine::id();
        });
        $parallel->add(function () {
            $this->a[]=11;
            sleep(1);
            return Coroutine::id();
        });

        try{
            // $results 结果为 [1, 2]
            $results = $parallel->wait();
            var_dump($results);
        } catch(ParallelExecutionException $e){
            var_dump($e->getThrowables());
            var_dump($e->getResults());
            var_dump($this->a);
            // $e->getResults() 获取协程中的返回值。
            // $e->getThrowables() 获取协程中出现的异常。
        }
    }
}
