<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\models\redis;

class DspInfo extends \Hyperf\Redis\Redis
{
    protected $poolName = "dsp_info";
}