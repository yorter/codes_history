<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\models\redis;

use Hyperf\Redis\Redis;

class DeviceMark extends Redis
{
    protected $poolName = "device_mark";
}