<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\models\redis;

class DeviceMd5Cache extends \Hyperf\Redis\Redis
{
    protected $poolName = "device_md5_cache";
}