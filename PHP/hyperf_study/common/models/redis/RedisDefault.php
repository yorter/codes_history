<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\models\redis;

use Hyperf\Utils\Context;

class RedisDefault extends \Hyperf\Redis\Redis
{
    protected $poolName = "default";

    public function __call($name, $arguments)
    {
        return parent::__call($name, $arguments); // TODO: Change the autogenerated stub
    }
}