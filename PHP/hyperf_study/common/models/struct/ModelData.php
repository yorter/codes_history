<?php

namespace common\models\struct;

use ArrayAccess;

/**
 * @author 韩石强
 * @time 2022/4/11
 */
abstract class ModelData implements ArrayAccess
{
    /**
     * @var string[] 数组类型的字段，数组内容的类型
     */
    protected array $model_data_array_offsets = [];
    /**
     * @var string[] model对象类型的字段对应的类型
     */
    protected array $model_data_offset_limit = [];


    public function offsetExists($offset): bool
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        if (!property_exists($this, $offset)) {
            return null;
        }
        try {
            if (array_key_exists($offset, $this->model_data_offset_limit)) {
                if (!is_object($value) and !is_array($value)) {
                    return null;
                }
                $this->$offset = new $this->model_data_offset_limit[$offset]();
                foreach ($value as $k => $v) {
                    $this->$offset[$k] = $v;
                }
            } elseif (is_array($value) and array_key_exists($offset, $this->model_data_array_offsets)) {
                //ModelData对象数组
                $array = [];
                foreach ($value as $item) {
                    $model_data = new $this->model_data_array_offsets[$offset]();
                    foreach ($item as $k => $v) {
                        $model_data[$k] = $v;
                    }
                    $array[] = $model_data;
                }
                $this->$offset = $array;
            } else {
                $this->$offset = $value;
            }
            return $this->$offset;
        } catch (\TypeError $exception) {

        }
        return $value;
    }

    public function offsetUnset($offset)
    {
        return;
    }

//    public function fillData($data)
//    {
//        foreach ($data as $key => $value) {
//            if (property_exists($this, $key)) {
//                $this->$key = $value;
//            }
//        }
//    }
}