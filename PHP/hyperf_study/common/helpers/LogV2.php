<?php
/**
 * @author 韩石强
 * @time 2022/4/24
 */

namespace common\helpers;

use common\services\adFilter\data\BidModel;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;

class LogV2
{
    const REQUEST_LOG = "request";
    const ERROR_CODE_LOG = "error_code";
    const CALLBACK_LOG = "callback";
    const BID_LOG = "bid";
    const DEVICE_LOG = "device";
    const EXCEPTION_LOG = "exception";
    const TB_CALLBACK_LOG = "tb_callback";
    const RTA_DELTA = "rta_delta";

    public static function log(string $message, $category = self::REQUEST_LOG)
    {
        self::save_log($message, $category);
    }

    public static function logArray(array $message, $category = self::REQUEST_LOG)
    {
        self::save_log($message, $category);
    }

    private static function save_log($message, $category)
    {
        $head = [(new \DateTime())->format("Y-m-d H:i:s.u"), "", 1];
        if (is_array($message)) {
            foreach ($message as $item) {
                $head[] = $item;
            }
        } elseif (is_string($message)) {
            $head[] = $message;
        }
        $logger = ApplicationContext::getContainer()->get(LoggerFactory::class)->get('log', $category);
        $logger->info(implode("\t", $head));
    }

    /**
     * 请求日志
     * @param int $platform 平台
     * @param string $request_id 请求id
     * @param $appid mixed appid
     * @param $pid mixed pid
     * @author 韩石强
     * @time 2022/4/24
     */
    public static function request(int $platform, string $request_id, $appid = "", $pid = "")
    {
        self::logArray([$platform, 'req', $request_id, $appid, $pid], self::REQUEST_LOG);
    }

    /**
     * 回复日志
     * @param int $platform 平台
     * @param string $request_id 请求id
     * @param string $appid mixed appid
     * @param string $pid mixed pid
     * @param string $errorCode mixed pid
     * @param array $data
     * @author 韩石强
     * @time 2022/4/24
     */
    public static function response(int $platform, string $request_id, string $appid = "", string $pid = "", string $errorCode = '', array $data = [])
    {
        self::logArray([$platform, 'res', $request_id, $appid, $pid, $errorCode], self::REQUEST_LOG);
    }

    /**
     * 错误信息日志
     * @param int $code 错误码
     * @param int $platform 平台
     * @param string $request_id 请求id
     * @param string $appid mixed appid
     * @param string $pid mixed pid
     * @param array $ext 扩展数据
     * @author 韩石强
     * @time 2022/4/24
     */
    public static function errorCode(int $code, int $platform, string $request_id, string $appid = "", string $pid = "", array $ext = [], bool $white = false)
    {
        $data = array_merge([$code, $platform, $request_id, $appid, $pid], $ext);
        self::logArray($data, self::ERROR_CODE_LOG);
    }

    /**
     * 使用BidModel传传错误码
     * @param int $code 错误码
     * @param BidModel $bid 竞价信息
     * @param array $ext 额外信息
     * @author 韩石强
     * @time 2022/5/11
     */
    public static function errorCodeWithBidModel(int $code, BidModel $bid, array $ext = [])
    {
        self::errorCode($code, $bid->platform, $bid->requestId, $bid->appId, $bid->pId, $ext, $bid->isWhite);
    }

    /**
     * 回调信息日志
     * @param int $platform 平台id
     * @param int $action_type 1查看 2点击 3dp唤醒
     * @param int $unit_id 推广单元id
     * @param int $plan_id 推广计划id
     * @param string $size 尺寸
     * @param int $creative_id 创意id
     * @param $appid mixed appid
     * @param $pid mixed pid
     * @author 韩石强
     * @time 2022/4/24
     */
    public static function callback(int $platform, string $request_id, $appid, $pid, int $action_type, int $unit_id, int $plan_id, string $size, int $creative_id)
    {
        self::logArray([$platform, $request_id, $appid, $pid, $action_type, $unit_id, $plan_id, $size, $creative_id], self::CALLBACK_LOG);
    }

    /**
     * 竞价日志
     * @param int $platform 平台id
     * @param string $request_id
     * @param $appid mixed appid
     * @param $pid mixed pid
     * @param int $unit_id 推广单元id
     * @param int $plan_id 推广计划id
     * @param string $size 尺寸
     * @param int $creative_id 创意id
     * @param float $price 竞价价格(元)
     * @param int $type 竞价状态 1竞价成功 0竞价
     * @author 韩石强
     * @time 2022/4/25
     */
    public static function bid(int $platform, string $request_id, $appid, $pid, int $unit_id, int $plan_id, string $size, int $creative_id, string $bidPrice, int $type = 0, string $bidFloor = '')
    {
        self::logArray([$platform, $request_id, $appid, $pid, $unit_id, $plan_id, $size, $creative_id, $bidPrice, $type, $bidFloor], self::BID_LOG);
    }

    /**
     * device
     * @param int $platform 平台
     * @param string $request_id 请求Id
     * @param string $appid
     * @param string $pid
     * @param string $device 设备信息
     * @return void
     */
    public static function device(int $platform, string $request_id, string $appid, string $pid, string $device)
    {
        self::logArray([$platform, $request_id, $appid, $pid, $device], self::DEVICE_LOG);
    }

    /**
     * tb-callback
     * @param string $data log信息
     * @return void
     */
    public static function tbCallback(string $data)
    {
        self::log($data, self::TB_CALLBACK_LOG);
    }

    /**
     * 异常日志
     * @param \Throwable $exception
     * @author 韩石强
     * @time 2022/5/17
     */
    public static function Exception(\Throwable $exception)
    {
        self::logArray(
            [$exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine()],
            LogV2::EXCEPTION_LOG
        );
    }

    /**
     * ipv6地址收集，测试用，后面会删除
     * @param string|null $ipv6
     * @author 韩石强
     * @time 2022/5/9
     */
    public static function ipv6(?string $ipv6)
    {
        if ($ipv6) {
            self::log($ipv6, 'ipv6');
        }
    }
}