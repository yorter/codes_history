<?php
/**
 * @author 韩石强
 * @time 2022/5/30
 */

namespace common\helpers\logger;

use Monolog\Handler\StreamHandler;

/**
 * 按小时拆分日志
 */
class HourRotatingFileHandler extends StreamHandler
{
    protected function write(array $record): void
    {
        $this->url = str_replace("__REPLACE__", date('YmdH'), $this->url);
        if (!file_exists($this->url)) {
            $this->close();
        }
        parent::write($record); // TODO: Change the autogenerated stub
    }
}