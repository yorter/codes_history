<?php
/**
 * @author 韩石强
 * @time 2022/4/18
 */

namespace common\helpers;

class Pid
{
    public static function GetPidRedisKey(int $platform, string $pid)
    {
        return sprintf("channel_%d_pid_%s", $platform, $pid);
    }

    public static function GetSizeRedisKey(int $platform, int $width, int $height)
    {
        return sprintf("pid_size_p_%d_w_%d_h_%d", $platform, $width, $height);
    }


    /**
     * cache 缓存根据尺寸+j_channel_pid.id存储
     * @param int $platform 渠道ID
     * @param int $width 宽
     * @param int $height 高
     * @param int $id j_channel_pid.id
     *
     * @return string
     */
    public static function GetSizeIDRedisKey(int $platform, int $width, int $height ,int $id)
    {
        return sprintf("pid_size_p_%d_w_%d_h_%d_id_%d", $platform, $width, $height,$id);
    }
}