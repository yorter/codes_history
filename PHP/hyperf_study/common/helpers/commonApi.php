<?php

namespace common\helpers;

use common\helpers\IpFinder\Ipdbv6;
use common\helpers\IpFinder\IpLocation;


class commonApi
{
    /**
     * 通过IP地址获取地理信息 兼容 IPV4/V6
     * @param $ip
     *
     * @return mixed
     */
    public static function ipFinder($ip)
    {
        if (!self::isipv6($ip)) {
            $res = json_encode(IpLocation::getLocation($ip), JSON_UNESCAPED_UNICODE);
        } else {
            $res = self::query($ip);
        }
        $result = json_decode($res, true);
        return $result['data'];
    }

    /**
     * //硬核判断是否为ipv6地址
     *
     * @param $s
     *
     * @return bool
     */
    private static function isipv6($s)
    {
        return strpos($s, ":") !== false;
    }

    private static function query($ip)
    {
        $db6 = new Ipdbv6();
        $code = 0;
        try {
            if (self::isipv6($ip)) {
                $result = $db6->query($ip);
            }
        } catch (\Exception $e) {
            $result = ["disp" => $e->getMessage()];
            $code = -400;
        }
        $i1 = $result["start"];
        $i2 = $result["end"];
        $disp = $result["disp"];
        $o1 = $result["addr"][0];
        $o2 = $result["addr"][1];
        $disp = str_replace("\"", "\\\"", $disp);
        $o1 = str_replace("\"", "\\\"", $o1);
        $o2 = str_replace("\"", "\\\"", $o2);
        $outstr = <<<EOL
{"code":$code,"data":{"ip":"$ip","range":{"start":"$i1","end":"$i2"},"country":"$o1","isp":"$o2","area":"$disp"}}
EOL;
        return $outstr;
    }
}
