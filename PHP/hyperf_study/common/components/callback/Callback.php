<?php

namespace common\components\callback;


class Callback
{
    //曝光上报
    const TYPE_CHECK = 1;
    //点击上报
    const TYPE_EXPOSURE = 2;
    //deepLink
    const TYPE_DEEP_LINK = 3;

    const CALLBACK_REQUEST_ID = '__REQUEST_ID__';
    const CALLBACK_UNIT_ID = '__UNIT_ID__';
    const CALLBACK_PLAN_ID = '__PLAN_ID__';
    const CALLBACK_CREATIVE_ID = '__CREATIVE_ID__';
    const CALLBACK_SIZE = '__SIZE__';
    const CALLBACK_USER_ID = '__USER_ID__';
    const CALLBACK_PID = '__PID__';
    const CALLBACK_APPID = '__APPID__';
    const CALLBACK_PLATFORM = '__PLATFORM__';
    const CALLBACK_IMEI = '__IMEI__';
    const CALLBACK_OAID = '__OAID__';
    const CALLBACK_OS = '__OS__';
    const CALLBACK_IDFA = '__IDFA__';


}