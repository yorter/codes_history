<?php
namespace common\components\taobao\requests;

/**
 * 广告曝光前判定批量接口V2
 * Class TBBatchAsk
 * @package common\components\taobao\requests
 * @property $oaid_md5
 * @property $idfa_md5
 * @property $imei_md5
 * @property $advertising_space_id
 * @property $channel
 */

class TBBatchAsk extends Request
{
    public string $method = 'taobao.usergrowth.dhh.delivery.batchask';

    public array $params = [
        'oaid_md5' => ['require'],
        'idfa_md5' => ['require'],
        'imei_md5' => ['require'],
        'advertising_space_id' => ['require'],
        'channel' => ['require'],
    ];
}
