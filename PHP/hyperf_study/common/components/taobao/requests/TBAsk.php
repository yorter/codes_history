<?php
namespace common\components\taobao\requests;

/**
 * 广告曝光前判定批量接口V2
 * Class TBAsk
 * @package common\components\taobao\requests
 * @property $profile
 * @property $oaid_md5
 * @property $idfa_md5
 * @property $imei_md5
 * @property $oaid
 * @property $idfa
 * @property $imei
 * @property $os
 * @property $advertising_space_id
 * @property $channel
 */

class TBAsk extends Request
{
    public string $method = 'taobao.usergrowth.dhh.delivery.ask';

    public array $params = [
        'profile' => '',
        'oaid_md5' => '',
        'idfa_md5' => '',
        'imei_md5' => '',
        'oaid' => '',
        'idfa' => '',
        'imei' => '',
        'os' => '',
        'advertising_space_id' => '',
        'channel' => '',
    ];

}
