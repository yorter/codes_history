<?php

namespace common\components\taobao;


use common\components\taobao\requests\Request;
use common\exception\RTAException;
use common\helpers\Log;
use common\helpers\LogV2;
use GuzzleHttp\Client;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \Yii;

class TaobaoClient extends BaseObject
{
    /**
     * 接口key
     * @var string
     */
    public string $appKey;

    /**
     * 接口密匙
     * @var string
     */
    public string $appSecret;

    public string $signMethod = 'md5';

    public string $format = 'json';

    public string $apiVersion = '2.0';

    /**
     * 初始化
     */
    public function init()
    {
        $this->appKey = Yii::$app->params['RTA_KEY'];
        $this->appSecret = Yii::$app->params['RTA_SECRET'];
    }

    /**
     * 入口
     * @param Request $request
     * @param null $session
     * @return mixed
     * @throws Exception
     */
    public function run($request, $session = null)
    {
        $sysParams = [
            'method' => $request->method,
            'app_key' => $this->appKey,
            'sign_method' => $this->signMethod,
            'timestamp' => date("Y-m-d H:i:s"),
            'format' => $this->format,
            'v' => $this->apiVersion,
        ];

        if (null != $session) {
            $sysParams["session"] = $session;
        }

        $apiParams = $request->getApiParams();

        $params = ArrayHelper::merge($sysParams, $apiParams);
        $params['sign'] = $this->sign($params);

        return $this->request(Yii::$app->params['AD_CALLBACK']['TAOBAO_RTA_HTTPS'], $params);
    }


    /**
     * 请求
     * @param $url
     * @param $params
     * @return array
     * @throws RTAException
     */
    public function request($url, $params)
    {
        $start = microtime(true);
        try {
            $client = new Client([
                'verify' => false,
                'timeout' => '0.12'
            ]);
            $response = $client->post($url, ['form_params' => $params])->getBody();
            $data = Json::decode($response->getContents(), true);
        } catch (\Exception $e) {
            Log::warning('TB请求超时');
            $this->deltaTime($start,1);
            throw new RTAException('TB请求超时');
        }
        $this->deltaTime($start,0);

        //如果拉新接口异常,不抛出,避免中断后面的客户结算
        if (!empty($data['error_response']) && $params['method'] == 'taobao.tbk.dg.newuser.order.sum') {
            $data['error'] = 1;
            return $data;
        }

        if (!empty($data['error_response'])) {
            //判断是否存在这个sub_code索引
            if (array_key_exists('sub_code', $data['error_response'])) {
                if ($data['error_response']['sub_code'] == '50001') {
                    return null;
                }
                if (array_key_exists('sub_msg', $data['error_response'])) {
                    throw new Exception('淘宝接口报错：[' . $data['error_response']['sub_code'] . '] ' . $data['error_response']['sub_msg']);
                }else{
                    throw new Exception('淘宝接口报错：[' . $data['error_response']['sub_code'] . '] ');
                }
            }else{
                throw new Exception('淘宝接口报错：['. implode(' ', $data['error_response']). '] ');
            }
        }
        return $data;
    }
    private function deltaTime($start,$isTimeout){
        $end = microtime(true);
        $delta = (string)intval(($end-$start)*1000);
        LogV2::logArray([$delta,$isTimeout],LogV2::RTA_DELTA);
    }

    /**
     * 签名
     * @param array $params
     * @return string
     */
    public function sign($params)
    {
        ksort($params);

        $stringToBeSigned = $this->appSecret;
        foreach ($params as $k => $v) {
            if (!is_array($v) && "@" != substr($v, 0, 1)) {
                $stringToBeSigned .= "$k$v";
            }
        }
        unset($k, $v);
        $stringToBeSigned .= $this->appSecret;

        return strtoupper(md5($stringToBeSigned));
    }

    /**
     * RTA
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function getRTA($request)
    {
        $client = new Client();
        $data = $request->getApiParams();
        $response = $client->post(Yii::$app->params['AD_CALLBACK']['TAOBAO_RTA_HTTPS'], ['query' => $data])->getBody();
        $arr = json_decode($response->getContents(), true, 512, JSON_BIGINT_AS_STRING);
        if ($arr['error'] != '0') {
            throw new Exception($arr['msg']);
        }
        return $arr['data'];
    }

    /**
     * 功    能：生成缓存key
     * @param array $array key数组
     * @return string
     */
    public function generateKey($array = array())
    {
        $key = '';
        ksort($array);
        foreach ($array as $k => $v) {
            $key .= $k . $v . '_';
        }
        unset($k, $v);
        return strtoupper($key);
    }

}
