<?php

namespace common\components\daHangHai;

use yii\base\BaseObject;
use common\components\CallbackInterface;
use common\components\callback\Callback;

class OptimizedCostPerX
{


    //曝光上报
    const TYPE_CHECK = 1;
    //点击上报
    const TYPE_EXPOSURE = 2;

    const APP = [
        'taobao' => 1,//淘宝
        'tmall' => 2,//天猫
        'xianyu' => 3,//咸鱼
        'alipay' => 4,//支付宝
        'dingtalk' => 5,//钉钉
        'tangping' => 6,//躺平
        'shoutao' => 7,//手淘
        'duke' => 9,//直播独客
        '1688' => 11,//1688
        'fliggy' => 14,//飞猪
        'freshhema' => 28//盒马
    ];

    const OCPX_ADAGENT = '__ACCOUNTID__';
    const OCPX_ADGROUPID = '__AID__';
    const OCPX_ADID = '__DID__';
    const OCPX_ADNAME = '__DNAME__';
    const OCPX_CID = '__CID__';
    const OCPX_IMEI = '__IMEI__';
    const OCPX_IMEIMD5 = '__IMEI2__';
    const OCPX_IDFA = '__IDFA__';
    const OCPX_IDFAMD5 = '__IDFA2__';
    const OCPX_OAID = '__OAID__';
    const OCPX_OAIDMD5 = '__OAID2__';

    const OCPX_OS = '__OS__';

}