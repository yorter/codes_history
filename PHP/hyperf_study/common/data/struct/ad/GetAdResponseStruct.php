<?php
/**
 * @author 韩石强
 * @time 2022/3/29
 */

namespace common\data\struct\ad;
/**
 * 返回广告信息的基类
 */
class GetAdResponseStruct
{
    /**
     * @var string|null 广告来源
     */
    public ?string $source = '';
    /**
     * @var int 交互类型：0-跳转落地页，1-下载， 2-deeplink广告。
     */
    public int $action_type;
    /**
     * @var int 广告类型 1图片 2视频（暂定）
     */
    public int $ad_type;
    /**
     * @var int 广告宽
     */
    public int $adw;
    /**
     * @var int 广告高
     */
    public int $adh;
    /**
     * @var string html 广告代码，需要用 webview 展示
     */
    public ?string $html;


    /**
     * @var string[] 广告图片地址数组，一般为 1 个，原生信息流广告可能为多个
     */
    public ?array $image_url;


    /**
     * @var string 落地页(广告点击跳转地址) / 下载地址.
     */
    public ?string $landing_url;

    /**
     * @var string 如果是deeplink广告，需要吊起deeplink_url.搜索终端是否安装对应 app，有则跳转至 app对应地址，无则跳转到 landing_url
     */
    public ?string $deeplink_url;


    /**
     * @var string 视频广告素材地址
     */
    public ?string $video_url;

    /**
     * @var CoverStruct 视频封面信息
     */
    public ?CoverStruct $cover;
    /**
     * @var int 视频广告播放时长，单位为秒
     */
    public int $duration;

    /**
     * @var string[] 展示监控数组，广告展示后触发上报
     */
    public ?array $show_report;
    /**
     * @var string[] 点击监控数组，用户点击后触发上报
     */
    public ?array $click_report;
    /**
     * @var string[] deeplink 类广告成功调用后，触发上报
     */
    public ?array $deeplink_report;
//    /**
//     * @var string[] 加载监控数组，加载广告的同时触发上报
//     */
//    public array $load_report;
//    /**
//     * @var string[] 下载开始上报监控
//     */
//    public array $app_downstart_report;
//    /**
//     * @var string[] 下载成功上报监控
//     */
//    public array $app_downend_report;
//    /**
//     * @var string[] 安装开始上报监控
//     */
//    public array $app_installstart_report;
//    /**
//     * @var string[] 安装成功上报监控
//     */
//    public array $app_installend_report;
//    /**
//     * @var array 安装成功后打开应用上报监控
//     */
//    public array $app_open_report;
//    public object $video_report;


//    /**
//     * @var string 下载包名称
//     */
//    public string $package_name;
//    /**
//     * @var string 信息流广告图标
//     */
//    public string $icon;
//    /**
//     * @var string 信息流广告标题
//     */
//    public string $title;
//    /**
//     * @var string 信息流广告描述
//     */
//    public string $desc;
//    /**
//     * @var string 广告标志 logo(图片 url)
//     */
//    public string $adlogo;
//    /**
//     * @var string 广告标志文字(图片 url)
//     */
//    public string $adtext;
}