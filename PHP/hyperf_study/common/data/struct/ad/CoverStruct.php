<?php
/**
 * @author 韩石强
 * @time 2022/3/29
 */

namespace common\data\struct\ad;

class CoverStruct
{
    public int $width = 0;
    public int $height = 0;
    public string $url;
}