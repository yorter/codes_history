<?php
/**
 * @author 韩石强
 * @time 2022/3/28
 */

namespace common\data\api_return_code;
final class AdServerReturnCodes
{
    //返回成功
    const SUCCESS = 200;
    /**
     * 没有数据
     */
    const NO_AD_DATA = 204;
    /**
     * 服务端错误
     */
    const SERVER_ERROR = 400;
    /**
     * 缺少必要参数
     */
    const REQUIRED = 1000;
    /**
     * ip字段类型不是string
     */
    const IP_MUST_BE_STRING = 1001;
    /**
     * ip错误
     */
    const IP_MATCH_FAIL = 1002;
    /**
     * idfa字段类型错误
     */
    const IDFA_MUST_BE_STRING = 1010;
    /**
     * idfv字段类型错误
     */
    const IDFV_MUST_BE_STRING = 1020;
    /**
     *iOS设备缺少IDFA或IDFV字段
     */
    const IDFA_IDFV_REQUIRED = 1021;
    /**
     * anid字段类型错误
     */
    const ANID_MUST_BE_STRING = 1030;
    /**
     * adid字段类型错误
     */
    const ADID_MUST_BE_STRING = 1040;
    /**
     * imei字段类型错误
     */
    const IMEI_MUST_BE_STRING = 1050;
    /**
     * imei字段无效
     */
    const IMEI_INVALID = 1051;
    /**
     * oaid字段类型错误
     */
    const OAID_MUST_BE_STRING = 1060;
    /**
     * 安卓设备缺少OAID或IMEI字段
     */
    const OAID_IMEI_REQUIRED = 1061;
    /**
     * imsi字段类型错误
     */
    const IMSI_MUST_BE_STRING = 1070;
    /**
     * mac地址字段类型错误
     */
    const MAC_MUST_BE_STRING = 1080;
    /**
     * ua字段类型错误
     */
    const UA_MUST_BE_STRING = 1090;
    /**
     * type不在指定范围内
     */
    const TYPE_NOT_IN_RANGE = 1100;
    /**
     * os字段类型错误
     */
    const OS_NOT_IN_RANGE = 1110;
    /**
     * 系统版本字段类型错误
     */
    const OSV_MUST_BE_STRING = 1120;
    /**
     * 设备品牌字段类型错误
     */
    const BRAND_MUST_BE_STRING = 1130;
    /**
     * 设备型号字段类型错误
     */
    const MODEL_MUST_BE_STRING = 1140;
    /**
     * 设备屏幕宽度必须是int类型
     */
    const SW_MUST_BE_INT = 1150;
    /**
     * 设备屏幕宽度无效
     */
    const SW_INVALID = 1151;
    /**
     * 设备屏幕高度必须是整数
     */
    const SH_MUST_BE_INT = 1160;
    /**
     * 设备屏幕高度无效
     */
    const SH_INVALID = 1161;
    /**
     * 纬度必须是数字
     */
    const LAT_MUST_BE_FLOAT = 1170;
    /**
     * 纬度值无效
     */
    const LAT_INVALID = 1171;
    /**
     * 纬度必须是数字
     */
    const LON_MUST_BE_FLOAT = 1180;
    /**
     * 纬度值无效
     */
    const LON_INVALID = 1181;
    /**
     * 横竖屏不在选项范围内
     */
    const ORIENTATION_NOT_IN_RANGE = 1190;
    /**
     * 移动国家码字段类型错误
     */
    const MCC_MUST_BE_STRING = 1200;
    /**
     * 移动网络码字段类型错误
     */
    const MNC_MUST_BE_STRING = 1210;
    /**
     * 网络类型不在指定范围内
     */
    const CONNECTION_NOT_IN_RANGE = 1220;
    /**
     * 屏幕像素密度必须是个数字
     */
    const DENSITY_MUST_BE_FLOAT = 1230;
    /**
     * 无效的屏幕像素密度
     */
    const DENSITY_INVALID = 1231;
    /**
     * 当前使用的语言必须是字符串
     */
    const LANG_MUST_BE_STRING = 1240;
    /**
     *厂商应用商店版本号字段类型错误
     */
    const APPSTOREVER_MUST_BE_STRING = 1250;
    /**
     * 华为 HMS Core 版本号字段类型错误
     */
    const HMSCORE_MUST_BE_STRING = 1260;
    /**
     * 系统更新标识字段类型错误
     */
    const UPDATEMARK_MUST_BE_STRING = 1270;
    /**
     * 系统启动标识字段类型错误
     */
    const BOOTMARK_MUST_BE_STRING = 1280;
    /**
     * 渠道id错误
     */
    const CHID_TYPE_ERROR = 1290;
    /**
     * 无效的渠道id
     */
    const CHID_INVALID = 1291;
    /**
     *pid类型错误
     */
    const PID_TYPE_ERROR = 1300;
    /**
     * 无效的pid
     */
    const PID_INVALID = 1301;

    /**
     * pid不存在
     */
    const PID_NON_EXISTENT = 1302;

    /**
     * 返回码与值对应的信息
     */
    const CODE_TO_MESSAGE = [
        //验证错误
        self::SUCCESS => '成功',
        self::REQUIRED => '缺少必要参数',
        self::IP_MUST_BE_STRING => 'ip字段字段类型错误',
        self::IP_MATCH_FAIL => 'ip错误',
        self::IDFA_MUST_BE_STRING => 'idfa字段类型错误',
        self::IDFV_MUST_BE_STRING => 'idfv字段类型错误',
        self::ANID_MUST_BE_STRING => 'anid字段类型错误',
        self::ADID_MUST_BE_STRING => 'adid字段类型错误',
        self::IMEI_MUST_BE_STRING => 'imei字段类型错误',
        self::IMEI_INVALID => 'imei字段无效',
        self::OAID_MUST_BE_STRING => 'oaid字段类型错误',
        self::IMSI_MUST_BE_STRING => 'imsi字段类型错误',
        self::OAID_IMEI_REQUIRED => '安卓设备缺少OAID或IMEI字段',
        self::MAC_MUST_BE_STRING => 'mac地址字段类型错误',
        self::UA_MUST_BE_STRING => 'ua字段类型错误',
        self::TYPE_NOT_IN_RANGE => 'type不在指定范围内',
        self::IDFA_IDFV_REQUIRED => 'iOS设备缺少IDFA或IDFV字段',
        self::OS_NOT_IN_RANGE => 'os字段类型错误',
        self::OSV_MUST_BE_STRING => '系统版本字段类型错误',
        self::BRAND_MUST_BE_STRING => '设备品牌字段类型错误',
        self::MODEL_MUST_BE_STRING => '设备型号字段类型错误',
        self::SW_MUST_BE_INT => '设备屏幕宽度必须是int类型',
        self::SW_INVALID => '设备屏幕宽度无效',
        self::SH_MUST_BE_INT => '设备屏幕高度必须是整数',
        self::SH_INVALID => '设备屏幕高度无效',
        self::LAT_MUST_BE_FLOAT => '纬度必须是数字',
        self::LAT_INVALID => '纬度值无效',
        self::LON_MUST_BE_FLOAT => '纬度必须是数字',
        self::LON_INVALID => '纬度值无效',
        self::ORIENTATION_NOT_IN_RANGE => '横竖屏不在选项范围内',
        self::MCC_MUST_BE_STRING => '移动国家码字段类型错误',
        self::MNC_MUST_BE_STRING => '移动网络码字段类型错误',
        self::CONNECTION_NOT_IN_RANGE => '网络类型不在指定范围内',
        self::DENSITY_MUST_BE_FLOAT => '屏幕像素密度必须是个数字',
        self::DENSITY_INVALID => '无效的屏幕像素密度',
        self::LANG_MUST_BE_STRING => '当前使用的语言必须是字符串',
        self::APPSTOREVER_MUST_BE_STRING => '厂商应用商店版本号字段类型错误',
        self::HMSCORE_MUST_BE_STRING => '华为 HMS Core 版本号字段类型错误',
        self::UPDATEMARK_MUST_BE_STRING => '系统更新标识字段类型错误',
        self::BOOTMARK_MUST_BE_STRING => '系统启动标识字段类型错误',
        self::CHID_TYPE_ERROR => '渠道id错误',
        self::CHID_INVALID => '无效的渠道id',
        self::PID_TYPE_ERROR => 'pid错误',
        self::PID_INVALID => '无效的pid',
        self::PID_NON_EXISTENT => 'PID不存在',


        //返回错误
        self::NO_AD_DATA => '没有相关广告',
        self::SERVER_ERROR => '服务端错误',
    ];

    /**
     * 获取返回code对应的信息
     * @param $code int 返回码
     * @return string
     * @author 韩石强 信息
     * @time 2022/3/29
     */
    public static function getCodeMessage(int $code): string
    {
        return array_key_exists($code, self::CODE_TO_MESSAGE) ? self::CODE_TO_MESSAGE[$code] : "未知错误";
    }
}