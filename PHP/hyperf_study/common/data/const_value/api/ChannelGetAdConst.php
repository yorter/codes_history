<?php

namespace common\data\const_value\api;
/**
 * @author 韩石强
 * @time 2022/4/6
 */
class ChannelGetAdConst
{
    /**
     * 安卓
     */
    const OS_ANDROID = 1;
    /**
     * ios
     */
    const OS_IOS = 0;
    /**
     * -跳转落地页
     */
    const ACTION_TYPE_LANDING = 0;
    /**
     * -下载
     */
    const ACTION_TYPE_DOWNLOAD = 1;
    /**
     * -deeplink广告
     */
    const ACTION_TYPE_DEEPLINK = 2;
}