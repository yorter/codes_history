<?php

namespace common\services\ad;


class BaseAd
{
    //定向名称
    const CACHE_KEY_REGION = 'region';
    const CACHE_KEY_CROWD = 'crowd';
    const CACHE_KEY_INTEREST = 'interest';
    const CACHE_KEY_EXPOSURE_FREQUENCY = 'exposure_frequency';
    const CACHE_KEY_CHECK_FREQUENCY = 'check_frequency';
    const CACHE_KEY_KEYWORD = 'keyword';
    const CACHE_KEY_IP = 'ip';
    const CACHE_KEY_VIDEO = 'video';
    const CACHE_KEY_MOBILE = 'mobile';
    const CACHE_KEY_MOBILES = 'mobiles';
    const CACHE_KEY_MEDIA = 'media';
    const CACHE_KEY_PC = 'pc';

    /**
     * 获取定向包筛选key
     * @param string $type
     * @param string $cacheKey
     * @param string $key
     * @return string
     */
    protected static function getDirectionalCacheKey(string $type, string $cacheKey, string $key): string
    {
        switch ($type) {
            case self::CACHE_KEY_REGION:
                return sprintf($cacheKey, self::CACHE_KEY_REGION, $key);
            case self::CACHE_KEY_CROWD:
                return sprintf($cacheKey, self::CACHE_KEY_CROWD, $key);
            case self::CACHE_KEY_INTEREST:
                return sprintf($cacheKey, self::CACHE_KEY_INTEREST, $key);
            case self::CACHE_KEY_EXPOSURE_FREQUENCY:
                return sprintf($cacheKey, self::CACHE_KEY_EXPOSURE_FREQUENCY, $key);
            case self::CACHE_KEY_CHECK_FREQUENCY:
                return sprintf($cacheKey, self::CACHE_KEY_CHECK_FREQUENCY, $key);
            case self::CACHE_KEY_KEYWORD:
                return sprintf($cacheKey, self::CACHE_KEY_KEYWORD, $key);
            case self::CACHE_KEY_IP:
                return sprintf($cacheKey, self::CACHE_KEY_IP, $key);
            case self::CACHE_KEY_VIDEO:
                return sprintf($cacheKey, self::CACHE_KEY_VIDEO, $key);
            case self::CACHE_KEY_MOBILE:
                return sprintf($cacheKey, self::CACHE_KEY_MOBILE, $key);
            case self::CACHE_KEY_MEDIA:
                return sprintf($cacheKey, self::CACHE_KEY_MEDIA, $key);
            case self::CACHE_KEY_PC:
                return sprintf($cacheKey, self::CACHE_KEY_PC, $key);
            default:
                return false;
        }
    }
}