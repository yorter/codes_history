<?php

namespace common\services\ad;

use common\bid\struct\DeviceModelData;
use common\models\redis\RTA;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class AdRequest extends BaseAd
{
    //redis指向库
    const REDIS_DATABASE = 0;
    //xx平台device每天的记录缓存key
    const DEVICE_CACHE_KEY = 'Platform:%s_OS:%s_Device:%s';
    //xx平台
    const RTA_DEVICE_CACHE_KEY = 'Platform:%s_TaskId:%s_Type:%s_OS:%s_Date:%s';
    //RTA黑名单
    const RTA_BLACK_LIST = 'RTA_Black_List_OS:%s';
    //白名单用户
    const DEVICE_WHITE_LIST = 'white_list';

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private static function redis()
    {
        $container = ApplicationContext::getContainer();
        return $container->get(RTA::class);
    }

    /**
     * 获取是否是白名单用户
     * @param string $device
     * @return bool
     */
    public static function getWhitList(string $device): bool
    {
        $white = self::redis()->sismember(self::DEVICE_WHITE_LIST, $device);
        if (!$white) {
            return false;
        }
        return true;
    }

    /**
     * 存入设备信息
     * @param int $platformId
     * @param string $os
     * @param string $device
     * @return bool
     */
    public static function setDevice(int $platformId, string $os, string $device): bool
    {
        return self::incr(sprintf(self::DEVICE_CACHE_KEY, $platformId, $os, $device));
    }

    /**
     * 获取设备信息
     * @param int $platformId
     * @param string $os
     * @param string $device
     * @return mixed
     */
    public static function getDevice(int $platformId, string $os, string $device)
    {
        return self::get(sprintf(self::DEVICE_CACHE_KEY, $platformId, $os, $device));
    }

    /**
     * 根据device存入RTA
     * @param int $platformId
     * @param string $taskId
     * @param string $os
     * @param string $deviceType
     * @param string $device
     * @return mixed
     */
    public static function setRTADevice(int $platformId, string $taskId, string $os, string $device, string $deviceType = '')
    {
        if (!empty($taskId)) {
            $date = date('Ymd');
        } else {
            $date = 'all';
        }
        $cacheKey = sprintf(
            self::RTA_DEVICE_CACHE_KEY,
            $platformId,
            $taskId,
            $deviceType,
            $os,
            $date
        );
        $isExist = self::existKey($cacheKey);
        $res = self::addSet($cacheKey, $device);
        if (!$isExist) {
            self::setTTL($cacheKey, self::tomorrowZeroTTL());
        }
        return $res;
    }

    public static function existKey($cacheKey)
    {
        return self::redis()->exists($cacheKey);
    }

    public static function setTTL($cacheKey, $ttl)
    {
        return self::redis()->expire($cacheKey, $ttl);
    }

    /**
     * 获取截至明日零点剩余秒数
     *
     * @return false|int
     */
    private static function tomorrowZeroTTL()
    {
        $res = strtotime(date("Ymd000000", strtotime("+ 1 day"))) - time();
        return $res;
    }

    /**
     * 根据device获取最近是否存入过RTA
     * @param int $platformId
     * @param string $taskId
     * @param string $os
     * @param string $deviceType
     * @param string $device
     * @return mixed
     */
    public static function getRTADevice(int $platformId, string $taskId, string $os, string $device, string $deviceType = '')
    {
        if (!empty($taskId)) {
            $date = date('Ymd');
        } else {
            $date = 'all';
        }
        return self::getExist(sprintf(self::RTA_DEVICE_CACHE_KEY, $platformId, $taskId, $deviceType, $os, $date), $device);
    }

    /**
     * 获取手机设备黑名单
     * @param string $os
     * @param string $member
     * @return mixed
     */
    public static function getBlackList(string $os, string $member)
    {
        return self::redis()->sismember(sprintf(self::RTA_BLACK_LIST, $os), $member);
    }

    /**
     * 存入手机设备黑名单
     * @param string $os
     * @param string $member
     * @return mixed
     */
    public static function setBlackList(string $os, string $member)
    {
        return self::redis()->sadd(sprintf(self::RTA_BLACK_LIST, $os), $member);
    }

    /**
     * 自增
     * @param $cacheKey
     * @return bool
     */
    private static function incr($cacheKey)
    {
        $seconds = self::tomorrowZeroTTL();
        $redis = self::redis();
        $redis->incr($cacheKey);
        $redis->expire($cacheKey, $seconds);
        return true;
    }

    /**
     * 获取字符串
     * @param $cacheKey
     * @return mixed
     */
    private static function get($cacheKey)
    {
        $data = self::redis()->get($cacheKey);
        return json_decode($data, true);
    }

    /**
     * 往集合里面添加数据
     * @param string $cacheKey
     * @param string $member
     * @return mixed
     */
    private static function addSet(string $cacheKey, string $member)
    {
        return self::redis()->sadd($cacheKey, $member);
    }

    /**
     * 查询集合里面是否存在某条数据
     * @param string $cacheKey
     * @param string $member
     * @return mixed
     */
    private static function getExist(string $cacheKey, string $member)
    {
        return self::redis()->sismember($cacheKey, $member);
    }

    /**
     * @param string $os
     * @param string $uniqueId
     * @param DeviceModelData $device
     * @return void
     */
    public static function setDeviceData(string $os, string $uniqueId, DeviceModelData $device)
    {
        $device->save(16, 17, false, 18);
//        if ($os != 'Android') {
//            $key = "ios___" . $uniqueId;
//        } else {
//            $key = $uniqueId;
//        }
//        $queue_no = crc32($key) % 100;
//        $redis_key = "pick_devices_" . $queue_no;
//
//        $redis =  \Yii::$app->redis;
//        $redis->select(16);
//        $redis->lpush($redis_key, json_encode($device, JSON_UNESCAPED_UNICODE));
    }
}