<?php

namespace common\services\ad;


class AdPlatform extends BaseAd
{
    //加速包
    const UNIT_DOMAIN_ON = 2;//开启

    /**
     * 推广单元
     * 批量修改
     */
    const UNIT_OPEN = 'open'; //开启
    const UNIT_CLOSURE = 'closure'; //关闭
    const UNIT_DELETE = 'delete'; //删除
    const UNIT_UPDATE_PRICE = 'price'; //修改价格
    const UNIT_UPDATE_DAY_BUDGET = 'day_budget'; //每日预算
    const UNIT_UPDATE_DOMAIN = 'domain'; //加速包
    const UNIT_UPDATE_START = 'start'; //修改投放日期
    const UNIT_UPDATE_AREA = 'area'; //修改地区
    const UNIT_INTEREST = 'interest'; //修改兴趣
    const UNIT_TERMINAL = 'terminal'; //终端定向
    const UNIT_CHECK = 'check';//点击上限
    const UNIT_VIEW = 'view'; //曝光
    const UNIT_CLICK_FREQUENCY = 'click_frequency'; //点击控制频次
    const UNIT_EXPOSURE_FREQUENCY = 'exposure_frequency'; //曝光频次控制
    /**
     * 推广计划
     * 批量修改
     */
    const PLAN_BUDGET = 'day_budget';
    const PLAN_TOTAL_BUDGET = 'total_budget';

    /**
     * 地区
     */
    const AREA_ALL = 0;//不限
    const AREA_CHINA = 1;//中国
    const AREA_OVERSEAS = 2;//海外

    //pc && mobile
    const UN_LIMITED = 0;//不限
    const DIRECTIONAL_SYSTEM = [
        1,//mobile
        2//pc
    ];

    //手机系统
    const MOBILE_OS = [
        0 => '不限',
        1 => 'IOS',
        2 => 'Android',
        3 => 'Other'
    ];

    //手机网络运营商
    const MOBILE_OPERATOR = [
        0 => '不限',
        1 => '移动',
        2 => '联通',
        3 => '电信'
    ];

    //手机网络环境
    const MOBILE_ENVIRONMENT = [
        0 => '不限',
        1 => 'WIFI',
        2 => '4G',
        3 => '3G',
        4 => '2G',
        5 => 'Other',
        6 => '5G'
    ];

    //手机品牌
    const MOBILE_BRAND = [
        0 => '不限',
        1 => '360',
        2 => 'Apple',
        3 => 'HUAWEI',
        4 => 'oppo',
        5 => 'vivo',
        6 => 'Honor',
        7 => 'XIAOMI',
        8 => 'Samsung',
        9 => 'MEIZU',
        10 => 'ONEPLUS',
        11 => 'SONY',
        12 => 'Nubia',
        13 => 'Lenovo',
        14 => 'MEITU',
        15 => 'ZTE',
        16 => 'HTC',
        17 => 'COOL',
        18 => 'Smartisan',
        19 => 'Other'
    ];

    //pc操作系统
    const PC_OS = [
        0 => '不限',
        1 => 'Windows',
        2 => 'Mac OS',
        3 => 'Linux'
    ];

    //pc浏览器
    const PC_BROWSER = [
        0 => '不限',
        1 => 'IE8',
        2 => 'IE9',
        3 => 'IE10',
        4 => 'IE11',
        5 => 'Chrome',
        6 => 'Firefox',
        7 => 'Safari',
        8 => 'Opera',
        9 => 'baidu',
        10 => 'Maxthon',
        11 => 'The World',
        12 => 'Sogou',
        13 => 'Liebao',
        14 => 'QQ',
        15 => '360',
        16 => 'Other'
    ];

    //频次时间
    const FREQUENCY_INTERVAL_UNIT = [
        0 => '不限期',
        1 => '月',
        2 => '周',
        3 => '天',
        4 => '小时'
    ];

    //默认
    const DEFAULT_ALL = 0;
    const STRING_DEFAULT_ALL = 0;

    //正常投放状态
    const UNIT_STATUS_ON = 1;


    /**
     * 根据平台尺寸获取广告位信息
     * @param int $platform
     * @param int $width
     * @param int $high
     * @return mixed
     */
    public static function getPid(int $platform, int $width, int $high)
    {
        $key = AdCache::GetSizeRedisKey($platform, $width, $high);
        $fuzzyRes = AdCache::getAdInfoCacheFuzzy($key);
        $res = [];
        if ($fuzzyRes) {
            $result = array_column($fuzzyRes, null, 'id');
            ksort($result);
            $res = array_pop($result);
        }
        return $res;
    }
}