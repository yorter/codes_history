<?php

namespace common\services\ad;


use common\models\redis\DspInfo;
use Hyperf\Utils\ApplicationContext;

class AdCache extends BaseAd
{
    const AD_CACHE_DATABASE = 2;
    //广告缓存筛选key %s过滤类型%s过滤值
    const AD_FILTER_CACHE_KEY = 'AdFilter:%s-%s';//set
    //手机定向
    const AD_MOBILE_FILTER_CACHE_KEY = 'Ad_Mobile_Filter_Os:%s_Operator:%s_Environment:%s_Brand:%s';
    //pc定向
    const AD_PC_FILTER_CACHE_KEY = 'Ad_Pc_Filter_Os:%s_Browser:%s';
    //广告缓存key %s宽高%s素材Id
    const AD_INFO_CACHE_KEY = 'Ad_PID:%s_Info_Id:%s';//string
    //广告长宽过滤key %s宽%s高
    const AD_CACHE_WIDTH_HIGH_KEY = 'Ad_Info_Pid:%s';//set
    //广告价格集合key %s出价类型
    const AD_CACHE_PRICE_KEY = 'Ad_Price_Info:%s';//zSet
    //日期筛选缓存key %s星期几%s小时段
    const AD_DATE_CACHE_KEY = 'Ad_Date_Filter:%s-%s';//set
    //具体到某天的日期筛选key %s具体日期
    const AD_TO_DAY_CACHE_KEY = 'Ad_Date_%s_Filter';//set
    //加速包
    const AD_DOMAIN_CACHE_KEY = 'Ad_Domain';//SET
    //推广单元上限 %s推广单元Id
    const AD_UNIT_CACHE_KEY = 'Ad_UnitId:%s_limit';//string
    //推广计划上限 %s推广计划Id
    const AD_PLAN_CACHE_KEY = 'Ad_Plan:%s_limit';//string
    //推广计划信息 %s推广单元Id
    const AD_UNIT_KEY = 'Ad_Unit_Info_id:%s';
    //素材信息 %s创意组Id
    const AD_INFO_KEY = 'Creative_Info_id:%s';
    //平台信息
    const AD_PLATFORM_KEY = 'platform:%s';
    //appid信息
    const AD_APPID_KEY = 'AppId:%s';
    //pid信息
    const AD_PID_KEY = 'Pid:%s';
    //pid信息
    const AD_DAHANGHHAI_SPACE_ID_KEY = 'dahanghai_Space_id:%s';
    //pid信息
    const AD_DAHANGHHAI_CHANNEL_ID_KEY = 'dahanghai_Channel_Id:%s';
    //频次控制
    const AD_FREQUENCY_STRATEGY_KEY = 'Ad_%s_UnitId:%s_Frequency_Strategy:%s';

    //筛选交集
    //时间交集筛选 %s平台%s时间%s_时间段appid_pid
    const GATHERED_DATE_KEY = '%s_gathered_date_%s_%s-%s_appid:%s_pid:%s';
    //范围交集筛选 %s平台%s价格%s宽%s高
    const GATHERED_SCOPE_KEY = '%s_gathered_scope:%s';
    //定向交集筛选%s平台%s地区%s人群 %s mobile||pc
    const GATHERED_DIRECTIONAL_KEY = 'platform:%s_gathered_directional_%s_%s_%s_%s_%s';
    //推广单元日预算
    const UNIT_COST_KEY = 'unit_cost:%s_date:%s';
    //推广计划日预算
    const PLAN_COST_KEY = 'plan_cost:%s_date:%s';
    //推广计划总预算
    const PLAN_TOTAL_COST_KEY = 'plan_total_cost:%s';
    //点击
    const CHECK_LIMIT_KEY = 'UnitId:%s_Check_Limit_Date:%s';
    //曝光
    const EXPOSURE_LIMIT_KEY = 'UnitId:%s_Exposure_Limit_Date:%s';
    //任务定向
    const TASK_ID_KEY = 'TaskId:%s';
    //出价策略
    const BID_STRATEGY_KEY = 'Bid_Strategy';
    //地区并集
    const BID_AREA_UNION = 'Bid_Area_Union_Appid:%s_Pid:%s';
    //手机设备并集
    const BID_MOBILE_UNION = 'Bid_Mobile_Union_Appid:%s_Pid:%s';

    const CACHE_EXPIRATION = -1;

    //地区
    const AREA_DEFAULT = '不限地区';
    const AREA_DEFAULT_TYPE = 0;
    //兴趣
    const INTEREST_DEFAULT = '不限';
    const INTEREST_DEFAULT_TYPE = 0;

    const TYPE_CLICK = 'click';
    const TYPE_EXPOSURE = 'exposure';

    const OS_DEFAULT = '不限';

    const DEFAULT_PRICE = 0;


    //Click过滤器 hit & eff 记录KEY
    const CLICK_ANCHOR_HIT_NUM_KEY = 'click_anchor:hit_num:%s:%s';
    const CLICK_ANCHOR_EFF_NUM_KEY = 'click_anchor:eff_num:%s:%s';

    //PV过滤器 hit & eff 记录KEY
    const PV_ANCHOR_HIT_NUM_KEY = 'pv_anchor:hit_num:%s:%s';
    const PV_ANCHOR_EFF_NUM_KEY = 'pv_anchor:eff_num:%s:%s';


    //系统设置-点击过滤器KEY
    const CLICK_ANCHOR_CONFIG = 'sysconfig:click_anchor_config';
    //系统设置-点击过滤器KEY
    const PV_ANCHOR_CONFIG = 'sysconfig:pv_anchor_config';


    const CLICK_BLACKLIST = 'blacklist:click_filter:%s:%s';//点击过滤器黑名单
    const PV_BLACKLIST = 'blacklist:pv_filter:%s:%s';//PV过滤器黑名单

    const ANCHOR_ENABLE = 'enable';//点击层面配置 开关 x天 y命中 z点击 N天拉黑
    const ANCHOR_DAYS = 'anchor_days';
    const ANCHOR_HIT_NUM = 'anchor_hit_num';
    const ANCHOR_EFF_NUM = 'anchor_eff_num';
    const BLACKLIST_TTL_DAYS = 'blacklist_ttl_days';


    private static function redis()
    {
        $container = ApplicationContext::getContainer();
        return $container->get(DspInfo::class);
    }

    /**
     * @param string $appid
     * @param string $pid
     * @return string
     */
    public static function getBidAreaUnionKey(string $appid, string $pid): string
    {
        return sprintf(self::BID_AREA_UNION, $appid, $pid);
    }

    /**
     * @param string $appid
     * @param string $pid
     * @return string
     */
    public static function getBidMobileUnionKey(string $appid, string $pid): string
    {
        return sprintf(self::BID_MOBILE_UNION, $appid, $pid);
    }

    /**
     * @return string
     */
    public static function getBidStrategyKey(): string
    {
        return self::BID_STRATEGY_KEY;
    }

    /**
     * @param int $platform
     * @param string $pid
     * @return string
     */
    public static function GetPidRedisKey(int $platform, string $pid): string
    {
        return sprintf("channel_%d_pid_%s", $platform, $pid);
    }

    /**
     * @param int $platform
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function GetSizeRedisKey(int $platform, int $width, int $height): string
    {
        return sprintf("pid_size_p_%d_w_%d_h_%d", $platform, $width, $height);
    }

    /**
     * @param string $taskId
     * @return string
     */
    public static function getTskIdKey(string $taskId): string
    {
        return sprintf(self::TASK_ID_KEY, $taskId);
    }

    /**
     * @param int $unitId
     * @return string
     */
    public static function getUnitCostKey(int $unitId): string
    {
        return sprintf(self::UNIT_COST_KEY, $unitId, date('Y-m-d'));
    }

    /**
     * @param int $planId
     * @return string
     */
    public static function getPlanCostKey(int $planId): string
    {
        return sprintf(self::PLAN_COST_KEY, $planId, date('Y-m-d'));
    }

    /**
     * @param int $planId
     * @return string
     */
    public static function getPlanTotalCostKey(int $planId): string
    {
        return sprintf(self::PLAN_TOTAL_COST_KEY, $planId, date('Y-m-d'));
    }

    /**
     * @param int $unitId
     * @return string
     */
    public static function getUnitCheckLimitByIdKey(int $unitId): string
    {
        return sprintf(self::CHECK_LIMIT_KEY, $unitId, date('Y-m-d'));
    }

    /**
     * @param int $unitId
     * @return string
     */
    public static function getUnitExposureLimitByIdKey(int $unitId): string
    {
        return sprintf(self::EXPOSURE_LIMIT_KEY, $unitId, date('Y-m-d'));
    }

    /**
     * @param int $unitId
     * @param string $type
     * @param string $strategy
     * @return string
     */
    public static function getFrequencyKey(string $type, int $unitId, string $strategy): string
    {
        return sprintf(self::AD_FREQUENCY_STRATEGY_KEY, $type, $unitId, $strategy);
    }

    /**
     * @param string $os
     * @param string $operator
     * @param string $environment
     * @param string $brand
     * @return string
     */
    public static function getMobileKey(string $os, string $operator, string $environment, string $brand): string
    {
        return sprintf(self::AD_MOBILE_FILTER_CACHE_KEY, $os, $operator, $environment, $brand);
    }

    /**
     * @param string $os
     * @param string $browser
     * @return string
     */
    public static function getPcKey(string $os, string $browser): string
    {
        return sprintf(self::AD_PC_FILTER_CACHE_KEY, $os, $browser);
    }

    /**
     * @param int $platformId
     * @param string $date
     * @param int $w
     * @param int $h
     * @param string $appId
     * @param string $pid
     * @return string
     */
    public static function getGatheredDateKey(int $platformId, string $date, int $w, int $h, string $appId, string $pid): string
    {
        return sprintf(self::GATHERED_DATE_KEY, $platformId, $date, $w, $h, $appId, $pid);
    }

    /**
     * @param int $platformId
     * @param string $price
     * @return string
     */
    public static function getGatheredScopeKey(int $platformId, string $price): string
    {
        return sprintf(self::GATHERED_SCOPE_KEY, $platformId, $price);
    }

    /**
     * @param int $platformId
     * @param string $os
     * @param string $area
     * @param string $interest
     * @param string $keyword
     * @param string $video
     * @return string
     */
    public static function getGatheredDirectionalKey(int $platformId, string $os, string $area, string $interest, string $keyword, string $video): string
    {
        return sprintf(self::GATHERED_DIRECTIONAL_KEY, $platformId, $area, $interest, $os, $keyword, $video);
    }

    /**
     * @param int $platformId
     * @return string
     */
    public static function getPlatformKey(int $platformId): string
    {
        return sprintf(self::AD_PLATFORM_KEY, $platformId);
    }

    /**
     * @param string $appid
     * @return string
     */
    public static function getAppIdKey(string $appid): string
    {
        return sprintf(self::AD_APPID_KEY, $appid);
    }

    /**
     * @param string $pId
     * @return string
     */
    public static function getPidKey(string $pId): string
    {
        return sprintf(self::AD_PID_KEY, $pId);
    }

    /**
     * @param string $spaceId
     * @return string
     */
    public static function getDaHangHaiSpaceIdKey(string $spaceId): string
    {
        return sprintf(self::AD_DAHANGHHAI_SPACE_ID_KEY, $spaceId);
    }

    /**
     * @param string $pid
     * @return string
     */
    public static function getDaHangHaiChannelIdKey(string $pid): string
    {
        return sprintf(self::AD_DAHANGHHAI_CHANNEL_ID_KEY, $pid);
    }

    /**
     * 获取加速包key
     * @return string
     */
    public static function getDomainKey(): string
    {
        return self::AD_DOMAIN_CACHE_KEY;
    }

    /**
     * 获取价格集合Key
     * @param string $type 出价类型
     * @return string
     */
    public static function getAdPriceKey(string $type): string
    {
        return sprintf(self::AD_CACHE_PRICE_KEY, $type);
    }

    /**
     * 获取广告详情Key
     * @param string $pId
     * @param $unitId
     * @return string
     */
    public static function getAdInfoKey(string $pId, $unitId): string
    {
        return sprintf(self::AD_INFO_CACHE_KEY, $pId, $unitId);
    }

    /**
     * 获取广告详情Key
     * @param int $unitId
     * @return string
     */
    public static function getUnitInfoKey(int $unitId): string
    {
        return sprintf(self::AD_UNIT_KEY, $unitId);
    }

    /**
     * 获取广告详情Key
     * @param int $adId
     * @return string
     */
    public static function getCreativeInfoKey(int $adId): string
    {
        return sprintf(self::AD_INFO_KEY, $adId);
    }

    /**
     * 获取推广计划预算上限Id
     * @param int $unitId
     * @return string
     */
    public static function getAdUnitKey(int $unitId): string
    {
        return sprintf(self::AD_UNIT_CACHE_KEY, $unitId);
    }

    /**
     * 获取推广单元预算上限Id
     * @param int $planId
     * @return string
     */
    public static function getAdPlanKey(int $planId): string
    {
        return sprintf(self::AD_PLAN_CACHE_KEY, $planId);
    }

    /**
     * 获取定向包过滤Key
     * @param string $type 过滤类型
     * @param string $info 过滤信息
     * @return string
     */
    public static function getAdFilterDataKey(string $type, string $info): string
    {
        return self::getDirectionalCacheKey($type, self::AD_FILTER_CACHE_KEY, $info);
    }

    /**
     * 获取指定日期Key
     * @param string $date 日期
     * @return string
     */
    public static function getAdSpecifiedDataKey(string $date): string
    {
        return sprintf(self::AD_TO_DAY_CACHE_KEY, $date);
    }

    /**
     * 获取指定时间段Key
     * @param int $w 星期
     * @param int $h 小时
     * @return string
     */
    public static function getAdPeriodDataKey(int $w, int $h): string
    {
        return sprintf(self::AD_DATE_CACHE_KEY, $w, $h);
    }

    /**
     * 获取尺寸宽度高度Key
     * @param string $pId
     * @return string
     */
    public static function getAdScopeDataKey(string $pId): string
    {
        return sprintf(self::AD_CACHE_WIDTH_HIGH_KEY, $pId);
    }

    /**
     * 往集合里面新增数据
     * set
     * @param string $cacheKey
     * @param int $data
     * @return mixed
     */
    public static function addSet(string $cacheKey, $data)
    {
        return self::redis()->sadd($cacheKey, $data);
    }

    public static function setTTL($cacheKey, $ttl)
    {
        return self::redis()->expire($cacheKey, $ttl);
    }

    public static function existKey($cacheKey)
    {
        return self::redis()->exists($cacheKey);
    }

    /**
     * 获取集合里面所有的数据
     * set
     * @param string $cacheKey
     * @return mixed
     */
    public static function getSet(string $cacheKey)
    {
        return self::redis()->smembers($cacheKey);
    }

    /**
     * 获取集合里面是否存在某个值
     * set
     * @param string $cacheKey
     * @param string $member
     * @return mixed
     */
    public static function getSismember(string $cacheKey, string $member)
    {
        return self::redis()->sismember($cacheKey, $member);
    }

    /**
     * 删除指定元素
     * set
     * @param string $cacheKey
     * @param int $data
     * @return mixed
     */
    public static function delSet(string $cacheKey, int $data)
    {
        return self::redis()->srem($cacheKey, $data);
    }

    /**
     * 获取交集
     * set
     * @param ...$keys
     * @return mixed
     */
    public static function sinterCache(...$keys)
    {
        return self::redis()->sinter(...$keys);
    }

    public static function sinterstore($destination, ...$keys)
    {
        return self::redis()->sinterstore($destination, ...$keys);
    }

    /**
     * 获取并集
     * @param ...$key
     * @return mixed
     */
    public static function redisSunion(...$key)
    {
        return self::redis()->sunion(...$key);
    }

    public static function RedisSunionStore($destination, ...$keys)
    {
        return self::redis()->sunionstore($destination, ...$keys);
    }

    /**
     * 将缓存存入redis
     * zSet
     * @param string $cacheKey
     * @param string $score
     * @param string $value
     * @return mixed
     */
    public static function addZSet(string $cacheKey, string $score, string $value)
    {
        return self::redis()->zadd($cacheKey, $score, $value);
    }


    public static function zCount($cacheKey, $min, $max)
    {
        return self::redis()->zcount($cacheKey, (string)$min, (string)$max);
    }


    /**
     * 获取Click hit记录KEY
     * @param $user
     *
     * @return string
     */
    public static function getClickAnchorHitNumKey($user, $os): string
    {
        return sprintf(self::CLICK_ANCHOR_HIT_NUM_KEY, $os, $user);
    }

    /**
     * 获取Click eff记录KEY
     * @param $user
     *
     * @return string
     */
    public static function getClickAnchorEffNumKey($user, $os): string
    {
        return sprintf(self::CLICK_ANCHOR_EFF_NUM_KEY, $os, $user);
    }

    /**
     * 获取Pv hit记录KEY
     * @param $user
     *
     * @return string
     */
    public static function getPvAnchorHitNumKey($user, $os): string
    {
        return sprintf(self::PV_ANCHOR_HIT_NUM_KEY, $os, $user);
    }

    /**
     * 获取Pv eff记录KEY
     * @param $user
     *
     * @return string
     */
    public static function getPvAnchorEffNumKey($user, $os): string
    {
        return sprintf(self::PV_ANCHOR_EFF_NUM_KEY, $os, $user);
    }


    /**
     * 按照区间从小到大排序
     * zSet
     * @param string $cacheKey
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public static function getAsc(string $cacheKey, int $min, int $max)
    {
        return self::redis()->zrangebyscore($cacheKey, (string)$min, (string)$max);
    }

    /**
     * 按照区间从大到小倒序排列
     * zSet
     * @param string $cacheKey
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public static function getDesc(string $cacheKey, int $min, int $max)
    {
        return self::redis()->zrevrangebyscore($cacheKey, $min, $max);
    }

    /**
     * 根据value删除指定元素
     * zSet
     * @param string $cacheKey
     * @param $data
     * @return mixed
     */
    public static function zRem(string $cacheKey, $data)
    {
        return self::redis()->zrem($cacheKey, $data);
    }

    /**
     * 根据score删除元素
     * zSet
     * @param string $cacheKey
     * @param $start
     * @param $end
     * @return mixed
     */
    public static function zRemRangeByScore(string $cacheKey, $start, $end)
    {
        return self::redis()->zremrangebyscore($cacheKey, $start, $end);
    }

    /**
     * 将缓存数据存入redis
     * string
     * @param string $cacheKey
     * @param string $value
     * @return mixed
     */
    public static function addCache(string $cacheKey, string $value)
    {
        return self::redis()->set($cacheKey, $value);
    }

    /**
     * 修改广告数据
     * string
     * @param string $cacheKey
     * @param string $value
     * @return mixed
     */
    public static function updateCacheEx(string $cacheKey, string $value)
    {
        return self::redis()->setex($cacheKey, self::CACHE_EXPIRATION, $value);
    }

    /**
     * 将缓存数据存入redis
     * string
     * @param string $cacheKey
     * @param string $value
     * @return mixed
     */
    public static function setCacheEx(string $cacheKey, string $value)
    {
        return self::redis()->setex($cacheKey, self::CACHE_EXPIRATION, $value);
    }

    /**
     * 获取广告数据
     * string
     * @param string $cacheKey
     * @return mixed
     */
    public static function getAdInfoCacheById(string $cacheKey)
    {
        $data = self::redis()->get($cacheKey);
        return json_decode($data, true);
    }

    /**
     * 缓存模糊查询
     *
     * @param string $cacheKey
     *
     * @return mixed
     */
    public static function getAdInfoCacheFuzzy(string $cacheKey)
    {
        $data = self::redis()->keys($cacheKey . '*');
        $res = [];
        if ($data) {
            array_walk($data, function ($item) use (&$res) {
                $res[] = json_decode(self::redis()->get($item), true);
            });
        }
        return $res;
    }

    /**
     * 获取string数据
     * @param string $cacheKey
     * @return mixed
     */
    public static function getStringCache(string $cacheKey)
    {
        return self::redis()->get($cacheKey);
    }

    /**
     * 删除指定key
     * string
     * @param string $cacheKey
     * @return mixed
     */
    public static function delAdInfoCacheById(string $cacheKey)
    {
        return self::redis()->del($cacheKey);
    }

    /**
     * @param string $cacheKey
     * @return mixed
     */
    public static function setIncr(string $cacheKey)
    {
        return self::redis()->incr($cacheKey);
    }

    /**
     * @param string $key
     * @param int $id
     * @param int $limit
     * @return string
     */
    public static function getCacheName(string $key, int $id, int $limit = 0)
    {
        if (!empty($limit)) {
            return sprintf($key, $id, $limit);
        }
        return sprintf($key, $id);
    }

    /**
     * @param string $key
     * @param float $price
     * @return mixed
     */
    public static function setPrice(string $key, float $price)
    {
        return self::redis()->incrbyfloat($key, $price);
    }

    /**
     * 通过管道设置价格
     * @param array $values
     * @author 韩石强
     * @time 2022/5/27
     */
    public static function setPriceV2(array $values){
        $redis = self::redis();
        $redis->pipeline();
        foreach ($values as $value){
            $redis->incrByFloat($value[0],$value[1]);
        }
        $redis->exec();
    }

    /**
     * 获取广告详情Key
     * @param int $width
     * @param int $height
     * @param $unitId
     * @return string
     */
    public static function getBaiduAdInfoKey(int $width, int $height, $unitId): string
    {
        return sprintf('Ad_%s*%s_Info_Id:%s', $width, $height, $unitId);
    }

    /**
     * Redis 位运算 set
     *
     * @param $key
     * @param $offset
     * @param $value
     *
     * @return mixed
     */
    public static function setBit($key, $offset, $value = 1)
    {
        //setbit key offset value
        return self::redis()->setbit($key, $offset, $value);
    }


    public static function getBit($key, $offset)
    {
        return self::redis()->getbit($key, $offset);
    }

    /**
     * Redis 位运算计数
     *
     * @param $key
     * @param $start
     * @param $end
     *
     * @return mixed
     */
    public static function bitCount($key, $start, $end)
    {
        return self::redis()->bitcount($key, $start, $end);
    }

    /**
     * 获取点击层面过滤筛选配置KEY
     *
     * @return string
     */
    public static function getClickAnchorConfigKey()
    {
        return sprintf(self::CLICK_ANCHOR_CONFIG);
    }

    /**
     * 获取PV层面过滤筛选配置KEY
     *
     * @return string
     */
    public static function getPvAnchorConfigKey()
    {
        return sprintf(self::PV_ANCHOR_CONFIG);
    }


    /**
     * 获取PV层面过滤筛选配置
     *
     * @return string
     */
    public static function hGet($key, $field)
    {
        return self::redis()->hget($key, $field);
    }

    /**
     * 获取Click黑名单KEY
     *
     * @param $uniqueKey
     * @param $uniqueId
     *
     * @return string
     */
    public static function getClickBlackListKey($uniqueKey, $uniqueId)
    {
        return sprintf(self::CLICK_BLACKLIST, $uniqueKey, $uniqueId);
    }

    /**
     * 获取PV黑名单KEY
     *
     * @param $uniqueKey
     * @param $uniqueId
     *
     * @return string
     */
    public static function getPvBlackListKey($uniqueKey, $uniqueId)
    {
        return sprintf(self::PV_BLACKLIST, $uniqueKey, $uniqueId);
    }

    /**
     * hkeys
     *
     * @param $key
     *
     * @return mixed
     */
    public static function hKeys($key)
    {
        return self::redis()->hkeys($key);
    }

    /**
     * hvals
     *
     * @param $key
     *
     * @return mixed
     */
    public static function hVals($key)
    {
        return self::redis()->hvals($key);
    }

    /**
     * Setex
     *
     * @param string $cacheKey
     * @param        $ttl
     * @param string $value
     *
     * @return mixed
     */
    public static function setEx(string $cacheKey, $ttl, string $value)
    {
        return self::redis()->setex($cacheKey, $ttl, $value);
    }
}