<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\data;

use common\models\struct\ModelData;

class BidDeviceModel extends  ModelData
{
    /**
     * @var string|null 设备操作系统
     */
    public ?string $os = "";
    /**
     * @var string|null 设备唯一id
     */
    public ?string $uniqueId = null;
    /**
     * @var string|null 唯一键
     */
    public ?string $uniqueKey = null;
    /**
     * @var string|null ios idfa
     */
    public ?string $idfa = "";
    /**
     * @var string|null ios14之后的唯一id
     */
    public ?string $caid = "";
    /**
     * @var string|null iOS IDFV
     */
    /**
     * @var string|null 设备imei
     */
    public ?string $imei = "";
    /**
     * @var string|null 设备oaid，oaid和imei二者必传其一，Android 10及以上版本推荐oaid
     */
    public ?string $oaid = "";
    /**
     * @var string|null 品牌
     */
    public ?string $brand = null;
    /**
     * @var string|null 运营商
     */
    public ?string $operator = null;
    /**
     * @var string|null 网络
     */
    public ?string $environment = null;
}