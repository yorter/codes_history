<?php
/**
 * @author 韩石强
 * @time 2022/5/10
 */

namespace common\services\adFilter\data;

class BidModelContainer
{
    /**
     * @var BidModel 竞价相关信息
     */
    public BidModel $bidModel;
    /**
     * @var BidAdInfoModel|null 返回的广告信息
     */
    public ?BidAdInfoModel $adInfo = null;
    /**
     * @var bool adInfo获取的广告是否为可用
     */
    public bool $adEnable = false;
    /**
     * @var FilterData 过滤器过程中产生的数据
     */
    public FilterData $filterData;

    public function __construct(BidModel $bidModel)
    {
        $this->bidModel = $bidModel;
        $this->filterData = new FilterData();
    }
}