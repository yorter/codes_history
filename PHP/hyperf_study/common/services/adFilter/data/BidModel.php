<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\data;

use common\bid\struct\DeviceModelData;
use common\services\ad\AdCache;

class BidModel
{
    /**
     * @var string|false 今天的日期
     */
    public string $today;
    /**
     * @var int 小时
     */
    public int $hour;
    /**
     * @var int 周几
     */
    public int $week;

    public function __construct()
    {
        $this->today = date('Y-m-d');
        $this->hour = intval(date('H'));
        $this->week = intval(date('w'));
        $this->device = new BidDeviceModel();
    }


    /**
     * 请求Id
     * @var string|null
     */
    public ?string $requestId;
    /**
     * @var bool 是否是白名单用户
     */
    public bool $isWhite = false;

    /**
     * 平台 渠道
     * @var int
     */
    public int $platform;
    /**
     * 流量源id
     * @var string|null
     */
    public ?string $appId = null;
    /**
     * 广告位Id
     * @var string|null
     */
    public ?string $pId;
    /**
     * 尺寸
     * @var int|null
     */
    public ?int $width = null;
    public ?int $height = null;
    /**
     * 大航海广告位Id
     * @var string
     */
    public string $spaceId;

    /**
     * 大航海渠道标识
     * @var string
     */
    public string $channel;
    /**
     * 设备信息
     * @var array
     */
    public BidDeviceModel $device;
    /**
     * @var DeviceModelData|null 统一设备信息
     */
    public ?DeviceModelData $deviceData = null;
    /**
     * 出价
     * @var string
     */
    public string $bidFloor;
    /**
     * 出价类型
     * @var int
     */
    public int $bidType;
    /**
     * 手机
     * @var array
     */
    public array $os;
    /**
     * 地区
     * @var string|null
     */
    public ?string $area = AdCache::AREA_DEFAULT;
    /**
     * 兴趣
     * @var array|null
     */
    public ?array $interest = null;
    /**
     * 关键字
     * @var array|null
     */
    public ?array $keyword = null;
    /**
     * 视频
     */
    public ?array $video = null;
    /**
     * @var string|null ip
     */
    public ?string $ip = null;
    /**
     * @var string|null ipv6地址
     */
    public ?string $ipv6 = null;
}