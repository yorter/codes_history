<?php
/**
 * @author 韩石强
 * @time 2022/5/11
 */

namespace common\services\adFilter\data;
/**
 * 过滤器过程中善生的临时数据
 */
class FilterData
{
    /**
     * @var array|null 广告选择器结果
     */
    public ?array $choose = null;
    /**
     * @var array|null 定向结果
     */
    public ?array $filter = null;
    /**
     * @var array|null 价格筛选结果
     */
    public ?array $priceList = null;
    /**
     * @var array|null 选择器和筛选器的交集
     */
    public ?array $adList = null;
    /**
     * @var array|null 手机定向结果
     */
    public array $mobileRes = [];
    /**
     * @var array 地域定向结果
     */
    public array $areaRes = [];
    /**
     * @var array|null 推广计划限制
     */
    public ?array $planLimit = null;
    /**
     * @var array|null 推广单元限制
     */
    public ?array $unitLimit = null;
}