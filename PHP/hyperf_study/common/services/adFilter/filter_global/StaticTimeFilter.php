<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\filter_global;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\adFilter\Handler;

/**
 * 每日固定时间过滤
 */
class StaticTimeFilter extends Handler
{
    /**
     * 0-6点不投放广告
     */
    const START_DATE = 0;
    const END_DATE = 6;

    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //0-6点不给广告
        if ($this->data->hour >= self::START_DATE && $this->data->hour <= self::END_DATE) {
            LogV2::errorCode(10032, $this->data->platform, $this->data->requestId, $this->data->appId, $this->data->pId);
            throw new BidException('非投放广告时间');
        }
    }
}