<?php

namespace common\services\adFilter\filter_global;

use common\exception\BidException;
use common\helpers\LogV2;
use common\models\redis\DspInfo;
use common\services\ad\AdCache;
use common\services\adFilter\Handler;
use Hyperf\Utils\ApplicationContext;

/**
 * PV过滤器
 */
class PvFilter extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        if ($this->data->isWhite) {
            return true;
        }
        //过滤器规则 x天 y点击 z唤起
        $anchorConfigKey = AdCache::getPvAnchorConfigKey();
        $container = ApplicationContext::getContainer();
        $redis = $container->get(DspInfo::class);
        $config = $redis->hGetAll($anchorConfigKey);
//        $config = array_combine(AdCache::hKeys($anchorConfigKey), AdCache::hVals($anchorConfigKey));
        list(
            $enable, $anchorDays, $anchorHitNumConfig, $anchorEffNumConfig, $blacklistTtlDaysConfig
            )
            = [
            $config[AdCache::ANCHOR_ENABLE],
            $config[AdCache::ANCHOR_DAYS],
            $config[AdCache::ANCHOR_HIT_NUM],
            $config[AdCache::ANCHOR_EFF_NUM],
            $config[AdCache::BLACKLIST_TTL_DAYS],
        ];
        if (!$enable) {
            return true;
        }

        $blackListKey = AdCache::getPvBlackListKey(
            $this->data->device->uniqueKey,
            $this->data->device->uniqueId
        );
        $dateMin = (string)($anchorDays > 0 ? date("Ymd000000", strtotime("-" . ($anchorDays - 1) . " day")) : 0);
        $dateNow = date('YmdHis');

        //判断是否在点击层面过滤筛选器黑名单中
        $redis->pipeline();
        $redis->exists($blackListKey);
        $redis->zCount(AdCache::getClickAnchorHitNumKey(
            $this->data->device->uniqueId,
            $this->data->device->os
        ), $dateMin, $dateNow);
        $redis->zCount(
            AdCache::getClickAnchorEffNumKey(
                $this->data->device->uniqueId,
                $this->data->device->os
            ), $dateMin, $dateNow);
        $result = $redis->exec();
        $isInBlack = $result[0];
        if ($isInBlack) {
            LogV2::errorCodeWithBidModel(10017, $this->data);
            throw new BidException('PV层面存量黑名单过滤');
        }

        $anchorHitnum = $result[1];
        $anchorEffNum = $result[2];
        if (($anchorHitnum >= $anchorHitNumConfig) && ($anchorEffNum <= $anchorEffNumConfig)) {
            //获取TTL设置
            $ttl = strtotime(
                    date("Ymd000000", strtotime("+" . $blacklistTtlDaysConfig . " day"))
                ) - time();
            go(function () use ($blackListKey, $ttl, $dateNow) {
                AdCache::setEx($blackListKey, $ttl, $dateNow);

            });
            LogV2::errorCodeWithBidModel(10018, $this->data);
            throw new BidException('PV层面过滤');
        }
    }
}