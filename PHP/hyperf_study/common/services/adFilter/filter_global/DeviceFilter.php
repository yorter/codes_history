<?php

namespace common\services\adFilter\filter_global;

use common\exception\UnstoppableBidException;
use common\helpers\LogV2;
use common\services\ad\AdRequest;
use common\services\adFilter\filter_global\device\DeviceBlacklist;
use common\services\adFilter\filter_global\device\DeviceFrequencyFilter;
use common\services\adFilter\filter_global\device\DeviceRTABlackList;
use common\services\adFilter\Handler;

/**
 * 设备信息验证
 */
class DeviceFilter extends Handler
{
    protected ?array $SubHandleClass = [
        DeviceBlacklist::class,
        DeviceFrequencyFilter::class,
        DeviceRTABlackList::class,
        PvFilter::class,
        ClickFilter::class,
    ];
    protected bool $useCoroutine = true;

    /**
     * @throws UnstoppableBidException
     */
    public function HandleRequest()
    {
        if ($this->data->device->os == 'IOS') {
            if (!empty($this->data->device->idfa)) {
                $this->data->device->uniqueId = $this->data->device->idfa;
                $this->data->device->uniqueKey = 'idfa';
            }
        } else {
            if (!empty($this->data->device->imei)) {
                $this->data->device->uniqueId = $this->data->device->imei;
                $this->data->device->uniqueKey = 'imei';
            } elseif (!empty($this->data->device->oaid)) {
                $this->data->device->uniqueId = $this->data->device->oaid;
                $this->data->device->uniqueKey = 'imei';
            }
        }
        if (!$this->data->device->uniqueId) {
            LogV2::errorCodeWithBidModel(10030, $this->data, ['os' => $this->data->device->os, 'user' => '']);
            throw new UnstoppableBidException("缺少设备唯一标识");
        }

        if (AdRequest::getWhitList($this->data->device->uniqueId)) {
            $this->data->isWhite = true;
        }
        parent::HandleRequest();
    }
}