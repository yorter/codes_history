<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\filter_global\device;

use common\services\ad\AdRequest;
use common\services\adFilter\Handler;

/**
 * 竞价成功记录设备信息
 */
class DeviceFrequencyAddCountFilter extends Handler
{
    public function HandleRequest()
    {
        AdRequest::setDevice($this->data->platform, $this->data->device->os, $this->data->device->uniqueId);
    }
}