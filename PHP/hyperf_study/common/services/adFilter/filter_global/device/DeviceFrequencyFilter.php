<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\filter_global\device;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdRequest;
use common\services\adFilter\Handler;

class DeviceFrequencyFilter extends Handler
{
    const REQUEST_SECOND = 10000;

    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //判定设备是否超出当日上限
        $requestSecond = AdRequest::getDevice($this->data->platform, $this->data->device->os, $this->data->device->uniqueId);
        if (self::REQUEST_SECOND <= $requestSecond && !$this->data->isWhite) {
            AdRequest::setDevice($this->data->platform, $this->data->device->os, $this->data->device->uniqueId);
            LogV2::errorCodeWithBidModel(10031, $this->data, ['limit' => self::REQUEST_SECOND, 'second' => $requestSecond]);
            throw new BidException('设备超出请求最大限制');
        }
    }
}