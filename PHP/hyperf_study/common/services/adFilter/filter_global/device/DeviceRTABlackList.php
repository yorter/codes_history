<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_global\device;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdRequest;

/**
 * 判定设备是否为通投
 */
class DeviceRTABlackList extends \common\services\adFilter\Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $requestPassSecond = AdRequest::getRTADevice($this->data->platform, self::PASS_TASK_ID, $this->data->device->os, $this->data->device->uniqueKey, $this->data->device->uniqueId);
        if (!empty($requestPassSecond)) {
            LogV2::errorCodeWithBidModel(10033, $this->data);
            throw new BidException('RTA非目标用户');
        }
    }
}