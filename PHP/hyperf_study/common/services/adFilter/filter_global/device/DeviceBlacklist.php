<?php
/**
 * @author 韩石强
 * @time 2022/5/11
 */

namespace common\services\adFilter\filter_global\device;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdRequest;
use common\services\adFilter\Handler;

/**
 * 设备黑名单
 */
class DeviceBlacklist extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //判定设备是否为RTA目标用户
        $blackList = AdRequest::getBlackList($this->data->device->os, $this->data->device->uniqueId);
        if ($blackList) {
            LogV2::errorCodeWithBidModel(10013, $this->data, ['os' => $this->data->device->os, 'user' => $this->data->device->uniqueId]);
            throw new BidException('RTA非目标用户');
        }
    }
}