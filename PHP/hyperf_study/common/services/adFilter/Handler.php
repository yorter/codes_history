<?php

namespace common\services\adFilter;


use common\exception\BaseFilter;
use common\services\adFilter\data\BidModel;
use common\services\adFilter\data\BidModelContainer;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;

abstract class Handler
{
    //定投任务Id
    const FIXED_TASK_ID = 32896;
    //通投任务Id
    const PASS_TASK_ID = 32768;

    /**
     * @var BidModel 竞价参数
     */
    protected BidModel $data;
    /**
     * @var BidModelContainer 包含竞价参数以及返回的广告信息
     */
    protected BidModelContainer $bid;

    protected $start;

    public function __construct(BidModel $data, BidModelContainer $bid)
    {
        $this->data = $data;
        $this->bid = $bid;
        $this->start = microtime(true);
    }

    const USE_COROUTINE = false;
    /**
     * @var bool 是否使用协程执行子任务
     */
    protected bool $useCoroutine = false;

    /**
     * @var array|null 子步骤
     */
    protected ?array $SubHandleClass = [];

    /**
     * 过滤器
     * @author 韩石强
     * @time 2022/5/7
     */
    public function HandleRequest()
    {
        if ($this->SubHandleClass) {
            if ($this->useCoroutine) {//使用协程
                $parallel = new Parallel();
                try {
                    foreach ($this->SubHandleClass as $class) {
                        $parallel->add(function () use ($class) {
                            $handler = self::getHandler($class, $this->data, $this->bid);
                            $handler->HandleRequest();
                        });
                    }
                    $parallel->wait();
                } catch (ParallelExecutionException $e) {
                    $this->CoroutineExceptionHandler($e);
                }
            } else {//同步
                foreach ($this->SubHandleClass as $class) {
                    $handler = self::getHandler($class, $this->data, $this->bid);
                    $handler->HandleRequest();
                }
            }
        }
    }

    public static function getHandler(string $class, BidModel $data, BidModelContainer $bid): Handler
    {
        return new $class($data, $bid);
    }

    /**
     * 捕获协程中的异常回调函数
     * @param ParallelExecutionException $e
     * @author 韩石强
     * @time 2022/5/30
     */
    protected function CoroutineExceptionHandler(ParallelExecutionException $e)
    {
        $errors = $e->getResults();
        foreach ($errors as $error) {
            if ($error instanceof BaseFilter) {
                $class = $error->getExceptionClassName();
                throw new $class($error->getMessage());
            }
        }
    }

    public function __destruct()
    {
        echo(static::class . "--结束,耗时   \033[42m" . (round(microtime(true) - $this->start, 3) * 1000)."\033[0m 毫秒\n");
    }
}