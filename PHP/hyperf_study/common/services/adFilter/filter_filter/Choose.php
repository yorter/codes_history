<?php

namespace common\services\adFilter\filter_filter;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;
use common\services\adFilter\Handler;

/**
 * 广告选择器
 */
class Choose extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $todayKey = AdCache::getAdSpecifiedDataKey($this->data->today);
        $time = AdCache::getAdPeriodDataKey($this->data->week, $this->data->hour);
        $timeSinter = AdCache::sinterCache($todayKey, $time);
        if (!$timeSinter) {
            LogV2::errorCodeWithBidModel(100021, $this->data);
            throw new BidException('找不到相关广告');
        }
        $appIdKey = AdCache::getAppIdKey($this->data->appId);
        $pidKey = AdCache::getPidKey($this->data->pId);

        $appidPidSinter = AdCache::sinterCache($appIdKey, $pidKey);
        if (!$appidPidSinter) {
            LogV2::errorCodeWithBidModel(100023, $this->data);
            throw new BidException('找不到相关广告');
        }
        $resChoose = array_intersect($timeSinter, $appidPidSinter);
        if (!$resChoose) {
            LogV2::errorCodeWithBidModel(10002, $this->data);
            throw new BidException('找不到相关广告');
        }
        $this->bid->filterData->choose = $resChoose;
    }
}