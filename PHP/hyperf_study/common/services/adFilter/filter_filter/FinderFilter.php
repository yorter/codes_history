<?php
/**
 * @author 韩石强
 * @time 2022/5/30
 */

namespace common\services\adFilter\filter_filter;

use common\services\adFilter\Handler;

class FinderFilter extends Handler
{
    protected bool $useCoroutine = true;

    protected ?array $SubHandleClass = [
        Choose::class,
        Filter::class
    ];
}