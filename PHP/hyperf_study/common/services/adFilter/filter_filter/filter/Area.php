<?php
/**
 * @author 韩石强
 * @time 2022/5/11
 */

namespace common\services\adFilter\filter_filter\filter;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;
use common\services\ad\BaseAd;
use common\services\adFilter\Handler;

/**
 * 地域定向
 */
class Area extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //根据Ip解析地址
        $location = $this->data->deviceData->location;

        //根据解析出来的地址获取到市，然后用市级做地区定向
        $address = '';
        if (!empty($location)) {
            if (preg_match('/(.*?(省|内蒙古|西藏|新疆|广西))/', $location, $matches)) {
                $city = $matches[count($matches) - 2];
                $address = str_replace($city, '', $location);
                if (preg_match('/(.*?(市|自治州|地区|区划|县))/', $address, $matches)) {
                    $address = $matches[count($matches) - 2];
                }
            } elseif (preg_match('/(.*?(北京市|上海市|重庆市|天津市))/', $location, $matches)) {
                $address = $matches[count($matches) - 2];
            }
        }

        if ($address) {
            $areaRes = AdCache::redisSunion(AdCache::getAdFilterDataKey(BaseAd::CACHE_KEY_REGION, $address), AdCache::getAdFilterDataKey(BaseAd::CACHE_KEY_REGION, AdCache::AREA_DEFAULT));
            if (!$areaRes) {
                LogV2::errorCodeWithBidModel(100022, $this->data);
                throw new BidException('找不到相关广告');
            }
        } else {
            $areaRes = AdCache::getSet(AdCache::getAdFilterDataKey(BaseAd::CACHE_KEY_REGION, AdCache::AREA_DEFAULT));
        }

        if (empty($areaRes)) {
            LogV2::errorCodeWithBidModel(100022, $this->data);
            throw new BidException('找不到相关广告');
        }
        $this->bid->filterData->areaRes = $areaRes;
    }
}