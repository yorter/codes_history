<?php
/**
 * @author 韩石强
 * @time 2022/5/11
 */

namespace common\services\adFilter\filter_filter\filter;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;
use common\services\adFilter\Handler;

/**
 * 手机过滤
 */
class Phone extends Handler
{
    const MOBILE_ENVIRONMENT = [
        0 => '不限',
        1 => 'WIFI',
        2 => '2G',
        3 => '3G',
        4 => '4G',
        5 => '5G',
        6 => 'Other'
    ];

    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $noLimit = self::MOBILE_ENVIRONMENT[0];
        $device = $this->data->device;
        $device->operator = $device->operator ?? $noLimit;
        $device->environment = $device->environment ?? $noLimit;
        $device->brand = $device->brand ?? $noLimit;


        //获取手机系统、品牌、运营商、网络环境并集
        $mobileKeyA = AdCache::getMobileKey($device->os, $device->operator, $device->environment, $device->brand);
        $mobileKeyB = AdCache::getMobileKey($noLimit, $noLimit, $noLimit, $noLimit);
        $mobileKeyC = AdCache::getMobileKey($device->os, $noLimit, $noLimit, $noLimit);
        $mobileRes = AdCache::redisSunion($mobileKeyA, $mobileKeyB, $mobileKeyC);

        if (!$mobileRes) {
            LogV2::errorCodeWithBidModel(100024, $this->data);
            throw new BidException('找不到相关广告');
        }
        $this->bid->filterData->mobileRes = $mobileRes;
    }
}