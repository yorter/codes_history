<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency\unit;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;

/**
 * 每日点击上限
 */
class UnitDayClickLimit extends \common\services\adFilter\Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //每日点击上限
        $unitLimit = $this->bid->filterData->unitLimit;
        $ad = $this->bid->adInfo;
        $checkLimit = AdCache::getAdInfoCacheById(AdCache::getUnitCheckLimitByIdKey($ad->unit_id));
        if ($unitLimit['day_click_limit'] != AdCache::DEFAULT_PRICE && $unitLimit['day_click_limit'] < $checkLimit) {
            LogV2::errorCodeWithBidModel(10023, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_click_limit' => $unitLimit['day_click_limit'], 'check' => $checkLimit]);
            throw new BidException('每日点击次数超出单元限制');
        }
    }
}