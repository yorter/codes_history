<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency\unit;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;

class UnitDayBudget extends \common\services\adFilter\Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $unitLimit = $this->bid->filterData->unitLimit;
        $ad = $this->bid->adInfo;
        $budget = AdCache::getAdInfoCacheById(AdCache::getUnitCostKey($ad->unit_id));
        if ($unitLimit['day_budget'] != AdCache::DEFAULT_PRICE && $unitLimit['day_budget'] < $budget) {
            LogV2::errorCodeWithBidModel(10022, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_budget' => $unitLimit['day_budget'], 'budget' => $budget]);
            throw new BidException('每日预算超出单元限制');
        }
    }
}