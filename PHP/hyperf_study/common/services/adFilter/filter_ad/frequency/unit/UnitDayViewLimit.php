<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency\unit;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;

class UnitDayViewLimit extends \common\services\adFilter\Handler
{
    public function HandleRequest()
    {
        //每日曝光上限
        $unitLimit = $this->bid->filterData->unitLimit;
        $ad = $this->bid->adInfo;
        $exposureLimit = AdCache::getAdInfoCacheById(AdCache::getUnitExposureLimitByIdKey($ad->unit_id));
        if ($unitLimit['day_viem_limit'] != AdCache::DEFAULT_PRICE && $unitLimit['day_viem_limit'] < $exposureLimit) {
            LogV2::errorCodeWithBidModel(10024, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_viem_limit' => $unitLimit['day_viem_limit'], 'exposure' => $exposureLimit]);
            throw new BidException('每日曝光次数超出单元限制');
        }
    }
}