<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency\plan;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;

/**
 * 推广计划每日预算
 */
class PlanDayBudget extends \common\services\adFilter\Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $ad = $this->bid->adInfo;
        $planLimit = $this->bid->filterData->planLimit;
        $planBudget = AdCache::getAdInfoCacheById(AdCache::getPlanCostKey($ad->plan_id));
        if ($planLimit['day_budget'] != AdCache::DEFAULT_PRICE && $planLimit['day_budget'] < $planBudget) {
            LogV2::errorCodeWithBidModel(10020, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_budget' => $planLimit['day_budget'], 'planBudget' => $planBudget]);
            throw new BidException('每日预算超出计划限制');
        }
    }
}