<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency\plan;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;

class PlanTotalBudget extends \common\services\adFilter\Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        $ad = $this->bid->adInfo;
        $planLimit = $this->bid->filterData->planLimit;
        //推广计划总预算
        $planTotalBudget = AdCache::getAdInfoCacheById(AdCache::getPlanTotalCostKey($ad->plan_id));
        if ($planLimit['total_budget'] != AdCache::DEFAULT_PRICE && $planLimit['total_budget'] < $planTotalBudget) {
            LogV2::errorCodeWithBidModel(10021, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'total_budget' => $planLimit['total_budget'], 'budget' => $planTotalBudget]);
            throw new BidException('总预算超出计划限制');
        }
    }
}