<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency;

use common\services\ad\AdCache;
use common\services\adFilter\filter_ad\frequency\unit\UnitDayBudget;
use common\services\adFilter\filter_ad\frequency\unit\UnitDayClickLimit;
use common\services\adFilter\filter_ad\frequency\unit\UnitDayViewLimit;
use common\services\adFilter\Handler;

/**
 * 推广计划总预算
 */
class UnitLimit extends Handler
{
    protected bool $useCoroutine = true;

    protected ?array $SubHandleClass = [
        UnitDayBudget::class,
        UnitDayClickLimit::class,
        UnitDayViewLimit::class
    ];

    public function HandleRequest()
    {
        //推广单元每日预算
        $this->bid->filterData->unitLimit = AdCache::getAdInfoCacheById(AdCache::getAdUnitKey($this->bid->adInfo->unit_id));
        parent::HandleRequest();
    }
}