<?php
/**
 * @author 韩石强
 * @time 2022/5/25
 */

namespace common\services\adFilter\filter_ad\frequency;

use common\services\ad\AdCache;
use common\services\adFilter\filter_ad\frequency\plan\PlanDayBudget;
use common\services\adFilter\filter_ad\frequency\plan\PlanTotalBudget;

class PlanLimit extends \common\services\adFilter\Handler
{
    protected ?array $SubHandleClass = [
        PlanDayBudget::class,
        PlanTotalBudget::class
    ];

    protected bool $useCoroutine = true;

    /**
     */
    public function HandleRequest()
    {
        //竞价计费,先加后排除是否超限
        $this->bid->filterData->planLimit = AdCache::getAdInfoCacheById(AdCache::getAdPlanKey($this->bid->adInfo->plan_id));
        parent::HandleRequest();
    }
}