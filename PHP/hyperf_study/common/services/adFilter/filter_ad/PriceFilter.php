<?php

namespace common\services\adFilter\filter_ad;

use common\exception\BidException;
use common\helpers\LogV2;
use common\services\ad\AdCache;
use common\services\adFilter\data\BidAdInfoModel;
use common\services\adFilter\Handler;

class PriceFilter extends Handler
{
    const PERCENTAGE = 100;
    const BID_CPM_PRICE = 100000;//1000CPM*100分=实际的元价格
    const BID_CPC_PRICE = 100;
    const CHARGE_TYPE_CPM = 2;//cpm出价

    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        LogV2::errorCodeWithBidModel(200004, $this->data);
        //价格筛选
        $priceList = AdCache::getAsc(AdCache::getAdPriceKey($this->data->bidType), intval($this->data->bidFloor), intval($this->data->bidFloor) + self::BID_CPM_PRICE);
        if (!$priceList) {
            LogV2::errorCodeWithBidModel(10003, $this->data, ['bidType' => $this->data->bidType, 'bidFloor' => $this->data->bidFloor]);
            throw new BidException('出价找不到相关广告');
        }
        $ad = array_intersect($this->bid->filterData->adList, $priceList);

        if (empty($ad)) {
            LogV2::errorCodeWithBidModel(10003, $this->data, ['bidType' => $this->data->bidType, 'bidFloor' => $this->data->bidFloor]);
            throw new BidException('出价找不到相关广告');
        }

        //随机吐出一个符合条件的广告
        $random = rand(0, count($ad) - 1);
        $adInfo = AdCache::getAdInfoCacheById(AdCache::getAdInfoKey($this->data->pId, array_values($ad)[$random]));

        if (empty($adInfo)) {
            LogV2::errorCodeWithBidModel(10005, $this->data, ['bidType' => $this->data->bidType, 'bidFloor' => $this->data->bidFloor]);
            throw new BidException('无广告填充');
        }
        //限制
        LogV2::errorCodeWithBidModel(200005, $this->data);
        //将数组映射成对象
        $this->bid->adInfo = new BidAdInfoModel();
        foreach ($adInfo as $k => $v) {
            $this->bid->adInfo[$k] = $v;
        }
        //竞价计费,先加后排除是否超限
        $adInfo = $this->bid->adInfo;
        $this->setBidPrice($adInfo->unit_id, $adInfo->plan_id, $adInfo->chargetype, $adInfo->price);

        //竞价调整
        if ($this->bid->adInfo->price != $this->data->bidFloor) {
            $percent = AdCache::getStringCache(AdCache::getBidStrategyKey());
            $this->bid->adInfo->price = intval($this->data->bidFloor) + (($this->bid->adInfo->price - $this->data->bidFloor) * $percent / self::PERCENTAGE);
        }
    }

    /**
     * 生成竞价信息
     * @param int $unitId 推广单元ID
     * @param int $planId 推广计划Id
     * @param int $chargeType
     * @param float $price bid价格
     * @return void
     */
    private function setBidPrice(int $unitId, int $planId, int $chargeType, float $price)
    {
        if ($chargeType == self::CHARGE_TYPE_CPM) {
            $bidPrice = $price / self::BID_CPM_PRICE;
        } else {
            $bidPrice = $price / self::BID_CPC_PRICE;
        }
        AdCache::setPriceV2([
            [AdCache::getUnitCostKey($unitId), $bidPrice],
            [AdCache::getPlanCostKey($planId), $bidPrice],
            [AdCache::getPlanTotalCostKey($planId), $bidPrice],
        ]);
    }
}