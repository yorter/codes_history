<?php

namespace common\services\adFilter\filter_ad;

use common\exception\BidException;
use common\helpers\LogV2;
use common\models\redis\DspInfo;
use common\services\ad\AdCache;
use common\services\adFilter\Handler;
use Hyperf\Utils\ApplicationContext;

class FrequencyFilter extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //竞价计费,先加后排除是否超限
        $ad = $this->bid->adInfo;
        $container = ApplicationContext::getContainer();
        $redis = $container->get(DspInfo::class);
        $redis->pipeline();
        $redis->get(AdCache::getAdPlanKey($ad->plan_id));//推广计划限制
        $redis->get(AdCache::getPlanCostKey($ad->plan_id));//推广计划每日预算
        $redis->get(AdCache::getPlanTotalCostKey($ad->plan_id));//推广计划总预算
        $redis->get(AdCache::getAdUnitKey($ad->unit_id));//推广单元每日预算
        $redis->get(AdCache::getUnitCostKey($ad->unit_id));//推广单元消耗
        $redis->get(AdCache::getUnitCheckLimitByIdKey($ad->unit_id));//每日点击上限
        $redis->get(AdCache::getUnitExposureLimitByIdKey($ad->unit_id));//每日曝光上限
        $result = $redis->exec();

        $planLimit = json_decode($result[0], true);
        //推广计划每日预算
        $planBudget = json_decode($result[1], true);
        if ($planLimit['day_budget'] != AdCache::DEFAULT_PRICE && $planLimit['day_budget'] < $planBudget) {
            LogV2::errorCodeWithBidModel(10020, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_budget' => $planLimit['day_budget'], 'planBudget' => $planBudget]);
            throw new BidException('每日预算超出计划限制');
        }

        //推广计划总预算
        $planTotalBudget = json_decode($result[2], true);
        if ($planLimit['total_budget'] != AdCache::DEFAULT_PRICE && $planLimit['total_budget'] < $planTotalBudget) {
            LogV2::errorCodeWithBidModel(10021, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'total_budget' => $planLimit['total_budget'], 'budget' => $planTotalBudget]);
            throw new BidException('总预算超出计划限制');
        }

        //推广单元每日预算
        $unitLimit = json_decode($result[3], true);
        $budget = json_decode($result[4], true);
        if ($unitLimit['day_budget'] != AdCache::DEFAULT_PRICE && $unitLimit['day_budget'] < $budget) {
            LogV2::errorCodeWithBidModel(10022, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_budget' => $unitLimit['day_budget'], 'budget' => $budget]);
            throw new BidException('每日预算超出单元限制');
        }

        //每日点击上限
        $checkLimit = $result[5] ?: 0;
        if ($unitLimit['day_click_limit'] != AdCache::DEFAULT_PRICE && $unitLimit['day_click_limit'] < $checkLimit) {
            LogV2::errorCodeWithBidModel(10023, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_click_limit' => $unitLimit['day_click_limit'], 'check' => $checkLimit]);
            throw new BidException('每日点击次数超出单元限制');
        }

        //每日曝光上限
        $exposureLimit = $result[6] ?: 0;
        if ($unitLimit['day_viem_limit'] != AdCache::DEFAULT_PRICE && $unitLimit['day_viem_limit'] < $exposureLimit) {
            LogV2::errorCodeWithBidModel(10024, $this->data, ['unit_id' => $ad->unit_id, 'plan_id' => $ad->plan_id, 'creative_id' => $ad->creative_id, 'day_viem_limit' => $unitLimit['day_viem_limit'], 'exposure' => $exposureLimit]);
            throw new BidException('每日曝光次数超出单元限制');
        }
    }
}