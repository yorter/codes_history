<?php

namespace common\services\adFilter\filter_ad;

use common\exception\BidException;
use common\exception\UnstoppableBidException;
use common\helpers\LogV2;
use common\services\adFilter\Handler;

/**
 * 根据随机数和填充率来决定是否返回广告
 */
class RateFilter extends Handler
{
    /**
     * @throws BidException
     */
    public function HandleRequest()
    {
        //填充率
        $rand = rand(0, 100);
        if ($this->bid->adInfo && !empty($this->bid->adInfo->rate) && ($rand > $this->bid->adInfo->rate)) {
            LogV2::errorCodeWithBidModel(10004, $this->data, ['rate' => $this->bid->adInfo->rate]);
            throw new UnstoppableBidException('无广告填充');
        }
    }
}