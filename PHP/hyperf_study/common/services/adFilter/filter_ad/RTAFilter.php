<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\services\adFilter\filter_ad;

use common\components\taobao\requests\TBAsk;
use common\components\taobao\TaobaoClient;
use common\exception\RTAException;
use common\helpers\Log;
use common\helpers\LogV2;
use common\services\ad\AdRequest;
use common\services\adFilter\Handler;
use yii\base\Exception;
use const YII_ENV_PROD;

/**
 * rta验证
 */
class RTAFilter extends Handler
{
    //定投任务Id
    const MOBILE_OS = [
        'IOS' => 1,
        'Android' => 0
    ];

    /**
     * @return void
     * @throws Exception
     * @throws RTAException
     */
    public function HandleRequest()
    {
        LogV2::errorCodeWithBidModel(200003, $this->data);
        if (config('app_env', 'product') != 'dev') {
            $getRTA = AdRequest::getRTADevice($this->data->platform, self::FIXED_TASK_ID, $this->data->device->os, $this->data->device->uniqueKey, $this->data->device->uniqueId);
            if (empty($getRTA)) {
                $rta = $this->getRTA();
                if (!$rta) {
                    //返回false直接存入手机设备黑名单,非大航海目标用户
                    AdRequest::setBlackList($this->data->device->os, $this->data->device->uniqueId);
                    LogV2::errorCodeWithBidModel(10010, $this->data, ['RTA_false']);
                    throw new RTAException('无广告填充');
                }
                //将rta返回的taskId都存起来
                foreach ($rta as $item) {
//                    AdRequest::setDeviceData($this->data->device->os, $this->data->device->uniqueId,$this->data->deviceData);
                    //记录设备信息
                    $this->data->deviceData->save(16, 17, false, 18);
                    AdRequest::setRTADevice($this->data->platform, $item, $this->data->device->os, $this->data->device->uniqueId, $this->data->device->uniqueKey);
                }
                //判断是否是定投
                if (!in_array(self::FIXED_TASK_ID, $rta)) {
                    LogV2::errorCodeWithBidModel(10011, $this->data, $rta);
                    throw new RTAException('无广告填充');
                } else {
                    LogV2::errorCodeWithBidModel(10012, $this->data, $rta);
                }
            } else {
                LogV2::errorCodeWithBidModel(10014, $this->data);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function getRTA()
    {
        $req = new TBAsk();
        //预留json参数，与手淘团队单独沟通
        //$req->profile = '';
        $uniqueIdLen = strlen($this->data->device->uniqueId);
        $key = $this->data->device->uniqueKey;
        if ($uniqueIdLen == 32 || $uniqueIdLen == 64) {
            $key = $key . "_md5";
        }
        $req->$key = $this->data->device->uniqueId;

        $req->os = self::MOBILE_OS[$this->data->device->os];
        $req->advertising_space_id = $this->data->spaceId;
        $req->channel = $this->data->channel;

        $model = new TaobaoClient();
        $response = $model->run($req);
        $response['RTA_requestId'] = $this->data->requestId;
//        Log::info(json_encode($response));
        if ($response['usergrowth_dhh_delivery_ask_response']['errcode'] == 0 && $response['usergrowth_dhh_delivery_ask_response']['result']) {
            return $response['usergrowth_dhh_delivery_ask_response']['task_id_list']['string'];
        }
        return false;
    }
}