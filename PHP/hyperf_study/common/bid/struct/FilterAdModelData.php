<?php

namespace common\bid\struct;

use common\models\struct\ModelData;

/**
 * @author 韩石强
 * @time 2022/4/13
 */
class FilterAdModelData extends ModelData
{
    /**
     * @var int 创意id
     */
    public int $creative_id = 0;
    /**
     * @var string|null 创意名称
     */
    public ?string $ad_name = null;
    /**
     * @var int 时长
     */
    public int $ad_length = 0;
    /**
     * @var string|null 广告着陆页地址
     */
    public ?string $target_url = null;
    /**
     * @var string|null 视频广告标题
     */
    public ?string $ad_title = null;
    /**
     * @var string|null 广告视频封面
     */
    public ?string $cover = null;
    /**
     * @var string|null 视频广告icon
     */
    public ?string $icon = null;
    /**
     * @var string|null 广告行动语
     */
    public ?string $action_text = null;
    /**
     * @var string|null 模板名称
     */
    public ?string $template_name = null;
    /**
     * @var int 宽
     */
    public int $file_width = 0;
    /**
     * @var int 高
     */
    public int $file_high = 0;
    /**
     * @var string 尺寸
     */
    public string $size = "";
    /**
     * @var string|null 文件类型
     */
    public ?string $file_type = null;
    /**
     * @var int
     */
    public int $file_size = 0;
    /**
     * @var string|null 创意组名称
     */
    public ?string $name = null;
    /**
     * @var string|null dp地址
     */
    public ?string $deep_link = null;
    /**
     * @var int 广告交互类型：1、网页，2、安卓，3、苹果，4、小程序
     */
    public int $type = 0;
    /**
     * @var string|null 包信息
     */
    public ?string $app_info = null;
    /**
     * @var string[]|null 曝光地址
     */
    public ?array $check_url = null;
    /**
     * @var array|null 点击检测地址
     */
    public ?array $view_url = null;
    /**
     * @var int 类型：1普通创意组，2贴片创意组，3原生创意组
     */
    public int $template_type = 0;
    /**
     * @var string[]|null  唤醒检测地址
     */
    public ?array $deeplink_url = null;
    /**
     * @var int
     */
//    public int $advertising_space_info = 0;
    /**
     * @var string 广告素材路径
     */
    public string $file_path;
    /**
     * @var int
     */
//    public int $rta = 0;
    /**
     * @var int 推广单元id
     */
    public int $unit_id = 0;
    /**
     * @var int 推广计划id
     */
    public int $plan_id = 0;
    /**
     * @var float|int 价格（分）
     */
    public float $price = 0;
    /**
     * @var float|int 1.cpc 2.cpm
     */
    public int $chargetype = 2;
}