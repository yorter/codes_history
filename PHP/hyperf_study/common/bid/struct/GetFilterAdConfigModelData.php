<?php
/**
 * @author 韩石强
 * @time 2022/4/18
 */

namespace common\bid\struct;


use common\services\adFilter\data\BidModel;

class GetFilterAdConfigModelData extends BidModel
{
    public int $platform = 0;
    public ?string $requestId = "";
    public ?string $appId = "";
    public ?string $pId = "";
    public string $channel = "";
    public string $spaceId = "";
    /**
     * @var bool 是否记录竞价日志，默认为true
     */
//    public bool $enable_bid_log = true;
    /**
     * @var bool 是否已经通过尺寸寻到pid或者通过pid寻找到尺寸
     */
//    public bool $have_get_pid_or_size = false;
}