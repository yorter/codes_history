<?php
/**
 * @author 韩石强
 * @time 2022/4/13
 */

namespace common\bid\struct;

use common\components\chunzhen\IpClient;
use common\data\const_value\api\ChannelGetAdConst;
use common\helpers\commonApi;
use common\models\redis\DeviceCache;
use common\models\struct\ModelData;
use Hyperf\Utils\ApplicationContext;

class DeviceModelData extends ModelData
{
    /**
     * @var string 日志类型
     */
//    public string $log_action = "device";
    /**
     * @var string 客户端ip
     */
    public string $ip = "";
    /**
     * @var string|null ipv6地址
     */
    public ?string $ipv6 = "";

    /**
     * @var string 实际地址
     */
    public string $location = "";
    /**
     * @var string|null iOS IDFA 唯一id
     */
    public ?string $idfa = "";
    /**
     * @var string|null ios14之后的唯一id
     */
    public ?string $caid = "";
    /**
     * @var string|null iOS IDFV
     */
    public ?string $idfv = "";
    /**
     * @var string|null Android ID
     */
    public ?string $anid = "";
    /**
     * @var string|null Android Advertising ID
     */
    public ?string $adid = "";
    /**
     * @var string|null 设备imei
     */
    public ?string $imei = "";
    /**
     * @var string|null 设备oaid，oaid和imei二者必传其一，Android 10及以上版本推荐oaid
     */
    public ?string $oaid = "";
    /**
     * @var string|null 设备imsi
     */
    public ?string $imsi = "";
    /**
     * @var string|null mac地址，如00:02:00:00:00:00
     */
    public ?string $mac = "";
    /**
     * @var string 浏览器userAgent
     */
    public string $ua = "";
    /**
     * @var int|null 设备类型(0 – phone，1 – pad，2 – 其他)
     */
    public ?int $type = 0;
    /**
     * @var int|null 操作系统类型(0 – iOS，1 – ANDROID)
     */
    public ?int $os = null;
    /**
     * @var string|null 操作系统版本，如：10.3.1
     */
    public ?string $osv = "";
    /**
     * @var int|null 操作系统版本号：如26
     */
    public ?int $osvcode;
    /**
     * @var string|null 设备品牌，如：XIAOMI
     */
    public ?string $brand = "";
    /**
     * @var string|null 设备型号，如：HONGMIRED6
     */
    public ?string $model = "";
    /**
     * @var int|null 设备屏幕宽度，像素，dip，如1080
     */
    public ?int $sw = 0;
    /**
     * @var int|null 设备屏幕高度，像素，dip，如1920
     */
    public ?int $sh = 0;
    /**
     * @var float|null 纬度
     */
    public ?float $lat = 0;
    /**
     * @var float|null 经度
     */
    public ?float $lon = 0;
    /**
     * @var string|null |null 横竖屏，0–竖屏，1–横屏
     */
    public ?string $orientation = "";
    /**
     * @var string|null 移动国家码，中国：460
     */
    public ?string $mcc = "";
    /**
     * @var string|null 移动网络码，移动00/02/07，联通01/06，电信03/05/11
     */
    public ?string $mnc = "";
    /**
     * @var int|null 网络类型：0-未知，1-wifi，2-2G，3-3G，4-4G，5-5G
     */
    public ?int $connection = 0;
    /**
     * @var string|null 屏幕像素密度，如，1/2/3/1.5
     */
    public ?string $density = "";
    /**
     * @var string|null 当前使用的语言，如zh-cn
     */
    public ?string $lang = "";
    /**
     * @var string|null
     */
    public ?string $appstorever = "";
    /**
     * @var  string|null 华为 HMS Core 版本号
     */
    public ?string $hmscore = "";
    /**
     * @var string|null 系统更新标识
     */
    public ?string $updatemark = "";
    /**
     * string|null 系统启动标识
     */
    public ?string $bootmark = "";

    /**
     * @var string|null 验证错误信息
     */
    protected ?string $errorInfo = null;
    /**
     * @var string|null 错误字段
     */
    protected ?string $field = null;
    /**
     * @var string|null 唯一值得key
     */
    protected ?string $unique_key = null;
    /**
     * @var string|null 设备唯一值
     */
    protected ?string $unique_id = null;
    /**
     * @var string|null appid
     */
    public ?string $appid = "";
    /**
     * @var int|null 平台id
     */
    public ?int $platform = null;

    /**
     * @var bool 达到保存设备信息条件
     */
    protected bool $can_save = false;


    /**
     * 验证参数的合法性
     * @param $save bool 是否保存日志
     * @return bool
     * @author 韩石强
     * @time 2022/4/22
     */
    public function validate(bool $save = true): bool
    {
        if (($this->os === ChannelGetAdConst::OS_ANDROID or $this->os === ChannelGetAdConst::OS_IOS) and $this->ua) {
            if ($this->os === ChannelGetAdConst::OS_IOS) {
                if ($this->idfa) {
                    $this->unique_id = $this->idfa;
                    $this->unique_key = 'idfa';
                } elseif ($this->caid) {
                    $this->unique_id = $this->caid;
                    $this->unique_key = 'caid';
                } else {
                    $this->field = "idfa or caid";
                    $this->errorInfo = "ios no idfa or caid";
                    return false;
                }
                $this->can_save = true;
                if ($save) {//保存到日志
                    $this->save();
                }
            } else {
                $imei_len = strlen($this->imei);
                if ($this->imei and ($imei_len == 15 or $imei_len == 16 or $imei_len == 32 or $imei_len == 64)) {
                    $this->unique_id = $this->imei;
                    $this->unique_key = 'imei';
                } elseif ($this->oaid) {
                    $this->unique_id = $this->oaid;
                    $this->unique_key = 'oaid';
                } else {
                    $this->field = "imei or oaid";
                    $this->errorInfo = "Android need imei or oaid,or imei invalid";
                    return false;
                }
                $this->can_save = true;
                if ($save) {//保存到日志
                    $this->save();
                }
                if (!$this->anid) {
                    $this->field = "anid";
                    $this->errorInfo = "no Android ID";
                    return false;
                }
            }
        } else {
            $this->errorInfo = "invalid oa or ua";
            $this->field = "os,ua";
            return false;
        }
//        if (!$this->mac) {
//            $this->field = "mac";
//            $this->errorInfo = "no mac";
//            return false;
//        }
        //过滤器支持ipv4和ipv6，但是设备验证只验证ipv4,ipv4和ipv6只要有一个存在，则通过验证
        if (!$this->ip and !$this->ipv6) {
            $this->errorInfo = "invalid ip";
            $this->field = "ip";
            return false;
        }
        if (!$this->osv) {
            $this->field = "osv";
            $this->errorInfo = "no osv";
            return false;
        }
        return true;
    }

    public function getError(): array
    {
        return [$this->errorInfo, $this->field];
    }

    public function getUnique()
    {
        return [$this->unique_key, $this->unique_id];
    }

    /**
     * @var string 唯一id
     */
    public string $uniq_key = "";
    /**
     * @var int 使用时间
     */
    public int $use_time = 0;

    public function save(int $normalDb = 14, int $md5Db = 15, $addCount = true, $markDb = 13)
    {
        go([$this, '_save'], $normalDb, $md5Db, $addCount, $markDb);
    }

    /**
     * 保存设备信息
     * @param int $normalDb 设备id没有经过md5加密的保存的库
     * @param int $md5Db 设备id经过md5加密的保存的库
     * @return void|null
     * @author 韩石强
     * @time 2022/5/23
     */
    public function _save(int $normalDb = 14, int $md5Db = 15, $addCount = true, $markDb = 13)
    {
        if (!$this->can_save) {
            return null;
        }
        //没有ip不入库
//        if (!$this->ip) {
//            return null;
//        }
        if (!$this->location) {
            $this->location = "";
            try {
                $location_array = null;
                if ($this->ip) {//获取ipv4实际地址
                    $model = new IpClient();
                    if ($this->ip != "127.0.0.1") {
                        $location_array = $model->getlocation($this->ip);
                    }
                } elseif ($this->ipv6) {//获取ipv6实际地址
                    $model = new commonApi();
                    $location_array = $model->ipFinder($this->ipv6);
                } else {
                    return;
                }
                if (is_array($location_array)) {
                    $this->location = $location_array['country'];
                }
            } catch (\Throwable $exception) {

            }
        }
        if (!$this->location) {
            return null;
        }
//        if ($this->os === ChannelGetAdConst::OS_IOS) {
//            $this->uniq_key = "ios___" . $this->unique_id;
//        } else {
//            $this->uniq_key = $this->unique_id;
//        }
        $today = date("Y-m-d");
        $this->uniq_key = $this->unique_id;
        $this->use_time = time();
        $container = ApplicationContext::getContainer();
        $redis = $container->get(DeviceCache::class);
        $redis->select($markDb);
        $expire = 3 * 24 * 3600;
        if (!$redis->setnx($this->unique_id, 1)) {//统计重复数的话才执行三天之内的验重
            if ($addCount) {
                $redis->select(0);
                $repeatKey = "repeat_device_:" . $today;
                $redis->hIncrBy($repeatKey,(string) $this->platform, 1);
            }
            return;
        }
        $redis->expire($this->unique_id, $expire);
        $uniq_id_len = strlen($this->unique_id);
        if ($uniq_id_len == 32 or $uniq_id_len == 64) {        //md5加密key放到加密库中
            $save_db = $md5Db;
        } else {//非加密key
            $save_db = $normalDb;
        }

        $redis->select($save_db);
        $queue_no = crc32($this->unique_id) % 100;
        $redis_key = "pick_devices_" . $queue_no;
        $device = get_object_vars($this);
        if (!$this->ip) {//如果事ipv6,就把ip和ipv6两个字段都填上。
            $device['ip'] = $this->ipv6;
        }
        //删除不需要的key
        if ($this->os === ChannelGetAdConst::OS_IOS) {
            unset($device['imei']);
            unset($device['oaid']);
        } else {
            unset($device['idfa']);
            unset($device['caid']);
            unset($device['idfv']);
        }
        unset($device['errorInfo']);
        unset($device['field']);
//        unset($device['uniq_key']);
        unset($device['unique_key']);
        unset($device['unique_id']);
        unset($device['can_save']);
//        unset($device['is_saved']);
        unset($device['model_data_array_offsets']);
        unset($device['model_data_offset_limit']);
        unset($device['appid']);
        unset($device['platform']);
        $redis->lpush($redis_key, json_encode($device, JSON_UNESCAPED_UNICODE));
        if ($addCount) {
            //添加新设备数量
            $redis->select(0);
            $key = "new_device:" . $today;
            $redis->hIncrBy($key, (string)$this->platform, 1);
        }
    }


    /**
     * @return bool 是否达到保存条件
     */
    public function isCanSave(): bool
    {
        return $this->can_save;
    }
}