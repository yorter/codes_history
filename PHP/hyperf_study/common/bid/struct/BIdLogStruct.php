<?php
/**
 * @author 韩石强
 * @time 2022/4/19
 */

namespace common\bid\struct;

class BIdLogStruct
{
    /**
     * @var string 日志标识
     */
    public string $log_mark = "monitor";
//    public float $price = 0;
    /**
     * @var float|int 默认出价(元)
     */
    public float $default_price = 0;
    /**
     * @var int 创意id
     */
    public int $creative_id = 0;
    /**
     * @var int 推广单元id
     */
    public int $unit_id = 0;
    /**
     * @var int 推广计划id
     */
    public int $plan_id = 0;
    /**
     * @var string|null 尺寸
     */
    public ?string $size = null;
    /**
     * @var int|null 平台
     */
    public int $platform = 0;
}