<?php

namespace common\bid;


use common\bid\struct\DeviceModelData;
use common\bid\struct\FilterAdModelData;
use common\bid\struct\GetFilterAdConfigModelData;
use common\components\callback\Callback;
use common\components\daHangHai\OptimizedCostPerX;
use common\exception\BaseFilter;
use common\exception\BidException;
use common\helpers\LogV2;
use common\helpers\Pid;
use common\models\redis\DspInfo;
use common\services\ad\AdPlatform;
use common\services\adFilter\FilterStarter;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Exception\ParallelExecutionException;


/**
 *  TODO公共基类
 */
class BaseBid
{
    protected RequestInterface $httpRequest;
    /**
     * @var DeviceModelData 设备信息
     */
    protected DeviceModelData $device;
    /**
     * @var GetFilterAdConfigModelData 请求广告的参数信息，包含一些子类需要的
     */
    protected GetFilterAdConfigModelData $config;
    /**
     * @var FilterAdModelData|null 从过滤器返回的广告信息
     */
    protected ?FilterAdModelData $ad = null;

    public function __construct(RequestInterface $request)
    {
        $this->device = new DeviceModelData();
        $this->config = new GetFilterAdConfigModelData();
    }

    /**
     * 获取请求过滤器的配置类
     *
     * @return GetFilterAdConfigModelData
     */
    public function getConfig(): GetFilterAdConfigModelData
    {
        return $this->config;
    }


    /**
     * 通过过滤器获取广告
     * @return FilterAdModelData|null
     * @throws BidException
     */
    protected function getAdModelFromFilter(): ?FilterAdModelData
    {
//        $this->getPidOrSize();
//        $filter = new AdFilter();
//        $filterAdArray = $filter->getAd($this->config);
        $this->config->deviceData = $this->device;
        try {
            $filterAdArray = (new FilterStarter($this->config))->HandleRequest();
        } catch (ParallelExecutionException $e) {
            $errors = $e->getThrowables();
            foreach ($errors as $error) {
                if ($error instanceof BaseFilter) {
//                    echo $error->getMessage()."\n".$error->getFile()."----".$error->getLine()."\n\n";
                    $class = $error->getExceptionClassName();
                    throw new $class($error->getMessage());
                }
            }
        }
        if ($filterAdArray) {//补充一些替换符数据
            $filterAdArray = get_object_vars($filterAdArray);
            $filterAdArray['request_id'] = $this->config->requestId;
            $filterAdArray['platform'] = $this->config->platform;
            $filterAdArray['pid'] = $this->config->pId;
            $filterAdArray['appid'] = $this->config->appId;
            //设备信息
            $filterAdArray['cid'] = $filterAdArray['creative_id'];
            $filterAdArray['os'] = $this->config->device->os;
            [$uniqKey, $uniqId] = $this->device->getUnique();
            $uniqIdLen = strlen($uniqId);
            if ($uniqIdLen == 32 or $uniqIdLen == 64) {
                $filterAdArray["md5" . $uniqKey] = $uniqId;
            } else {
                $filterAdArray[$uniqKey] = $uniqId;
            }
            $filterAdArray['size'] = "{$filterAdArray['file_width']}*{$filterAdArray['file_high']}";
            $filterAdArray['check_url'] = $this->replaceUrl($filterAdArray['check_url'], $filterAdArray) ?: [];
            $filterAdArray['view_url'] = $this->replaceUrl($filterAdArray['view_url'], $filterAdArray) ?: [];
            $filterAdArray['deeplink_url'] = $this->replaceUrl($filterAdArray['deeplink_url'], $filterAdArray) ?: [];
            $ad = new FilterAdModelData();
            foreach ($filterAdArray as $k => $v) {
                $ad[$k] = $v;
            }
            $this->ad = $ad;
            return $ad;
        } else {
            return null;
        }
    }

    /**
     * 通过pid获取尺寸或者通过尺寸获取pid
     *
     * @throws BidException
     */
    protected function getPidOrSize()
    {
        if (!$this->config->spaceId or !$this->config->channel or !$this->config->pId or !$this->config->appId) {
            $container = ApplicationContext::getContainer();
            $redis = $container->get(DspInfo::class);
            if ($this->config->pId) {//通过pid获取宽高
                $key = Pid::GetPidRedisKey($this->config->platform, $this->config->pId);
                $pid = $redis->get($key);
                $pidInfo = json_decode($pid, true);
            } else {//通过宽高获取pid
                $pidInfo = AdPlatform::getPid(
                    $this->config->platform,
                    $this->config->width,
                    $this->config->height
                );
            }
            if (empty($pidInfo)) {
                LogV2::errorCode(
                    10042,
                    $this->config->platform,
                    $this->config->requestId,
                    $this->config->appId ?? "",
                    $this->config->pId ?: ""
                );
                throw new BidException("pid错误(2)");
            }
            $this->config->pId = $pidInfo['pid'];
            if (!$this->config->appId) {
                $this->config->appId = $pidInfo['appid'];
            }
            $this->config->spaceId = $pidInfo['dahanghai_space_id'];
            $this->config->channel = $pidInfo['dahanghai_channel_id'];
            //            $this->device->appid = $this->config->appId;
        }
    }

    /**
     * @var array|string[] 要替换掉额字符串=>在ad成员属性中对应的key
     */
    public array $replace = [
        "__PLATFORM__" => 'platform',
        Callback::CALLBACK_REQUEST_ID => 'request_id',
        Callback::CALLBACK_UNIT_ID => 'unit_id',
        Callback::CALLBACK_PLAN_ID => 'plan_id',
        Callback::CALLBACK_SIZE => 'size',
        Callback::CALLBACK_CREATIVE_ID => 'creative_id',
        OptimizedCostPerX::OCPX_CID => 'cid',
        OptimizedCostPerX::OCPX_IMEI => 'imei',
        OptimizedCostPerX::OCPX_IMEIMD5 => 'md5imei',
        OptimizedCostPerX::OCPX_IDFA => 'idfa',
        OptimizedCostPerX::OCPX_IDFAMD5 => 'md5idfa',
        OptimizedCostPerX::OCPX_OAID => 'oaid',
        OptimizedCostPerX::OCPX_OAIDMD5 => 'md5oaid',
        Callback::CALLBACK_PID => 'pid',
        Callback::CALLBACK_APPID => 'appid',
        OptimizedCostPerX::OCPX_ADID => 'unit_id',
        OptimizedCostPerX::OCPX_OS => 'os',
    ];

    /**
     * 替换回调地址中的占位符
     *
     * @param string $s
     * @param array $ad
     *
     * @return array|string|string[]
     */
    public function replaceUrl(string $s, array $ad)
    {
        if (!$s) {
            return false;
        }
        $urls = json_decode($s, true);
        if (!$urls) {
            $urls = [$s];
        }
        foreach ($urls as $k => $url) {
            foreach ($this->replace as $before => $after) {
                if (substr_count($url, $before)) {
                    $url = str_replace($before, $ad[$after], $url);
//                } elseif ($ad[$after]) {
//                    $url = $url . "&" . $after . "=" . $ad[$after];
                }
            }
            $url = $this->filterUrl($url);
            $urls[$k] = $url;
        }
        return $urls;
    }


    private function filterUrl($url)
    {
        $urlParms = parse_url($url);
        $urlQu = $urlParms['query'];
        $urlQuArr = explode('&', $urlQu);
        $resStr = '';
        foreach ($urlQuArr as $a => $b) {
            $bArr = explode('=', $b);
            if (isset($bArr[1]) && $bArr[1]) {
                $resStr = $resStr . '&' . $bArr[0] . '=' . $bArr[1];
            }
        }
        $urlParms['query'] = trim($resStr, '&');
        $url = $urlParms['scheme'] . '://' . $urlParms['host'] . $urlParms['path'] . '?'
            . $urlParms['query'];
        return $url;
    }
}

