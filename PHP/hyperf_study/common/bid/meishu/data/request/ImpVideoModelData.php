<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpVideoModelData extends ModelData
{
    /**
     * @var string[]|null 支持的广告播放格式,通常包括mp4,flv Y
     */
    public array $mimes = [];
    /**
     * @var int|null 广告位宽 N
     */
    public ?int $w = null;
    /**
     * @var int|null 广告位高 N
     */
    public ?int $h = null;
    /**
     * @var int|null 视频广告最小时长 N
     */
    public ?int $minduration = null;
    /**
     * @var int|null 视频广告最大时长 N
     */
    public ?int $maxduration = null;
}