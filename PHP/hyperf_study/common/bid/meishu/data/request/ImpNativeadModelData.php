<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpNativeadModelData extends ModelData
{
    protected array $model_data_array_offsets = [
        'assets' => ImpNativeadAssetsModelData::class
    ];

    /**
     * @var string[] 原生广告模板ID数组返回任一符合模板条件的创意即可（参照附录6） Y
     */
    public array $template_ids = [];
    /**
     * @var ImpNativeadAssetsModelData[] 原生广告规则要求 Y
     */
    public array $assets = [];

}