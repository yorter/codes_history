<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpNativeadAssetsExtModelData extends ModelData
{
    protected array $model_data_offset_limit = [
        'image' => ImpNativeadAssetsExtImageModelData::class,
        'video' => ImpNativeadAssetsExtVideoModelData::class,
        'icon' => ImpNativeadAssetsExtIconModelData::class,
    ];
    /**
     * @var ImpNativeadAssetsExtImageModelData|null 图片素材属性要求 N
     */
    public ?ImpNativeadAssetsExtImageModelData $image = null;
    /**
     * @var ImpNativeadAssetsExtVideoModelData|null 音视频素材属性要求 N
     */
    public ?ImpNativeadAssetsExtVideoModelData $video = null;
    /**
     * @var ImpNativeadAssetsExtIconModelData|null icon属性要求 N
     */
    public ?ImpNativeadAssetsExtIconModelData $icon = null;
}