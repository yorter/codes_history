<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;
/**
 * 原生广告规则要求
 */
class ImpNativeadAssetsModelData extends \common\models\struct\ModelData
{
    protected array $model_data_offset_limit = [
        'ext' => ImpNativeadAssetsExtModelData::class
    ];

    /**
     * @var string 原生广告模板ID（参照附录6） Y
     */
    public string $id = "";
    /**
     * @var int|null 原生广告位宽限制 N
     */
    public ?int $w = null;
    /**
     * @var int|null 原生广告位高限制 N
     */
    public ?int $h = null;
    /**
     * @var ImpNativeadAssetsExtModelData|null 扩展对象
     */
    public ?ImpNativeadAssetsExtModelData $ext = null;
}