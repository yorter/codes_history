<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpNativeadAssetsExtIconModelData extends ModelData
{
    /**
     * @var string[]|null icon支持的文件格式,通常包括（jpg,jpeg,png,gif） N
     */
    public ?array $mimes = null;
    /**
     * @var int|null icon图片宽限制 N
     */
    public ?int $w = null;
    /**
     * @var int|null icon图片高限制 N
     */
    public ?int $h = null;
}