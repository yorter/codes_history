<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * Site 流量方对象，只针对 website 流量时存在
 */
class SiteModelData extends ModelData
{
    /**
     * @var string|null site在SSP的唯一标识ID N
     */
    public ?string $id = null;
    /**
     * @var string|null site名称 N
     */
    public ?string $name = null;
    /**
     * @var int[]|null 站点内容类别列表 (暂保留) N
     */
    public ?array $cat = null;
    /**
     * @var string|null 当前页面url N
     */
    public ?string $page = null;
    /**
     * @var string|null 当前页面url N
     */
    public ?string $ref = null;
    /**
     * @var string|null 搜索词 N
     */
    public ?string $search = null;
    /**
     * @var array|null 关键词描述 N
     */
    public ?array $keywords = null;
    /**
     * @var string|null 站点domain N
     */
    public ?string $domain = null;
    /**
     * @var string|null 拓展字段
     */
    public ?string $ext = null;


}