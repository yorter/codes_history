<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * APP流量方对象，只针对app流量时存在
 */
class AppModelData extends ModelData
{
    /**
     * @var string 在ssp的唯一标识ID Y
     */
    public string $id = "";
    /**
     * @var string|null 应用名称 N
     */
    public ?string $name = null;
    /**
     * @var string APP包名，具有唯一性
     */
    public string $bundle = "";
    /**
     * @var array|null app 的内容分类列表（暂保留） N
     */
    public ?array $cat = null;
    /**
     * @var string|null APP版本号 N
     */
    public ?string $ver = null;
    /**
     * @var array|null APP关键字描述
     */
    public ?array $keywords = null;
    /**
     * @var string|null 拓展字段
     */
    public ?string $ext = null;
}