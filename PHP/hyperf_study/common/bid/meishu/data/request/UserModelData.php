<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * 投放受众的用户信息描述
 */
class UserModelData extends ModelData
{
    /**
     * @var string|null 用戶ID N
     */
    public ?string $id = null;
    /**
     * @var int|null 4位数字出生年 N
     */
    public ?int $yob = null;
    /**
     * @var string|null 性别“M” = male, “F”=female, “U”=unknown N
     */
    public ?string $gender = null;
    /**
     * @var array|null 用户兴趣，爱好 N
     */
    public ?array $keywords = null;
    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext = null;
}