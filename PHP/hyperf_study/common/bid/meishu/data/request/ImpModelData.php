<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpModelData extends ModelData
{
    protected array $model_data_array_offsets = [

    ];
    protected array $model_data_offset_limit = [
        'banner' => ImpBannerModelData::class,
        'nativead' => ImpNativeadModelData::class,
        'video' => ImpVideoModelData::class,
        'pmp' => ImpPmpModelData::class,
    ];

    /**
     * @var string 曝光标识ID，通常只在多次曝光有意义 Y
     */
    public string $id = "";
    /**
     * @var int 广告类型（0:横幅,1:开屏,3:插屏,4:视频贴片,5:原生,7:激励视频） Y
     */
    public int $type = 0;
    /**
     * @var string 广告位ID Y
     */
    public string $pid = "";
    /**
     * @var ImpBannerModelData|null Banner广告该字段必须存在 N
     */
    public ?ImpBannerModelData $banner = null;
    /**
     * @var ImpNativeadModelData|null 原生广告该字段必须存在 N
     */
    public ?ImpNativeadModelData $nativead = null;
    /**
     * @var ImpVideoModelData|null 视频贴片广告该字段必须存在 N
     */
    public ?ImpVideoModelData $video = null;
    /**
     * @var int|null 售卖方式，0=cpm，1=cpc，默认值0（暂时只支持cpm） N
     */
    public ?int $bidtype = null;
    /**
     * @var int|null 底价，当bidtype＝0时，单位:分/CPM；当bidtype＝1时，单位:分/点击 Y
     */
    public int $bidfloor = 0;
    /**
     * @var ImpPmpModelData|null 当该字段存在且不为‘’空时，标识是PD或者PDB 流量。 N
     */
    public ?ImpPmpModelData $pmp = null;
    /**
     * @var int|null 是否支持应用唤起(0:未知，1:支持，2:不支持) N
     */
    public ?int $support_dp = null;
    /**
     * @var int|null 是否支持应用下载(0:未知，1:支持，2:不支持) N
     */
    public ?int $support_down = null;
    /**
     * @var int|null 是否支持微信小程序(0:未知，1:支持，2:不支持) N
     */
    public ?int $support_wx = null;
}