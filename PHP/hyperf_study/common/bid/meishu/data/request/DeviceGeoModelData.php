<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * 设备位置属性
 */
class DeviceGeoModelData extends ModelData
{
    /**
     * @var int|null 定位类型：gps(默认) N
     */
    public ?int $type = null;
    /**
     * @var float|null 纬度（-90~90） N
     */
    public ?float $lat = null;
    /**
     * @var float|null 经度（-180~180） N
     */
    public ?float $lon = null;
}