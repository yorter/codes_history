<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * 当该字段存在且不为‘’空时，标识是PD或者PDB 流量。
 */
class ImpPmpModelData extends ModelData
{
    protected array $model_data_array_offsets = [
        'deals' => ImpPmpDealsModelData::class
    ];
    /**
     * @var ImpPmpDealsModelData[] deal对象
     */
    public array $deals = [];
}