<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class DeviceModelData extends ModelData
{
    protected array $model_data_offset_limit = [
        'geo' => DeviceGeoModelData::class
    ];

    /**
     * @var string|null 设备IPv4地址 N
     */
    public ?string $ip = null;
    /**
     * @var string|null 设备IPv6地址 N
     */
    public ?string $ipv6 = null;
    /**
     * @var string|null 浏览器User-agent N
     */
    public ?string $ua = null;
    /**
     * @var string|null 运营商ID （参照附录5.1） N
     */
    public ?string $carrier = null;
    /**
     * @var int|null 网络类型 （参照附录5.2）N
     */
    public ?int $connectiontype = null;
    /**
     * @var int|null 设备类型 （参照附录5.3） N
     */
    public ?int $devicetype = null;
    /**
     * @var string|null imsi N
     */
    public ?string $imsi = null;
    /**
     * @var string|null 安卓ID(请根据长度判断等于32时为md5加密后的值) N for andriod
     */
    public ?string $androidid = null;
    /**
     * @var string|null    明文imei或者md5加密imei (请根据长度判断等于32时为md5加密后的值) N for android
     */
    public ?string $imei = null;
    /**
     * @var string|null oaid N for android
     */
    public ?string $oaid = null;
    /**
     * @var string|null md5加密后的值 N for android
     */
    public ?string $oaid_md5 = null;
    /**
     * @var string|null 明文mac地址或者md5加密mac (请根据长度判断等于32时为md5加密后的值) N
     */
    public ?string $mac = null;
    /**
     * @var string|null idfa(请根据长度判断等于32时为md5加密后的值) N for ios
     */
    public ?string $idfa = null;
    /**
     * @var string|null 设备生产商例：HuaWei N
     */
    public ?string $make = null;
    /**
     * @var string|null 设备型号例：Meta 8 N
     */
    public ?string $model = null;
    /**
     * @var string|null 厂商应用商店版本号(vovi、小米、华为、oppo 等厂商应 用商店) N
     */
    public ?string $appstore_ver = null;
    /**
     * @var string|null 华为 HMS Core 版本号 N
     */
    public ?string $hmscore = null;
    /**
     * @var string|null 系统启动标识（阿里） N
     */
    public ?string $boot_mark = null;
    /**
     * @var string|null 系统更新标识（阿里） N
     */
    public ?string $update_mark = null;
    /**
     * @var string|null 当前设备语言例:zh-CH N
     */
    public ?string $lan = null;
    /**
     * @var string|null 设备操作系统 (Android,iOS,WP,Others) 四种 N
     */
    public ?string $os = null;
    /**
     * @var string|null 操作系统版本 N
     */
    public ?string $osv = null;
    /**
     * @var string|null 设备屏幕高度 N
     */
    public ?string $h = null;
    /**
     * @var string|null 设备屏幕宽度 N
     */
    public ?string $w = null;
    /**
     * @var string|null 设备屏幕像素密度:286 N
     */
    public ?string $ppi = null;
    /**
     * @var string|null 横竖屏(-1:未知，1:横屏，0:竖屏) N
     */
    public ?string $orientation = null;
    /**
     * @var DeviceGeoModelData|null 设备位置属性 N
     */
    public ?DeviceGeoModelData $geo = null;
    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext = null;
}