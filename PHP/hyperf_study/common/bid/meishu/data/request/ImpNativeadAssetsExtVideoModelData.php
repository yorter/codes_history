<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

class ImpNativeadAssetsExtVideoModelData extends \common\models\struct\ModelData
{
    /**
     * @var array|null 支持的素材文件格式,通常包括（mp4、mp3、flv…） N
     */
    public ?array $mimes = null;
    /**
     * @var int|null 音视频素材宽限制(音频一般为0) N
     */
    public ?int $w = null;
    /**
     * @var int|null 音视频素材高限制(音频一般为0) N
     */
    public ?int $h = null;
    /**
     * @var int|null 音视频广告最小时长 N
     */
    public ?int $minduration = null;
    /**
     * @var int|null 音视频广告最大时长 N
     */
    public ?int $maxduration = null;
}