<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpBannerModelData extends ModelData
{
    /**
     * @var int|null 广告位顺序ID，一般从1开始 N
     */
    public ?int $id = null;
    /**
     * @var string[] 支持的素材文件格式,通常包括（jpg,jpeg,png,gif） Y
     */
    public array $mimes = [];
    /**
     * @var int 广告位宽 Y
     */
    public int $w = 0;
    /**
     * @var int 广告位高 Y
     */
    public int $h = 0;
    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext = null;
}