<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpPmpDealsModelData extends ModelData
{
    /**
     * @var string Dealid Y
     */
    public string $id = "";
    /**
     * @var int|string 竞价方式，目前只能是1，竞价最高的deal获得成功，取最高出价作为胜出价 Y
     */
    public int $at = 0;
    /**
     * @var int Deal出价 单位 分/CPM Y
     */
    public int $price = 0;
}