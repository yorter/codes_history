<?php

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

/**
 * @author 韩石强
 * @time 2022/4/11
 */
class MeishuRequestData extends ModelData
{
    protected array $model_data_array_offsets = [
        'imp' => ImpModelData::class
    ];
    protected array $model_data_offset_limit = [
        'site' => SiteModelData::class,
        'app' => AppModelData::class,
        'device' => DeviceModelData::class,
        'user' => UserModelData::class,
    ];
    /**
     * @var string Bid Request唯一ID，由SSP 生成。 Y
     */
    public string $id = "";
    /**
     * @var ImpModelData[] 标识广告位曝光的IMP对象列表 Y
     */
    public array $imp = [];
    /**
     * @var SiteModelData|null Site 流量方对象，只针对 website 流量时存在 N
     */
    public ?SiteModelData $site = null;
    /**
     * @var AppModelData|null APP流量方对象，只针对app流量时存在 N
     */
    public ?AppModelData $app = null;
    /**
     * @var DeviceModelData|null 描述用户的设备信息 N
     */
    public ?DeviceModelData $device = null;
    /**
     * @var UserModelData|null 投放受众的用户信息描述 N
     */
    public ?UserModelData $user = null;
    /**
     * @var int|null 超时时间，默认200ms。单位毫秒 N
     */
    public ?int $tmax = null;
    /**
     * @var int|null 标记返回链接是否必须https(0 =否,1 =是)，默认0 N
     */
    public ?int $secure = null;
    /**
     * @var string[]|null 广告行业黑名单 N
     */
    public ?array $bcat = null;
    /**
     * @var string|null 扩展对象 N
     */
    public ?string $ext = null;


}