<?php
/**
 * @author 韩石强
 * @time 2022/4/11
 */

namespace common\bid\meishu\data\request;

use common\models\struct\ModelData;

class ImpNativeadAssetsExtImageModelData extends ModelData
{
    /**
     * @var array|null 支持的素材文件格式,通常包括（jpg,jpeg,png,gif） N
     */
    public ?array $mimes = [];
    /**
     * @var int|null 素材图片宽限制 N
     */
    public ?int $w = null;
    /**
     * @var int|null 素材图片高限制 N
     */
    public ?int $h = null;
}