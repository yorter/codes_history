<?php

namespace common\bid\meishu\data;

use common\bid\meishu\data\template\Template1_2;
use common\bid\meishu\data\template\Template2_2;
use common\bid\meishu\data\template\Template2_4;
use common\bid\meishu\data\template\TemplateImage;

/**
 * @author 韩石强
 * @time 2022/4/12
 */
class MeishuDataConst
{
    /**
     * 广告类型-横幅
     */
    const IMP_TYPE_BANNER = 0;
    /**
     * 广告类型-开屏
     */
    const IMP_TYPE_OPEN = 1;
    /**
     * 广告类型-插屏
     */
    const IMP_TYPE_INSERT = 3;
    /**
     * 广告类型-视频贴片
     */
    const IMP_TYPE_video = 4;
    /**
     * 广告类型-原生
     */
    const IMP_TYPE_NATIVE = 5;
    /**
     * 广告类型-激励视频
     */
    const IMP_TYPE_INCENTIVE_VIDEO = 7;
    /**
     * 安卓设备
     */
    const DEVICE_OS_ANDROID = 'android';
    /**
     * ios设备
     */
    const DEVICE_OS_IOS = 'ios';

    /**
     * 运营商转为AdPlatform::MOBILE_OPERATOR数组下标
     */
    const CARRIER_TO_ID = [
        '46000' => 1,//移动
        '46001' => 2,//联通
        '46003' => 3,//电信
    ];
    /**
     * 网络类型转为AdPlatform::MOBILE_ENVIRONMENT数组下标
     */
    const ENVIRONMENT_TO_ID = [
        2 => 1,//WIFI
        4 => 4,//2G
        5 => 3,//3G
        6 => 2,//4G
        7 => 6,//5G
    ];
    /**
     * 模板对应类
     */
    const TEMPLATE_ID_TO_CLASS = [
        '1-1' => TemplateImage::class,
        '1-8' => TemplateImage::class,
        '1-4' => TemplateImage::class,
        '1-2' => Template1_2::class,
        '2-2' => Template2_2::class,
        '2-3' => Template2_2::class,
        '2-4' => Template2_4::class,
    ];
}