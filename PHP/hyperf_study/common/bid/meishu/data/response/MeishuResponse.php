<?php

namespace common\bid\meishu\data\response;

use common\models\struct\ModelData;

/**
 * 广告应答
 * @author 韩石强
 * @time 2022/4/12
 */
class MeishuResponse extends ModelData
{
    protected array $model_data_offset_limit = [
        'seatbid' => SeatbidModelData::class
    ];
    /**
     * @var string 对应Bid Request的请求ID Y
     */
    public string $id = "";
    /**
     * @var SeatbidModelData[] 广告请求的应答信息(block_device_id为true时非必) Y
     */
    public array $seatbid = [];

    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext;
}