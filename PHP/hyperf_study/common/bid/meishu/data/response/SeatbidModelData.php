<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\response;

use common\models\struct\ModelData;

/**
 * 广告请求的应答信息(block_device_id为true时非必)
 */
class SeatbidModelData extends ModelData
{
    protected array $model_data_array_offsets = [
        'bid' => SeatbidBidModelData::class
    ];

    /**
     * @var SeatbidBidModelData[] 广告请求的应答结果 Y
     */
    public array $bid = [];
    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext;
}