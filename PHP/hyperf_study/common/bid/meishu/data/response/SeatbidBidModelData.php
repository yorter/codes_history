<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\response;

use common\models\struct\ModelData;

/**
 * 广告请求的应答结果
 */
class SeatbidBidModelData extends ModelData
{
    public function __construct()
    {
//        $this->ader_id = \Yii::$app->params['ADER_ID'];
    }

    /**
     * @var string 广告主ID：(流量平台广告主送审返回的ID),白名单广告主创意无须推送审核即可投放，要享有白单权限时此字段必须填写 Y
     */
    public string $ader_id = '216018';
    /**
     * @var string 创意ID：(流量平台创意送审返回的ID)非白名单广告主必填，白名单广告主非必填，只要填写了平台就会进行审核验证。 Y
     */
    public string $cid = '';
    /**
     * @var int 分/CPM计，低于底价的将竞价失败 Y
     */
    public int $price = 0;
    /**
     * @var string|int 非信息流广告填写物料访问地址；信息流广告填写信息流模板相关JSON 数据并且 templateid必填 ， JSON String（详细请参考附录6模板定义）。 Y
     */
    public string $adm = "";
    /**
     * @var string 到达页地址 Y
     */
    public string $durl = "";
    /**
     * @var int 落页类型：1:网页类型（默认）2:应用下载类型 Y
     */
    public int $adck = 1;
    /**
     * @var int 创意类型(1:图片、2:视频) Y
     */
    public int $ad_type = 1;
    /**
     * @var string|null 应用唤起地址：(当媒体支持应用唤起时此字段才有效，唤起失败时调用durl) N
     */
    public ?string $deep_link;
    /**
     * @var string|null 微信小程序原始id，(支持微信小程序唤起时此字段才有效，该字段有值时同跳转至小程序，跳失败时调用durl) N
     */
    public ?string $wx_username;
    /**
     * @var string|null 拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入query部分，来实现传参效果，如：传入 "?foo=bar" N
     */
    public ?string $wx_path;
    /**
     * @var int|null 物料宽(建议填写，宽高符合请求中创意要求) N
     */
    public ?int $w;
    /**
     * @var int|null 物料高(建议填写，宽高符合请求中创意要求) N
     */
    public ?int $h;
    /**
     * @var string[] 曝光监测地址，支持价格宏替换 Y
     */
    public array $nurl = [];
    /**
     * @var string[]|null 点击监测异步上报地址 N
     */
    public ?array $curl;
    /**
     * @var array|null 尝试唤起上报 N
     */
    public ?array $dpstart;
    /**
     * @var array|null 唤起成功上报 N
     */
    public ?array $dpsucc;
    /**
     * @var array|null 唤起失败上报 N
     */
    public ?array $dpfail;
    /**
     * @var array|null 开始下载上报 N
     */
    public ?array $dn_start;
    /**
     * @var array|null 下载成功上报 N
     */
    public ?array $dn_succ;
    /**
     * @var array|null 安装开始上报 N
     */
    public ?array $dn_inst_start;
    /**
     * @var array|null 安装成功上报 N
     */
    public ?array $dn_inst_succ;
    /**
     * @var array|null 下载激活上报 N
     */
    public ?array $dn_active;
    /**
     * @var string|null PMP售卖方式必须带上,并且必须跟广告请求所带相等 N
     */
    public ?string $dealid;
    /**
     * @var string|null 原生广告必填（Request请求中对应的模板ID之一） N
     */
    public ?string $templateid;
    /**
     * @var string|null AppStore应用市的应用ID N
     */
    public ?string $appstore_id;
    /**
     * @var string|null 安卓:应用包名、ios 设备:使用bundle id (下载类广告填写) N
     */
    public ?string $package_name;
    /**
     * @var string|null 应用名如：“锤子便签”； (下载类广告填写) N
     */
    public ?string $app_name;
    /**
     * @var string|null 应用版本号：(下载类广告填写) N
     */
    public ?string $app_version;
    /**
     * @var string|null 应用ICON：(下载类广告填写) N
     */
    public ?string $app_icon;
    /**
     * @var string|null 应用介绍(下载类广告填写) N
     */
    public ?string $app_intro;
    /**
     * @var string|null 当前版本特征(下载类广告填写) N
     */
    public ?string $app_feature;
    /**
     * @var string|null 包体大小(下载类广告填写) N
     */
    public ?string $app_size;
    /**
     * @var string|null N    隐私权限（
     * ● 获取定位
     * 允许应用通过网络或卫星对设备进行定位
     * ● 获取手机信息
     * 允许应用获取手机号、IMEI、IMSI
     * …）
     * 格式:大标题:介绍;大标题:介绍
     * 示例: 获取定位:允许应用通过网络或卫星对设备进行定位;获取手机信息:允许应用获取手机号、IMEI、IMSI;…(下载类广告填写)
     */
    public ?string $app_privacy;
    /**
     * @var string|null 隐私协议，外网可访问链接(下载类广告填写) N
     */
    public ?string $privacy_agreement;
    /**
     * @var string|null 开发者(下载类广告填写) N
     */
    public ?string $developer;
    /**
     * @var string|null N 付费类型（
     * 1：该应用是免费版，不包含任何付费内容；
     * 2：该应用是试用版，付费后才能使用完整功能；
     * 3：该应用包含付费内容，如表情、道具、电子书等虚拟物品；
     * 4：该应用是试用版，付费后才能使用完整功能；同事包含付费内容，如表情、道具、电子书等虚拟物品
     * ）(下载类广告填写)
     */
    public ?string $payment_types;
    /**
     * @var string|null 广告来源(JD、GDT…) N
     */
    public ?string $from;
    /**
     * @var string|null 广告来源logo(logo图片地址) N
     */
    public ?string $from_logo;
    /**
     * @var string|null 拓展字段 N
     */
    public ?string $ext;
}