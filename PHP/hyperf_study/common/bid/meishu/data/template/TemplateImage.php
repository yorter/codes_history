<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\template;
/**
 *1-1(一图一文) 1-8(两图一文) 1-4(三图一文) 通用
 */
class TemplateImage extends TemplateBase
{
    /**
     * @var string 素材URL Y
     */
    public string $imgurl = "";
}