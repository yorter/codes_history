<?php

namespace common\bid\meishu\data\template;
/**
 * @author 韩石强
 * @time 2022/4/12
 */
abstract class TemplateBase
{
    /**
     * @var string 标题 Y
     */
    public string $title = "";
    /**
     * @var string|null 图标url(非必填字段，以媒体列表要求为准) N
     */
    public ?string $icon;
    /**
     * @var string|null 图标slogan(非必填字段，以媒体列表要求为准) N
     */
    public ?string $icon_title;
}