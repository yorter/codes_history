<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\template;
/**
 * 2-2 (原生信息流视频) 2-3(音频贴片)
 */
class Template2_2 extends TemplateVideo
{
    /**
     * @var string 封面URL(必须) Y
     */
    public string $imgurl = "";
}