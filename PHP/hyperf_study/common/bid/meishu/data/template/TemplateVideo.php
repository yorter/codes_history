<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\template;
/**
 * 视频类基类 2-3(音频贴片)
 */
class TemplateVideo extends TemplateBase
{
    /**
     * @var string 视频URL(必须) Y
     */
    public string $videourl = "";
    /**
     * @var string 音视频播放时长单位：秒(必须) Y
     */
    public string $videoduration = "";
}