<?php

namespace common\bid\meishu\data\template;
/**
 * 图文摘要
 * @author 韩石强
 * @time 2022/4/12
 */
class Template1_2 extends TemplateImage
{
    /**
     * @var string 摘要
     */
    public string $content = "";
}