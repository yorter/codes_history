<?php
/**
 * @author 韩石强
 * @time 2022/4/12
 */

namespace common\bid\meishu\data\template;

class Template2_4 extends TemplateVideo
{
    /**
     * @var string 结尾封面URL
     */
    public string $end_cover = "";
    /**
     * @var string 行动语（点击下载、查看详情...）
     */
    public string $action_text = "";
}