<?php

namespace common\bid\meishu;


use common\adx\AdxFactory;
use common\bid\BaseBid;
use common\bid\meishu\data\MeishuDataConst;
use common\bid\meishu\data\request\MeishuRequestData;
use common\bid\meishu\data\response\MeishuResponse;
use common\bid\meishu\data\response\SeatbidBidModelData;
use common\bid\meishu\data\response\SeatbidModelData;
use common\bid\meishu\data\template\Template1_2;
use common\bid\meishu\data\template\Template2_2;
use common\bid\meishu\data\template\Template2_4;
use common\bid\meishu\data\template\TemplateBase;
use common\bid\meishu\data\template\TemplateImage;
use common\bid\meishu\data\template\TemplateVideo;
use common\bid\struct\FilterAdModelData;
use common\data\const_value\api\ChannelGetAdConst;
use common\exception\BidException;
use common\exception\RTAException;
use common\helpers\LogV2;
use common\services\ad\AdPlatform;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * @author 韩石强
 * @time 2022/4/11
 */
class GateWay extends BaseBid
{

    /**
     * @var MeishuRequestData 美数请求转换成对象
     */
    protected MeishuRequestData $request;
    protected MeishuResponse $response;
    /**
     * @var mixed 从过滤器中获取的广告信息
     */
    protected ?FilterAdModelData $ad = null;


    public function __construct(RequestInterface $request)
    {
        parent::__construct($request);
        $this->httpRequest = $request;
        $this->request = new MeishuRequestData();
        $this->response = new MeishuResponse();
    }


    /**
     * 入口函数
     * @return MeishuResponse|null
     * @throws BidException
     * @throws RTAException
     * @author 韩石强
     * @time 2022/5/25
     */
    public function getAd(): ?MeishuResponse
    {
        $post = $this->httpRequest->post();
        if (is_array($post)) {
            foreach ($post as $k => $v) {
                $this->request[$k] = $v;
            }
            //根据情况填充appid
            if ($this->request->app and $this->request->app->id) {
                $this->config->appId = $this->device->appid = $this->request->app->id;
            }
            $this->config->requestId = $this->request->id;
            //根据情况填充pid
            if ($this->request->imp) {
                $this->config->pId = ($this->request->imp[0])->pid;
            }
            LogV2::request(//请求日志
                AdxFactory::ADX_PLATFORM_MEISHU,
                $this->request->id,
                $this->config->appId,
                $this->config->pId
            );
            if (!$this->deviceValidate()) {
                return null;
            }
            $this->config->ip = $this->device->ip;
            $this->config->ipv6 = $this->request->device->ipv6 ?? "";
            LogV2::ipv6($this->request->device->ipv6);
            $this->paramValidate();//验证参数信息
            $this->config->platform = AdxFactory::ADX_PLATFORM_MEISHU;
            $this->getPidOrSize();//只能获取第一个pid相关内容，如果将来出现多个pid同时访问，修改次项位置到getAdFromFilter循环中并去掉结尾的break
            $this->response->id = $this->request->id;
            if ($this->getAdFromFilter()) {
                return $this->response;
            }
        } else {
            //抛出异常
            LogV2::request(//请求日志
                AdxFactory::ADX_PLATFORM_MEISHU, ""
            );
            LogV2::errorCode(10000, AdxFactory::ADX_PLATFORM_MEISHU, "", "", "", ["no post data"]);
        }
        return null;
    }

    /**
     * 请求的key对应的设备信息存储的key的对应关系,[存储设备key，预处理参数方法]
     */
    const device_map = [
        'ip' => ['ip', null],
        'ipv6' => ['ipv6', null],
        'mac' => ['mac', null],
        'make' => ['brand', null],
        'model' => ['model', null],
        'appstore_ver' => ['appstorever', null],
        'hmscore' => ['hmscore', null],
        'boot_mark' => ['bootmark', null],
        'update_mark' => ['updatemark', null],
        'lan' => ['lang', null],
        'osv' => ['osv', null],
        'h' => ['sh', null],
        'w' => ['sw', null],
        'ppi' => ['density', null],
        'orientation' => ['orientation', null],
    ];

    /**
     * 验证设备的合法性病入日志
     * @return bool
     * @author 韩石强
     * @time 2022/4/13
     */
    private function deviceValidate(): bool
    {
        $device = $this->request->device;
        if (!$device) {
            LogV2::errorCode(10041, AdxFactory::ADX_PLATFORM_MEISHU, $this->request->id, $this->config->appId, $this->config->pId, ["no device"]);
            return false;
        }
        $this->device->ua = $device->ua;
        $os = strtolower($device->os);
        if ($os == MeishuDataConst::DEVICE_OS_ANDROID) {
            $this->device->os = ChannelGetAdConst::OS_ANDROID;
            $this->config->device->os = AdPlatform::MOBILE_OS[2];
            $this->config->device->oaid = $this->device->oaid = $device->oaid ?? "";
            $this->config->device->imei = $this->device->imei = $device->imei ?? "";
            $this->device->anid = $device->androidid;
        } elseif ($os == MeishuDataConst::DEVICE_OS_IOS) {
            $this->config->device->idfa = $this->device->idfa = $device->idfa ?? "";
            $this->device->os = ChannelGetAdConst::OS_IOS;
            $this->config->device->os = AdPlatform::MOBILE_OS[1];
        } else {
            $this->config->device->os = AdPlatform::MOBILE_OS[3];
        }
        //运营商
        if ($device->carrier and array_key_exists($device->carrier, MeishuDataConst::CARRIER_TO_ID)) {
            $this->config->device->operator = AdPlatform::MOBILE_OPERATOR[MeishuDataConst::CARRIER_TO_ID[$device->carrier]];
            $this->device->mcc = substr($device->carrier, 0, 3);
            $this->device->mnc = substr($device->carrier, 3, 2);
        }
        //网络环境
        if ($device->connectiontype) {
            $key = array_key_exists($device->connectiontype, MeishuDataConst::ENVIRONMENT_TO_ID) ? $device->connectiontype : 5;
            $this->config->device->environment = AdPlatform::MOBILE_ENVIRONMENT[$key];
        }
        $this->device->platform = AdxFactory::ADX_PLATFORM_MEISHU;
        foreach (self::device_map as $request_key => $info) {
            $this->tryAddValue($request_key, $info);
        }
        if ($device->geo) {
            $this->device->lat = $device->geo->lat ?? 0;
            $this->device->lon = $device->geo->lon ?? 0;
        }
        if ($this->device->validate()) {
            return true;
        } else {
            [$error, $field] = $this->device->getError();
            LogV2::errorCode(10041, AdxFactory::ADX_PLATFORM_MEISHU, $this->request->id, $this->config->appId, $this->config->pId, [$error]);
        }
        return false;
    }


    /**
     * 请求参数验证
     * @throws BidException
     * @author 韩石强
     * @time 2022/4/28
     */
    private function paramValidate()
    {
        if (!$this->request->id) {//没有request_id，则生成一个
            $this->request->id = md5(uniqid(AdxFactory::ADX_PLATFORM_MEISHU . microtime(true)));
        }
        if (!$this->request->imp) {
            $this->throwError(10000, "缺少必要参数imp", "imp");
        }
        foreach ($this->request->imp as $imp) {//验证pid
            if (!$imp->pid) {
                $this->throwError(10042, "pid错误", "imp.pid");
            }
            break;//只验证第一个pid
        }
        if (!$this->request->app or !$this->request->app->id) {//Appid验证
            $this->throwError(10000, "缺少必要参数app.id", "app.id");
        }
        if (!$this->request->device) {//设备信息
            $this->throwError(10000, "缺少必要参数device", "device");
        }
    }

    /**
     * 抛出异常
     * @param int $code 错误码
     * @param $msg string 错误信息
     * @param string $field 错误字段(额外信息，会记录日志)
     * @return mixed
     * @throws BidException
     * @author 韩石强
     * @time 2022/4/28
     */
    private function throwError(int $code, string $msg, string $field = "")
    {
        LogV2::errorCode(
            $code,
            AdxFactory::ADX_PLATFORM_MEISHU,
            $this->request->id,
            $this->config->appId ?? "",
            $this->config->pId ?? "",
            [$field]
        );
        throw new BidException($msg);
    }

    /**
     * 尝试写入device成员对象的属性值
     * @param $request_key
     * @param array $info
     * @author 韩石强
     * @time 2022/4/13
     */
    private function tryAddValue($request_key, array $info)
    {
        [$key, $callback] = $info;
        try {
            $value = $this->request->device[$request_key];
            if (is_null($value)) {
                return;
            }
            $value = $callback ? call_user_func([$this, $callback], $value) : $value;
            $this->device[$key] = $value;
        } catch (\TypeError $exception) {

        }
    }


    /**
     * 横幅 开屏 插屏走banner结构
     */
    const BANNER_AD_TYPE = [MeishuDataConst::IMP_TYPE_BANNER, MeishuDataConst::IMP_TYPE_OPEN, MeishuDataConst::IMP_TYPE_INSERT];

    /**
     * @var array 已经尝试获取广告的pid
     */
    private array $pids = [];

    /**
     * 从过滤器中获取广告信息
     * @return bool 是否成功获取广告
     * @throws BidException
     * @author 韩石强
     * @time 2022/5/25
     */
    private function getAdFromFilter(): bool
    {
        foreach ($this->request->imp as $imp) {
            if (in_array($imp->pid, $this->pids)) {
                continue;
            }
            $this->pids[] = $imp->pid;
            $this->config->platform = AdxFactory::ADX_PLATFORM_MEISHU;
            $this->config->bidFloor = $imp->bidfloor;//出价
            $this->config->bidType = $imp->bidtype == 0 ? 2 : 1;//出价类型
            //pid
            if (!$imp->pid) {
                continue;
            }
            $this->config->pId = $imp->pid;
            if ($this->request->site) {
                $this->config->keyword = $this->request->site->keywords ?: [];
            }
            //广告类型
            if (in_array($imp->type, self::BANNER_AD_TYPE)) {//横幅 开屏 插屏走banner结构
                $this->config->width = $imp->banner->w;
                $this->config->height = $imp->banner->h;
                $this->ad = $this->getAdModelFromFilter();
                if ($this->ad) {
                    $bid = $this->fillResponseBaseInfo();
                    $bid->adm = $this->ad->file_path;
                    return true;
                }

            } elseif ($imp->type == MeishuDataConst::IMP_TYPE_video) {//视频贴片
                $this->throwError(10043, "invalid ad type");
                return false;
                $this->config->width = $imp->video->w;
                $this->config->height = $imp->video->h;
                $this->ad = $this->getAdModelFromFilter();
                if ($this->ad) {
                    $bid = $this->fillResponseBaseInfo();
                    $bid->adm = $this->ad->file_path;
                    return true;
                }

            } else {//原生、激励视频(信息流)
                if ($imp->type == MeishuDataConst::IMP_TYPE_INCENTIVE_VIDEO) {
                    continue;
                }
                foreach ($imp->nativead->assets as $asset) {
                    try {
                        $this->ad = $this->getAdModelFromFilter();
                    } catch (BidException $exception) {
                        break;
                    }
                    if ($this->ad) {
                        $bid = $this->fillResponseBaseInfo();
                        $bid->templateid = $asset->id;//模板id
                        $template = MeishuDataConst::TEMPLATE_ID_TO_CLASS[$asset->id];
                        if (!$template) {
                            break;
                        }
                        $template = new $template();
                        if ($template instanceof TemplateBase) {
                            if ($this->ad->template_type == 2) {//视频
                                $template->title = $this->ad->ad_title;
                                $template->icon = $this->ad->icon;
                                $template->icon_title = $this->ad->action_text;
                            } else {
                                $template->title = $this->ad->ad_name;
                            }
                            if ($template instanceof TemplateImage) {//图片类型
                                $template->imgurl = $this->ad->file_path;
                                if ($template instanceof Template1_2) {//图文摘要
                                    $template->content = "";
                                }
                            } else if ($template instanceof TemplateVideo) {//视频类型
                                $template->videoduration = $this->ad->ad_length;
                                $template->videourl = $this->ad->file_path;
                                if ($template instanceof Template2_2) {
                                    $template->imgurl = $this->ad->cover;
                                } elseif ($template instanceof Template2_4) {
                                    $template->end_cover = $this->ad->cover;
                                    $template->action_text = $this->ad->action_text;
                                }
                            }
                            $bid->adm = json_encode($template);
                        } else {
                            break;
                        }
                        return true;
                    }
                    break;
                }
            }
            break;//让循环只进行一次，防止意外情况发生
        }
        return false;
    }

    /**
     * 从广告内获取基本信息
     * @author 韩石强
     * @time 2022/4/12
     */
    private function fillResponseBaseInfo()
    {
        if (!$this->ad) {
            return null;
        }
        $bid = new SeatbidBidModelData();
        $bid->price = $this->ad->price;
        $bid->w = $this->ad->file_width;
        $bid->h = $this->ad->file_high;
        $bid->ad_type = $this->ad->template_type == 2 ? 2 : 1;//创意类型(1:图片、2:视频)
        $bid->durl = $this->ad->target_url;//到达页地址。
        $bid->curl = $this->ad->view_url ?? [];//点击检测地址
        $bid->nurl = $this->ad->check_url ?? [];//曝光检测地址
        if ($this->ad->deep_link) {//dp唤醒方式
            $bid->deep_link = $this->ad->deep_link;
            $bid->adck = 1;
            $bid->dpsucc = $this->ad->deeplink_url ?: [];
        } else if ($this->ad->type == 1) {
            $bid->adck = 1;
        } else {
            $bid->adck = 2;
            $app_info = $this->ad->app_info ? json_decode($this->ad->app_info, true) : [];
            if ($this->ad->type == 3) {//ios
                $bid->appstore_id = $app_info['appstore_id'];
            } else if ($this->ad->type == 2) {//安卓
                $app_info = json_decode($this->ad->app_info, true);
                $bid->package_name = $app_info['package_name'];
                $bid->app_name = $app_info['app_name'];
                $bid->app_version = $app_info['app_version'];
                $bid->app_icon = $app_info['app_icon'];
                $bid->app_intro = $app_info['app_direction'];
                $bid->app_feature = $app_info['app_version_feature'];
                $bid->app_size = $app_info['app_size'];
                $bid->app_privacy = $app_info['app_privacy'];
                $bid->privacy_agreement = $app_info['app_privacy_address'];
                $bid->payment_types = $app_info['app_pay_type'];
            } else {//微信小程序
                $bid->wx_username = $app_info['applet_id'];
                $bid->wx_path = $app_info['applet_path'];
            }
        }
        $seatbid = new SeatbidModelData();
        $seatbid->bid = [$bid];
        $this->response->seatbid[] = $seatbid;
        return $bid;
    }
}