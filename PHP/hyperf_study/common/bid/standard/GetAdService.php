<?php
/**
 * @author 韩石强
 * @time 2022/3/29
 */

namespace common\bid\standard;

use common\bid\BaseBid;
use common\bid\struct\GetFilterAdConfigModelData;
use common\data\api_return_code\AdServerReturnCodes;
use common\data\const_value\api\ChannelGetAdConst;
use common\data\struct\ad\CoverStruct;
use common\data\struct\ad\GetAdResponseStruct;
use common\exception\BidException;
use common\exception\RTAException;
use common\exception\ValidateFailException;
use common\helpers\LogV2;
use common\services\ad\AdPlatform;
use Hyperf\HttpServer\Contract\RequestInterface;

/**
 * 获取广告
 */
class GetAdService extends BaseBid
{
    /**
     * @var array|null 请求信息
     */
    private ?array $data;
    /**
     * @var string|null 请求id
     */
    public ?string $request_id = null;

    public function __construct(RequestInterface $request)
    {
        parent::__construct($request);
        $this->replace['__SIZE__'] = "size";
        $this->data = $request->post();
    }


    /**
     * @var string[] 给广告过滤器的操作系统类型转为特定字符串
     */
    private array $os2name = [
        ChannelGetAdConst::OS_ANDROID => AdPlatform::MOBILE_OS[2],
        ChannelGetAdConst::OS_IOS => AdPlatform::MOBILE_OS[1],
    ];
    /**
     * @var GetAdResponseStruct 返回广告结果
     */
    private GetAdResponseStruct $result;

    /**
     * @return GetAdResponseStruct|null
     * @throws BidException
     * @throws ValidateFailException
     * @throws RTAException
     */
    public function getAd(): ?GetAdResponseStruct
    {
        $this->config->requestId = md5(uniqid(microtime(true)));
        if (is_array($this->data)) {
            $chid = intval($this->data['chid']);
            $pid = (string)$this->data['pid'];
            if ($chid <= 0 or !strlen($pid)) {//验证chid和pid的合法性
                LogV2::errorCode(10042, $chid, "", "", $pid);
                throw new ValidateFailException(AdServerReturnCodes::REQUIRED, "chid,pid");
            }
            //映射设备对象
            foreach ($this->data as $k => $v) {
                if (is_null($v)) {
                    continue;
                }
                $this->device[$k] = $v;
            }
            //填充pid和平台  方便查询appid
            $this->config->pId = $pid;
            $this->device->platform = $this->config->platform = $chid;
            try {
                $this->getPidOrSize();
            } finally {
                LogV2::request($chid, $this->config->requestId, $this->config->appId, $this->config->pId);
            }
            //验证设备信息，但不保存设备信息
            if (!$this->device->validate(false)) {
                //设备验证失败，即将抛出异常退出前保存设备信息，暂时先不保存标准化接口的设备信息
//                $this->saveDeviceLog($this->config);
                [$error, $field] = $this->device->getError();
                throw new ValidateFailException(AdServerReturnCodes::REQUIRED, $field, $error);
            }
        } else {
            throw new ValidateFailException(AdServerReturnCodes::REQUIRED, "os,ua");
        }
        //保存设备信息，暂时先不保存标准化接口的设备信息
//        $this->saveDeviceLog($this->config);
        $this->config->ip = $this->device->ip;
        $this->config->bidFloor = 0.01;
        $this->config->bidType = 2;
        $this->config->device->os = $this->os2name[$this->device->os];
        if ($this->device->os === ChannelGetAdConst::OS_ANDROID) {
            $this->config->device->oaid = $this->device->oaid;
            $this->config->device->imei = $this->device->imei;
        } else {
            $this->config->device->idfa = $this->device->idfa;
        }
        $this->ad = $this->getAdModelFromFilter();
        if (!$this->ad) {
            throw new ValidateFailException(AdServerReturnCodes::NO_AD_DATA, '');
        }

        $this->changeResult();
        return $this->result;
    }

    /**
     * 保存设备信息
     * @param GetFilterAdConfigModelData $getAdConfig
     * @throws BidException
     * @author 韩石强
     * @time 2022/4/22
     */
    protected function saveDeviceLog(GetFilterAdConfigModelData $getAdConfig)
    {
        if ($this->device->isCanSave()) {
            $this->device->appid = $this->config->appId;
            $this->device->save();
//            $this->config->have_get_pid_or_size = true;
        }
    }


    /**
     * 将广告过滤器返回的信息转为标准的response
     *
     * @author 韩石强
     * @time 2022/3/29
     */
    private function changeResult()
    {
        //为替换回调地址做准备

        //生成数据结构
        $this->result = new GetAdResponseStruct();
        $this->result->ad_type = $this->ad->template_type == 2 ? 2 : 1;
        $type = $this->ad->type;
        $this->result->landing_url = $this->ad->target_url;
        if ($type == 1) {//-跳转落地页
            $this->result->action_type = ChannelGetAdConst::ACTION_TYPE_LANDING;
        } elseif ($this->ad->deep_link) {//deeplink唤醒
            $this->result->action_type = ChannelGetAdConst::ACTION_TYPE_DEEPLINK;
            $this->result->deeplink_url = $this->ad->deep_link;
        } else {//下载
            $this->result->action_type = ChannelGetAdConst::ACTION_TYPE_DOWNLOAD;
        }
        //回调地址
        $this->result->adw = $this->ad->file_width;
        $this->result->adh = $this->ad->file_high;
        $this->result->show_report = $this->ad->check_url ?? [];
        $this->result->click_report = $this->ad->view_url ?? [];
        if (!empty($this->ad->deep_link)) {
            $this->result->deeplink_report = $this->ad->deeplink_url;
        } else {
            $this->result->deeplink_report = [];
        }
        //根据视频还是图片类型返回需要的内容
        if ($this->result->ad_type == 1) {//图片
            $this->result->image_url = [$this->ad->file_path];
        } else {
            $this->result->duration = $this->ad->ad_length;
            $this->result->video_url = $this->ad->file_path;
            $this->result->cover = new CoverStruct();
            $this->result->cover->url = $this->ad->cover;//视频封面
        }
    }
}