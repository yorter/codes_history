<?php
namespace common\exception;

use yii\base\UserException;

/**
 *  自定义 JJException异常父类
 */
class JJException extends \Exception
{

    public $data;
    /**
     * Constructor.
     * @param string $message error message
     * @param int $code error code
     * @param array $data the other data
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message,$data = '',$code = 0,\Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;
    }

    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'JJException';
    }
}