<?php
/**
 * @author 韩石强
 * @time 2022/3/29
 */

namespace common\exception;

/**
 * api模块自定义异常基类
 */
abstract class ApiException extends \Exception
{

}