<?php
/**
 * @author 韩石强
 * @time 2022/3/29
 */

namespace common\exception;

use common\data\api_return_code\AdServerReturnCodes;

/**
 * 输入验证失败
 */
class ValidateFailException extends ApiException
{
    /**
     * @var string 错误字段
     */
    public string $field;
    /**
     * @var string|mixed|null 错误信息
     */
    private ?string $errorInfo = null;

    public function __construct(int $code, $field, $errorInfo = null)
    {
        parent::__construct();
        $this->code = $code;
        $this->field = $field;
        $this->errorInfo = $errorInfo;
    }

    /**
     * 获取错误信息 [code,错误字段,错误信息]
     * @return array
     * @author 韩石强
     * @time 2022/3/29
     */
    public function getError(): array
    {
        return [
            $this->field,
            $this->errorInfo ?? AdServerReturnCodes::getCodeMessage($this->code)
        ];
    }
}