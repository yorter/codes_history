<?php

namespace common\exception;

use common\log\JJLog;
use Throwable;

class JJLogException extends \Exception
{
    private $jjlog;
    private $data;

    /**
     * @return mixed
     */
    public function getJJlog()
    {
        return $this->jjlog;
    }

    public function getData()
    {
        return $this->data;
    }

    public function __construct(JJLog $jjlog, $data = '',$message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->jjlog = $jjlog;
        $this->data = $data;
    }
}