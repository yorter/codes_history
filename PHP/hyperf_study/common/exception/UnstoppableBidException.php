<?php
/**
 * @author 韩石强
 * @time 2022/5/7
 */

namespace common\exception;
/**
 * 竞价失败，不用再次使用过滤器的异常
 */
class UnstoppableBidException extends BaseFilter
{

}