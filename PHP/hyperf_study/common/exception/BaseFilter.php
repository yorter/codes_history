<?php

namespace common\exception;

class BaseFilter extends \Exception
{
    public function getExceptionClassName(): string
    {
        return self::class;
    }
}