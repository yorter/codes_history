<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use common\helpers\logger\DayRotatingFileHandler;
use common\helpers\logger\HourRotatingFileHandler;

return [
    'default' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/system___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
//                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'request' => [
        'handler' => [
            'class' => HourRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/request___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'error_code' => [
        'handler' => [
            'class' => HourRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/error_code___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'callback' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/callback___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'bid' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/bid___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'device' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/device___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'exception' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/exception___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'tb_callback' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/tb_callback___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
    'rta_delta' => [
        'handler' => [
            'class' => DayRotatingFileHandler::class,
            'constructor' => [
                'stream' => BASE_PATH . '/../log/rta_delta___REPLACE__.log',
                'level' => Monolog\Logger::DEBUG,
            ],
        ],
        'formatter' => [
            'class' => Monolog\Formatter\LineFormatter::class,
            'constructor' => [
                'format' => "%message%\n",
//                'dateFormat' => 'Y-m-d H:i:s',
//                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
];
