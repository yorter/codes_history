<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'default' => [
        'host' => env('DEFAULT_REDIS_HOST', 'localhost'),
        'auth' => env('DEFAULT_REDIS_AUTH', null),
        'port' => (int)env('DEFAULT_REDIS_PORT', 6379),
        'db' => 0,
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 2,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ]
    ],

    'dsp_info' => [
        'host' => env('DEFAULT_REDIS_HOST', 'localhost'),
        'auth' => env('DEFAULT_REDIS_AUTH', null),
        'port' => (int)env('DEFAULT_REDIS_PORT', 6379),
        'db' => 2,
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 20,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ],
        'options' => [
            Redis::OPT_TCP_KEEPALIVE
        ]
    ],

    'device_mark' => [
        'host' => env('DEFAULT_REDIS_HOST', 'localhost'),
        'auth' => env('DEFAULT_REDIS_AUTH', null),
        'port' => (int)env('DEFAULT_REDIS_PORT', 6379),
        'db' => 13,
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 20,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ],
        'options' => [
            Redis::OPT_TCP_KEEPALIVE
        ]
    ],
    'device_cache' => [
        'host' => env('DEFAULT_REDIS_HOST', 'localhost'),
        'auth' => env('DEFAULT_REDIS_AUTH', null),
        'port' => (int)env('DEFAULT_REDIS_PORT', 6379),
        'db' => 14,
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 20,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ],
        'options' => [
            Redis::OPT_TCP_KEEPALIVE
        ]
    ],
    'device_md5_cache' => [
        'host' => env('DEFAULT_REDIS_HOST', 'localhost'),
        'auth' => env('DEFAULT_REDIS_AUTH', null),
        'port' => (int)env('DEFAULT_REDIS_PORT', 6379),
        'db' => 15,
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 20,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ],
        'options' => [
            Redis::OPT_TCP_KEEPALIVE
        ]
    ],

    'rta' => [
        'host' => env('RTA_REDIS_HOST', 'localhost'),
        'auth' => env('RTA_REDIS_AUTH', null),
        'port' => (int)env('RTA_REDIS_PORT', 6379),
        'db' => (int)env('RTA_REDIS_DB', 0),
        'pool' => [
            'min_connections' => 20,
            'max_connections' => 20,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('REDIS_MAX_IDLE_TIME', 60),
        ],
        'options' => [
            Redis::OPT_TCP_KEEPALIVE
        ]
    ],
];
